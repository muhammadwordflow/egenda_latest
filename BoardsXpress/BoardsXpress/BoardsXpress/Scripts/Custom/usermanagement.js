﻿$(".interPhone").intlTelInput({
    allowExtensions: true,
    preferredCountries: ['au'],
    onlyCountries: ['au', 'us', 'gb'],
    utilsScript: "Scripts/intl-tel-utils.js"
});
// Edit user data
$(".editRecord").unbind().click(function (event) {
    // get parent TR
    var parentRow = $(this).closest("#row");

    // Set values in the modal:
    $("#editUserId").val(parentRow.find("td[data-column='userId']").text());
    $("#editFullName").val(parentRow.find("td[data-column='fullName']").text());
    $("#editEmail").val(parentRow.find("td[data-column='email']").text());
    $("#editPhone").intlTelInput(parentRow.find("td[data-column='phone']").text());
    $("#editOrganisation").val(parentRow.find("td[data-column='organisation']").text());
    $("#editDesignation").val(parentRow.find("td[data-column='designation']").text());
    $("#avatarImage").val(parentRow.find("td[data-column='avatar']").text());
    $("#meetingId").val(parentRow.find("td[data-column='meetingId']").text());
    $("#agendaId").val(parentRow.find("td[data-column='agendaId']").text());
    $("#meetingId").val(parentRow.find("td[data-column='meetingId']").text());
    $("#agendaId").val(parentRow.find("td[data-column='agendaId']").text());
    $("#meetingId").val(parentRow.find("td[data-column='meetingId']").text());

    //Get items 
    //var id = $(this).closest('tr').children()[0].textContent.trim();
    //var phoneNo = $(this).closest('tr').children()[1].textContent.trim();
    //var organisation = $(this).closest('tr').children()[2].textContent.trim();
    //var designation = $(this).closest('tr').children()[3].textContent.trim();
    //var avatar = $(this).closest('tr').children()[4].textContent.trim();
    //var fullName = $(this).closest('tr').children()[5].textContent.trim();
    //var userName = $(this).closest('tr').children()[6].textContent.trim();
    //var email = $(this).closest('tr').children()[7].textContent.trim();
    //var gender = $(this).closest('tr').children()[8].textContent.trim();

    //// and set them in the modal:
    //$("#editFullName").val(fullName);
    //$("#editEmail").val(email);
    //$("#editPhone").intlTelInput("setNumber", phoneNo);
    //$("#editOrganisation").val(organisation);
    //$("#editDesignation").val(designation);
    //$("#editEmail").val(email);
    //$("#editGender").val(gender);
    //$("#editUserId").val(id);
    //document.getElementById("avatarImage").src = avatar;
    
    $('#done').click(function () {

        var formData = new FormData();
        formData.append('Id', $("#editUserId").val());
        formData.append('FirstName', $("#editFullName").val());
        formData.append('Organisation', $("#editOrganisation").val());
        formData.append('Designation', $("#editDesignation").val());
        formData.append('Avatar', $("#editAvatar").val());
        formData.append('Email', $("#editEmail").val());
        formData.append('PhoneNumber', $("#editPhone").intlTelInput("getNumber"));
        formData.append("Avatar", $("#avatarImage").val());
        formData.append('Gender', $("#editGender").val());
        formData.append('Password', $("#editPassword").val());
        formData.append('ImageUpload', document.getElementById('editAvatar').files[0]);

        $.ajax({
            url: 'UserManagement/Edit',
            async: true,
            type: "POST",
            contentType: false,
            processData: false,
            data: formData,
            error: function (data) {
                alert(data);
            },
            success: function (data) {
                location.reload();
            }
        });
    })
});

// Delete users
$(".delete").unbind().click(function (event) {
    // get parent TR
    var parentRow = $(this).closest("#row");
    
    $('#yes').click(function () {
        $.ajax({
            url: 'UserManagement/DeleteConfirmed',
            async: false,
            type: "POST",
            data: JSON.stringify({
                "Id": parentRow.find("td[data-column='userId']").text(),
                "meetingTypeId": parentRow.find("td[data-column='meetingTypeId']").text()
            }),
            dataType: "json",
            contentType: "application/json",
            error: function (data) {
                alert(data);
            },
            success: function (data) {
                location.reload();
            }
        });
    });
});

// Reset User Password
$(".reset").unbind().click(function (event) {
    console.log("m here");
    var email = $(this).closest('tr').children()[2].textContent.trim();
    $('#doneReset').click(function () {
        var newPassword = $('#resetNew').val();
        var newPasswordConf = $('#resetnewConfirm').val();
        if (newPassword !== newPasswordConf) {
            $('#resetNew').css("border", "1px solid red");
            $('#resetnewConfirm').css("border", "1px solid red");
            alert('Password did not match');
            return;
        }
        $.ajax({
            url: 'UserManagement/ResetPassword',
            async: false,
            type: "POST",
            data: JSON.stringify({ "Email": email, "newPassword": newPasswordConf }),
            dataType: "json",
            contentType: "application/json",
            error: function (data) {
                alert(data);
            },
            success: function (data) {
                location.reload();
            }
        });
    });
});

//add new user
$("#addNew").click(function () {
    if (checkIfUserExist()) {
        $('#addUserModal').modal('hide');
        $('#userExisting').modal('show');
        return;
    }

    // Append form data to a new form to be sent to the back end.
    var fd = new FormData();

    fd.append('FirstName', $("#addFullName").val());
    fd.append('Organisation', $("#addOrganisation").val());
    fd.append('Designation', $("#addDesignation").val());
    fd.append('Avatar', $("#addAvatar").val());
    fd.append('Email', $("#addEmail").val());
    fd.append('PhoneNumber', $("#addPhone").intlTelInput("getNumber"));
    fd.append('Gender', $("#addGender").val());
    fd.append('ImageUpload', document.getElementById('addAvatar').files[0]);
    fd.append('meetingTypeId', $("#meetingTypeId").val())

    $.ajax({
        url: 'UserManagement/Create',
        async: false,
        type: "POST",
        contentType: false,
        processData: false,
        data: fd,
        error: function (data) {
            alert(data);
        },
        success: function (data) {
            location.reload();
        }
    });
});

function getUserInformation() {

    //console.log($("#addAvatar").files[0]);
    return JSON.stringify({
        FirstName: $("#addFirstName").val(),
        MiddleName: $("#addMiddleName").val(),
        LastName: $("#addLastName").val(),
        Organisation: $("#addOrganisation").val(),
        Designation: $("#addDesignation").val(),
        Avatar: $("#addAvatar").val(),
        Email: $("#addEmail").val(),
        PhoneNumber: $("#addPhone").val(),
        Gender: $("#addGender").val(),
        Password: $("#addPassword").val(),
        ImageUpload: document.getElementById('addAvatar').files[0],

    });
}


//reset password validation
$('.resetPasswordForm').validate({
    feedback: {
        success: 'glyphicon-ok',
        error: 'glyphicon-remove'
    },
    rules: {
        reset: {
            minlength: 6
        },
        confirm: {
            minlength: 6,
            equalTo: "#resetNew"
        }
    }
});

//check password and confirm password
function checkPass() {
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('addPassword');
    var pass2 = document.getElementById('confirmPassword');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if (pass1.value == pass2.value) {
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
        $('#addNew').removeAttr('disabled');
    } else {
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
        $('#addNew').attr('disabled', 'disabled');
    }
}

function  checkIfUserExist() {
    var email = $('#addEmail').val();
    var successVal = false;
    $.ajax({
        url: 'UserManagement/CheckUser',
        async: false,
        type: "POST",
        data: JSON.stringify({ "email": email }),
        contentType: "application/json",
        error: function (data) {
            //user exist already, show use already.
            successVal = false;
        },
        success: function (data) {
            //user doesn't exist, let them continue
            if (data !== null && data.success) {
            } else {
                successVal = true;
            }
        }
    });
    return successVal;
}

$('#btnUseExistingOk').click(function () {
    $('#userExisting').modal('hide');
    var email = $('#addEmail').val();
    var meetingTypeId = $('#meetingTypeID').val();
    //console.log(meetingTypeId);
    $.ajax({
        url: 'UserManagement/AddUserRole',
        async: false,
        type: "POST",
        data: JSON.stringify({ "email": email,"meetingTypeId":meetingTypeId }),
        contentType: "application/json",
        error: function (data) {
            console.log(data);
        },
        success: function (data) {
            console.log(data);
            //$('#addUserModal').modal('hide');
            $('#userExisting').modal('hide');
            location.reload();
            //addUserModal
        }

    });
});

$('.close').click(function () {
    location.reload();
})

// set forms validation
function setFormValidation(frmId, saveBtnId) {

    $(frmId + ' input').on('keyup blur', function () { // fires on every keyup & blur
        $(saveBtnId).prop('disabled', !$(frmId).valid());        // enable/disable button
    });
}

$(document).ready(function () {

    // Set validation rules to all the needed forms on the page.
    setFormValidation('#addUserForm', '#addNew');
    setFormValidation('#editUserForm', '#done');
    //Set validation rules to all the needed forms on the page.
    
    $(".modal").on("hidden.bs.modal", function () {
        $(this).removeData('bs.modal');
    });

    //remove data when modal is hidden
    $(".modal").on("shown.bs.modal", function () {
        $("#addFullName").focus()
    });

});
