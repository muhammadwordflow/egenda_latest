﻿// Delete reference documents
$(".delete").unbind().click(function (event) {
    var refDocId = $(this).closest('tr').children()[0].textContent.trim();
    var docName = $(this).closest('tr').children()[1].textContent.trim();
    var docType = $(this).closest('tr').children()[3].textContent.trim();
    $('#deleteRefDoc').click(function () {
        $.ajax({
            url: '/RefDocs/Delete',
            async: false,
            type: "POST",
            data: JSON.stringify({ "id": refDocId }),
            dataType: "json",
            contentType: "application/json",
            error: function (data) {
                console.log(data);
            },
            success: function (data) {
                location.reload();
            }
        });
    });
});

$('#refDocUpload').submit(function (e) {
    e.preventDefault();

    var formData = new FormData(this);

    var ajaxUrl = window.ApplicationRootUrl("uploadRefDocument", "RefDocs");
    
    $.ajax({
        cache: false,
        dataType: 'json',
        processData: false, // Don't process the files
        contentType: false,
        data: formData,
        type: "POST",
        url: ajaxUrl,
        success: function (data) {
            $("#popup-5").hide();
            $("#referenceDocModalMessage").text(data.Message);
            $('#referenceDocModal').modal('toggle');
        },
        error: function (err) {

        }
    });
});

$('#manageDocResponse').click(function () {
    location.reload();
});