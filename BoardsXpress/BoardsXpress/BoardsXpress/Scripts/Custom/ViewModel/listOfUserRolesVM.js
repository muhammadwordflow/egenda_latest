﻿var userManagementVM = {
    ClickId: ko.observable(),
    RoleId: ko.observable(),
    Action: ko.observable(false),
    AspNetUserRoleId: ko.observable(),
    MeetingTypeId: ko.observable(),
    //StartMeetingDate: ko.observable(new Date()),
    MinAllowedDate: ko.observable(new Date()),
    MaxAllowedDate: ko.observable(new Date(new Date().getTime() + 30 * 86400000) ), 
    EndMeetingDate: ko.observable(),
    //responseData: ko.observableArray(),
    responseSetUpData: ko.observable({
        userList: ko.observable(""),
        userListViewModel: ko.observableArray([]),
        MeetingId: ko.observable(""),
        MeetingTypeId: ko.observable(""),
        MeetingType: ko.observable("")
    }),
    responseUserSetUpData: ko.observable({
        userList: ko.observable(""),
        userListViewModel: ko.observableArray([]),
        MeetingId: ko.observable(""),
        MeetingTypeId: ko.observable(""),
        MeetingType: ko.observable("")
    }),
    GetListOfUserRoles: function () {
        var self = this;
        var ajaxUrl = ApplicationRootUrl("GetUserRoleList", "UserManagement");
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: ajaxUrl,
            dataType: "json",
            success: function (data) {
                if (data.ErrorCode != 0) {
                    $("#successBodyText").addClass("text-danger");
                    $("#successBodyText").text("Failed to connect to database.Please contact your system administrator or the Egenda help desk on support@egenda.info and we will get this sorted for you. ");
                    $('#sucessUploadModal').modal('toggle');
                }
                //self.
                self.responseSetUpData(data);
            },
            error: function (err) {

            }
        });
    },

    SetListOfUserInRoles: function () {
        
        var self = this;
        self.responseUserSetUpData({});
        var ajaxUrl = ApplicationRootUrl("GetUserRoleAllIndividual", "UserManagement") + "?userId=" + userManagementVM.ClickId() + "&roleId=" + userManagementVM.RoleId();
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: ajaxUrl,
            dataType: "json",
            success: function (data) {
                if (data.ErrorCode != 0) {
                    $("#successBodyText").addClass("text-danger");
                    $("#successBodyText").text("Failed to connect to database.Please contact your system administrator or the Egenda help desk on support@egenda.info and we will get this sorted for you. ");
                    $('#sucessUploadModal').modal('toggle');
                }
                self.responseUserSetUpData(data);
            },
            error: function (err) {

            }
        });
    },
    addUserToRoleCLick: function (data, event) {
        $('#addUserRolesModal').modal('show');
        userManagementVM.ClickId(event.target.id);
        userManagementVM.RoleId(event.target.getAttribute('rid'));
        userManagementVM.MeetingTypeId(event.target.getAttribute('meetTyId'));
        userManagementVM.Action(false);
        userManagementVM.SetListOfUserInRoles();
        
    },
    editUserToRoleCLick: function (ata, event) {
        $('#addUserRolesModal').modal('show');
        userManagementVM.ClickId(event.target.id);
        userManagementVM.RoleId(event.target.getAttribute('rid'));
        console.log("click id = " + $(event.target).attr('id'));
        console.log("Role Id = " + userManagementVM.RoleId());
        userManagementVM.MeetingTypeId(event.target.getAttribute('meetTyId'));
        userManagementVM.Action(true);
        userManagementVM.SetListOfUserInRoles();
    },
    saveRolesClick: function () {
        var self = this;
        var ajaxUrl = ApplicationRootUrl("AddEditUserRole", "UserManagement") + "?edit=" + userManagementVM.Action();
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: ajaxUrl,
            dataType: "json",
            data: ko.toJSON(self.responseUserSetUpData()),
            success: function (data) {
                if (data.ErrorCode != 0) {
                    $("#successBodyText").addClass("text-danger");
                    $("#successBodyText").text("Failed to connect to database.Please contact your system administrator or the Egenda help desk on support@egenda.info and we will get this sorted for you. ");
                    $('#sucessUploadModal').modal('toggle');
                } else {
                    self.responseSetUpData(data);
                    $('#addUserRolesModal').modal('toggle');
                    if (userManagementVM.Action()) {
                        $("#successBodyText").text("Updated Successfully.");
                    } else {
                        $("#successBodyText").text(data.ErrorMessage);
                    }
                    $('#sucessUploadModal').modal('toggle');
                }

            },
            error: function (err) {

            }
        });
    },
    userToDeleteClick: function (data, event) {
        userManagementVM.ClickId(event.target.id);
        userManagementVM.RoleId(event.target.getAttribute('rid'));
        userManagementVM.AspNetUserRoleId(event.target.getAttribute('AspNetUserRoleId'));
    },
    deleteUserRole: function (data, event) {
        var self = this;
        var ajaxUrl = ApplicationRootUrl("DeleteUserRole", "UserManagement") + "?userId=" + userManagementVM.ClickId() + "&roleId=" + userManagementVM.RoleId() + "&aspnetuserId=" + userManagementVM.AspNetUserRoleId();
        $.ajax({
            type: "DELETE",
            contentType: "application/json; charset=utf-8",
            url: ajaxUrl,
            success: function (data) {
                if (data.ErrorCode != 0) {
                    $("#successBodyText").addClass("text-danger");
                    $("#successBodyText").text("Unable to delete this user. Please try again or contact Support.");
                    $('#sucessUploadModal').modal('toggle');
                } else {
                    self.responseSetUpData(data);
                    $('#deleteModal').modal('toggle');
                    $("#successBodyText").text("Deleted Successfully.");
                    $('#sucessUploadModal').modal('toggle');
                }

            },
            error: function (err) {

            }
        });
    }
}


$(document).ready(function () {
    ko.applyBindings(userManagementVM, document.getElementById("rightdash-container"));
    userManagementVM.GetListOfUserRoles();

    ko.bindingHandlers.datePicker = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var unwrap = ko.utils.unwrapObservable;
            var dataSource = valueAccessor();
            var binding = allBindingsAccessor();
            var options = {
                keyboardNavigation: true,
                todayHighlight: true,
                autoclose: true,
                daysOfWeekDisabled: [0, 6],
                format: 'dd/mm/yyyy'
                //startDate: userManagementVM.StartMeetingDate()
            };
            if (binding.datePickerOptions) {
                options = $.extend(options, binding.datePickerOptions);
            }
            $(element).datepicker(options);
            $(element).datepicker('update', dataSource());
            $(element).on("changeDate", function (ev) {
                var observable = valueAccessor();
                if ($(element).is(':focus')) {
                    $(element).one('blur', function (ev) {
                        var dateVal = $(element).datepicker("getDate");
                        observable(dateVal);
                    });
                }
                else {
                    observable(ev.date);
                }
            });
            //handle removing an element from the dom
            ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                $(element).datepicker('remove');
            });
        },
        update: function (element, valueAccessor) {
            var value = ko.utils.unwrapObservable(valueAccessor());
            $(element).datepicker('update', value);
        }
    };

});