﻿var UserProfileVM = {
    myMessage: ko.observable("testing"),
    userId: ko.observableArray(),
    pagination: ko.observableArray(),
    PageNoClick: ko.observable(),
    userProfileId: ko.observable(),
    gotUserProfileResponseById: ko.observable(),
    responseSetUpData: ko.observable({
        UserProfileList: ko.observableArray([]),
        Pager: ko.observable()
    }),

    profileDetailsCLick: function (data, event) {
        UserProfileVM.userProfileId(event.target.id);
        $('#profileModal').modal('show');
        UserProfileVM.getUserProfileById();
    },

    getUserProfileReportList: function (pageNo) {
        var self = this;
        var ajaxUrl = window.ApplicationRootUrl("GetUserProfileReport", "UserReportManagement") + "?page=" + pageNo;
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: ajaxUrl,
            dataType: "json",
            success: function (data) {
                if (data.ErrorCode != 0) {
                    $("#successBodyText").addClass("text-danger");
                    $("#successBodyText").text("Connection to Database Fail. Please reload the page or contact System Admin.");
                    $('#sucessUploadModal').modal('toggle');
                }
                //self.
                self.responseSetUpData(data);
                UserProfileVM.displayPaginationSetting(data.Pager.TotalPages);
            },
            error: function (err) {

            }
        });
    },

    getUserProfileById: function () {
        var self = this;
        var ajaxUrl = window.ApplicationRootUrl("GetUserProfileById", "UserReportManagement") + "?id=" + UserProfileVM.userProfileId();
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: ajaxUrl,
            dataType: "json",
            success: function (data) {
                if (data.ErrorCode != 0) {
                    $("#successBodyText").addClass("text-danger");
                    $("#successBodyText").text("Please check your internet connection as you may not be connected to Egenda.If your internet connection is working, try again in a few minutes.If you are still not successful contact support@egenda.info for help.");
                    $('#sucessUploadModal').modal('toggle');
                }
                self.gotUserProfileResponseById(data);
                $("#nametext").text(data.Title + "    " + data.FirstName);
                $("#emailtext").text(data.Email);
                if (data.Avatar !== "") {
                    $("#imgSrcProf").attr("src", data.Avatar);
                }
                $("#phoneText").text(data.PhoneNumber);
                $("#positionText").text(data.Designation !== "null" ? data.Designation : "");
                $("#memberType").text(data.TypeOfMemberSelected !== "null" ? data.TypeOfMemberSelected : "");
                $("#universityIdText").text(data.UniversityId !== "null" ? data.UniversityId : "");
                $("#execAssiName").text(data.ExecutiveAssistantName !== "null" ? data.ExecutiveAssistantName : "");
                $("#execAssisEmail").text(data.ExecutiveAssistantemail !== "null" ? data.ExecutiveAssistantemail : "");
                $("#postAddress").text(data.PostalAddress !== "null" ? data.PostalAddress : "");
                $("#diesReq").text(data.DietaryRequirements !== "null" ? data.DietaryRequirements : "");
                $("#myModalLabeltitle").text(data.Username !== "null" ? data.Username : "");
            },
            error: function (err) {

            }
        });
    },

    paginationClick: function (data, event) {
        UserProfileVM.PageNoClick(event.target.id);
        UserProfileVM.getUserProfileReportList(UserProfileVM.PageNoClick());
    },
    displayPaginationSetting: function (totalPage) {
        UserProfileVM.pagination([]);
        for (var i = 1; i <= totalPage; i++) {
            UserProfileVM.pagination.push({ id: i, pageNo: i });
        };
    }
}



$(document).ready(function () {
    window.ko.applyBindings(UserProfileVM, document.getElementById("userProfileForm"));
    UserProfileVM.getUserProfileReportList(1);

    $(".search").keyup(function () {
        var searchTerm = $(".search").val();
        console.log(searchTerm);
        var listItem = $('.results tbody').children('tr');
        var searchSplit = searchTerm.replace(/ /g, "'):containsi('");

        $.extend($.expr[':'], {
            'containsi': function (elem, i, match, array) {
                return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
            }
        });

        $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function (e) {
            $(this).attr('visible', 'false');
        });

        $(".results tbody tr:containsi('" + searchSplit + "')").each(function (e) {
            $(this).attr('visible', 'true');
        });

        var jobCount = $('.results tbody tr[visible="true"]').length;
        $('.counter').text(jobCount + ' item');

        if (jobCount == '0') { $('.no-result').show(); }
        else { $('.no-result').hide(); }
    });


});