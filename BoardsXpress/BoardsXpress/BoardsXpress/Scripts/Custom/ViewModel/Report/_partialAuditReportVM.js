﻿var AuditVM = {

    ClickId: ko.observable(),
    PageNoClick: ko.observable(),
    pagination: ko.observableArray(),
    SelectedMeetingTypeId: ko.observable(),
    SelectedMeetingId: ko.observable(),
    listMeetingTypeId: ko.observableArray([]),
    listMeetingId: ko.observableArray([]),
    responseAuditData: ko.observable({
        AuditTrailList: ko.observableArray(),
        Pager: ko.observable()
    }),

    paginationClick: function (data, event) {
        AuditVM.PageNoClick(event.target.id);
        AuditVM.getAuditReport(AuditVM.PageNoClick());
    },
    getAuditReport: function (pageNo) {
        var self = this;
        var ajaxUrl = window.ApplicationRootUrl("GetAuditReport", "UserReportManagement") + "?page=" + pageNo + "&meetingType=" + self.SelectedMeetingTypeId() + "&meetingDate=" + self.SelectedMeetingId();
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: ajaxUrl,
            dataType: "json",
            success: function (data) {
                //if (data.ErrorCode != 0) {
                //    $("#successBodyText").addClass("text-danger");
                //    $("#successBodyText").text("Something went wrong Try Again !!");
                //    $('#sucessUploadModal').modal('toggle');
                //}
                //self.
                self.responseAuditData(data);
                AuditVM.displayPaginationSetting(data.Pager.TotalPages);
            },
            error: function (err) {

            }
        });
    },
    getMeetingType: function (pageNo) {
        var self = this;
        var ajaxUrl = window.ApplicationRootUrl("GetMeetingType", "UserReportManagement");
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: ajaxUrl,
            dataType: "json",
            success: function (data) {
                //if (data.ErrorCode != 0) {
                //    $("#successBodyText").addClass("text-danger");
                //    $("#successBodyText").text("Something went wrong Try Again !!");
                //    $('#sucessUploadModal').modal('toggle');
                //}
                //self.
                self.listMeetingTypeId(data);
            },
            error: function (err) {

            }
        });
    },
    displayPaginationSetting: function (totalPage) {        
        AuditVM.pagination([]);
        for (var i = 1; i <= totalPage; i++) {
            AuditVM.pagination.push({ id: i, pageNo: i });
        };

    },
    filterAuditClick: function() {
        if ($("#filterAuditForm").valid()) {
            AuditVM.getAuditReport(1);
            $('#filterAudit').modal('toggle');
        }
    }
}

$("#filterAuditForm").validate({
    rules: {
        meetingType: {
            required: true
        }
    },

    messages: {
        meetingType: {
            valueNotEquals: "Select Meeting Type"
        }
    },

    highlight: function (element) {
        $(element).closest('div').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function (element) {
        $(element).closest('div').removeClass('has-error').addClass('has-success');
    }
});


$(document).ready(function () {
    ko.applyBindings(AuditVM, document.getElementById("audit"));    
    AuditVM.getAuditReport(1);
    AuditVM.getMeetingType();


    AuditVM.SelectedMeetingTypeId.subscribe(function (newValue) {
        var ajaxUrl = window.ApplicationRootUrl("GetMeetingByMeetingType", "UserReportManagement") + "?meetingType=" + newValue;;
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: ajaxUrl,
            dataType: "json",
            success: function (data) {
                AuditVM.listMeetingId(data);
            },
            error: function (err) {

            }
        });
    });
});