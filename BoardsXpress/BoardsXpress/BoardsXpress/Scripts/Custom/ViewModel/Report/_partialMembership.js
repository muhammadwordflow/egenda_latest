﻿var memberVM = {
    ClickId: ko.observable(), PageNoClick: ko.observable(),
    pagination: ko.observableArray(),
    SelectedRoleId: ko.observable(),
    responseSetUpData: ko.observable({
        FstartDate: ko.observable(),
        ListUsers: ko.observableArray([]),
        FendDate: ko.observable(""),
        selectedMeetingTyeId: ko.observable(),
        MeetingTypes: ko.observableArray([]),
        Pager: ko.observable()

    }),
    searchClick: function () {
        memberVM.getUserReportList(1);
        $('#filterMembership').modal('toggle');
    },
    paginationClick: function (data, event) {
        memberVM.PageNoClick(event.target.id);
        memberVM.getUserReportList(memberVM.PageNoClick());
    },
    getUserReportList: function (pageNo) {
        var self = this;
        var ajaxUrl = window.ApplicationRootUrl("GetUserReportList", "UserReportManagement") + "?mtId=" + memberVM.SelectedRoleId() + "&sd=" + $('#startDateReport').val() + "&ed=" + $('#endDateReport').val() + "&page=" + pageNo;
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: ajaxUrl,
            dataType: "json",
            success: function (data) {
                if (data.ErrorCode != 0) {
                    $("#successBodyText").addClass("text-danger");
                    $("#successBodyText").text("Connection to Database Fail. Please reload the page or contact System Admin.");
                    $('#sucessUploadModal').modal('toggle');
                }
                //self.
                self.responseSetUpData(data);
                memberVM.displayPaginationSetting(data.Pager.TotalPages);
            },
            error: function (err) {

            }
        });
    },
    displayPaginationSetting: function (totalPage) {
        memberVM.pagination([]);
        for (var i = 1; i <= totalPage; i++) {
            memberVM.pagination.push({ id: i, pageNo: i });
        };

    }
};


$(document).ready(function () {
    window.ko.applyBindings(memberVM, document.getElementById("formMembership"));
    memberVM.getUserReportList(1);
    $('#startDateReport').datepicker({
        format: "dd/mm/yyyy"
    });
    $('#endDateReport').datepicker({
        format: "dd/mm/yyyy"
    });
});