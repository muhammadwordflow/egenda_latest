﻿var userReportManagementVM = {
    ClickId: ko.observable(), PageNoClick: ko.observable(),
    pagination: ko.observableArray(),
    MeetingId: ko.observable(),
    SelectedMeetingTypeId: ko.observable(),
    SelectedUserId: ko.observable(),
    responseMeetingsData: ko.observable(),
    responseAuditData: ko.observable({
        AuditTrailList: ko.observableArray(),
        Pager: ko.observable()
    }),
    responseSetUpData: ko.observable({
        FstartDate: ko.observable(),
        ListUsers: ko.observableArray([]),
        FendDate: ko.observable(""),
        selectedMeetingTyeId: ko.observable(),
        MeetingTypes: ko.observableArray([]),
        Pager: ko.observable(),
        UserList: ko.observableArray([])
    }),
    
    searchClick: function () {
        userReportManagementVM.getUserReportList(1);
    },
    paginationClick: function (data, event) {
        userReportManagementVM.PageNoClick(event.target.id);
        userReportManagementVM.getUserReportList(userReportManagementVM.PageNoClick());
    },
    filterAuditClick: function() {
        userReportManagementVM.getUserReportList(1);   
    },
    //meetingTypeCLick: function (data, event) {
    //    userReportManagementVM.ClickId(event.target.id);
    //    userReportManagementVM.MeetingId(event.target.getAttribute('mId'));
    //    var self = this;
    //    var ajaxUrl = ApplicationRootUrl("GetMeeingMeetingTypeId", "UserReportManagement") + "?userId=" + userReportManagementVM.ClickId() + "&meetingTypeId=" + userReportManagementVM.MeetingId();
    //    $.ajax({
    //        type: "GET",
    //        contentType: "application/json; charset=utf-8",
    //        url: ajaxUrl,
    //        dataType: "json",
    //        success: function (data) {
    //            if (data.ErrorCode != 0) {
    //                $("#successBodyText").addClass("text-danger");
    //                $("#successBodyText").text("Something went wrong Try Again !!");
    //                $('#sucessUploadModal').modal('toggle');
    //            }
    //            //self.
    //            userReportManagementVM.responseMeetingsData(data.ListMeetings);
    //        },
    //        error: function (err) {

    //        }
    //    });

    //},
    getUserReportList: function (pageNo) {
        var self = this;
        var ajaxUrl = window.ApplicationRootUrl("GetAttendanceReport", "UserReportManagement") + "?page=" + pageNo + "&userId=" + userReportManagementVM.SelectedUserId() + "&meetingTypeid=" + userReportManagementVM.SelectedMeetingTypeId();
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: ajaxUrl,
            dataType: "json",
            success: function (data) {
                if (data.ErrorCode != 0) {
                    $("#successBodyText").addClass("text-danger");
                    $("#successBodyText").text("Please contact meeting administrator");
                    $('#sucessUploadModal').modal('toggle');
                }
                $('#filterAttendance').modal('hide');
                self.responseSetUpData(data);
               //userReportManagementVM.displayPaginationSetting(data.Pager.TotalPages);
            },
            error: function (err) {

            }
        });
    },   
    displayPaginationSetting: function (totalPage) {
        userReportManagementVM.pagination([]);
        for (var i = 1; i <= totalPage; i++) {           
            userReportManagementVM.pagination.push({ id: i, pageNo: i });            
        };        
        
    }

}

$(document).ready(function () {
    ko.applyBindings(userReportManagementVM, document.getElementById("attendance"));
    userReportManagementVM.getUserReportList(1);   

    //userReportManagementVM.SelectedUserId.subscribe(function (newValue) {
    //    userReportManagementVM.userID(newValue);
    //    userReportManagementVM.getUserReportList(1);
    //});

});