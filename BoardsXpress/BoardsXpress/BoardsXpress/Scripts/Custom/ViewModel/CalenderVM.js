﻿var CalenderVM = {
    pagination: ko.observableArray(),
    ClickId: ko.observable(),
    MeetingTypeId: ko.observable(),
    PageNoClick: ko.observable(''),
    paginationClick: function (data, event) {
        CalenderVM.PageNoClick(event.target.id);
        CalenderVM.getUserCalenderList(CalenderVM.PageNoClick());
    },
    responseSetUpData: ko.observable({
        CalendersMeetings: ko.observableArray([]),
        Pager: ko.observable()
    }),
    responseMeetingsData: ko.observable({   }),
    getUserCalenderList: function (pageNo) {
        var self = this;
        var ajaxUrl = ApplicationRootUrl("GetUserCalenderList", "Calendar") + "?page=" + pageNo;
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: ajaxUrl,
            dataType: "json",
            success: function (data) {
                if (data.ErrorCode != 0) {
                    $("#successBodyText").addClass("text-danger");
                    $("#successBodyText").text("Unable to get the meetings. Please try again or contact Support.");
                    $('#sucessUploadModal').modal('toggle');
                }
                self.responseSetUpData(data);
                CalenderVM.displayPaginationSetting(data.Pager.TotalPages);
            },
            error: function (err) {

            }
        });
    },

    meetingTypeCLick: function (data, event) {
        CalenderVM.responseMeetingsData({});
        CalenderVM.MeetingTypeId(event.target.getAttribute('mId'));
        var self = this;
        var ajaxUrl = ApplicationRootUrl("GetMeeingMeetingTypeId", "Calendar") + "?meetingId=" + CalenderVM.MeetingTypeId();
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: ajaxUrl,
            dataType: "json",
            success: function (data) {
                if (data.ErrorCode != 0) {
                    $("#successBodyText").addClass("text-danger");
                    $("#successBodyText").text("Unable to get the meetings. Please try again or contact Support.");
                    $('#sucessUploadModal').modal('toggle');
                }
                
                CalenderVM.responseMeetingsData(data);
                console.log(CalenderVM.responseMeetingsData().ListMeetings.length);
            },
            error: function (err) {

            }
        });

    },
    displayPaginationSetting: function (totalPage) {
        CalenderVM.pagination([]);
        for (var i = 1; i <= totalPage; i++) {
            CalenderVM.pagination.push({ id: i, pageNo: i });
        };
    }
}


$(document).ready(function () {
    ko.applyBindings(CalenderVM, document.getElementById("calenderForm"));
    CalenderVM.getUserCalenderList(1);
});