﻿
var usersManagementVM = {
    ClickId: ko.observable(""), UniversityId: ko.observable(""), ExecutiveAssistantName: ko.observable(""), 
    Action: ko.observable(false), ExecutiveAssistantemail: ko.observable(""), PostalAddress: ko.observable(""), DietaryRequirements: ko.observable(""),
    //  User Basic Info Variables
    Title: ko.observable(""), FirstName: ko.observable(""), typeOfMemberSelected: ko.observable(""),
    Organisation: ko.observable(""), Designation: ko.observable(""), Avatar: ko.observable(""),
    Email: ko.observable(""), PhoneNumber: ko.observable(""), Gender: ko.observable(""),
    meetingTypeId: ko.observable(""),
   // availableTitles: ko.observableArray([{ Type: "Mr", Name: "Mr" }, { Type: "Mrs", Name: "Mrs" }, { Type: "Ms", Name: "Ms" }, { Type: "Dr", Name: "Dr" }, { Type: "Professor", Name: "Professor" }, { Type: "Associate Professor", Name: "Associate Professor" }]),
   // typeOfMember: ko.observableArray([{ Type: "Elected", Name: "Elected" }, { Type: "Ex-Officio", Name: "Ex-Officio" }, { Type: "Appointed", Name: "Appointed" }]),
    // End of User Basic Info Variables


    responseSetUpData: ko.observable({
        MeetingId: ko.observable(),
        userList: ko.observableArray([]),
        userListViewModel: ko.observableArray([]),
        MeetingTypeId: ko.observable(""),
        MeetingType: ko.observable(),
        UserTitle: ko.observableArray([]),
        TypeOfMember: ko.observableArray([])

    }),
    AddModelClick: function () {
        usersManagementVM.ClickId('');
        $('#addUserModal').modal('show');
        usersManagementVM.Action(false);
        usersManagementVM.ClickId(''); usersManagementVM.Title(''); usersManagementVM.FirstName(''); usersManagementVM.Organisation(''); usersManagementVM.Designation('');
        usersManagementVM.Avatar(''); usersManagementVM.Email(''); usersManagementVM.PhoneNumber(''); usersManagementVM.Gender('');

    },

    AddEditUserSave: function () {
        var self = this;
        if ($("#addUserForm").valid()) {
            var ajaxUrl = ApplicationRootUrl("AddEditUser", "UserManagement");
            var model = new FormData();
            model.append('Id', usersManagementVM.ClickId());
            model.append('Title', usersManagementVM.Title());
            model.append('FirstName', usersManagementVM.FirstName());
            model.append('Organisation', usersManagementVM.Organisation());
            model.append('Designation', usersManagementVM.Designation());
            model.append('Avatar', usersManagementVM.Avatar());
            model.append('Email', usersManagementVM.Email());
            model.append('PhoneNumber', usersManagementVM.PhoneNumber());
            model.append('Gender', usersManagementVM.Gender());
            model.append('ImageUpload', document.getElementById('addAvatar').files[0]);
            model.append('meetingTypeId', usersManagementVM.meetingTypeId());

            model.append('UniversityId', usersManagementVM.UniversityId());
            model.append('ExecutiveAssistantName', usersManagementVM.ExecutiveAssistantName());
            model.append('ExecutiveAssistantemail', usersManagementVM.ExecutiveAssistantemail());
            model.append('PostalAddress', usersManagementVM.PostalAddress());
            model.append('DietaryRequirements', usersManagementVM.DietaryRequirements());
            model.append('TypeOfMemberSelected', usersManagementVM.typeOfMemberSelected());

            $.ajax({
                type: "POST",
                contentType: false,
                processData: false,
                url: ajaxUrl,
                data: model,
                dataType: "json",
                success: function (data) {
                    if (data.ErrorCode != 0) {
                        $("#successBodyText").addClass("text-danger");
                        $("#successBodyText").text("Sorry but your creation of this new user was not successful.Please check your internet connection as you may not be connected to Egenda.If your internet connection is working, try again in a few minutes.If you are still not successful contact support@egenda.info for help.");
                    } else {
                        self.responseSetUpData(data);
                        $('#addUserModal').modal('toggle');
                        if (usersManagementVM.Action()) {
                            $("#successBodyText").text("Updated Successfully.");
                        } else {
                            $("#successBodyText").text("New User has been successfully created. Please return to Set Up Users in your Meeting Type and Assign this User to their meeting role.");

                        }
                    }
                    $('#sucessUploadModal').modal('toggle');
                },
                error: function (err) {

                }

            });
        }
    },

    EditUserCLick: function (data, event) {
        usersManagementVM.ClickId(event.target.id);
        usersManagementVM.Action(true);
        var self = this;
        var ajaxUrl = ApplicationRootUrl("GetUserById", "UserManagement") + "?id=" + usersManagementVM.ClickId();
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: ajaxUrl,
            dataType: "json",
            success: function (data) {
                if (data.ErrorCode != 0) {
                    $("#successBodyText").addClass("text-danger");
                    $("#successBodyText").text("Unable to find this user. Please try again or contact Support.");
                    $('#sucessUploadModal').modal('toggle');
                } else {
                    data.SelectTitle === 'undefined' ? usersManagementVM.Title('') : usersManagementVM.Title(data.SelectTitle);
                    data.FirstName === 'undefined' ? usersManagementVM.FirstName('') : usersManagementVM.FirstName(data.FirstName);
                    data.Organisation === 'undefined' ? usersManagementVM.Organisation('') : usersManagementVM.Organisation(data.Organisation);
                    data.Designation === 'undefined' ? usersManagementVM.Designation(data.Designation) : usersManagementVM.Designation(data.Designation);
                    data.Email === 'undefined' ? usersManagementVM.Email('') : usersManagementVM.Email(data.Email);
                    data.PhoneNumber === 'undefined' ? usersManagementVM.PhoneNumber('') : usersManagementVM.PhoneNumber(data.PhoneNumber);
                    usersManagementVM.Avatar('');
                    data.UniversityId === 'undefined' ? usersManagementVM.UniversityId('') : usersManagementVM.UniversityId(data.UniversityId);
                    data.ExecutiveAssistantName === 'undefined' ? usersManagementVM.ExecutiveAssistantName('') : usersManagementVM.ExecutiveAssistantName(data.ExecutiveAssistantName);
                    data.ExecutiveAssistantemail === 'undefined' ? usersManagementVM.ExecutiveAssistantemail('') : usersManagementVM.ExecutiveAssistantemail(data.ExecutiveAssistantemail);
                    data.PostalAddress === 'undefined' ? usersManagementVM.PostalAddress('') : usersManagementVM.PostalAddress(data.PostalAddress);
                    data.DietaryRequirements === 'undefined' ? usersManagementVM.DietaryRequirements('') : usersManagementVM.DietaryRequirements(data.DietaryRequirements);

                }
            },
            error: function (err) {

            }
        });


    },
    DeleteUserCLick: function (data, event) {
        usersManagementVM.ClickId(event.target.id);
        $('#deleteModal').modal('show');
    },

    ConfirmDeleteUserCLick: function (data, event) {
        var self = this;
        var ajaxUrl = ApplicationRootUrl("DeleteUser", "UserManagement") + "?id=" + usersManagementVM.ClickId();
        $.ajax({
            type: "DELETE",
            contentType: "application/json; charset=utf-8",
            url: ajaxUrl,
            success: function (data) {
                if (data.ErrorCode != 0) {
                    $("#successBodyText").addClass("text-danger");
                    $("#successBodyText").text("Failed to Delete User. Please Reload Page or Contact System Admin.");
                    $('#sucessUploadModal').modal('toggle');
                } else {
                    self.responseSetUpData(data);
                    $('#deleteModal').modal('toggle');
                    $("#successBodyText").text("Deleted Successfully.");
                    $('#sucessUploadModal').modal('toggle');
                }

            },
            error: function (err) {

            }
        });
    },

    getUsersList: function () {
        var self = this;
        var ajaxUrl = ApplicationRootUrl("GetUsersList", "UserManagement");
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: ajaxUrl,
            dataType: "json",
            success: function (data) {
                if (data.ErrorCode != 0) {
                    $("#successBodyText").addClass("text-danger");
                    $("#successBodyText").text("Unable to get all users. Please try again or contact Support.");
                    $('#sucessUploadModal').modal('toggle');
                }
                //self.
                self.responseSetUpData(data);
            },
            error: function (err) {

            }
        });
    }
}



$.validator.addMethod(
    "regex",
    function (value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
    }
);
$("#addUserForm").validate({
    rules: {
        titleSelect: {
            required: true
        },
        eaEmail: {
            email: true
        },
        email: {
            required: true,
            email: true,
            remote: function () {
                // $("#loading").show();
                var r = {
                    url: ApplicationRootUrl("CheckUser", "UserManagement"),
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{'email': '" + usersManagementVM.Email() + "'}",
                    dataFilter: function (data) {
                        // $("#loading").hide();
                        return (JSON.parse(data));
                    }

                }
                return r;
            }
        },
        fullName: {
            required: true
        },
        phone: {
            required: true,
            regex: /[0-9]{2}\s\([0-9]{2}\)\s[0-9]{3}\s[0-9]{4}/
        }
    },

    messages: {
        titleSelect: {
            valueNotEquals: "This Field is Required."
        },
        eaEmail: {
            email: "Please enter a valid email address"
        },
        email: {
            required: "This Field is Required.",
            remote: "User Already Exists"
        },
        fullName: {
            required: "This Field is Required."
        },
        phone: {
            digits: "Invalid Phone Number.",
            regex: "Invalid Phone Number."
        }
    },

    highlight: function (element) {
        $(element).closest('div').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function (element) {
        $(element).closest('div').removeClass('has-error').addClass('has-success');
    }
});






$(document).ready(function () {
    ko.applyBindings(usersManagementVM, document.getElementById("userManagementForm"));
    usersManagementVM.getUsersList();
    $("#phone").mask("+99 (99) 999 9999");

});