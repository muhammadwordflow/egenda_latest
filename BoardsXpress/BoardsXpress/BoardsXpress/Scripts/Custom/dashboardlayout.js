﻿var Dashboard = {
    init: function () {
        var url = $(location).attr('href').split('/').pop();
        if (url === 'MeetingTypes?del') {
            $('#meeting-types').hide();
            $('#delete-meetingtype').show();
        };

        // Set meeting-create onClick event
        $(document).on('click', '#meeting-create', function () {
            $('#meetingType-inputForm').show();
        });

        $(document).on('show.bs.modal', '#createMeetingType', function () {
            $('#frmCreateMeetingType').clearForm();
            $('#frmCreateMeetingType').valid();
        });

        $(document).on('shown.bs.modal', '#createMeetingType', function () {
            $('#meetingType-input').focus();
        });

        $(document).on('submit', '#frmCreateMeetingType', function (e) {
            e.preventDefault();

            $.ajax({
                url: '/MeetingTypes/Create',
                type: "POST",
                datatype: "json",
                data: JSON.stringify({
                    meetingType: $('#meetingType-input').val(),
                    meetingAbbreviation: $('#meetingAbbreviation-input').val(),
                    useAbbreviation: $("#useAbbreviation").is(':checked') ? $('#useAbbreviation').prop("checked") : false 

                }),
                contentType: "application/json",
                success: function (result) {
                    window.location.reload();
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });

        $(document).on('show.bs.modal', '#editMeetingType', function () {
            $.ajax({
                url: 'MeetingTypes/Details',
                async: false,
                type: "GET",
                data: { meetingTypeId: $('#editMeetingType').data('meetingtypeid') },
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    if (data.Success) {
                        $('#hdnMeetingTypeId').val(data.MeetingType.meetingTypeId);
                        $('#meetingTypeName').val(data.MeetingType.name);
                        $('#meetingAbbreviation').val(data.MeetingType.abbreviation);

                        if (data.MeetingType.useabbreviation)
                            $('#useAbbreviation').prop("checked", "checked");
                        else
                            $('#useAbbreviation').prop("checked", "");
                    }

                    return false;
                }
            });
        });

        $(document).on('shown.bs.modal', '#editMeetingType', function () {
            $('#frmMeetingType').focus_first();
        });

        // Set meeting-create onClick event
        $(document).on('submit', '#frmMeetingType', function (ev) {
            ev.preventDefault();

            $.ajax({
                url: 'MeetingTypes/Edit',
                async: false,
                type: "POST",
                data: JSON.stringify({
                    "meetingTypeId": $('#hdnMeetingTypeId').val(),
                    "name": $('#meetingTypeName').val(),
                    "abbreviation": $('#meetingAbbreviation').val(),
                    "useAbbreviation": $('#useAbbreviation').prop("checked")
                }),
                dataType: 'json',
                contentType: "application/json",
                success: function (result) {
                    if (result.Success) 
                        $('#spanMeetingTypeName').text(
                            $('#useAbbreviation').prop("checked") ? $('#meetingAbbreviation').val() : $('#meetingTypeName').val());
                    
                    Dashboard.closeModalOnSuccess();
                    Main.openModal(result);
                },
                error: function (error) {
                    Dashboard.closeModalOnSuccess();
                    Main.openModal(result);
                }
            });
        });

        $(document).on('click', '#delete-meeting', function () {
            var url = $(location).attr('href').split('/').pop();
            if (url !== 'MeetingTypes') {
                location.replace('/MeetingTypes?del');
            }
            $('#meeting-types').hide();
            $('#delete-meetingtype').show();
        });

        $(document).on('click', '#delete-meetingtype>button', function () {
            $(this).find('.tick-meeting').css('border', '2px solid #f58220;');
            $(this).find('.tick-meeting').toggle();
        });

        $(document).on('click', '#cancel-deletemeeting', function () {
            location.replace('/MeetingTypes');
        });

        $(document).on('click', '#deletemeeting-button', function () {
            var meetingsID = [];
            $('.tick-meeting').each(function (i) {
                if ($(this).css('display') === 'block') {
                    meetingsID.push($(this).parent().prop('id'));
                }
            })
            $('#deleteMtypeModal').modal('show');
            $('#deleteMtype').click(function () {
                $.ajax({
                    url: 'MeetingTypes/DeleteConfirmed',
                    async: false,
                    type: "POST",
                    data: JSON.stringify({ "meetingTypeId": meetingsID }),
                    dataType: "json",
                    contentType: "application/json",
                    error: function (data) {
                    },
                    success: function (data) {
                        location.reload();
                    }
                });

            })

        });
    },
    setFormValidation:
    // set forms validation
    function setFormValidation(frmId, btnId) {
        $(frmId + ' input').on('keyup blur', function () { // fires on every keyup & blur
            $(btnId).prop('disabled', !$(frmId).valid()); // enable/disable button
        });
    },
    closeModalOnSuccess:
    function closeModalOnSuccess() {
        $('#editMeetingType').modal('hide');
    }
}

$(document).ready(function () {
    Dashboard.init();
});

