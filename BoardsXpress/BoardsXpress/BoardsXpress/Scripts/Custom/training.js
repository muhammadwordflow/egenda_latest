﻿$('#finishTraining').click(function () {
    $.ajax({
        url: '/Training/finishTraining',
        type: "POST",
        data: JSON.stringify({
            "training": true
        }),
        contentType: "application/json",
        success: function (result) {
            $('#sucessModal').modal('show');
        },
        error: function (error) {
            console.log(error);
        }
    }); 
    });