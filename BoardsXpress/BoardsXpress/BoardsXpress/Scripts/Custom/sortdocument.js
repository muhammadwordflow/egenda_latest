﻿var originalRow = 1;

// Return a helper with preserved width of cells
var fixHelper = function (e, ui) {
    ui.children().each(function () {
        $(this).width($(this).width());
    });
    return ui;
};

$(document).ready(function () {
    $("#sort tbody").sortable({
        cursor: 'move',
        connectWith: '.relatedItems',
        opacity: 0.7,
        start: function (event, ui) {
            originalRow = ui.item[0].rowIndex;
        },
        update: function (event, ui) {
            var draggedItem = ui.item[0];
            var swappedRow = $(draggedItem.closest("tbody")).find('tr').eq(originalRow);
            var iaaaa = 0;
            //$.ajax({
            //    url: '/AgendaItems/UpdateSortingOrder',
            //    type: "POST",
            //    dataType: "json",
            //    contentType: "application/json",
            //    data: JSON.stringify({
            //        "meetingId": $(draggedItem).find("td[data-column='meetingId']").text(),
            //        "agendaItemId": $(draggedItem).find("td[data-column='agendaItemId']").text(),
            //        "agendaItemRelatedId": $(draggedItem).find("td[data-column='relatedItemId']").text().length > 0 ? $(draggedItem).find("td[data-column='relatedItemId']").text() : null,
            //        "draggedOrder": $(draggedItem).data('order'),
            //        "swappedAgendaItemdId": $(swappedRow).find("td[data-column='agendaItemId']").text(),
            //        "swappedAgendaItemRelatedId": $(swappedRow).find("td[data-column='relatedItemId']").text().length > 0 ? $(swappedRow).find("td[data-column='relatedItemId']").text() : null,
            //        "swappedOrder": $(swappedRow).data('order')
            //    }),
            //    success: function (result) {
            //        window.location.reload(true);
            //    },
            //    error: function (error) {
            //        console.log(error);
            //    }
            //});
        }
    }).disableSelection();
});

var fixHelperModified = function (e, tr) {
    var $originals = tr.children();
    var $helper = tr.clone();
    $helper.children().each(function (index) {
        $(this).width($originals.eq(index).width())
    });
    return $helper;
};

function movePlaceholder(e, ui) {
    if (ui.placeholder.prev().attr("id").length == 3)
        ui.placeholder.insertAfter(ui.placeholder.next());
}

function hasChanged(e, ui) {
    var newPosition = ui.item.index();
    var id = ui.item.attr("id");

    // whatever you need to do goes here
}