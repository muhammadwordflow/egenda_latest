﻿var AnnotatorJS = {
    isInitialised: false,
    docId: '',
    init: function (docId) {
        if (AnnotatorJS.docId != docId)
        {
            AnnotatorJS.docId = docId;
            $('.doc-content').annotator();
        }
    },
    load: function () {
        if (AnnotatorJS.isInitialised) {
            $('.doc-content').annotator('destroy');
            $('.doc-content').annotator();
        }

        $('.doc-content').annotator('addPlugin', 'Store', {
            // The endpoint of the store on your server.
            prefix: '/AnnotationManagement',

            // Attach the uri of the current page to all annotations to allow search.
            annotationData: {
                'docId': $('.doc-content').data('doc-id'),
                'uri': $(location).attr('href')
            },

            urls: {
                // These are the default URLs.
                create: '/annotations',
                update: '/annotations/:id',
                destroy: '/annotations/:id',
                search: '/search'
            },
            
            loadFromSearch: {
                'limit': 0,
                'all_fields': 1,
                'uri': $(location).attr('href'),
                'docId': $('.doc-content').data('doc-id')
            }
        });

        AnnotatorJS.isInitialised = true;
    }
}