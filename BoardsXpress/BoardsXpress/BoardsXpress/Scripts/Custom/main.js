﻿"use strict";

var Main = {
    init:
    function () {
        /*tool tip*/
        $('body').tooltip({
            selector: '[data-toggle="tooltip"]'
        });

        // Focus first element
        $.fn.focus_first = function () {
            var elem = $('input.auto-focus', this).get(0);
            var select = $('select.auto-focus', this).get(0);
            if (select && elem) {
                if (select.offsetTop < elem.offsetTop) {
                    elem = select;
                }
            }
            var textarea = $('textarea.auto-focus', this).get(0);
            if (textarea && elem) {
                if (textarea.offsetTop < elem.offsetTop) {
                    elem = textarea;
                }
            }

            if (elem) {
                elem.focus();
            }
            return this;
        };
        
        $('#search').click(function () {
            $('#rightnav-menu').hide()
            $('#rightnav-search').fadeIn(500);
        });

        $(document).on('click', '.nav a', function () {
            $('.nav').find('.active').removeClass('active')
            $(this).addClass('active');
        });

        /*open modal*/
        $('#upload-file').click(function () {
            $('#fileModal').modal('show');
        });

        /*After selection of the meeting*/
        $('#continue').click(function () {
            $('#fileModal').modal('hide');
            $('.login-header').val('Select Meeting Agenda');
            $('#fileModal').modal('')
        });

        /*custom modal js*/
        //----- OPEN
        $(document).on('click', '[data-popup-open]', function (e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-open');
            $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

            e.preventDefault();
        });

        //----- CLOSE
        $(document).on('click', '[data-popup-close]', function (e) {
            //$('[data-popup-close]').on('click', function (e) {
            var targeted_popup_class = jQuery(this).attr('data-popup-close');
            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

            e.preventDefault();
        });

        $('#frmUploadDocument').submit(function (ev) {
            ev.preventDefault();

            var formData = new FormData(this);
            
            var uploadFileProgressBar = $('#uploadFileProgressBar');
            var bar = uploadFileProgressBar.find('.progress-bar');

            var interval_progress;
            var percentValue = 25;
            var speed = 670;

            function getPercent() {
                return percentValue;
            }

            // Check if document already exists
            $.ajax({
                url: '/DocumentLibraries/CheckIfDocumentExists',
                async: true,
                data: {
                    agendaItem: $('#magendaItem').val(),
                    agendaItemRelatedId: $('#agendaItemRelated').val()
                },
                type: "POST",
                dataType: 'json',
                success: function (result) {

                    if (result.Success) {
                        $("#uploadFileModal").modal('hide');

                        uploadFileProgressBar.hide();

                        bar.text('Uploading file...');

                        // enabled the submit button
                        $("#btnUploadDocument").prop("disabled", false);
                        $("#btnCloseUploadDocument").prop("disabled", false);

                        // Clear the form
                        $('#agendaItemRelated').html("");
                        $('#file').val("");

                        $('#btnSuccessUploadModal').data('success', 'false');

                        $('#sucessUploadModal').find('.modal-title')[0].innerHTML = result.Title;
                        $('#sucessUploadModal').find('.modal-body')[0].innerHTML = result.Message;
                        $('#sucessUploadModal').modal('show');
                    }
                    else {

                        $.ajax({
                            xhr: function () {
                                var xhr = new window.XMLHttpRequest();
                                xhr.upload.addEventListener("progress", function (evt) {
                                    if (evt.lengthComputable) {
                                        var percentComplete = evt.loaded / evt.total;
                                        bar.css({
                                            width: percentComplete * 25 + '%'
                                        });
                                    }
                                }, false);

                                xhr.upload.addEventListener("load", function () {
                                    var update_progress = function () {

                                        if (percentValue < 100) {
                                            percentValue++;
                                            bar.css({
                                                width: percentValue + '%'
                                            });
                                        }
                                        else {
                                            bar.text('Taking a little longer, please hold...');
                                        }

                                        interval_progress = setTimeout(update_progress, speed);
                                    };

                                    interval_progress = setTimeout(update_progress, speed);
                                });

                                return xhr;
                            },
                            url: '/DocumentLibraries/UploadFiletoDatabase',
                            async: true,
                            data: formData,
                            type: "POST",
                            dataType: 'json',
                            processData: false,
                            contentType: false,
                            beforeSend: function () {
                                // disabled the submit button
                                $("#btnUploadDocument").prop("disabled", true);
                                $("#btnCloseUploadDocument").prop("disabled", true);

                                uploadFileProgressBar.show();
                            },
                            success: function (result) {
                                clearTimeout(interval_progress);

                                bar.css({
                                    width: result.Success ? '100%' : '25%'
                                });

                                setTimeout(function () {
                                    $("#uploadFileModal").modal('hide');

                                    uploadFileProgressBar.hide();

                                    bar.css({
                                        width: '0%'
                                    });

                                    bar.text('Uploading file...');

                                    $('#file').val('');

                                    // enabled the submit button
                                    $("#btnUploadDocument").prop("disabled", false);
                                    $("#btnCloseUploadDocument").prop("disabled", false);

                                    $('#btnSuccessUploadModal').data('success', result.Success.toString());

                                    $('#sucessUploadModal').find('.modal-title')[0].innerHTML = result.Title;
                                    $('#sucessUploadModal').find('.modal-body')[0].innerHTML = result.Message;
                                    $('#sucessUploadModal').modal('show');
                                }, 1000);
                            },
                            error: function (error) {
                                var data = error.responseJSON;

                                clearTimeout(interval_progress);

                                bar.css({
                                    width: '0%'
                                });

                                bar.text('Uploading file...');

                                uploadFileProgressBar.hide();

                                $("#uploadFileModal").modal('hide');

                                // enabled the submit button
                                $("#btnUploadDocument").prop("disabled", false);
                                $("#btnCloseUploadDocument").prop("disabled", false);

                                // Clear the form
                                $('#agendaItemRelated').html("");
                                $('#file').val("");

                                $('#btnSuccessUploadModal').data('success', "false");

                                $('#sucessUploadModal').find('.modal-title')[0].innerHTML = "Error";
                                $('#sucessUploadModal').find('.modal-body')[0].innerHTML = "Your document upload was not successful. Please try the upload again. if it still does not upload successfully, try deleting and reloading the document into the document repository. If the upload does not work after reloading, please contact your System Administrator for support.";
                                $('#sucessUploadModal').modal('show');
                            }
                        });

                    }

                }
            });
        });

        $('#btnSuccessUploadModal').click(function () {
            if ($('#btnSuccessUploadModal').data('success') === "true") {
                if ($(location).attr('pathname').includes('Publish'))
                    getPublishedDocumentsList($('#MeetingTypesID').text(), $('#currentMeetingId').text());
            }

            $('#sucessUploadModal').modal('hide');
        });

        $('#sucessUploadModal').on('hide.bs.dropdown', function () {
            if ($(location).attr('pathname').includes('Publish'))
                getPublishedDocumentsList($('#MeetingTypesID').text(), $('#currentMeetingId').text());
            else if ($(location).attr('pathname').includes('DisplayMeeting'))
                getTreeView();

            $('#sucessUploadModal').modal('hide');
        });

        // Load the related agenda items
        $('#magendaItem').on('change', function (ev) {
            var selectedValue = $(this).val();

            $.ajax({
                url: '/AgendaItems/GetAgendaItemRelatedList',
                async: true,
                type: "GET",
                data: { agendaItemId: selectedValue },
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    if (data.length > 1) {
                        $('#agendaItemRelatedSelectRow').show();

                        $("#agendaItemRelated").html(""); // clear before appending new list 
                        $.each(data, function (i, item) {
                            $("#agendaItemRelated").append(
                                $('<option></option>').val(item.Value).html(item.Text));
                        });
                    }
                    else {
                        $('#agendaItemRelatedSelectRow').hide();
                    }
                }
            });
        });
    },
    toggleFullScreen:
    /*full screen*/
    function () {
        if ((document.fullScreenElement && document.fullScreenElement !== null) ||
            (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            $('#go-fullscreen').hide();
            $('#hide-fullscreen').show();
            if (document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            $('#hide-fullscreen').hide();
            $('#go-fullscreen').show();
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    },
    startUpload: //When file is selected and get Started is clicked
    //Upload file progress bar
    function () {
        var bar = $('.progress-bar');
        var percent = $('.progress-bar');
        var status = $('#status');
        var interval_progress;
        var percentValue = 0;
        var speed = 50;

        function getPercent() {
            return percentValue;
        }

        $('form').ajaxForm({
            beforeSend: function () {
                status.empty();
                bar.width(percentValue + '%');
                //percent.html(percentValue + '%');
                percent.html('Uploading file...');
            },
            uploadProgress: function (event, position, total, percentComplete) {

                $(".progress").show();

                var update_progress = function () {

                    if (percentValue < 100) {
                        percentValue++;
                        bar.width(percentValue + '%');
                        //percent.html(percentValue + '%');
                        percent.html('Uploading file...');
                    }

                    interval_progress = setTimeout(update_progress, speed);
                };

                interval_progress = setTimeout(update_progress, speed);

            },
            success: function (d) {

                speed = 50;

                var success_ = function () {

                    if (getPercent() === 100) {
                        clearTimeout(interval_progress);
                        $('.popup').fadeOut(350);

                        $('#sucessUploadModal').find('.modal-title')[0].innerHTML = d.Title;
                        $('#sucessUploadModal').find('.modal-body')[0].innerHTML = d.Message;
                        $('#sucessUploadModal').modal('show');

                        var percentValue = 0;
                        $(".progress").hide();

                        bar.width(percentValue + '%');
                        //percent.html(percentValue + '%');
                        percent.html('Uploading file...');

                        clearInterval(time_out);
                    }
                };

                var time_out = setInterval(success_, 2000);

                $('.successful').click(function () {
                    $('#sucessUploadModal').modal('hide');
                    window.location.reload();
                });
            }
        });
    },
    setFormValidation:
    // set forms validation
    function (frmId, btnId) {
        $(frmId + ' input').on('keyup blur', function () { // fires on every keyup & blur
            $(btnId).prop('disabled', !$(frmId).valid());        // enable/disable button
        });
    },
    openModal: 
    function (data) {
        $('#generalSucessModalTitle').text(data.Success ? 'Message' : 'Error');
        $('#generalSucessModalBody').text(data.Message);
        $('#generalSucessModal').modal('show');
    },
    getAllUrlParams: function (url) {

        // get query string from url (optional) or window
        var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

        // we'll store the parameters here
        var obj = {};

        // if query string exists
        if (queryString) {

            // stuff after # is not part of query string, so get rid of it
            queryString = queryString.split('#')[0];

            // split our query string into its component parts
            var arr = queryString.split('&');

            for (var i = 0; i < arr.length; i++) {
                // separate the keys and the values
                var a = arr[i].split('=');

                // in case params look like: list[]=thing1&list[]=thing2
                var paramNum = undefined;
                var paramName = a[0].replace(/\[\d*\]/, function (v) {
                    paramNum = v.slice(1, -1);
                    return '';
                });

                // set parameter value (use 'true' if empty)
                var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

                // (optional) keep case consistent
                paramName = paramName.toLowerCase();
                paramValue = paramValue.toLowerCase();

                // if parameter name already exists
                if (obj[paramName]) {
                    // convert value to array (if still string)
                    if (typeof obj[paramName] === 'string') {
                        obj[paramName] = [obj[paramName]];
                    }
                    // if no array index number specified...
                    if (typeof paramNum === 'undefined') {
                        // put the value on the end of the array
                        obj[paramName].push(paramValue);
                    }
                    // if array index number specified...
                    else {
                        // put the value at that index number
                        obj[paramName][paramNum] = paramValue;
                    }
                }
                // if param name doesn't exist yet, set it
                else {
                    obj[paramName] = paramValue;
                }
            }
        }

        return obj;
    }
}

$(document).ready(function () {
    Main.init();
});
