﻿$(".deleteMyDoc").unbind().click(function (event) {
    var docId = $(this).closest('tr').children()[0].textContent.trim();
    $('#deleteMyDocConfirm').click(function () {
        $.ajax({
            url: '/DocumentLibraries/DeleteMyDoc',
            async: false,
            type: "POST",
            data: JSON.stringify({ "Id": docId }),
            dataType: "json",
            contentType: "application/json",
            error: function (data) {
                alert(data.Message);
            },
            success: function (data) {
                //window.location.href = window.location.href;
                location.reload(true);
            }
        });
    });
});