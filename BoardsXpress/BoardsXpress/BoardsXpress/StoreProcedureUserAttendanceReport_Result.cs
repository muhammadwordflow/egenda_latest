//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BoardsXpress
{
    using System;
    
    public partial class StoreProcedureUserAttendanceReport_Result
    {
        public System.Guid meetingTypeId { get; set; }
        public string meetingType { get; set; }
        public string meetingAbbreviation { get; set; }
        public bool useAbbreviation { get; set; }
        public Nullable<System.DateTime> meetingDate { get; set; }
        public System.Guid meetingId { get; set; }
        public string UserId { get; set; }
        public Nullable<bool> userResponse { get; set; }
    }
}
