//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BoardsXpress
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblRefDoc
    {
        public System.Guid refDocId { get; set; }
        public Nullable<System.Guid> meetingTypeId { get; set; }
        public string filePath { get; set; }
        public string fileName { get; set; }
        public string refDocName { get; set; }
        public string htmlContent { get; set; }
        public Nullable<bool> publish { get; set; }
        public string webLink { get; set; }
    
        public virtual tblMeetingType tblMeetingType { get; set; }
    }
}
