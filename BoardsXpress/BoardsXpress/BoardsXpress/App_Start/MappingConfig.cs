﻿using AutoMapper;
using BoardsXpress.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BoardsXpress.App_Start
{
    public class MappingConfig:Profile
    {
        public static void RegisterMaps()
        {

            Mapper.Initialize(config =>
            {
                config.CreateMap<tblMeetingType, MeetingViewModels>()
                .ForMember(dest => dest.meetingTypeId, opt => opt.MapFrom(src => src.meetingTypeId))
                .ForMember(dest => dest.meetingType, opt => opt.MapFrom(src => src.meetingType));

                config.CreateMap<tblRssFeedManager, FeedManagerViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.id))
                .ForMember(dest => dest.MeetingTypeId, opt => opt.MapFrom(src => src.meetingId))
                .ForMember(dest => dest.FeedProvider, opt => opt.MapFrom(src => src.feeProvider))
                .ForMember(dest => dest.FeedURL, opt => opt.MapFrom(src => src.feedUrl));

                config.CreateMap<tblMeetingType, MeetingTypeViewModel>();
                
                config.CreateMap<AnnotationRange, tblAnnotationsRanx>();
                config.CreateMap<tblAnnotationsRanx, AnnotationRange>();

                config.CreateMap<Annotator, tblAnnotation>()
                .ForMember(dest => dest.annotationId, opt => opt.MapFrom(src => src.id))
                //.ForMember(dest => dest.annotatorSchemaVersion, opt => opt.MapFrom(src => src.annotator_schema_version))
                .ForMember(dest => dest.createdOn, opt => opt.MapFrom(src => src.created))
                .ForMember(dest => dest.docId, opt => opt.MapFrom(src => src.docId))
                .ForMember(dest => dest.quote, opt => opt.MapFrom(src => src.quote))
                .ForMember(dest => dest.text, opt => opt.MapFrom(src => src.text))
                //.ForMember(dest => dest.updatedOn, opt => opt.MapFrom(src => src.updated))
                .ForMember(dest => dest.userId, opt => opt.MapFrom(src => src.user));

                config.CreateMap<tblAnnotation, Annotator>()
                .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.annotationId))
                //.ForMember(dest => dest.annotator_schema_version, opt => opt.MapFrom(src => src.annotatorSchemaVersion))
                .ForMember(dest => dest.created, opt => opt.MapFrom(src => src.createdOn))
                .ForMember(dest => dest.docId, opt => opt.MapFrom(src => src.docId))
                .ForMember(dest => dest.quote, opt => opt.MapFrom(src => src.quote))
                .ForMember(dest => dest.text, opt => opt.MapFrom(src => src.text))
                //.ForMember(dest => dest.updated, opt => opt.MapFrom(src => src.updatedOn))
                .ForMember(dest => dest.user, opt => opt.MapFrom(src => src.userId));

                config.CreateMap<tblMeeting, MeetingDDLViewModel>()
                .ForMember(dest => dest.MeetingId, opt => opt.MapFrom(src => src.meetingId))
                .ForMember(dest => dest.MeetingDate, opt => opt.MapFrom(src => src.meetingDate.Value.ToString("dd/MM/yyyy")));

                config.CreateMap<tblMeeting,MeetingViewModels>()
                .ForMember(dest => dest.meetingId, opt => opt.MapFrom(src => src.meetingId))
                .ForMember(dest => dest.meetingTypeId, opt => opt.Ignore())
                .ForMember(dest => dest.createdDate, opt => opt.MapFrom(src => src.meetingDate))
                .ForMember(dest => dest.startTime, opt => opt.MapFrom(src => src.startTime))
                .ForMember(dest => dest.finishTime, opt => opt.MapFrom(src => src.finishTime))
                .ForMember(dest => dest.meetingVenue, opt => opt.MapFrom(src => src.meetingVenue))
                .ForMember(dest => dest.meetingNotes, opt => opt.MapFrom(src => src.meetingNotes));

                config.CreateMap<tblMeetingType, MeetingViewModels>()
                .ForMember(dest => dest.meetingTypeId, opt => opt.MapFrom(src => src.meetingTypeId))
                .ForMember(dest => dest.createdDate, opt => opt.MapFrom(src => src.createdDate))
                .ForMember(dest => dest.meetingAbbreviation, opt => opt.MapFrom(src => src.meetingAbbreviation))
                .ForMember(dest => dest.meetingType, opt => opt.MapFrom(src => src.meetingType))
                .ForMember(dest => dest.useAbbreviation, opt => opt.MapFrom(src => src.useAbbreviation));

                config.CreateMap<MeetingViewModels,tblMeeting>()
                .ForMember(dest => dest.meetingId, opt => opt.MapFrom(src => src.meetingId))
                .ForMember(dest => dest.meetingType, opt => opt.MapFrom(src=>src.meetingTypeId))
                .ForMember(dest => dest.meetingDate, opt => opt.MapFrom(src => src.createdDate))
                .ForMember(dest => dest.startTime, opt => opt.MapFrom(src => src.startTime))
                .ForMember(dest => dest.finishTime, opt => opt.MapFrom(src => src.finishTime))
                .ForMember(dest => dest.meetingVenue, opt => opt.MapFrom(src => src.meetingVenue))
                .ForMember(dest => dest.meetingNotes, opt => opt.MapFrom(src => src.meetingNotes));

                config.CreateMap<tblAgendaItemsRelated, AgendaItemsRelatedViewModel>();
            });
        }
    }
}