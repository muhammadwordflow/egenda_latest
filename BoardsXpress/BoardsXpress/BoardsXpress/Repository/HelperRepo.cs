﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BoardsXpress.Models;
namespace BoardsXpress.Repository
{
    public class HelperRepo
    {
        BoardExpressEntities db = new BoardExpressEntities();
        public  Guid meetingIdGlobal;
        public  Guid meetingTypeIdGlobal;
        public List<LoggedUserRolesModels> LoggedUserRoleInformation()
        {
            var LoggedUserRolesList = new List<LoggedUserRolesModels>();
            var LoggedUserId = HttpContext.Current.User.Identity.GetUserId();
    
            if (LoggedUserId != null)
            {
                var userRoles = db.AspNetUserRoles.Where(w => w.UserId == LoggedUserId).ToList();

                foreach (var item in userRoles)
                {
                    var roleInfo = new LoggedUserRolesModels
                    {
                        RoleId = item.RoleId,
                        RoleName = db.AspNetRoles.Where(w => w.Id == item.RoleId).Select(s => s.Name).FirstOrDefault(),
                        userId = LoggedUserId,
                        MeetingTypeId = item.MeetingTypeId.HasValue ? item.MeetingTypeId.Value : Guid.Empty
                    };

                    LoggedUserRolesList.Add(roleInfo);
                }
            }

            return LoggedUserRolesList;
        }
        
    }

}