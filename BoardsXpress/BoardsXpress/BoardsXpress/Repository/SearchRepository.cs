﻿using BoardsXpress.Models;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;



namespace BoardsXpress.Repository
{
    public class SearchRepository
    {
        private BoardExpressEntities db = new BoardExpressEntities();

        public List<SearchResultViewModel> SimpleSearch(string searchText)
        {
           // SearchRepository s = new SearchRepository();
           // var allContents = s.GetAllFromHtmlLibrary();
           //// var searchResultViewList = s.SearchContent(allContents, searchText);
            var searchResultViewList = new List<SearchResultViewModel>();
           // if (searchResultViewList != null)
           // {
           //     foreach (var item in searchResultViewList)
           //     {
                    
           //         var htmlContent = item.HtmlContent;
                   
           //         var htmlData = ConvertHtml(htmlContent);
           //         var trimTextExtract = TakeLastLines(htmlData, 4);
           //        item.HtmlContent = trimTextExtract;
           //         //searchContent.HtmlContent=  HighlightKeyWords(trimTextExtract, searchText, "yellow", true);
           //         //searchResultViewList.Add(searchContent);
           //     }
           // }
            return searchResultViewList;

        }

        public List<SearchResultViewModel> ComplexSearch(string searchText,string meetingType,string meeting,string agendaItem)
        {
           // Guid meetingTypeId = Guid.NewGuid();
           // Guid meetingId = Guid.NewGuid();
           // int agendaId = 1;

           // SearchRepository s = new SearchRepository();
           // var allContents = s.GetAllFromHtmlLibrary();
           // ///if meetingType is given
           // if (meetingType != "Select Meeting Type")
           // {
           //     meetingTypeId = Guid.Parse(meetingType);
           //     //meetingTypeId = db.tblMeetingTypes.Where(w => w.meetingType == meetingType).FirstOrDefault().meetingTypeId;
           //     allContents = s.SearchContentBasedOnMeetingType(allContents, meetingTypeId);
           // }
           // ///if meeting id given
           // ///
           // if (meeting != "Select Meeting Date")
           // {
           //     var meetingDate = DateTime.Parse(meeting);
           //     meetingId = db.tblMeetings.Where(w => w.meetingDate == meetingDate).FirstOrDefault().meetingId;
           //     allContents = s.SearchContentBasedOnMeeting(allContents, meetingId);
           // }
           // ////if agenda is given
           // if (agendaItem != "Select Agenda")
           // {
           //     agendaId = Convert.ToInt32(agendaItem);
           //     //agendaId = db.tblAgendaItems.Where(w => w.agendaItem == agendaItem).FirstOrDefault().Id;
           //     allContents = s.SearchContentBasedOnAgendaItem(allContents, agendaId);
           // }
           // ///then perform text search
           // var searchResultViewList = s.SearchContent(allContents, searchText);
            var searchResultViewList = new List<SearchResultViewModel>();
           // if (searchResultViewList != null)
           // {

           //     foreach (var item in searchResultViewList)
           //     {
                   
           //         var htmlContent=item.HtmlContent;
                  
           //        // var htmlTextExtract = ExtractTextFromHtml(htmlContent);

           //         var htmlData = ConvertHtml(htmlContent);
           //         var trimTextExtract = TakeLastLines(htmlData, 4);
           //        item.HtmlContent = trimTextExtract;
           //      //searchContent.HtmlContent=  HighlightKeyWords(trimTextExtract, searchText, "yellow", true);
           //       //  searchResultViewList.Add(searchContent);
           //     }
           // }
            return searchResultViewList;

        }
        public List<tblHtmlLibrary> GetAllFromHtmlLibrary()
        {
            var results=db.tblHtmlLibraries.ToList();

            return results;
        }


        //public List<SearchResultViewModel> SearchContent(List<tblHtmlLibrary> SearchContainer,string SearchContent)
        //{
           
        // //results = GetAllFromHtmlLibrary().Where(w=>w.docId==Guid.Parse("5B1B8F49-D23E-4E5A-8B57-1B997ACB7E71")).Select(s=>s.htmlContent).FirstOrDefault();

        //    var searchTestResults = SearchContainer.Where(w => w.htmlContent.ToString().Contains(SearchContent.Trim())).ToList();
        //    var searchResultList = new List<SearchResultViewModel>();

        //    foreach (var item in searchTestResults)
        //    {
        //        var docName=db.tblDocumentLibraries.Where(w => w.docId == item.docId).Select(s => s.docName).FirstOrDefault();
        //        docName=Path.GetFileNameWithoutExtension(docName);
        //        var searchResult = new SearchResultViewModel();
        //        searchResult.DocumentId =Guid.Parse(item.docId.ToString());
        //        searchResult.DocumentName = docName;
        //        searchResult.HtmlContent = item.htmlContent;
        //        searchResultList.Add(searchResult);

        //    }
        //    return searchResultList;


        //}


        public List<tblHtmlLibrary> SearchContentBasedOnMeeting(List<tblHtmlLibrary> SearchContainer,Guid meetingId)
        {
           


            var searchResult= SearchContainer.Where(w => w.tblDocumentLibrary.meetingId == meetingId).ToList();
            return searchResult;

        }


        public List<tblHtmlLibrary> SearchContentBasedOnAgendaItem(List<tblHtmlLibrary> SearchContainer,int agendaId)
        {
            

            var searchTestResults = SearchContainer.Where(w => w.tblDocumentLibrary.agendaItem==agendaId).ToList();
            return searchTestResults;
        }

        public List<tblHtmlLibrary> SearchContentBasedOnMeetingType(List<tblHtmlLibrary> SearchContainer, Guid meetingTypeId)
        {

            var v=SearchContainer.Select(s => new { s.tblDocumentLibrary,s.htmlContent}).ToList();
            var ct=v.Select(s => new { s.tblDocumentLibrary.tblMeeting, s.htmlContent }).ToList();
            var cts=ct.Where(w => w.tblMeeting.meetingType == meetingTypeId).Select(s =>  s.htmlContent );
            var searchTestResults = SearchContainer.Where(w => w.tblDocumentLibrary.tblMeeting.meetingType == meetingTypeId).ToList();
            return searchTestResults;
        }

        public string HighlightKeyWords(string text, string keywords, string cssClass, bool fullMatch)
        {
            if (text == String.Empty || keywords == String.Empty || cssClass == String.Empty)
                return text;
            var words = keywords.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (!fullMatch)
                return words.Select(word => word.Trim()).Aggregate(text,
                             (current, pattern) =>
                             Regex.Replace(current,
                                             pattern,
                                               string.Format("<span style=\"background-color:{0}\">{1}</span>",
                                               cssClass,
                                               "$0"),
                                               RegexOptions.IgnoreCase));
            return words.Select(word => "\\b" + word.Trim() + "\\b")
                        .Aggregate(text, (current, pattern) =>
                                   Regex.Replace(current,
                                   pattern,
                                     string.Format("<span style=\"background-color:{0}\">{1}</span>",
                                     cssClass,
                                     "$0"),
                                     RegexOptions.IgnoreCase));

        }
        public  string TakeLastLines(string text, int count)
        {
            List<string> lines = new List<string>();
            string htmlData = string.Empty;
            Match match = Regex.Match(text, "^.*$", RegexOptions.Multiline | RegexOptions.RightToLeft);

            while (match.Success && lines.Count < count)
            {
                lines.Insert(0, match.Value);
                match = match.NextMatch();
            }
            //lines.Reverse();
            foreach (var item in lines)
            {
                htmlData += item;
            }
            return htmlData;
        }


        private string ExtractTextFromHtml(string htmlContent)
        {
            HtmlAgilityPack.HtmlDocument  htmlDoc= new HtmlAgilityPack.HtmlDocument();
            htmlDoc.LoadHtml(htmlContent);
            string extractedText = string.Empty;
            var data = htmlDoc.DocumentNode.SelectNodes("//text()");
            foreach (var item in data)
            {
                extractedText += item.InnerText.Trim();
            }

            return extractedText;



        }



        public string ConvertHtml(string html)
        {

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);

            StringWriter sw = new StringWriter();
            ConvertTo(doc.DocumentNode, sw);
            sw.Flush();
            return sw.ToString();
        }

        public void ConvertTo(HtmlNode node, TextWriter outText)
        {
            string html;
            switch (node.NodeType)
            {
                case HtmlNodeType.Comment:
                    // don't output comments
                    break;

                case HtmlNodeType.Document:
                    ConvertContentTo(node, outText);
                    break;

                case HtmlNodeType.Text:
                    // script and style must not be output
                    string parentName = node.ParentNode.Name;
                    if ((parentName == "script") || (parentName == "style"))
                        break;

                    // get text
                    html = ((HtmlTextNode)node).Text;

                    // is it in fact a special closing node output as text?
                    if (HtmlNode.IsOverlappedClosingElement(html))
                        break;

                    // check the text is meaningful and not a bunch of whitespaces
                    if (html.Trim().Length > 0)
                    {
                        outText.Write(HtmlEntity.DeEntitize(html));
                    }
                    break;

                case HtmlNodeType.Element:
                    switch (node.Name)
                    {
                        case "p":
                            // treat paragraphs as crlf
                            outText.Write("\r\n");
                            break;
                    }

                    if (node.HasChildNodes)
                    {
                        ConvertContentTo(node, outText);
                    }
                    break;
            }
        }

        private void ConvertContentTo(HtmlNode node, TextWriter outText)
        {
            foreach (HtmlNode subnode in node.ChildNodes)
            {
                ConvertTo(subnode, outText);
            }
        }
    }
}