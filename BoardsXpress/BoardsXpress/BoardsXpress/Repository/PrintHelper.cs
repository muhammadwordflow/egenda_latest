﻿using System;
using System.IO;
using System.Drawing;
using Aspose.Words;
using Aspose.Words.Layout;
using Aspose.Words.Rendering;
using Aspose.Words.Saving;
using Aspose.Words.Drawing;
using System.Drawing.Imaging;
using Aspose.Words.Tables;
namespace BoardsXpress.Repository
{
    public class PrintHelper
    {
        private Document doc = new Document();
        public PrintHelper() { }
        public PrintHelper(Document document)
        {
            doc = document;
        }
        public void PrintDocument()
        {            
            // Obtain the settings of the default printer
            System.Drawing.Printing.PrinterSettings settings = new System.Drawing.Printing.PrinterSettings();

            // The standard print controller comes with no UI
            System.Drawing.Printing.PrintController standardPrintController = new System.Drawing.Printing.StandardPrintController();
            
            // Print the document using the custom print controller
            AsposeWordsPrintDocument prntDoc = new AsposeWordsPrintDocument(doc);
            prntDoc.PrinterSettings = settings;
            prntDoc.PrintController = standardPrintController;
            
            try
            {
                prntDoc.Print();
            }
            catch(Exception) { }
            
        }
        
    }
}