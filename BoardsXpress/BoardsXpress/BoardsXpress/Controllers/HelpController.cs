﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BoardsXpress.Controllers
{
    public class HelpController : Controller
    {
        // GET: Help
        public ActionResult Index()
        {
            return View("UserGuide");
        }
        
        public ActionResult ShowUserGuide(int pageNumber = 0)
        {
            return View(pageNumber == 0 ?
                "~/Views/Help/UserGuide.cshtml" : 
                string.Format("~/Views/Help/UserGuide-{0}.cshtml", pageNumber.ToString()));
        }
    }
}