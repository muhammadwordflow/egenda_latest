﻿using Aspose.Words.Saving;
using System;
using NReadability;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using BoardsXpress.Repository;
using Aspose.Words.Tables;

namespace BoardsXpress.Controllers
{
    public class ClsConverter
    {
        /// <summary>
        /// finds the cross ref witin doc
        /// and links it
        /// before converting it to html
        /// </summary>
        /// 
        public ClsConverter()
        {
            try
            {
                Aspose.Words.License docLicense = new Aspose.Words.License();
                docLicense.SetLicense("Aspose.Words.lic");
            }
            catch (Exception ex) { throw; }

            //Aspose.Pdf.License pdfLicense = new Aspose.Pdf.License();
            //pdfLicense.SetLicense("Aspose.Pdf.lic");
        }
        private static string MakeRelative(string filePath, string referencePath)
        {
            var fileUri = new Uri(filePath);
            var referenceUri = new Uri(referencePath);
            return referenceUri.MakeRelativeUri(fileUri).ToString();
        }
        /// <summary>
        /// converts word document into html
        /// </summary>
        /// <param name="path"></param>
        /// <returns>html as string</returns>
        public string convertDocument(string path)
        {
            string htmlFileContent = string.Empty;
            string filename = Path.GetFileName(path);
            string savepath = path;
            string dir = Path.GetDirectoryName(path);
            string pdfDir = dir;
            if (Path.GetExtension(filename) == ".pdf")
            {
                Aspose.Pdf.Document Pdfdoc = new Aspose.Pdf.Document(path);
                string wordFileName = Path.GetFileNameWithoutExtension(filename) + "doc";
                pdfDir += "/" + wordFileName;
                try
                {
                    GC.Collect();

                    Aspose.Pdf.DocSaveOptions op = new Aspose.Pdf.DocSaveOptions();
                    op.Mode = Aspose.Pdf.DocSaveOptions.RecognitionMode.Flow;
                    op.RelativeHorizontalProximity = 2.5F;
                    op.RecognizeBullets = true;
                    Pdfdoc.Save(pdfDir, op);

                    path = pdfDir;
                }
                catch (Exception ex) { }
                finally
                {
                    Pdfdoc.Dispose();
                }
            }

            Aspose.Words.Document doc = new Aspose.Words.Document(path);

            HtmlSaveOptions s = new HtmlSaveOptions();
            s.CssStyleSheetType = CssStyleSheetType.Inline;
            s.PrettyFormat = true;
            s.ExportHeadersFootersMode = ExportHeadersFootersMode.None;
            s.ExportListLabels = ExportListLabels.ByHtmlTags;

            foreach (var table in doc.GetChildNodes(Aspose.Words.NodeType.Table, true))
            {
                var currentTable = table as Table;

                currentTable.PreferredWidth = PreferredWidth.FromPercent(100);
            }

            string htmlFileName = Path.GetFileNameWithoutExtension(filename) + ".html";
            
            dir += "/" + htmlFileName;

            try
            {
                doc.Save(dir, s);
                htmlFileContent = replaceImageSrc(dir);
            }
            catch (Exception ex) { }
            
            return htmlFileContent;
        }
        /// <summary>
        /// fix image changes the image url to the desired one from the moved files
        /// </summary>
        /// <param name="htmlContent"></param>
        /// <returns></returns>
        /// 
        private string replaceImageSrc(string path)
        {
            //DirectoryInfo dirInfo = new DirectoryInfo(path);
            XDocument htmlDoc = new XDocument();
            //// XmlReaderSettings setting = new XmlReaderSettings();
            ////setting.DtdProcessing = DtdProcessing.Parse;

            //string htmlContent = string.Empty;

            //Uri baseUri = new Uri(dirInfo.Parent.FullName.ToString());
            using (var stream = System.IO.File.Open(path, FileMode.Open, FileAccess.ReadWrite))
            using (var reader = XmlReader.Create(stream))
            {
                htmlDoc = XDocument.Load(reader, System.Xml.Linq.LoadOptions.PreserveWhitespace);
            }

            var images = htmlDoc.GetElementsByTagName("img").ToArray();

            foreach (XElement image in images)
            {
                string existingSrc = image.Attribute("src").Value;
                image.Attribute("style").Remove();
                var imagePath = Path.GetDirectoryName(path);
                var a = AppDomain.CurrentDomain.BaseDirectory;
                imagePath = MakeRelative(imagePath, a);
                imagePath = Path.Combine("/", imagePath, existingSrc);
                try
                {
                    image.Attribute("src").Value = imagePath;
                }
                catch (Exception ex) { }
            }
            var headin1s = htmlDoc.GetElementsByTagName("h1").ToArray();
            var headin2s = htmlDoc.GetElementsByTagName("h2").ToArray();
            int id = 0;
            foreach (var item in headin1s)
            {
                item.SetId(id.ToString());
                id++;
            }

            foreach (var item in headin2s)
            {
                item.SetId(id.ToString());
                id++;
            }

            return htmlDoc.ToString();


        }
    }
}