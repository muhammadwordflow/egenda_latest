﻿
using BoardsXpress.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BoardsXpress.Controllers
{
    [Authorize]
    public class AnnotationManagementController : BaseController
    {
        // GET: AnnotationManagement
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            return View();
        }


        //public dynamic GetAnnotation()
        //{
        //    var docId = Request.Form["docId"];
        //    try
        //    {
        //        var result = db.tblAnnotations.ToList();
        //        string annotationData = "{\"rows\":[";

        //        if (result.Count()>0)
        //        {
        //            foreach (var data in result)
        //            {
        //                annotationData += data.annotationData + ",";
        //            }
        //        }

        //        annotationData += "]}";

        //        var opData = JsonConvert.DeserializeObject(annotationData);
        //        return opData;
        //    }
        //    catch (Exception ex) { }
        //    return null;

        //}
        //[HttpPost]
        //public JsonResult CreateAnnotation(object JSON)
        //{

        //    Request.InputStream.Seek(0, SeekOrigin.Begin);
        //    string jsonData = new StreamReader(Request.InputStream).ReadToEnd();
        //    var resultantValue = jsonData.Remove(jsonData.Length - 1, 1);
        //    var id = Guid.NewGuid();
        //    resultantValue = resultantValue + ",\"id\":\"" + id + "\"}";
        //    if (SaveAnnotation(resultantValue))
        //    {
        //        var results = new { Success = "True", id = id };
        //        return Json(results, JsonRequestBehavior.AllowGet);
        //    }
        //    var result = new { Success = "False" };
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
    
        //public JsonResult updateAnnotation(object JSON)
        //{
        //    Request.InputStream.Seek(0, SeekOrigin.Begin);
        //    string jsonData = new StreamReader(Request.InputStream).ReadToEnd();
        //    if (SaveAnnotation(jsonData, true))
        //    {
        //        var results = new { Success = "True" };
        //        return Json(results, JsonRequestBehavior.AllowGet);
        //    }
        //    var result = new { Success = "False" };
        //    return Json(result, JsonRequestBehavior.AllowGet);

        //}

        //public JsonResult destroyAnnotation(object JSON)
        //{
            
        //    Request.InputStream.Seek(0, SeekOrigin.Begin);
        //    string jsonData = new StreamReader(Request.InputStream).ReadToEnd();
        //    var annotJson = JObject.Parse(jsonData);
        //    var savedAnnotId = (Guid)annotJson.SelectToken("id");
        //    try
        //    {
        //       var annotationData= db.tblAnnotations.Where(w => w.annotationId == savedAnnotId).FirstOrDefault();
        //        db.tblAnnotations.Remove(annotationData);
        //        var result = db.SaveChanges();
        //        if (result == 1)
        //        {
        //            var outcome = new { Success = "True" };
        //            return Json(outcome, JsonRequestBehavior.AllowGet);
        //        }
        //        else
        //        {
        //            var outcome = new { Success = "False" };
        //            return Json(outcome, JsonRequestBehavior.AllowGet);

        //        }
        //    }
        //    catch (Exception ex) { }
        //    var results = new { Success = "False" };
        //    return Json(results, JsonRequestBehavior.AllowGet);
        //}

        //private bool SaveAnnotation(string annotationData, bool isupdate = false)
        //{

           
        //    var user= User.Identity.GetUserId();
        //    var userId = user;
        //    var annotJson = JObject.Parse(annotationData);
        //    var savedAnnotId = (Guid)annotJson.SelectToken("id");
        //    //  AIST.Repo.ProjectSettings _ssp = new Repo.ProjectSettings();
        //    var annotationInfo = new tblAnnotation();
        //    annotationInfo.annotationId = Guid.NewGuid();
        //    annotationInfo.userId = userId;
        //    annotationInfo.annotationData = annotationData;
        //    annotationInfo.createdDate = DateTime.Now;
        //    ///note meetingId needs to be passed from view
        //    annotationInfo.meetingId =Guid.Parse("8016c974-5188-4261-846e-7a10df0f90e1");

        //    //try
        //    //{
        //    //    db.Entry(db.tblAnnotations).State = EntityState.Modified;
        //    //   // db.tblAnnotations.Add(annotationInfo);
        //    //    var result = db.SaveChanges();
        //    //    if (result == 1)
        //    //    {
        //    //        return true;


        //    //    }
        //    //    else
        //    //    { return false;
        //    //    }
        //    //}

        //    //catch (Exception ex)
        //    //{

        //    //    throw;
        //    //}

        //    try
        //    {
        //        if (isupdate)
        //        {
        //            var changes = db.tblAnnotations.Where(s => s.annotationId == annotationInfo.annotationId).FirstOrDefault();
        //            db.tblAnnotations.Add(changes);
        //            db.SaveChanges();
        //        }
        //        else
        //        {
        //            db.tblAnnotations.Add(annotationInfo);
        //            db.SaveChanges();
        //        }

        //        return true;
        //    }
        //    catch (Exception ex) { }
        //    return false;

        //}
        
        [HttpPost]
        public JsonResult annotations(Annotator annotator)
        {
            var document = db.tblDocumentLibraries.First(m => m.docId == annotator.docId);

            annotator.id = Guid.NewGuid();
            annotator.created = DateTime.Today;

            var annotation = new tblAnnotation();

            AutoMapper.Mapper.Map(annotator, annotation);

            annotation.meetingId = document.meetingId;
            annotation.agendaItemId = document.agendaItem;
            annotation.userId = User.Identity.GetUserId();

            AutoMapper.Mapper.Map(annotator.ranges, annotation.tblAnnotationsRanges);
            
            foreach (var annotationRange in annotation.tblAnnotationsRanges)
            {
                annotationRange.annotationId = annotation.annotationId;
                annotationRange.annotationRangeId = Guid.NewGuid();
            }

            db.tblAnnotations.Add(annotation);

            var result = db.SaveChanges();

            AutoMapper.Mapper.Map(annotation, annotator);
            AutoMapper.Mapper.Map(annotation.tblAnnotationsRanges, annotator.ranges);

            return Json(annotator);
        }

        [HttpPut]
        public JsonResult annotations(Annotator annotator, Guid id)
        {
            var annotation = db.tblAnnotations
                .Include(m => m.tblAnnotationsRanges)
                .First(m => m.annotationId == annotator.id);

            AutoMapper.Mapper.Map(annotator, annotation);
            AutoMapper.Mapper.Map(annotator.ranges, annotation.tblAnnotationsRanges);

            db.Entry(annotation).State = EntityState.Modified;

            var result = db.SaveChanges();

            return Json(null);
        }

        [HttpDelete]
        public JsonResult annotations(Guid id)
        {
            var annotation = db.tblAnnotations
                .Include(m => m.tblAnnotationsRanges)
                .Include(m => m.tblAnnotationsTags)
                .First(m => m.annotationId == id);

            db.tblAnnotations.Remove(annotation);

            var result = db.SaveChanges();

            return Json(null);
        }

        [HttpGet]
        public JsonResult search(Annotator annotator)
        {
            var userId = User.Identity.GetUserId();
            var annotation = db.tblAnnotations
                .Include(m => m.tblAnnotationsRanges)
                .Where(m => m.userId == userId && m.docId == annotator.docId);

            var annotationList = new List<Annotator>();
            
            foreach (var item in annotation)
            {
                var annotationFrontEnd = new Annotator
                {
                    uri = annotator.uri,
                    ranges = new List<AnnotationRange>()
                };

                AutoMapper.Mapper.Map(item, annotationFrontEnd);
                AutoMapper.Mapper.Map(item.tblAnnotationsRanges, annotationFrontEnd.ranges);

                annotationList.Add(annotationFrontEnd);
            }
            
            var annotationSearch = new AnnotationSearch
            {
                rows = annotationList,
                total = annotationList.Count()
            };

            return Json(annotationSearch, JsonRequestBehavior.AllowGet);
        }
    }
}