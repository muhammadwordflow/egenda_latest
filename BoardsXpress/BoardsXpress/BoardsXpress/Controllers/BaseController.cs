﻿using log4net;
using System.Reflection;
using System.Web.Mvc;

namespace BoardsXpress.Controllers
{
    public class BaseController : Controller
    {
        protected static readonly ILog log = LogManager.GetLogger("Egenda");
        protected BoardExpressEntities db;

        public BaseController()
        {
            db = new BoardExpressEntities();
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}