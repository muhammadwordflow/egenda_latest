﻿using BoardsXpress.Models;
using BoardsXpress.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace BoardsXpress.Controllers
{
    [Authorize]

    public class RefDocsController : Controller
    {
        private BoardExpressEntities db = new BoardExpressEntities();
        HelperRepo repo = new HelperRepo();
        // GET: RefDocs
        public ActionResult Index(Guid id)
        {
            var tblRefDocs = db.tblRefDocs.Include(t => t.tblMeetingType).Where(t=>t.meetingTypeId == id);
            repo.meetingIdGlobal = id;
            ViewBag.meetingTypeId = id;
            return View(tblRefDocs.ToList());
        }


        #region SystemGeneratedCodes
        // GET: RefDocs/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblRefDoc tblRefDoc = db.tblRefDocs.Find(id);
            if (tblRefDoc == null)
            {
                return HttpNotFound();
            }
            ViewBag.meetingTypeId = new SelectList(db.tblMeetingTypes, "meetingTypeId", "meetingType", tblRefDoc.meetingTypeId);
            return View(tblRefDoc);
        }

        // POST: RefDocs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "refDocId,meetingTypeId,filePath,fileName,refDocName,htmlContent,publish,webLink")] tblRefDoc tblRefDoc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblRefDoc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.meetingTypeId = new SelectList(db.tblMeetingTypes, "meetingTypeId", "meetingType", tblRefDoc.meetingTypeId);
            return View(tblRefDoc);
        }

        // GET: RefDocs/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblRefDoc tblRefDoc = db.tblRefDocs.Find(id);
            if (tblRefDoc == null)
            {
                return HttpNotFound();
            }
            return View(tblRefDoc);
        }

        // POST: RefDocs/Delete/5
        [HttpPost, ActionName("Delete")]
        public JsonResult DeleteConfirmed(Guid id)
        {

            try
            {
                tblRefDoc tblRefDoc = db.tblRefDocs.Find(id);
                db.tblRefDocs.Remove(tblRefDoc);
                db.SaveChanges();
                var result = new { Success = "True", Message = "Success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex) { }

            var results = new { Success = "False", Message = "Failure" };
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion

        [HttpPost]
        public JsonResult uploadRefDocument()
        {
            try
            {
                var meetingType = repo.meetingIdGlobal;
                if (Request.Files.Count > 0)
                {
                    var file = Request.Files[0];
                    
                    if (file != null && file.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file.FileName);
                        var path = Path.Combine(Server.MapPath("~/Documents/referenceDocument"), fileName);
                        if (!Directory.Exists(Server.MapPath("~/Documents/referenceDocument")))
                        {
                            Directory.CreateDirectory(Server.MapPath("~/Documents/referenceDocument"));
                        }
                        file.SaveAs(path);
                        var meetingTypeID = Request.Form["meetingTypeId"];
                        meetingType = Guid.Parse(meetingTypeID);
                        var refDocType = Request.Form["pegionHoleName"];
                        ClsConverter oClsConverter = new ClsConverter();
                        var HtmlContent = oClsConverter.convertDocument(path);
                        if (insertIntoDatabase(fileName, path, refDocType, meetingType, null, HtmlContent))
                        {
                            var results = new { Title = "Message", Message = "Reference Document has been created successfully." };
                            return Json(results, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        var refDocType = Request.Form["LinkName"];
                        var linkTitle = Request.Form["weblinkTitle"];
                        var meetingTypeID = Request.Form["meetingTypeId"];
                        meetingType = Guid.Parse(meetingTypeID);
                        if (insertIntoDatabase(linkTitle, null, refDocType, meetingType, linkTitle, null))
                        {
                            var results = new { Title = "Message", Message = "Reference Weblink has been created successfully." };
                            return Json(results, JsonRequestBehavior.AllowGet);
                        }
                       
                    }
                }
                else
                {
                    var docType = Request.Form["LinkName"];
                    var meetingTypeId = Request.Form["meetingTypeId"];
                    meetingType = Guid.Parse(meetingTypeId);
                    var linkTitle = Request.Form["weblinkTitle"];
                    if (insertIntoDatabase(linkTitle, null, docType, meetingType, linkTitle, null))
                    {
                        var results = new { Title = "Message", Message = "Reference document uploaded successfully." };
                        return Json(results, JsonRequestBehavior.AllowGet);
                    }
                }
               
            }
            catch (Exception ex)
            {
                var resultant = new { Title = "Message", Message = "Reference document could not be uploaded." };
                return Json(resultant, JsonRequestBehavior.AllowGet);
            }
            var result = new { Title = "Message", Message = "Reference document could not be uploaded." };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public bool insertIntoDatabase(string fileName, string filePath, string refDocType,Guid meetingType,string webLink = null,string htmlContent = null)
        {
            Guid refDocId = Guid.NewGuid();
            tblRefDoc oRefDocModel = new tblRefDoc()
            {
                refDocId = refDocId,
                fileName = fileName,
                filePath = Path.Combine("/Documents/referenceDocument",fileName),
                refDocName = refDocType,
                publish = false,
                webLink = webLink,
                meetingTypeId = meetingType,
                htmlContent = htmlContent
            };
            try
            {
                db.tblRefDocs.Add(oRefDocModel);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex) { }
            return false;
        }
        public JsonResult publishRefDocs(Guid id)
        {
            tblRefDoc tblRefDoc = db.tblRefDocs.Find(id);
            if (tblRefDoc != null)
            {
                try
                {
                    tblRefDoc.publish = true;
                    db.tblRefDocs.Add(tblRefDoc);
                    db.SaveChanges();
                    var result = new { Success = "True", Message = "Success" };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                catch(Exception ex) { throw; }
            }

            var results = new { Success = "False", Message = "Failure" };
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult getRefNavItem()
        {
            List<RefDocViewModel> RefDocItems = new List<RefDocViewModel>();
            try
            {
                var result = db.tblRefDocs.ToList().OrderBy(x => x.meetingTypeId);
                var meetingTypeDetails = db.tblMeetingTypes;
                if (result != null)
                {
                    foreach (var item in result)
                    {
                        RefDocViewModel refDoc = new RefDocViewModel()
                        {
                            refDocId = item.refDocId,
                            refDocName = item.refDocName,
                            meetingType = meetingTypeDetails.Where(x => x.meetingTypeId == item.meetingTypeId).Select(x => x.meetingType).FirstOrDefault().ToString()
                        };
                        RefDocItems.Add(refDoc);
                    }
                }
            }
            catch (Exception ex) { }
            return View("ReferenceDocuments",RefDocItems);

        }
        [HttpPost]
        public ActionResult loadHtmlData(Guid refDocId)
        {
            try
            {
                var htmlData = db.tblRefDocs.Where(x => x.refDocId == refDocId).FirstOrDefault();
                string msg = string.Empty;
                if (!string.IsNullOrEmpty(htmlData.htmlContent))
                    msg = htmlData.htmlContent;
                else
                    msg = htmlData.webLink;

                var result = new { Success = "True", Data = new { isHtml = !string.IsNullOrEmpty(htmlData.htmlContent), msg = msg } };
                return Json(result, JsonRequestBehavior.AllowGet);


            }
            catch(Exception ex) { throw; }



            var results = new { Success = "False", Data = "Could not load data." };
            return Json(results, JsonRequestBehavior.AllowGet);
        }
    }
}
