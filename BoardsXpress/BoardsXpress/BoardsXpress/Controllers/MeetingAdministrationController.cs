﻿
using BoardsXpress.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace BoardsXpress.Controllers
{
    [Authorize]
    public class MeetingAdministrationController : BaseController
    {
        public ActionResult Index(Guid id)
        {
            var meetingTypeViewModel = new MeetingTypeViewModel();

            var meetingType = db.tblMeetingTypes
                .First(y => y.meetingTypeId == id);
            
            AutoMapper.Mapper.Map(meetingType, meetingTypeViewModel);
            AutoMapper.Mapper.Map(meetingType.tblMeetings, meetingTypeViewModel.MeetingsList);           
            return View(meetingTypeViewModel);
        }
    }
}