﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BoardsXpress.Controllers
{
    [Authorize]
    public class DisplayHtmlController : Controller
    {
        private BoardExpressEntities db = new BoardExpressEntities();
        // GET: DisplayHtml
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult getHtmlData(Guid docId)
        {
            // Get the HTML from the DocumentsLibrary table
            var docLib = db.tblDocumentLibraries.First(m => m.docId == docId);

            var returnString = "";

            switch (Path.GetExtension(docLib.docPath))
            {
                case ".jpg":
                case ".png":
                case ".jpeg":
                case ".bmp":
                case ".gif":

                    returnString = "<img width='100%' src='" + docLib.docPath + "' alt='" + docLib.docName + "' />";

                    break;

                case ".pdf":

                    returnString = "<a href ='" + docLib.docPath + "' target='_blank'>Click on this link to view the PDF attachment.</a>";

                    break;

                case ".xls":
                case ".xlsx":

                    returnString = "<a href ='" + docLib.docPath + "' target='_blank'>Click on this link to view the Excel spreadsheet.</a>";

                    break;

                case ".ppt":
                case ".pptx":

                    returnString = "<a href ='" + docLib.docPath + "' target='_blank'>Click on this link to view the Power Point Presentation attachment.</a>";

                    break;

                default:

                    returnString = docLib.htmlContent;

                    break;
            }
            
            var jsonResult = Json(new { htmlContent = returnString, dataType = Path.GetExtension(docLib.docPath), meetingId = docLib.meetingId }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
    }
}