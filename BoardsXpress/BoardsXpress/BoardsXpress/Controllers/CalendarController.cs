﻿using BoardsXpress.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BoardsXpress.Email;
using Microsoft.Exchange.WebServices.Data;
using WebGrease.Css.Extensions;
using CalendarView = BoardsXpress.Models.CalendarView;

namespace BoardsXpress.Controllers
{
    [Authorize]
    public class CalendarController : Controller
    {
        private BoardExpressEntities db = new BoardExpressEntities();

        // GET: Calendar

        public ActionResult Index()
        {
            return View();
        }

        #region Ajax Call

        [HttpGet]
        public ActionResult GetUserCalenderList(int? page)
        {
            CalendarView calendarViewList = new CalendarView();
            calendarViewList.MeetingTypeList = new List<MeetingTypeViewModel>();
            List<MeetingTypeViewModel> holdMeetingTypeesValue = new List<MeetingTypeViewModel>();
            if (User.IsInRole("Super Admin"))
            {

                db.tblMeetingTypes.ForEach(item =>
                {
                    MeetingTypeViewModel meetingType = new MeetingTypeViewModel
                    {
                        meetingType = item.meetingType,
                        createdDate = item.createdDate,
                        meetingTypeId = item.meetingTypeId,
                        meetingAbbreviation = item.meetingAbbreviation,
                        useAbbreviation = item.useAbbreviation,
                        ListMeetings = new List<MeetingViewModels>()
                    };
                    var list = item.tblMeetings.OrderBy(x => x.meetingDate).ToList();
                    list.ForEach(meeting =>
                    {
                        meetingType.ListMeetings.Add(new MeetingViewModels
                        {
                            meetingId = meeting.meetingId,
                            meetingType = meeting.meetingType.ToString(),
                            meetingDate = meeting.meetingDate ?? DateTime.Now,
                            startTime = meeting.startTime,
                            finishTime = meeting.finishTime,
                            meetingVenue = meeting.meetingVenue,
                            meetingNotes = meeting.meetingNotes
                        });
                    });
                    holdMeetingTypeesValue.Add(meetingType);

                });
            }
            else
            {
                var currentUser = User.Identity.GetUserId();

                var meetingTypeList = (from mt in db.tblMeetingTypes
                                       join ur in db.AspNetUserRoles on mt.meetingTypeId equals ur.MeetingTypeId
                                       where ur.UserId == currentUser
                                       select mt).ToList();

                meetingTypeList.ForEach(mItem =>
                {
                    MeetingTypeViewModel typeViewModel = new MeetingTypeViewModel
                    {
                        meetingType = mItem.meetingType,
                        createdDate = mItem.createdDate,
                        meetingTypeId = mItem.meetingTypeId,
                        meetingAbbreviation = mItem.meetingAbbreviation,
                        useAbbreviation = mItem.useAbbreviation,
                        ListMeetings = new List<MeetingViewModels>()
                    };
                    mItem.tblMeetings.Where(m => m.meetingType == mItem.meetingTypeId).ForEach(meetings =>
                    {
                        typeViewModel.ListMeetings.Add(new MeetingViewModels
                        {
                            meetingId = meetings.meetingId,
                            meetingType = meetings.meetingType.ToString(),
                            meetingDate = meetings.meetingDate ?? DateTime.Now,
                            startTime = meetings.startTime,
                            finishTime = meetings.finishTime,
                            meetingVenue = meetings.meetingVenue,
                            meetingNotes = meetings.meetingNotes

                        });
                    });
                    holdMeetingTypeesValue.Add(typeViewModel);
                });
            }

            calendarViewList.Pager = new Pager(holdMeetingTypeesValue.Count(), page);
            calendarViewList.MeetingTypeList = holdMeetingTypeesValue.Skip((calendarViewList.Pager.CurrentPage - 1) * calendarViewList.Pager.PageSize).Take(calendarViewList.Pager.PageSize).ToList();

            return Json(calendarViewList, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult GetMeeingMeetingTypeId(Guid meetingId)
        {
            MeetingAttendeesView meeting = new MeetingAttendeesView();
            meeting.ListMeetings = new List<MeetingAttendeesViewModel>();
            try
            {
                var meetings = (from u in db.AspNetUsers
                                join mt in db.tblMeetingAttendees on u.Id equals mt.UserId
                                where mt.meetingId == meetingId
                                select new
                                {
                                    u.FirstName,
                                    u.Id,
                                    mt.userResponse,
                                    mt.ExchangeInvitationId
                                }).ToList();

                meetings.ForEach(item =>
                {
                    ServicePointManager.ServerCertificateValidationCallback = SendInvitationAndEmail.CertificateValidationCallBack;
                    ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2013);
                    service.Credentials = new WebCredentials(SendInvitationAndEmail.UserName, SendInvitationAndEmail.Password);

                    service.TraceEnabled = true;
                    service.TraceFlags = TraceFlags.All;

                    checkUserResponse(service, item.ExchangeInvitationId, meetingId);
                    MeetingAttendeesViewModel meetingViewModels = new MeetingAttendeesViewModel
                    {
                        UserId = item.Id,
                        UserResponse = item.userResponse,
                        FirstName = item.FirstName
                    };

                    meeting.ListMeetings.Add(meetingViewModels);
                });

            }
            catch (Exception ex)
            {
                meeting.ErrorCode = ex.HResult;
                meeting.ErrorMessage = ex.Message;
            }

            return Json(meeting, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region Private Method
        private void checkUserResponse(ExchangeService service, ItemId newId, Guid meetingId)
        {
            service.AutodiscoverUrl(SendInvitationAndEmail.UserName, SendInvitationAndEmail.RedirectionUrlValidationCallback);

            // Create the appointment.            
            Appointment appointment = Appointment.Bind(service, newId);
            foreach (Attendee t in appointment.RequiredAttendees)
            {
                if (t.ResponseType != null)
                    //Console.WriteLine("Required attendee - " + t.Address + ": " + t.ResponseType.Value.ToString());
                    if (t.ResponseType.Value.ToString().Equals("Accept"))
                    {
                        UpdateStatusOfResponseMeeting(meetingId, true);
                    }
                    else if (t.ResponseType.Value.ToString().Equals("Decline"))
                    {
                        UpdateStatusOfResponseMeeting(meetingId, false);
                    }
                    else
                    {
                        UpdateStatusOfResponseMeeting(meetingId, null);
                    }

            }
        }


        private void UpdateStatusOfResponseMeeting(Guid meetingId, bool? resposne)
        {
            var meetingAttendess = db.tblMeetingAttendees.FirstOrDefault(m => m.meetingId == meetingId);
            if (meetingAttendess != null)
            {
                meetingAttendess.userResponse = resposne;

                db.Entry(meetingAttendess).State = EntityState.Modified;
            }
            db.SaveChanges();
        }
        #endregion
    }
}