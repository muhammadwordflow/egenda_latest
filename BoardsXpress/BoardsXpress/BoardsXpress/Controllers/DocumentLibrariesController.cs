﻿using BoardsXpress.Models;
using BoardsXpress.Util;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace BoardsXpress.Controllers
{
    [Authorize]
    public class DocumentLibrariesController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblDocumentLibrary tblDocumentLibrary = db.tblDocumentLibraries.Find(id);
            if (tblDocumentLibrary == null)
            {
                return HttpNotFound();
            }
            return View(tblDocumentLibrary);
        }

        public ActionResult Create()
        {
            ViewBag.agendaItem = new SelectList(db.tblAgendaItems, "Id", "agendaItem");
            ViewBag.meetingId = new SelectList(db.tblMeetings, "meetingId", "meetingVenue");
            ViewBag.docAuthor = new SelectList(db.tblUserProfiles, "userId", "UserFullName");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "docId,docAuthor,docName,docCreatedDate,docPath,meetingId,agendaItem")] tblDocumentLibrary tblDocumentLibrary)
        {
            if (ModelState.IsValid)
            {
                tblDocumentLibrary.docId = Guid.NewGuid();
                db.tblDocumentLibraries.Add(tblDocumentLibrary);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.agendaItem = new SelectList(db.tblAgendaItems, "Id", "agendaItem", tblDocumentLibrary.agendaItem);
            ViewBag.meetingId = new SelectList(db.tblMeetings, "meetingId", "meetingVenue", tblDocumentLibrary.meetingId);
            ViewBag.docAuthor = new SelectList(db.tblUserProfiles, "userId", "UserFullName", tblDocumentLibrary.docAuthor);
            return View(tblDocumentLibrary);
        }

        public ActionResult Edit(Guid? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var tblDocumentLibrary = db.tblDocumentLibraries.Find(id);

            if (tblDocumentLibrary == null)
                return HttpNotFound();

            ViewBag.agendaItem = new SelectList(db.tblAgendaItems, "Id", "agendaItem", tblDocumentLibrary.agendaItem);
            ViewBag.meetingId = new SelectList(db.tblMeetings, "meetingId", "meetingVenue", tblDocumentLibrary.meetingId);
            ViewBag.docAuthor = new SelectList(db.tblUserProfiles, "userId", "UserFullName", tblDocumentLibrary.docAuthor);

            return View(tblDocumentLibrary);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "docId,docAuthor,docName,docCreatedDate,docPath,meetingId,agendaItem")] tblDocumentLibrary tblDocumentLibrary)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblDocumentLibrary).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.agendaItem = new SelectList(db.tblAgendaItems, "Id", "agendaItem", tblDocumentLibrary.agendaItem);
            ViewBag.meetingId = new SelectList(db.tblMeetings, "meetingId", "meetingVenue", tblDocumentLibrary.meetingId);
            ViewBag.docAuthor = new SelectList(db.tblUserProfiles, "userId", "UserFullName", tblDocumentLibrary.docAuthor);

            return View(tblDocumentLibrary);
        }

        public ActionResult Delete(Guid? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var tblDocumentLibrary = db.tblDocumentLibraries.Find(id);

            if (tblDocumentLibrary == null)
                return HttpNotFound();

            return View(tblDocumentLibrary);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            var tblDocumentLibrary = db.tblDocumentLibraries.Find(id);

            db.tblDocumentLibraries.Remove(tblDocumentLibrary);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult DeleteMyDoc(Guid Id)
        {
            try
            {
                //check if there is a record in document library
                tblDocumentLibrary tblDocumentLibrary = db.tblDocumentLibraries.Find(Id);

                //check if this has been published, abort otherwise
                var checkPublish = db.tblPublishLibraries.Where(x => x.docId == Id).Count();

                if (checkPublish > 0)
                {
                    var result = new { Success = "false", Message = "Can not delete published items." };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

                db.tblDocumentLibraries.Remove(tblDocumentLibrary);
                db.SaveChanges();

                return Json(new { Success = "true", Message = "Deleted" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Success = "false", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CheckIfDocumentExists(int agendaItem, int? agendaItemRelatedId)
        {
            var jsonResponse = new JsonResponse();

            if (agendaItemRelatedId != null && agendaItemRelatedId.Value > 0)
            {
                if (db.tblDocumentLibraries.Any(x => x.agendaItem == agendaItem && x.agendaItemRelatedId == agendaItemRelatedId))
                {
                    jsonResponse.Success = true;
                    jsonResponse.Title = "Failure";
                    jsonResponse.Message = "Document already uploaded for this related agenda item.";
                    //AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
                    //{
                    //    UserName = User.Identity.GetUserName(),
                    //    UserId = new Guid(User.Identity.GetUserId()),
                    //    AfterData = JsonConvert.SerializeObject(jsonResponse, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }),
                    //    BeforeData = JsonConvert.SerializeObject(null),
                    //    IpAddress = Request.UserHostAddress,
                    //    SessionId = HttpContext.Session.SessionID,
                    //    ActionPerform = "GET",
                    //    UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
                    //};
                    //auditTrailViewModel.AuditTrail();
                    return Json(jsonResponse, JsonRequestBehavior.AllowGet);
                }
            }
            else if (db.tblDocumentLibraries.Any(x => x.agendaItem == agendaItem && x.agendaItemRelatedId == null))
            {
                jsonResponse.Success = true;
                jsonResponse.Title = "Failure";
                jsonResponse.Message = "Document already uploaded for this agenda item.";
                //AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
                //{
                //    UserName = User.Identity.GetUserName(),
                //    UserId = new Guid(User.Identity.GetUserId()),
                //    AfterData = JsonConvert.SerializeObject(jsonResponse, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }),
                //    BeforeData = JsonConvert.SerializeObject(null),
                //    IpAddress = Request.UserHostAddress,
                //    SessionId = HttpContext.Session.SessionID,
                //    ActionPerform = "GET",
                //    UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
                //};
                //auditTrailViewModel.AuditTrail();
                return Json(jsonResponse, JsonRequestBehavior.AllowGet);
            }

            jsonResponse.Success = false;

            return Json(jsonResponse, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UploadFiletoDatabase()
        {
            var jsonResponse = new JsonResponse();

            try
            {
                if (Request.Files.Count > 0)
                {
                    var file = Request.Files[0];

                    if (file != null && file.ContentLength > 0)
                    {
                        int? agendaItemRelatedId = null;

                        if (Request.Form["agendaItemRelated"] != null && Request.Form["agendaItemRelated"] != "0")
                            agendaItemRelatedId = Convert.ToInt32(Request.Form["agendaItemRelated"]);

                        var fileName = Path.GetFileName(file.FileName);
                        var meetingTypeId = Guid.Parse(Request.Form["mtypes"].ToString());
                        var meetingId = Guid.Parse(Request.Form["mdates"].ToString());
                        var meetingAgendaItem = Request.Form["magendaItem"].ToString();
                        var existingAgendaItem = Convert.ToInt32(meetingAgendaItem);

                        DirectoryInfo info = new DirectoryInfo(Server.MapPath("~/Documents/") + "/" + meetingTypeId.ToString());
                        CreateDirectory(info);

                        file.SaveAs(info.FullName + "//" + fileName);

                        ClsConverter oClsConvert = new ClsConverter();                       
                        tblDocumentLibrary oDocLib = new tblDocumentLibrary
                        {
                            docId = Guid.NewGuid(),
                            docName = fileName,
                            docCreatedDate = DateTime.Now,
                            docPath = Path.Combine("/Documents/", meetingTypeId.ToString(), fileName),
                            meetingId = meetingId,
                            userId = User.Identity.GetUserId(),
                            agendaItem = existingAgendaItem,
                            printRestriction = Request.Form["printRestriction"] != null && Request.Form["printRestriction"] == "on" ? true : false,
                            htmlContent = (Path.GetExtension(fileName).ToLower() != ".doc" && Path.GetExtension(fileName).ToLower() != ".docx") ?
                                "" :
                                oClsConvert.convertDocument(Path.Combine(info.FullName, fileName))
                        };

                        int maxValueForOrderIn = 0;
                        var DocLibDataForOrderIn = db.tblDocumentLibraries.Where(x => x.meetingId == meetingId);

                        if (DocLibDataForOrderIn.Count() != 0)
                            maxValueForOrderIn = DocLibDataForOrderIn.Max(x => x.orderIn);

                        oDocLib.orderIn = maxValueForOrderIn + 1;

                        if (agendaItemRelatedId.HasValue)
                            oDocLib.agendaItemRelatedId = agendaItemRelatedId.Value;

                        db.tblDocumentLibraries.Add(oDocLib);

                        if (db.SaveChanges() > 0)
                        {
                            Response.StatusCode = (int)HttpStatusCode.OK;
                            jsonResponse.Success = true;
                            jsonResponse.Title = "Success";
                            AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
                            {
                                UserName = User.Identity.GetUserName(),
                                UserId = new Guid(User.Identity.GetUserId()),
                                AfterData = oDocLib.docName,//JsonConvert.SerializeObject(oDocLib.docName, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }),
                                BeforeData = null,//JsonConvert.SerializeObject(null),
                                IpAddress = Request.UserHostAddress,
                                MeetingTypeId = meetingTypeId,
                                MeetingsId = meetingId,
                                SessionId = HttpContext.Session.SessionID,
                                ActionPerform = "Upload",
                                TimeAccessed = DateTime.Now,
                                UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
                            };
                            auditTrailViewModel.AuditTrail();


                            if (Path.GetExtension(fileName).ToLower() == ".pdf")
                            {
                                var myPath = Path.Combine("/Documents/", meetingTypeId.ToString(), fileName);
                                //jsonResponse.Message = "The file has been uploaded successfully.\r\n\r\nWould you like to display the document? <a href='"+myPath+"' target ='_blank'>Click Here</a>";
                                jsonResponse.Message = "The file has been uploaded successfully.";

                            }
                            else
                            {
                                jsonResponse.Message = "The file has been uploaded successfully.";
                            }
                            return Json(jsonResponse, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    jsonResponse.Success = false;
                    jsonResponse.Title = "Alert";
                    jsonResponse.Message = "Please select a file to be sent.";

                    return Json(jsonResponse, JsonRequestBehavior.AllowGet);
                }

                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                jsonResponse.Success = false;
                jsonResponse.Title = "Failure";
                jsonResponse.Message = "Something wrong happened while uploading the file. Please try again!";

                return Json(jsonResponse, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                jsonResponse.Success = false;
                jsonResponse.Title = "Failure";
                jsonResponse.Message = ex.Message;

                return Json(jsonResponse, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetAgendaItemNameByDocId(Guid docId)
        {
            var jsonResponse = new JsonResponse();

            try
            {
                var document = db.tblDocumentLibraries.Include(m => m.tblAgendaItem).Include(m => m.tblAgendaItemsRelated).FirstOrDefault(m => m.docId == docId);

                if (document != null)
                {
                    Response.StatusCode = (int)HttpStatusCode.OK;
                    jsonResponse.Success = true;
                    jsonResponse.Title = "Success";

                    var itemName = document.tblAgendaItemsRelated != null ?
                        string.Format("{0}", document.tblAgendaItemsRelated.name) :
                        string.Format("{0}", document.tblAgendaItem.agendaItem);

                    jsonResponse.Data = new { itemName = itemName };

                    return Json(jsonResponse, JsonRequestBehavior.AllowGet);
                }

                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                jsonResponse.Success = false;
                jsonResponse.Title = "Failure";
                jsonResponse.Message = "We could not get the Document Name now. Please try again.";

                return Json(jsonResponse, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                jsonResponse.Success = false;
                jsonResponse.Title = "Failure";
                jsonResponse.Message = ex.Message;

                return Json(jsonResponse, JsonRequestBehavior.AllowGet);
            }
        }



        private Dictionary<string, string> FormNotifationBody(string agendaItem, string fileName, string meetingId, string userName)
        {
            var MeetingGuid = Guid.Parse(meetingId);
            Dictionary<string, string> DataValues = new Dictionary<string, string>();
            var agendaItemID = Convert.ToInt32(agendaItem);
            try
            {
                var angedaItemName = db.tblAgendaItems.Where(x => x.Id == agendaItemID).Select(x => x.agendaItem).FirstOrDefault();

                var meetingDetails = from x in db.tblMeetings
                                     join y in db.tblMeetingTypes
                                     on x.meetingType equals y.meetingTypeId
                                     where x.meetingId == MeetingGuid
                                     select new
                                     {
                                         meetingDate = x.meetingDate,
                                         meetingType = y.meetingType
                                     };

                DataValues.Add("User", userName);
                DataValues.Add("fileName", fileName);
                DataValues.Add("meetingType", meetingDetails.Select(x => x.meetingType).FirstOrDefault());
                DataValues.Add("meetingDate", meetingDetails.Select(x => x.meetingDate).FirstOrDefault().ToString());
                DataValues.Add("agendaItem", angedaItemName.ToString());
                DataValues.Add("callBack", Url.Action("Index", "Home"));

            }
            catch (Exception ex) { }
            return DataValues;
        }

        /// <summary>
        /// call once for every meeting item, creates folder on the system based on the directory info provided on argument
        /// directoryInfo is a native class that comes from the meatadata itself
        /// </summary>
        /// <param name="dirInfo"></param>
        public static void CreateDirectory(DirectoryInfo dirInfo)
        {
            if (dirInfo.Parent != null) CreateDirectory(dirInfo.Parent);
            if (!dirInfo.Exists) dirInfo.Create();
        }

        private bool insertHtmlRecordInDb(string htmlContent, tblDocumentLibrary tblDocumentLibrary)
        {
            var oLib = new tblHtmlLibrary
            {
                htmlDocId = Guid.NewGuid(),
                docId = tblDocumentLibrary.docId,
                meetingId = tblDocumentLibrary.meetingId,
                agendaItemId = tblDocumentLibrary.agendaItem,
                htmlContent = htmlContent
            };

            try
            {
                db.tblHtmlLibraries.Add(oLib);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex) { throw; }
        }

        public ActionResult MyDocuments()
        {
            List<MyDocumentsViewModel> oListModel = new List<MyDocumentsViewModel>();

            var currentUser = User.Identity.GetUserId();

            var currentUserDocuments = from x in db.tblDocumentLibraries
                                       join y in db.tblAgendaItems
                                       on x.agendaItem equals y.Id
                                       join z in db.tblMeetings
                                       on x.meetingId equals z.meetingId
                                       join a in db.tblMeetingTypes
                                       on z.meetingType equals a.meetingTypeId
                                       where x.userId == currentUser
                                       select new
                                       {
                                           docId = x.docId,
                                           docName = x.docName,
                                           docPath = x.docPath,
                                           meetingDate = z.meetingDate,
                                           meetingType = a.meetingType,
                                           agendaItem = y.agendaItem,
                                           dateSubmitted = x.docCreatedDate
                                       };

            if (currentUserDocuments != null)
            {
                foreach (var item in currentUserDocuments)
                {
                    var oModel = new MyDocumentsViewModel
                    {
                        docId = item.docId,
                        docName = item.docName,
                        docPath = item.docPath,
                        meetingDate = item.meetingDate,
                        meetingType = item.meetingType,
                        agendaItem = item.agendaItem,
                        dateSubmitted = item.dateSubmitted
                    };

                    oListModel.Add(oModel);
                }
            }

            return View(oListModel);
        }

        [HttpPost]
        public JsonResult updateDocOrder(string[] documents)
        {
            if (documents != null && documents.Count() > 0)
            {
                for (int i = 0; i < documents.Count(); i++)
                {
                    try
                    {

                        if (documents[i] != "")
                        {
                            Guid docId = Guid.Parse(documents[i].ToString());
                            tblDocumentLibrary doc = db.tblDocumentLibraries.Where(x => x.docId == docId).Single();
                            doc.orderIn = i;
                        }

                    }
                    catch (Exception ex) { throw; }
                }

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex) { throw; }
            }

            return Json(new { Success = "False", Message = "Status could not be changed!" }, JsonRequestBehavior.AllowGet);
        }
    }
}
