﻿using BoardsXpress.Models;
using BoardsXpress.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace BoardsXpress.Controllers
{
    public class SearchController : Controller
    {
        BoardExpressEntities db = new BoardExpressEntities();

        // GET: Search
        public ActionResult Index(string searchText)
       {

            var results = new List<SearchResultViewModel>();
            searchText = Request.Form["searchPlaceholder"];
            var searchKeyText = Request.Form["searchAdvanced-text"];
            var meetingType = Request.Form["meetingTypeAdvanced"];
            var meetingDate = Request.Form["meetingDateAdvanced"];
            var meetingAgenda = Request.Form["agendaAdvanced"];

            #region queryBuilder

           var joiningTables = 
               from  y in db.tblDocumentLibraries
               join z in db.tblMeetings on y.meetingId equals z.meetingId
               join a in db.tblMeetingTypes on z.meetingType equals a.meetingTypeId
               join b in db.tblAgendaItems on y.agendaItem equals b.Id
               select new
               {
                   htmlContent = y.htmlContent,
                   htmlDocId = y.docId,
                   meetingId = z.meetingId,
                   meetingType = z.meetingType,
                   agendaItem = b.agendaItem,
                   agendaItemId = b.Id,
                   meetingDate = z.meetingDate,
                   meetingTypeName = a.meetingType
               };

            //var joiningTables = from x in db.tblHtmlLibraries
            //                    join y in db.tblDocumentLibraries on x.docId equals y.docId
            //                    join z in db.tblMeetings on y.meetingId equals z.meetingId
            //                    join a in db.tblMeetingTypes on z.meetingType equals a.meetingTypeId
            //                    join b in db.tblAgendaItems on y.agendaItem equals b.Id
            //                    select new
            //                    {
            //                        htmlContent = x.htmlContent,
            //                        htmlDocId = x.htmlDocId,
            //                        meetingId = z.meetingId,
            //                        meetingType = z.meetingType,
            //                        agendaItem = b.agendaItem,
            //                        agendaItemId = b.Id,
            //                        meetingDate = z.meetingDate,
            //                        meetingTypeName = a.meetingType
            //                    };

            if (!string.IsNullOrEmpty(searchText))
            {
                joiningTables = joiningTables.Where(x => x.htmlContent.Contains(searchText));
            }
            else
            {
                string whereClause = string.Empty;
                if (searchKeyText != "")
                    joiningTables = joiningTables.Where(x => x.htmlContent.Contains(searchKeyText));
                if (meetingType != "")
                    joiningTables = joiningTables.Where(x => x.meetingType.ToString() == meetingType);
                if (meetingDate != "")
                    joiningTables = joiningTables.Where(x => x.meetingId.ToString() == meetingDate);
                if (meetingAgenda != "" && meetingAgenda != null)
                {
                    var agendaId = Convert.ToInt32(meetingAgenda);
                    joiningTables = joiningTables.Where(x => x.agendaItemId == agendaId);
                }
            }
            try
            {
                //var records = db.tblHtmlLibraries.SqlQuery(finalQuery);
                if (joiningTables != null)
                {
                    SearchRepository sRepo = new SearchRepository();
                    foreach (var item in joiningTables)
                    {
                        var htmlData = sRepo.ConvertHtml(item.htmlContent);
                        var trimTextExtract = sRepo.TakeLastLines(htmlData, 4);
                        SearchResultViewModel _model = new SearchResultViewModel()
                        {
                            meetingId = item.meetingId,
                            htmlId = item.htmlDocId,
                            HtmlContent = trimTextExtract,
                            meetingTypeId = item.meetingType,
                            agendaItem = item.agendaItem,
                            meetingDate = item.meetingDate,
                            meetingType = item.meetingTypeName
                        };
                        results.Add(_model);
                    }
                }
            }
            catch (Exception ex) { throw; }
            
            #endregion
            return View(results);
        }

        string AddCondition(string clause, string appender, string condition)
        {
            if (clause.Length <= 0)
            {
                return String.Format("WHERE {0}", condition);
            }
            return string.Format("{0} {1} {2}", clause, appender, condition);
        }

        [HttpPost]
        public JsonResult NormalSearch(string searchText)
        {
            SearchRepository s = new SearchRepository();
          var result= s.SimpleSearch(searchText);
          return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult TextSearch()
        {
            return View("NormalSearch");
        }

        public ActionResult AdvanceSearch()
        {
            var searchText = Request.Form["searchAdvanced-text"];
            var meetingType = Request.Form["meetingTypeAdvanced"];
            var meetingDate = Request.Form["meetingDateAdvanced"];
            var meetingAgenda = Request.Form["agendaAdvanced"];
            SearchRepository s = new SearchRepository();
            var results = s.ComplexSearch(searchText, meetingType, meetingDate, meetingAgenda);
           return RedirectToAction("Index", new {content=results });
        }



    }
}