﻿using BoardsXpress.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BoardsXpress.Repository;
using Aspose.Words;
using System.Threading.Tasks;
using System.Data.Entity;
using Newtonsoft.Json.Linq;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity;
using Aspose.Words.Markup;
using Node = BoardsXpress.Models.Node;

namespace BoardsXpress.Controllers
{

    [RequireHttps]
    public class HomeController : Controller
    {
        private BoardExpressEntities db = new BoardExpressEntities();
        private List<LoggedUserRolesModels> UserRoleHelper;
        private string nodetext;

        public HomeController()
        {
            var helper = new HelperRepo();
            UserRoleHelper = helper.LoggedUserRoleInformation();
        }
        public ActionResult Index()
        {
            Dictionary<string, List<tblMeeting>> navDataFields = new Dictionary<string, List<tblMeeting>>();
            var allMeetings = db.tblMeetingTypes.OrderBy(m => m.meetingType).ToList();

            if (User.IsInRole("Super Admin"))
            {
                foreach (var item in allMeetings)
                {
                    var meetingDetails = db.tblMeetings.Where(x => x.meetingType == item.meetingTypeId).OrderBy(m => m.meetingDate).ToList();
                    if (meetingDetails.Count != 0)
                        navDataFields.Add(item.useAbbreviation ? item.meetingAbbreviation : item.meetingType, meetingDetails);
                }
                navigationViewModel oNavModel = new navigationViewModel();
                oNavModel.navDataFields = navDataFields;
                return View(oNavModel);

            }
            else
            {
                var userMeetingList = allMeetings.Where(y => UserRoleHelper.Any(z => z.MeetingTypeId == y.meetingTypeId));
                foreach (var item in userMeetingList)
                {
                    var meetingDetails = db.tblMeetings.Where(x => x.meetingType == item.meetingTypeId).ToList();
                    if (meetingDetails.Count != 0)
                        navDataFields.Add(item.useAbbreviation ? item.meetingAbbreviation : item.meetingType, meetingDetails);
                }
                navigationViewModel oNavModel = new navigationViewModel();
                oNavModel.navDataFields = navDataFields;
                return View(oNavModel);
            }
        }

        #region Bootstrap Tree View model

        public JsonResult GetTreeStucuteForMeeting()
        {
            List<BootstrapTreeViewViewModel> bootStrapTreeRoot = new List<BootstrapTreeViewViewModel>();
            var allMeetings = db.tblMeetingTypes.OrderBy(m => m.meetingType).ToList();

            if (User.IsInRole("Super Admin"))
            {
                foreach (var item in allMeetings)
                {
                    var meetingDetails = db.tblMeetings.Where(x => x.meetingType == item.meetingTypeId).OrderByDescending(m => m.meetingDate).ToList();
                    
                    if (meetingDetails.Count != 0)
                    {
                        BootstrapTreeViewViewModel bootstrapTree = new BootstrapTreeViewViewModel();
                        bootstrapTree.text = item.useAbbreviation ? item.meetingAbbreviation : item.meetingType;
                        bootstrapTree.nodes = new List<Node>();
                        meetingDetails.ForEach(val =>
                        {
                            
                                if (val.rotaryResolution == true)
                                {
                                    nodetext = val.meetingDate != null ? val.meetingDate.Value.ToString("dd MMMM, yyyy") + "</br><span style='margin-left:40px;'>(Rotary Resolution)</span>" : "";
                                }
                                else
                                {
                                    nodetext = val.meetingDate != null ? val.meetingDate.Value.ToString("dd MMMM, yyyy") : "";
                                }
                                bootstrapTree.nodes.Add(new Node
                                {
                                    
                                    text = nodetext,
                                    meetingId = val.meetingId,
                                    meetingType = val.meetingType,
                                    href = Url.Action("DisplayMeeting", "Home", new { meetingId = val.meetingId, meetingType = bootstrapTree.text })
                                });
                           

                        });
                        bootStrapTreeRoot.Add(bootstrapTree);
                    }
                        
                }
                return Json(bootStrapTreeRoot, JsonRequestBehavior.AllowGet);

            }
            else
            {
                var userMeetingList = allMeetings.Where(y => UserRoleHelper.Any(z => z.MeetingTypeId == y.meetingTypeId));
                foreach (var item in userMeetingList)
                {
                    var meetingDetails = db.tblMeetings.Where(x => x.meetingType == item.meetingTypeId).ToList();
                    if (meetingDetails.Count != 0)
                    {
                        BootstrapTreeViewViewModel bootstrapTree = new BootstrapTreeViewViewModel();
                        bootstrapTree.text = item.useAbbreviation ? item.meetingAbbreviation : item.meetingType;
                        bootstrapTree.nodes = new List<Node>();
                        meetingDetails.ForEach(val =>
                        {
                            //Document Check
                            var docCheck = db.tblDocReviews.FirstOrDefault(x => x.meetingId == val.meetingId);
                            if (docCheck != null && docCheck.review == true)
                            {
                            if (val.rotaryResolution == true)
                            {
                                nodetext = val.meetingDate != null ? val.meetingDate.Value.ToString("dd MMMM, yyyy") + "</br><span style='margin-left:40px;'>(Rotary Resolution)</span>" : "";
                            }
                            else
                            {
                                nodetext = val.meetingDate != null ? val.meetingDate.Value.ToString("dd MMMM, yyyy") : "";
                            }
                            bootstrapTree.nodes.Add(new Node
                            {
                                text = nodetext,
                                meetingId = val.meetingId,
                                meetingType = val.meetingType
                            });

                            }
                        });
                        bootStrapTreeRoot.Add(bootstrapTree);
                    }
                        
                }
                return Json(bootStrapTreeRoot, JsonRequestBehavior.AllowGet);
            }
            
        }

        #endregion

        #region Print Document
        /// <summary>
        /// Directly print a document based on these parameters.
        /// </summary>
        /// <param name="_meetingId">Meeting ID</param>
        /// <param name="_docId">Selected document ID</param>
        /// <returns>Returns a Json with the status and a message.</returns>
        [HttpPost]
        public JsonResult PrintSelectedDocument(string _meetingId, string _docId)
        {
            var meetingId = Guid.Parse(_meetingId);
            var docId = Guid.Parse(_docId);

            // Get the document name
            var document = db.tblDocumentLibraries
                .Where(q => q.meetingId == meetingId && q.docId == docId)
                .FirstOrDefault();

            if (document != null)
            {
                var baseUrl = AppDomain.CurrentDomain.BaseDirectory;
                var completeUrl = string.Format("{0}{1}", @baseUrl, @document.docPath);

                // Check if file exists
                if (System.IO.File.Exists(completeUrl))
                {
                    // Convert the path to an Aspose document
                    Document asposeDocument = new Document(completeUrl);

                    // Initialise the PrintHelper
                    var printHelper = new PrintHelper(asposeDocument);

                    // Used to simulate a 5 seconds printing document.
                    //System.Threading.Thread.Sleep(5000);

                    printHelper.PrintDocument();
                }
                else
                {
                    return Json(new { Success = false, Message = "The document does not exist on the server." }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { Success = true, Message = "The document was printed successfully." }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Display Meeting

        public ActionResult DisplayMeeting(Guid meetingId, string meetingType, Guid? htmlId)
        {
            var userId = User.Identity.GetUserId();

            // Check if the user has full access rights? Eg. Super Admin or Meeting Admin
            var haveFullAccess = db.AspNetUsers.Any(m => m.Id == userId && m.AspNetUserRoles.Any(n => n.AspNetRole.fullAccess.Value));

            DisplayMeetingViewModel displayMeetingViewModel = new DisplayMeetingViewModel();

            var printCheck = db.tblDocumentLibraries.FirstOrDefault(x => x.meetingId == meetingId);
            if (printCheck != null) displayMeetingViewModel.Print = printCheck.printRestriction;
            // Get the Meeting
            var tblMeeting = db.tblMeetings
                .Include(m => m.tblPublishLibraries)
                .Include(m => m.tblMeetingType)
                .First(m => m.meetingId == meetingId);

            //Get the comments
            var tblComments = db.tblComments.ToList().Where(w => w.meetingId == meetingId).OrderByDescending(o => o.dateTime);

            //AutoMapper.Mapper.Map(tblComments, displayMeetingViewModel.CommentsModel);
            AutoMapper.Mapper.Map(tblMeeting, displayMeetingViewModel.MeetingViewModel);
            AutoMapper.Mapper.Map(tblMeeting.tblMeetingType, displayMeetingViewModel.MeetingTypeViewModel);

            if (haveFullAccess)
                displayMeetingViewModel.Environment = !tblMeeting.tblPublishLibraries.Any(m => m.meetingId == tblMeeting.meetingId) ? "Staging" : "";

            return View(displayMeetingViewModel);
        }

        private void GetPublishedDocumentsList(Guid meetingId, List<PublishItemViewModel> publishItemViewModelList)
        {
            var tblDocumentLibraries = db.tblPublishLibraries
                                .Where(m =>
                                    m.meetingId == meetingId &&
                                    m.tblDocumentLibrary.agendaItemRelatedId == null)
                                .Select(m => m.tblDocumentLibrary);

            foreach (var agendaItemDocument in tblDocumentLibraries)
            {
                var publishItemViewModel = new PublishItemViewModel
                {
                    docId = agendaItemDocument.docId,
                    meetingId = agendaItemDocument.meetingId,
                    docAuthor = agendaItemDocument.userId,
                    publisher = db.AspNetUsers.Where(s => s.Id == agendaItemDocument.userId).Select(p => p.UserName).FirstOrDefault(),
                    agendaItemId = agendaItemDocument.agendaItem,
                    agendaItem = agendaItemDocument.tblAgendaItem.agendaItem,
                    agendaItemRelatedId = agendaItemDocument.agendaItemRelatedId,
                    docName = agendaItemDocument.docName,
                    docPath = agendaItemDocument.docPath,
                    docCreatedDate = (DateTime)agendaItemDocument.docCreatedDate,
                };

                publishItemViewModelList.Add(publishItemViewModel);
            }
        }

        private void GetPublishedRelatedDocumentsList(Guid meetingId, List<PublishItemViewModel> publishItemViewModelList)
        {
            var tblRelatedDocumentLibraries = db.tblPublishLibraries
                                .Where(m =>
                                    m.meetingId == meetingId &&
                                    m.tblDocumentLibrary.agendaItemRelatedId != null)
                                .Select(m => m.tblDocumentLibrary);

            foreach (var agendaItemRelatedDocument in tblRelatedDocumentLibraries)
            {
                // Get the parent Agenda Item
                var agendaItemRelated = publishItemViewModelList
                    .FirstOrDefault(m =>
                        m.agendaItemId == agendaItemRelatedDocument.agendaItem &&
                        m.agendaItemRelatedId == null);

                if (agendaItemRelated != null)
                {
                    agendaItemRelated.ListRelatedDocuments.Add(new PublishItemViewModel
                    {
                        docId = agendaItemRelatedDocument.docId,
                        meetingId = agendaItemRelatedDocument.meetingId,
                        docAuthor = agendaItemRelatedDocument.userId,
                        publisher = db.AspNetUsers.Where(s => s.Id == agendaItemRelatedDocument.userId).Select(p => p.UserName).FirstOrDefault(),
                        agendaItemId = agendaItemRelatedDocument.agendaItem,
                        agendaItem = agendaItemRelatedDocument.tblAgendaItemsRelated.name,
                        agendaItemRelatedId = agendaItemRelatedDocument.agendaItemRelatedId,
                        docName = agendaItemRelatedDocument.docName,
                        docPath = agendaItemRelatedDocument.docPath,
                        docCreatedDate = (DateTime)agendaItemRelatedDocument.docCreatedDate,
                    });
                }
            }
        }

        /// <summary>
        /// Get the navigation to be used in the DisplayMeeting.cshtml
        /// </summary>
        /// <param name="meetingId">meetingId from tblMeetings</param>
        /// <returns>Returns a bootstrap treeview json object</returns>
        [HttpGet]
        public JsonResult GetTreeViewNavigation(string meetingId)
        {
            var userId = User.Identity.GetUserId();

            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);

            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                writer.Formatting = Formatting.Indented;

                writer.WriteStartArray();

                if (meetingId != null && meetingId.Length > 0)
                {
                    var _meetingId = Guid.Parse(meetingId);

                    // Is there any document published for this agenda item?
                    var anyPublishedDocument = db.tblAgendaItems
                        .Any(m =>
                            m.meetingTypeId == _meetingId &&
                            m.tblDocumentLibraries.Any(n => n.tblPublishLibraries.Any()));

                    // Does user have access to see the menu before being published? Eg. Super Admin or Meeting Admin
                    var haveFullAccess = db.AspNetUsers.Any(m => m.Id == userId && m.AspNetUserRoles.Any(n => n.AspNetRole.fullAccess.Value));

                    if (anyPublishedDocument || haveFullAccess)
                    {
                        var tblAgendaItems = db.tblAgendaItems
                        .Where(m => m.meetingTypeId == _meetingId)
                        .Include(m => m.tblAgendaItemsRelateds)
                        .OrderBy(m => m.itemOrder);

                        foreach (var agendaItem in tblAgendaItems)
                        {
                            writer.WriteStartObject();
                            writer.WritePropertyName("text");
                            writer.WriteValue(string.Format("<strong>{0}.</strong> {1}", agendaItem.itemOrder, agendaItem.agendaItem));

                            var document = agendaItem
                                .tblDocumentLibraries
                                .FirstOrDefault(m =>
                                    m.meetingId == _meetingId &&
                                    m.agendaItem == agendaItem.Id &&
                                    m.agendaItemRelatedId == null);

                            if (document != null)
                            {
                                writer.WritePropertyName("data-meetingId");
                                writer.WriteValue(document.meetingId);
                                writer.WritePropertyName("data-agendaItemId");
                                writer.WriteValue(document.agendaItem);
                                writer.WritePropertyName("data-docId");
                                writer.WriteValue(document.docId);
                            }

                            if (agendaItem.tblAgendaItemsRelateds.Any())
                            {
                                writer.WritePropertyName("nodes");
                                writer.WriteStartArray();

                                foreach (var relatedItem in agendaItem.tblAgendaItemsRelateds.OrderBy(m => m.itemOrder))
                                {
                                    var relatedItemDoc = relatedItem.
                                            tblDocumentLibraries.
                                            FirstOrDefault(m =>
                                                m.agendaItem == relatedItem.agendaItemId &&
                                                m.agendaItemRelatedId == relatedItem.id &&
                                                m.meetingId == agendaItem.meetingTypeId);

                                    writer.WriteStartObject();
                                    writer.WritePropertyName("text");
                                    writer.WriteValue(string.Format("<strong>{0}.{1}.</strong> {2}", agendaItem.itemOrder, relatedItem.itemOrder, relatedItem.name));
                                    writer.WritePropertyName("data-meetingId");
                                    writer.WriteValue(relatedItem.tblAgendaItem.meetingTypeId);
                                    writer.WritePropertyName("data-agendaItemId");
                                    writer.WriteValue(relatedItem.agendaItemId);
                                    writer.WritePropertyName("data-agendaItemRelatedId");
                                    writer.WriteValue(relatedItem.id);

                                    if (relatedItemDoc != null)
                                    {
                                        writer.WritePropertyName("data-docId");
                                        writer.WriteValue(relatedItemDoc.docId);
                                    }

                                    writer.WriteEndObject();
                                }
                            }

                            writer.WriteEndObject();
                        }
                    }
                }

                writer.WriteEndArray();

                return Json(sb.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Right Panel Content

        [HttpGet]
        public ActionResult GetRightPanelContent(Guid? docId, Guid? meetingId)
        {
            var rightPanelContentModel = new RightPanelContentModel
            {
                ShowContent = true,
                ShowLogo = true
            };

            if ((docId.HasValue && docId.Value != Guid.Empty))
            {
                var tblDocumentLibrary = db.tblDocumentLibraries.First(m => m.docId == docId && m.meetingId == meetingId);

                // Get the documents directory info
                // Delete the file and folder.
                DirectoryInfo info = new DirectoryInfo(Server.MapPath("~/Documents/") + "/" + tblDocumentLibrary.tblMeeting.meetingType);

                if (info.Exists)
                {
                    var files = info.GetFiles(tblDocumentLibrary.docName);

                    if (files.Any())
                    {
                        var file = files[0];

                        if (file.Extension == ".doc" || file.Extension == ".docx")
                        {
                            // Extract DLM from ADHA documents
                            Document doc = new Document(file.FullName);

                            foreach (StructuredDocumentTag sdt in doc.GetChildNodes(NodeType.StructuredDocumentTag, true))
                            {
                                if (sdt.SdtType == SdtType.ComboBox)
                                {
                                    if (sdt.Tag.ToLower() == "protective marking")
                                        rightPanelContentModel.ProtectiveMarking = sdt.ListItems.SelectedValue.DisplayText;
                                    else if (sdt.Tag.ToLower() == "reason for sensitive classification")
                                        rightPanelContentModel.ReasonForSensitivity = sdt.ListItems.SelectedValue.DisplayText;
                                }
                            }
                        }
                    }
                }
            }

            return PartialView("_RightPanelContent", rightPanelContentModel);
        }

        #endregion
    }
}