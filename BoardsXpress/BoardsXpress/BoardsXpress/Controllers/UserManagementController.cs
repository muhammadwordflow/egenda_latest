﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

using BoardsXpress.Models;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.IO;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace BoardsXpress.Controllers
{

    [Authorize]
    public class UserManagementController : BaseController
    {
        private bool RoleAddFlag = false;

        #region Constructor

        public UserManagementController() { }

        public UserManagementController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        #endregion

        #region Properties

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        EmailProviderController oEmailProvider = new EmailProviderController();
        private Guid MeetingTypeId;

        #endregion

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UserInRoleManagement(Guid id)
        {
            ViewBag.meetingTypeId = id;
            var meetingTypeName = (from x in db.tblMeetingTypes where x.meetingTypeId == id select new { x.meetingType, x.useAbbreviation, x.meetingAbbreviation }).FirstOrDefault();
            ViewBag.meetingTypeName = meetingTypeName.useAbbreviation ? meetingTypeName.meetingAbbreviation : meetingTypeName.meetingType;
            TempData["MeetingTypeId"] = id;
            TempData.Keep("MeetingTypeId");
            return View();
        }


        #region "Ajax call"

        public ActionResult GetUsersList()
        {
            UserRoleAssignmentViewModels userList = new UserRoleAssignmentViewModels();
            userList.userListViewModel = new List<UserManagementViewModels>();
            userList.UserTitle = new List<UserTitleViewModel>();
            userList.TypeOfMember = new List<UserTitleViewModel>();
            try
            {
                userList.userListViewModel = getDbUserList();
                var dbuserTitle = db.tblUserTitles.ToList();
                var dbTypeofMember = db.tblTypeOfmembers.ToList();
                dbuserTitle.ForEach(item =>
                {
                    userList.UserTitle.Add(new UserTitleViewModel
                    {
                        Id = item.Id,
                        Title = item.Title
                    });
                });
                dbTypeofMember.ForEach(item =>
                {
                    userList.TypeOfMember.Add(new UserTitleViewModel
                    {
                        Id = item.Id,
                        Title = item.MemberType
                    });
                });
            }
            catch (Exception ex)
            {
                userList.ErrorCode = ex.HResult;
                userList.ErrorMessage = ex.Message;
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetUserRoleList()
        {

            //var meetingType = db.tblMeetingTypes.Where(w => w.meetingTypeId == id).Select(s => s.meetingType).FirstOrDefault().ToString();
            //ViewBag.meetingType = meetingType;
            //MeetingTypeId = id;
            //ViewBag.meetingTypeId = id;
            UserRoleAssignmentViewModels userRoleAssignmentViewModels = new UserRoleAssignmentViewModels();
            try
            {
                userRoleAssignmentViewModels.userListViewModel = new List<UserManagementViewModels>();
                userRoleAssignmentViewModels.userListViewModel = getListOfUserInRole();
            }
            catch (Exception ex)
            {
                userRoleAssignmentViewModels.ErrorMessage = ex.InnerException.Message;
                userRoleAssignmentViewModels.ErrorCode = ex.HResult;

            }


            return Json(userRoleAssignmentViewModels, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetUserRoleAllIndividual(string userId, string roleId)
        {
            var user = (dynamic)null;
            string getRole = null;
            UserRoleAssignmentViewModels userRoleAssignmentViewModels = new UserRoleAssignmentViewModels();
            userRoleAssignmentViewModels.userListViewModel = new List<UserManagementViewModels>();
            List<AspNetRole> roles = new List<AspNetRole>();
            try
            {
                if (!string.IsNullOrWhiteSpace(userId) && !string.IsNullOrWhiteSpace(roleId) && !string.IsNullOrWhiteSpace(TempData.Peek("MeetingTypeId").ToString()))
                {
                    Guid meetingTypeId = new Guid(TempData.Peek("MeetingTypeId").ToString());
                    user = (from u in db.AspNetUsers
                            join ur in db.AspNetUserRoles on u.Id equals ur.UserId
                            where u.Id == userId && ur.RoleId == roleId && ur.MeetingTypeId == meetingTypeId
                            select new
                            {
                                u.Email,
                                u.PhoneNumber,
                                u.Organisation,
                                u.Designation,
                                u.Avatar,
                                u.Title,
                                u.FirstName,
                                ur.RoleId,
                                ur.Id,
                                ur.StartDate,
                                ur.EndDate,
                                ur.MeetingTypeId
                            }).ToList();
                }
                else
                {
                    user = (from x in db.FunctionASPNetUsersRoleList(new Guid(TempData.Peek("MeetingTypeId").ToString())) select x).ToList();
                    getRole = (from r in db.AspNetRoles
                               where r.fullAccess == false && r.readAccess == true && r.writeAccess == true
                               select r.Id).FirstOrDefault();

                }
                roles = (from y in db.AspNetRoles where y.Name != "Super Admin" select y).ToList();
                foreach (var item in user)
                {
                    var UserManagementViewModels = new UserManagementViewModels();
                    UserManagementViewModels.UserId = item.Id.ToString();
                    UserManagementViewModels.AspNetUserRoleId = item.Id is Guid ? item.Id : new Guid(item.Id);
                    UserManagementViewModels.Email = item.Email;
                    UserManagementViewModels.PhoneNumber = item.PhoneNumber;
                    UserManagementViewModels.FirstName = item.FirstName;
                    UserManagementViewModels.Organisation = item.Organisation;
                    UserManagementViewModels.Designation = item.Designation;
                    UserManagementViewModels.Avatar = item.Avatar;
                    UserManagementViewModels.SelectTitle = item.Title;
                    if (!string.IsNullOrWhiteSpace(userId) && !string.IsNullOrWhiteSpace(roleId) && !string.IsNullOrWhiteSpace(TempData.Peek("MeetingTypeId").ToString()))
                    {
                        UserManagementViewModels.userInRole = new RoleViewModels
                        {
                            Id = item.RoleId
                        };
                        UserManagementViewModels.FormatStartDate = item.StartDate == null ? DateTime.Now.ToString("dd/MM/yyyy") : item.StartDate.ToString("dd/MM/yyyy");
                        UserManagementViewModels.FormatEndDate = item.EndDate == null ? null : item.EndDate.ToString("dd/MM/yyyy");
                        UserManagementViewModels.SelectedUser = (!string.IsNullOrWhiteSpace(userId) && !string.IsNullOrWhiteSpace(roleId) && !string.IsNullOrWhiteSpace(TempData.Peek("MeetingTypeId").ToString())) ? true : false;
                    }
                    else
                    {
                        UserManagementViewModels.userInRole = new RoleViewModels();
                    }
                    UserManagementViewModels.SelectedRoleId = (!string.IsNullOrWhiteSpace(userId) && !string.IsNullOrWhiteSpace(roleId) && !string.IsNullOrWhiteSpace(TempData.Peek("MeetingTypeId").ToString())) ? item.RoleId : getRole;
                    UserManagementViewModels.Roles = new List<RoleViewModels>();
                    roles.ForEach(i =>
                    {
                        var role = new RoleViewModels
                        {
                            Id = i.Id,
                            Name = i.Name,
                            Description = i.Description,
                            fullAccess = i.fullAccess,
                            readAccess = i.readAccess,
                            writeAccess = i.writeAccess
                        };
                        UserManagementViewModels.Roles.Add(role);
                    });
                    userRoleAssignmentViewModels.userListViewModel.Add(UserManagementViewModels);
                    userRoleAssignmentViewModels.MeetingTypeId = new Guid(TempData.Peek("MeetingTypeId").ToString());
                }
            }
            catch (Exception ex)
            {
                userRoleAssignmentViewModels.ErrorCode = ex.HResult;
                userRoleAssignmentViewModels.ErrorMessage = ex.InnerException.Message;
            }

            return Json(userRoleAssignmentViewModels, JsonRequestBehavior.AllowGet);
        }

        [HttpDelete]
        public ActionResult DeleteUserRole(string userId, string roleId, Guid aspnetuserId)
        {
            UserRoleAssignmentViewModels userRoleAssignmentViewModels = new UserRoleAssignmentViewModels();
            userRoleAssignmentViewModels.userListViewModel = new List<UserManagementViewModels>();
            try
            {
                var findUserById = db.AspNetUserRoles.Find(userId, roleId, aspnetuserId);
                db.AspNetUserRoles.Remove(findUserById);
                db.SaveChanges();
                userRoleAssignmentViewModels.userListViewModel = getListOfUserInRole();
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }

            return Json(userRoleAssignmentViewModels, JsonRequestBehavior.AllowGet);

        }

        private List<UserManagementViewModels> getListOfUserInRole()
        {
            Guid id = new Guid(TempData.Peek("MeetingTypeId").ToString());
            var UserMangementViewList = new List<UserManagementViewModels>();
            var user = (from x in db.AspNetUsers
                        join y in db.AspNetUserRoles on x.Id equals y.UserId
                        join z in db.AspNetRoles on y.RoleId equals z.Id
                        where y.MeetingTypeId == id && x.FirstName != "Super Admin"
                        select new UserManagementViewModels
                        {
                            UserId = x.Id,
                            FirstName = x.FirstName,
                            userInRole = new RoleViewModels
                            {
                                Id = z.Id,
                                Name = z.Name
                            },
                            EndDate = y.EndDate,
                            StartDate = y.StartDate,
                            AspNetUserRoleId = y.Id,
                            MeetingTypeId = y.MeetingTypeId ?? default(Guid)
                        }).ToList();
            foreach (var item in user)
            {
                var UserManagementViewModels = new UserManagementViewModels();
                UserManagementViewModels.UserId = item.UserId;
                UserManagementViewModels.Email = item.Email;
                UserManagementViewModels.PhoneNumber = item.PhoneNumber;
                UserManagementViewModels.FirstName = item.FirstName;
                UserManagementViewModels.Organisation = item.Organisation;
                UserManagementViewModels.Designation = item.Designation;
                UserManagementViewModels.Avatar = item.Avatar;
                //UserManagementViewModels.MeetingId = meetingId;
                UserManagementViewModels.AspNetUserRoleId = item.AspNetUserRoleId;
                UserManagementViewModels.FormatStartDate = item.StartDate == null ? DateTime.Now.ToString("dd/MM/yyyy") : item.StartDate.ToString("dd/MM/yyyy");
                UserManagementViewModels.FormatEndDate = item.EndDate == null ? null : item.EndDate.Value.ToString("dd/MM/yyyy");
                UserManagementViewModels.SelectTitle = item.SelectTitle;
                UserManagementViewModels.UserGroupList = UserGroupList();
                UserManagementViewModels.userInRole = new RoleViewModels
                {
                    Id = item.userInRole.Id,
                    Name = item.userInRole.Name
                };
                UserManagementViewModels.MeetingTypeId = new Guid(TempData.Peek("MeetingTypeId").ToString());

                UserMangementViewList.Add(UserManagementViewModels);
            }

            return UserMangementViewList;
        }
        [HttpDelete]
        public ActionResult DeleteUser(string id)
        {
            UserRoleAssignmentViewModels userList = new UserRoleAssignmentViewModels();
            userList.userListViewModel = new List<UserManagementViewModels>();

            try
            {
                var tblUserList = (from x in db.AspNetUsers where x.Id == id select x).FirstOrDefault();
                db.Entry(tblUserList).State = EntityState.Deleted;
                db.SaveChanges();

                //AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
                //{
                //    UserName = User.Identity.GetUserName(),
                //    UserId = new Guid(User.Identity.GetUserId()),
                //    AfterData = JsonConvert.SerializeObject(null),
                //    BeforeData = JsonConvert.SerializeObject(tblUserList),
                //    IpAddress = Request.UserHostAddress,
                //    SessionId = HttpContext.Session.SessionID,
                //    ActionPerform = "Delete",
                //    UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
                //};
                //auditTrailViewModel.AuditTrail();

                userList.userListViewModel = getDbUserList();
            }
            catch (Exception ex)
            {
                userList.userListViewModel = getDbUserList();
                userList.ErrorCode = ex.HResult;
                userList.ErrorMessage = ex.Message;
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetUserById(string id)
        {
            UserManagementViewModels userViewModel = new UserManagementViewModels();
            try
            {
                var tblUser = (from x in db.AspNetUsers where x.Id == id select x).FirstOrDefault();
                userViewModel.UserId = tblUser.Id;
                userViewModel.Email = tblUser.Email;
                userViewModel.PhoneNumber = tblUser.PhoneNumber;
                userViewModel.UserName = tblUser.UserName;
                userViewModel.FirstName = tblUser.FirstName;
                userViewModel.Organisation = tblUser.Organisation;
                userViewModel.Designation = tblUser.Designation;
                userViewModel.Avatar = tblUser.Avatar;
                userViewModel.SelectTitle = tblUser.Title;
            }
            catch (Exception ex)
            {
                userViewModel.ErrorCode = ex.HResult;
                userViewModel.ErrorMessage = ex.Message;
            }
            return Json(userViewModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditUser(RegisterViewModel model)
        {
            UserRoleAssignmentViewModels userRoleAssignmentViewModels = new UserRoleAssignmentViewModels();
            userRoleAssignmentViewModels.userListViewModel = new List<UserManagementViewModels>();
            userRoleAssignmentViewModels.TypeOfMember = new List<UserTitleViewModel>();
            userRoleAssignmentViewModels.UserTitle = new List<UserTitleViewModel>();
            if (model.TypeOfMemberSelected == "undefined")
            {
                model.TypeOfMemberSelected = null;
            }
            try
            {
                if (model.Id != null)
                {
                    bool result = EditUserToDb(model);
                    if (!result)
                    {
                        userRoleAssignmentViewModels.ErrorCode = -123;
                        userRoleAssignmentViewModels.ErrorMessage = "Error in connection";
                    }
                }
                else
                {
                    Create(model);

                }
                //else
                //{
                //    string isExitsEmail = Regex.Replace(model.Email, @"\s+", "");
                //    var user = db.AspNetUsers.FirstOrDefault(x => x.Email == isExitsEmail);
                //    if (user != null)
                //    {
                //        model.Id = user.Id;
                //        model.Active = true;
                //        bool result = EditUserToDb(model);
                //        if (!result)
                //        {
                //            userRoleAssignmentViewModels.ErrorCode = -123;
                //            userRoleAssignmentViewModels.ErrorMessage = "Error in connection";
                //        }
                //    }

                //}

                userRoleAssignmentViewModels.userListViewModel = getDbUserList();
                var dbuserTitle = db.tblUserTitles.ToList();
                var dbTypeofMember = db.tblTypeOfmembers.ToList();
                dbuserTitle.ForEach(item =>
                {
                    userRoleAssignmentViewModels.UserTitle.Add(new UserTitleViewModel
                    {
                        Id = item.Id,
                        Title = item.Title
                    });
                });
                dbTypeofMember.ForEach(item =>
                {
                    userRoleAssignmentViewModels.TypeOfMember.Add(new UserTitleViewModel
                    {
                        Id = item.Id,
                        Title = item.MemberType
                    });
                });
            }
            catch (Exception ex)
            {
                userRoleAssignmentViewModels.userListViewModel = getDbUserList();
                userRoleAssignmentViewModels.ErrorCode = ex.HResult;
                userRoleAssignmentViewModels.ErrorMessage = ex.Message;
            }
            return Json(userRoleAssignmentViewModels, JsonRequestBehavior.AllowGet);

        }

        #endregion

        #region Private Method
        private List<UserManagementViewModels> AddUserToRoleDBOperation(UserRoleAssignmentViewModels model)
        {
            var UserMangementViewList = new List<UserManagementViewModels>();

            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    bool success = true;

                    foreach (var i in model.userListViewModel)
                    {
                        i.StartDate = DateTime.ParseExact(i.FormatStartDate, "dd/MM/yyyy", null);
                        i.EndDate = i.FormatEndDate == null ? (DateTime?)null : DateTime.ParseExact(i.FormatEndDate, "dd/MM/yyyy", null);
                        if (i.SelectedUser)
                        {
                            RoleAddFlag = true;
                            var userRole = new AspNetUserRole
                            {
                                MeetingTypeId = new Guid(TempData.Peek("MeetingTypeId").ToString()),
                                RoleId = i.SelectedRoleId,
                                UserId = i.UserId,
                                StartDate = i.StartDate,
                                EndDate = i.EndDate,
                                Id = Guid.NewGuid()
                            };

                            db.AspNetUserRoles.Add(userRole);

                            if (db.SaveChanges() == 0)
                            {
                                dbContextTransaction.Rollback();
                                success = false;
                                break;
                            }

                        }
                    }

                    if (success)
                    {
                        dbContextTransaction.Commit();
                        UserMangementViewList = getListOfUserInRole();
                    }


                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    UserMangementViewList = getListOfUserInRole();

                }
            }

            return UserMangementViewList;

        }

        private List<UserManagementViewModels> EditUserToRoleDBOperation(UserRoleAssignmentViewModels model)
        {
            var UserMangementViewList = new List<UserManagementViewModels>();
            try
            {
                // find the user and delete from the table and add the user with new role id.
                Guid id = model.userListViewModel[0].AspNetUserRoleId;
                var findUserById = (from x in db.AspNetUserRoles where x.Id == id select x).FirstOrDefault();
                db.AspNetUserRoles.Remove(findUserById);
                db.SaveChanges();

                //Add the new role in the AspNetRole Table

                var userRole = new AspNetUserRole
                {
                    MeetingTypeId = findUserById.MeetingTypeId,
                    RoleId = model.userListViewModel[0].SelectedRoleId,
                    UserId = findUserById.UserId,
                    StartDate = DateTime.ParseExact(model.userListViewModel[0].FormatStartDate, "dd/MM/yyyy", null),
                    EndDate = model.userListViewModel[0].FormatEndDate == null ? (DateTime?)null : DateTime.ParseExact(model.userListViewModel[0].FormatEndDate, "dd/MM/yyyy", null),
                    Id = Guid.NewGuid()
                };
                db.AspNetUserRoles.Add(userRole);
                db.SaveChanges();
                UserMangementViewList = getListOfUserInRole();
            }
            catch (Exception ex)
            {
                UserMangementViewList = getListOfUserInRole();
            }

            return UserMangementViewList;

        }

        private List<UserManagementViewModels> getDbUserList()
        {
            List<UserManagementViewModels> listOfUser = new List<UserManagementViewModels>();
            var userDb = (from x in db.AspNetUsers
                          where x.FirstName != "Super Admin"
                          select x).Distinct().ToList();
            foreach (var item in userDb)
            {
                var UserManagementViewModels = new UserManagementViewModels();
                UserManagementViewModels.UserId = item.Id;
                UserManagementViewModels.SelectTitle = item.Title;
                UserManagementViewModels.Designation = item.Designation;
                UserManagementViewModels.UserName = item.UserName;
                UserManagementViewModels.Email = item.Email;
                UserManagementViewModels.PhoneNumber = item.PhoneNumber;
                UserManagementViewModels.FirstName = item.FirstName;
                UserManagementViewModels.Organisation = item.Organisation;
                UserManagementViewModels.Designation = item.Designation;
                UserManagementViewModels.Avatar = item.Avatar;
                UserManagementViewModels.UserGroupList = UserGroupList();
                listOfUser.Add(UserManagementViewModels);
            }
            return listOfUser;
        }


        private bool EditUserToDb(RegisterViewModel model)
        {
            string filePath = string.Empty;
            var userById = UserManager.FindById(model.Id);
            if (model.Avatar != null)
            {
                filePath = ProfileImageUpload(model);
                userById.Avatar = filePath;
            }
            userById.UserName = model.Email;
            userById.Email = model.Email;
            userById.FirstName = model.FirstName;
            userById.Organisation = model.Organisation;
            userById.Designation = model.Designation;
            userById.PhoneNumber = model.PhoneNumber;
            userById.Title = model.Title;
            var result = UserManager.Update(userById);
            //AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
            //{
            //    UserName = User.Identity.GetUserName(),
            //    UserId = new Guid(User.Identity.GetUserId()),
            //    AfterData = JsonConvert.SerializeObject(model),
            //    BeforeData = JsonConvert.SerializeObject(userById),
            //    IpAddress = Request.UserHostAddress,
            //    SessionId = HttpContext.Session.SessionID,
            //    ActionPerform = "Edit",
            //    UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
            //};
            //auditTrailViewModel.AuditTrail();
            return result.Succeeded;
        }

        private string ProfileImageUpload(RegisterViewModel model)
        {
            //if (ModelState.IsValid)
            //{
            // RegisterViewModel model = new RegisterViewModel();
            var uploadPath = Server.MapPath("/Media/userProfile/");
            var filePath = @"/Media/userProfile/";
            if (model.ImageUpload == null)
            {
                filePath = "";
            }
            else
            {
                uploadPath = uploadPath + model.FirstName.ToString();
                if (!Directory.Exists(uploadPath))
                {

                    Directory.CreateDirectory(uploadPath);
                }

                var ext = Path.GetExtension(model.ImageUpload.FileName);

                uploadPath = uploadPath + "\\" + model.FirstName + ext;
                filePath = filePath + model.FirstName + "/" + model.FirstName + ext;
                model.ImageUpload.SaveAs(uploadPath);
                var x = model.meetingTypeId;
            }
            return filePath;
        }

        #endregion

        [HttpPost]
        public ActionResult AddEditUserRole(UserRoleAssignmentViewModels model, bool edit)
        {
            UserRoleAssignmentViewModels userRoleAssignmentViewModels = new UserRoleAssignmentViewModels();
            userRoleAssignmentViewModels.userListViewModel = new List<UserManagementViewModels>();
            if (edit)
            {
                userRoleAssignmentViewModels.userListViewModel = EditUserToRoleDBOperation(model);
            }
            else
            {
                userRoleAssignmentViewModels.userListViewModel = AddUserToRoleDBOperation(model);
                if (!RoleAddFlag)
                {
                    userRoleAssignmentViewModels.ErrorMessage = "Please select users to assign role.";
                }
                else
                {
                    var meetingType = db.tblMeetingTypes.FirstOrDefault(x => x.meetingTypeId == model.MeetingTypeId);
                    if (meetingType != null)
                        userRoleAssignmentViewModels.ErrorMessage =
                            $"User Role for {meetingType.meetingType} has been assigned.";
                }
            }


            return Json(userRoleAssignmentViewModels, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var userDetails = UserManager.Users.ToList().Find(s => s.Id == id);
            if (userDetails == null)
            {
                return HttpNotFound();
            }
            return View(userDetails);
        }

        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var userDetails = UserManager.Users.ToList().Find(s => s.Id == id);
            if (userDetails == null)
            {
                return HttpNotFound();
            }
            return View(userDetails);
        }
        [HttpPost]
        public JsonResult Edit(RegisterViewModel aspNetUser)
        {
            var user = UserManager.FindById(aspNetUser.Id);
            if (aspNetUser.Password != null)
            {
                user.PasswordHash = aspNetUser.Password;
            }
            if (aspNetUser.Avatar != null)
            {
                var uploadPath = Server.MapPath("/Media/userProfile/");
                var filePath = @"/Media/userProfile/";
                if (aspNetUser.ImageUpload != null || aspNetUser.ImageUpload.ContentLength == 0)
                {

                    uploadPath = uploadPath + aspNetUser.FirstName.ToString();
                    uploadPath = uploadPath.Replace(" ", "");
                    if (!Directory.Exists(uploadPath))
                    {

                        Directory.CreateDirectory(uploadPath);
                    }

                    var ext = Path.GetExtension(aspNetUser.ImageUpload.FileName);

                    uploadPath = uploadPath + "\\" + aspNetUser.FirstName.Trim() + ext;
                    uploadPath = uploadPath.Replace(" ", "");
                    filePath = filePath + aspNetUser.FirstName + "/" + aspNetUser.FirstName + ext;
                    filePath = filePath.Replace(" ", "");
                    aspNetUser.ImageUpload.SaveAs(uploadPath);
                    user.Avatar = filePath;

                }
            }
            user.UserName = aspNetUser.Email;
            user.Email = aspNetUser.Email;
            user.FirstName = aspNetUser.FirstName;
            user.LastName = aspNetUser.LastName;
            user.MiddleName = aspNetUser.MiddleName;
            user.Gender = aspNetUser.Gender;
            user.Organisation = aspNetUser.Organisation;
            user.Designation = aspNetUser.Designation;
            //user.Avatar = aspNetUser.Avatar;
            user.PhoneNumber = aspNetUser.PhoneNumber;
            user.EmailConfirmed = true;

            var result = UserManager.Update(user);

            if (result.Succeeded)
            {
                var results = new { Success = "True", Message = "User  has been Updated" };  //Need to fix this
                return Json(results, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var results = new { Success = "False", Message = "Event Creation Failed!!" };  //Need to fix this
                return Json(results, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = UserManager.FindById(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        [HttpPost]
        public JsonResult DeleteConfirmed(ApplicationUser aspNetUser, Guid meetingTypeId)
        {
            var user = db.AspNetUsers.First(m => m.Id == aspNetUser.Id);

            var userRole = db.AspNetUserRoles.First(m => m.UserId == user.Id && m.MeetingTypeId == meetingTypeId);

            db.AspNetUserRoles.Remove(userRole);

            // If there's no roles for any meeting, delete the user from the database - TODO: Think about it clearly.
            //if(!db.AspNetUserRoles.Any(m => m.UserId == user.Id))
            //    db.AspNetUsers.Remove(user);

            if (db.SaveChanges() > 0)
            {
                var results = new { Success = "True", Message = "User  has been Deleted" };  //Need to fix this
                return Json(results, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var results = new { Success = "False", Message = "Event Creation Failed!!" };  //Need to fix this
                return Json(results, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(model);

                //Check for email confirmation
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user != null)
                {

                    if (user.EmailConfirmed == true)
                    {

                        // This doesn't count login failures towards account lockout
                        // To enable password failures to trigger account lockout, change to shouldLockout: true
                        var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
                        switch (result)
                        {
                            case SignInStatus.Success:
                                return RedirectToLocal("/Home/Index");
                            case SignInStatus.LockedOut:
                                return View("Lockout");
                            case SignInStatus.RequiresVerification:
                                return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                            case SignInStatus.Failure:
                            default:
                                ModelState.AddModelError("", "Invalid login attempt.");
                                return View(model);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Confirm Email Address.");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Invalid login attempt.");
                }

                return View(model);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                log.Error(ex.InnerException != null ? ex.InnerException.Message : "");

                return View("Error");
            }
        }

        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Create()
        {
            var genderList = GenderList();
            var viewModel = new RegisterViewModel();
            viewModel.GenderList = genderList;
            return View(viewModel);
        }


        public List<SelectListItem> UserGroupList()
        {

            List<SelectListItem> groupList = new List<SelectListItem>();
            var userGroups = db.tblUserGroups.Where(w => w.groupName != "SuperAdmin").ToList();
            foreach (var item in userGroups)
            {
                groupList.Add(new SelectListItem
                {
                    Text = item.groupName,
                    Value = item.groupId.ToString()
                });
            }
            return groupList;
        }

        public List<SelectListItem> GenderList()
        {
            List<SelectListItem> genderList = new List<SelectListItem>();



            genderList.Add(new SelectListItem
            {

                Text = "Male",
                Value = "Male"

            });
            genderList.Add(new SelectListItem
            {

                Text = "Female",
                Value = "Female"

            });
            genderList.Add(new SelectListItem
            {

                Text = "Not Identified",
                Value = "Not Identified"

            });


            return genderList;


        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        public void Create(RegisterViewModel model)
        // public JsonResult Create(HttpPostedFileBase file)
        {
            string filePath = ProfileImageUpload(model);
            //AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
            //{
            //    UserName = User.Identity.GetUserName(),
            //    UserId = new Guid(User.Identity.GetUserId()),
            //    AfterData = JsonConvert.SerializeObject(model),
            //    BeforeData = JsonConvert.SerializeObject(null),
            //    IpAddress = Request.UserHostAddress,
            //    SessionId = HttpContext.Session.SessionID,
            //    ActionPerform = "Add",
            //    UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
            //};
            //auditTrailViewModel.AuditTrail();
            var user = new ApplicationUser
            {

                UserName = model.Email,
                Email = model.Email,
                FirstName = model.FirstName,
                //LastName = model.LastName,
                //MiddleName = model.MiddleName,
                Organisation = model.Organisation,
                Designation = model.Designation,
                Avatar = filePath,
                PhoneNumber = model.PhoneNumber,
                EmailConfirmed = true,
                Title = model.Title,
                UniversityID = model.UniversityId,
                TypeOfMember = model.TypeOfMemberSelected,
                PostalAddress = model.PostalAddress,
                DietaryRequirements = model.DietaryRequirements,
                ExecutiveAssistantName = model.ExecutiveAssistantName,
                ExecutiveAssistantemail = model.ExecutiveAssistantemail
            };

            var result = UserManager.Create(user, "Boards@Express123");
            if (result.Succeeded)
            {

                //give the user default role of content provider
                //AddUserRole(model.Email, Guid.Parse(model.meetingTypeId));
                //SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                // var data = new { Success = "true", Message = "User has been created" };  //Need to fix this
                string code = UserManager.GenerateEmailConfirmationToken(user.Id);
                //string code = UserManager.GenerateUserToken("Email Confirmation", user.Id);
                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                //string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                List<string> userId = new List<string>();
                userId.Add(user.Email);
                oEmailProvider.sendMail(userId, "Welcome to Egenda. Please Confirm Your Account.", callbackUrl, "Welcome");
                // return Json(data, JsonRequestBehavior.AllowGet);
            }
            //AddErrors(result);
            //}
            // var results = new { Success = "False", Message = "User creation failed!!" };  //Need to fix this
            // return Json(results, JsonRequestBehavior.AllowGet);
            // If we got this far, something failed, redisplay form
            // return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("../Account/ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                //await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                List<string> userId = new List<string>();
                userId.Add(user.Email);
                //string passwordResetMessage = @"Thank you for your request to reset your password. Please click the link
                //                                below to reset your password  <a href=\" + callbackUrl + "\">Reset</a>.@";
                //passwordResetMessage = passwordResetMessage.Replace("@", "@" + System.Environment.NewLine);
                oEmailProvider.sendMail(userId, "Reset Password", callbackUrl, "Reset");
                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }
        /*user profile*/
        public ActionResult UserProfile(string id)
        {
            var currentUser = User.Identity.GetUserId();
            ViewBag.id = currentUser;
            var userProfileInformation = UserManager.Users.ToList().Find(s => s.Id == currentUser);
            MeetingViewModels meetingViewModel = new MeetingViewModels();
            meetingViewModel.userProfile = userProfileInformation;
            return View(meetingViewModel);
        }
        [HttpPost]
        public JsonResult CheckUser(string email)
        {
            bool isExhits = true;
            string isExitsEmail = Regex.Replace(email, @"\s+", "");
            var user = db.AspNetUsers.FirstOrDefault(x => x.Email == isExitsEmail);
            if (user != null)
            {
                isExhits = false;
            }
            //bool isExhits = user != null ? false : true;
            return Json(isExhits, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// give role to existing user
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddUserRole(string email, Guid meetingTypeId)
        {
            var user = db.AspNetUsers.Where(x => x.Email == email).FirstOrDefault();
            var testId = user.Id;
            MeetingAttendeesController oController = new MeetingAttendeesController();
            try
            {
                bool suc = oController.AddToRoleAsync(user.Id.ToString(), "b3485279-ad4f-41ba-8d74-2ce8d02ac9cc", null, meetingTypeId);
                if (suc)
                {
                    var results = new { Success = "True", Message = "User Added." };
                    return Json(results, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var resultant = new { Success = "False", Message = "User couldn't be added." };
                    return Json(resultant, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex) { }

            var result = new { Success = "False", Message = "User couldn't be added." };
            return Json(result, JsonRequestBehavior.AllowGet);

        }
        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }


}
