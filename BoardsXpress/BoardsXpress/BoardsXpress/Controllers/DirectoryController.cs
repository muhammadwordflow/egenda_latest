﻿using BoardsXpress.Models;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BoardsXpress.Controllers
{
    [Authorize]
    public class DirectoryController : Controller
    {
        // GET: Directory
        private ApplicationUserManager _userManager;
        // GET: Directory

        public ActionResult Index()
        {
            List<ApplicationUser> user = UserManager.Users.Where(w => w.FirstName != "Super Admin").ToList();
            return View(user);
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }
}