﻿using AutoMapper;
using BoardsXpress.Models;
using BoardsXpress.Repository;
using BoardsXpress.Util;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Web.Mvc;
using System.Xml;

namespace BoardsXpress.Controllers
{
    public class FeedManagerController : BaseController
    {
        // GET: FeedManager
        public ActionResult Index()
        {
            var feedsViewModel = new FeedsViewModel();

            foreach (var feed in db.tblRssFeedManagers.ToList())
            {
                try
                {
                    XmlDocument xmldoc = new XmlDocument();
                    XmlNodeList items = default(XmlNodeList);
                    
                    xmldoc.Load(feed.feedUrl);

                    items = xmldoc.SelectNodes("/rss/channel/item");

                    string topArticles = ReadTopArticles(items);

                    if (!feedsViewModel.Feeds.ContainsKey(feed.feeProvider))
                        feedsViewModel.Feeds.Add(feed.feeProvider, topArticles);
                }
                catch (Exception) { throw; }
            }

            return View(feedsViewModel);
        }

        public ActionResult ManageFeed(Guid id)
        {
            var meetingType = db.tblMeetingTypes.First(m => m.meetingTypeId == id);

            var manageFeedViewModel = new ManageFeedViewModel
            {
                MeetingTypeId = id,
                MeetingName = meetingType.meetingType
            };

            manageFeedViewModel.ListFeedManagerViewModel = Mapper.Map<List<tblRssFeedManager>, List<FeedManagerViewModel>>(
                db.tblRssFeedManagers.Where(x => x.meetingId == id).ToList()
            );
            
            return View(manageFeedViewModel);
        }

        // POST: /FeedManager/ManageFeed
        [HttpPost]
        [AllowAnonymous]    
        public JsonResult ManageFeed(FeedManagerViewModel model)
        {
            if(!IsValidFeedUrl(model.FeedURL))
                return Json(new { success = false, message = "Feed provider could not be added." });

            var feedManagerModel = new tblRssFeedManager()
                {
                    
                    feedUrl = model.FeedURL,
                    feeProvider = model.FeedProvider,
                    dateAdded = DateTime.Now,
                    meetingId = model.MeetingTypeId
                };

                try
                {
                    db.tblRssFeedManagers.Add(feedManagerModel);
                    db.SaveChanges();
                    return Json(new { success = true, message = "Feed provider added successfully." });
                }
                catch(Exception ex) { throw; }

        }


        /// <summary>
        /// check if the url contains feed
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="url"></param>
        /// <returns>bool</returns>
        public bool IsValidFeedUrl(string url)
        {
            bool isValid = true;
            try
            {
                XmlReader reader = XmlReader.Create(url);
                Rss20FeedFormatter formatter = new Rss20FeedFormatter();
                formatter.ReadFrom(reader);
                reader.Close();
            }
            catch
            {
                isValid = false;
            }

            return isValid;
        }
        
        private string ReadTopArticles(XmlNodeList items)
        {
            string title = string.Empty;
            string link = string.Empty;
            string desc = string.Empty;
            string pubDesc = string.Empty;
            string feed = string.Empty;
            string st = "";
            int i = 0;

            foreach (XmlNode item1 in items)
            {
                foreach (XmlNode node1 in item1.ChildNodes)
                {
                    if (node1.Name == "title")
                    {
                        title = node1.InnerText;
                    }
                    if (node1.Name == "link")
                    {
                        link = node1.InnerText;
                    }
                    if (node1.Name == "description")
                    {
                        desc = node1.InnerText;
                        if (desc.Length > 90)
                        {
                            pubDesc = desc.Substring(0, 90);
                        }
                        else
                        { pubDesc = desc; }
                    }
                }
                st += String.Format("<a target='_blank' href='{0}'>{1}</a><br />{2} ... <div style='border-bottom: 1px dotted #84acfd; padding-top:10px;'></div></br>", link, title, pubDesc);
                i += 1;
                if (i == 3)
                    break;
            }
            //lblBlogOutput.Text += st;
            feed += st;
            return feed;
        }

        #region Manage Feed 


        // POST: /FeedManager/ManageFeed
        [HttpPost]
        public JsonResult Create(Guid meetingTypeId, string feedProvider, string feedURL)
        {
            if (!IsValidFeedUrl(feedURL))
                return Json(new JsonResponse { Title = "Error", Success = false, Message = "Feed provider could not be added." });

            var feed = new tblRssFeedManager()
            {
                meetingId = meetingTypeId,
                feedUrl = feedURL,
                feeProvider = feedProvider,
                dateAdded = DateTime.Now
            };

            try
            {
                db.tblRssFeedManagers.Add(feed);

                if (db.SaveChanges() == 0)
                    return Json(new JsonResponse { Title = "Error", Success = false, Message = "Feed provider could not be added." });

                return Json(new JsonResponse { Title = "Success", Success = true, Message = "Feed provider added successfully." });
            }
            catch (Exception)
            {
                return Json(new JsonResponse { Title = "Error", Success = false, Message = "Feed provider could not be added." });
            }
        }

        [HttpPost]
        public JsonResult Edit(int id, Guid meetingTypeId, string feedProvider, string feedURL)
        {
            try
            {
                if (!IsValidFeedUrl(feedURL))
                    return Json(new JsonResponse { Title = "Error", Success = false, Message = "Feed provider could not be added." });

                var feed = db.tblRssFeedManagers.FirstOrDefault(m => m.id == id && m.meetingId == meetingTypeId);

                if (feed != null)
                {
                    db.Entry(feed).State = EntityState.Modified;

                    feed.meetingId = meetingTypeId;
                    feed.feedUrl = feedURL;
                    feed.feeProvider = feedProvider;
                    feed.dateAdded = DateTime.Now;
                }

                if (db.SaveChanges() == 0)
                    return Json(new JsonResponse { Title = "Error", Success = false, Message = "Feed provider could not be added." });

                return Json(new JsonResponse { Title = "Success", Success = true, Message = "Feed provider added successfully." });
            }
            catch (Exception)
            {
                return Json(new JsonResponse { Title = "Error", Success = false, Message = "Feed provider could not be added." });
            }
        }

        [HttpPost]
        public JsonResult DeleteFeed(int id, Guid meetingTypeId)
        {
            var tblFeed = db.tblRssFeedManagers.First(m => m.id == id && m.meetingId == meetingTypeId);

            db.tblRssFeedManagers.Remove(tblFeed);

            try
            {
                if (db.SaveChanges() == 0)
                {
                    return Json(new { Success = true, Message = "Feed could not be deleted." });
                }
            }
            catch (Exception ex) { throw; }

            return Json(new { Success = true, Message = "Feed deleted sucessfully!" });
        }

        public ActionResult GetManageFeedList(Guid meetingTypeId)
        {
            var meetingType = db.tblMeetingTypes.First(m => m.meetingTypeId == meetingTypeId);

            var manageFeedViewModel = new ManageFeedViewModel
            {
                MeetingTypeId = meetingTypeId,
                MeetingName = meetingType.meetingType
            };

            manageFeedViewModel.ListFeedManagerViewModel = Mapper.Map<List<tblRssFeedManager>, List<FeedManagerViewModel>>(
                db.tblRssFeedManagers.Where(x => x.meetingId == meetingTypeId).ToList()
            );

            return PartialView("_ManageFeedList", manageFeedViewModel);
        }

        #endregion

    }
}