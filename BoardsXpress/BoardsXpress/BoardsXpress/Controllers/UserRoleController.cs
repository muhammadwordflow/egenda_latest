﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BoardsXpress.Models;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;

namespace BoardsXpress.Controllers
{
    [Authorize(Roles = "Super Admin,Meeting Administrator")]
    public class UserRoleController : Controller
    {
        private BoardExpressEntities db = new BoardExpressEntities();


        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        // GET: UserRole
        public ActionResult Index()
        {
            var roleViewModelList = new List<RoleViewModels>();
            var roleList = new List<ApplicationRole>();
            //  var result = task.WaitAndUnwrapException();
            try
            {
                roleList = RoleManager.Roles.ToList();
            }
            catch (Exception ex)
            {

                throw;
            }

            foreach (var item in roleList)
            {
                var roleView = new RoleViewModels();
                roleView.Id = item.Id;
                roleView.Name = item.Name;
                roleView.Description = item.Description;
                roleView.readAccess = item.readAccess;
                roleView.writeAccess = item.writeAccess;
                roleView.fullAccess = item.fullAccess;
                roleViewModelList.Add(roleView);


            }
            return View(roleViewModelList);
        }

        // GET: UserRole/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var role = await RoleManager.FindByIdAsync(id);
            var roleView = new RoleViewModels();
            roleView.Id = role.Id;
            roleView.Name = role.Name;
            roleView.Description = role.Description;
            roleView.readAccess = role.readAccess;
            roleView.writeAccess = role.writeAccess;
            roleView.fullAccess = role.fullAccess;
            return View(roleView);


        }

        // GET: UserRole/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserRole/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]

        public async Task<ActionResult> Create(RoleViewModels roleViewModel)
        {
            if (ModelState.IsValid)
            {
                var role = new ApplicationRole(roleViewModel.Name);
                role.Description = roleViewModel.Description;

                role.fullAccess = roleViewModel.fullAccess;
                role.writeAccess = roleViewModel.writeAccess;
                role.readAccess = roleViewModel.readAccess;
                IdentityResult roleresult;
                try
                {
                    roleresult = await RoleManager.CreateAsync(role);
                }
                catch (Exception ex)
                {

                    throw;
                }

                if (roleresult.Succeeded)
                {

                    return RedirectToAction("Index");
                }
                else
                {
                    return View();
                }

            }
            return View();
        }


        // GET: UserRole/Edit/5
        public async Task<ActionResult> Edit(string id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var role = await RoleManager.FindByIdAsync(id);
            if (role == null)
            {
                return HttpNotFound();
            }
            var roleView = new RoleViewModels();
            roleView.Id = role.Id;
            roleView.Name = role.Name;
            roleView.Description = role.Description;
            roleView.readAccess = role.readAccess;
            roleView.writeAccess = role.writeAccess;
            roleView.fullAccess = role.fullAccess;
            return View(roleView);
        }

        // POST: UserRole/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]

        public async Task<ActionResult> Edit(RoleViewModels roleModel)
        {
            if (ModelState.IsValid)
            {
                var role = await RoleManager.FindByIdAsync(roleModel.Id);
                role.Name = roleModel.Name;

                // Update the new Description property:
                role.Description = roleModel.Description;

                role.readAccess = roleModel.readAccess;
                role.writeAccess = roleModel.writeAccess;
                role.fullAccess = roleModel.fullAccess;
                await RoleManager.UpdateAsync(role);
                return RedirectToAction("Index");
            }
            return View();
        }

        // GET: UserRole/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var role = await RoleManager.FindByIdAsync(id);
            if (role == null)
            {
                return HttpNotFound();
            }
            var roleView = new RoleViewModels();
            roleView.Id = role.Id;
            roleView.Name = role.Name;
            roleView.Description = role.Description;
            roleView.readAccess = role.readAccess;
            roleView.writeAccess = role.writeAccess;
            roleView.fullAccess = role.fullAccess;
            return View(roleView);
        }

        // POST: UserRole/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var role = await RoleManager.FindByIdAsync(id);
            if (role == null)
            {
                return HttpNotFound();
            }
            var result = await RoleManager.DeleteAsync(role);

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        
        

    }


}
