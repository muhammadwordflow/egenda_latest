﻿using BoardsXpress.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace BoardsXpress.Controllers
{
    public class UserReportManagementController : BaseController
    {
        public ActionResult UserReport()
        {
            return View();
        }


        public ActionResult Index()
        {
            var inductionTraining = db.tblTrainings
                .Where(m => m.Training == true);
            return View();
        }


        #region Ajax call
        [HttpGet]
        public ActionResult GetUserReportList(string mtId, string sd, string ed, int? page)
        {
            if (mtId.Equals("undefined"))
            {
                mtId = null;
            }
            if (sd.Equals(""))
            {
                sd = null;
            }
            if (ed.Equals(""))
            {
                ed = null;
            }
            UserReportViewModel userReportViewModel = new UserReportViewModel();
            try
            {
                userReportViewModel.selectedMeetingTyeId = mtId == null ? (Guid?)null : new Guid(mtId);
                userReportViewModel.FstartDate = sd;
                userReportViewModel.FendDate = ed;
                userReportViewModel = GetDataFromDB(userReportViewModel);
                //AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
                //{
                //    UserName = User.Identity.GetUserName(),
                //    UserId = new Guid(User.Identity.GetUserId()),
                //    AfterData = JsonConvert.SerializeObject(userReportViewModel),
                //    BeforeData = JsonConvert.SerializeObject(userReportViewModel),
                //    IpAddress = Request.UserHostAddress,
                //    SessionId = HttpContext.Session.SessionID,
                //    ActionPerform = "GET",
                //    UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
                //};
                //auditTrailViewModel.AuditTrail();
                userReportViewModel.Pager = new Pager(userReportViewModel.ListUsers.Count(), page);
                userReportViewModel.ListUsers.Skip((userReportViewModel.Pager.CurrentPage - 1) * userReportViewModel.Pager.PageSize).Take(userReportViewModel.Pager.PageSize);

            }
            catch (Exception ex)
            {
                userReportViewModel.ErrorCode = ex.HResult;
                userReportViewModel.ErrorMessage = ex.Message;
            }

            return Json(userReportViewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetMeeingMeetingTypeId(string userId, Guid meetingTypeId)
        {
            MeetingViewModels meeting = new MeetingViewModels();
            meeting.ListMeetings = new List<MeetingViewModels>();
            try
            {
                var meetings = (from u in db.AspNetUserRoles
                                join
                                ur in db.tblMeetings on u.MeetingTypeId equals ur.meetingType
                                join
                                ma in db.tblMeetingAttendees on ur.meetingId equals ma.meetingId
                                where u.UserId == userId && u.MeetingTypeId == meetingTypeId
                                select new
                                {
                                    ur.startTime,
                                    ur.finishTime,
                                    ur.meetingDate,
                                    ur.meetingVenue,
                                    ma.userResponse
                                }).ToList();
                meetings.ForEach(item =>
                {
                    MeetingViewModels meetingViewModels = new MeetingViewModels
                    {
                        startTime = item.startTime,
                        finishTime = item.finishTime,
                        createdDate = item.meetingDate ?? DateTime.Now,
                        Venue = item.meetingVenue,
                        UserResponse = item.userResponse
                    };

                    meeting.ListMeetings.Add(meetingViewModels);
                });
            }
            catch (Exception ex)
            {
                meeting.ErrorCode = ex.HResult;
                meeting.ErrorMessage = ex.Message;
            }
            //AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
            //{
            //    UserName = User.Identity.GetUserName(),
            //    UserId = new Guid(User.Identity.GetUserId()),
            //    AfterData = JsonConvert.SerializeObject(meeting),
            //    BeforeData = JsonConvert.SerializeObject(meeting),
            //    IpAddress = Request.UserHostAddress,
            //    SessionId = HttpContext.Session.SessionID,
            //    ActionPerform = "GET",
            //    UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
            //};
            //auditTrailViewModel.AuditTrail();
            return Json(meeting, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Audit Report

        [HttpGet]
        public JsonResult GetAuditReport(int? page, Guid? meetingType, Guid? meetingDate)
        {
            AuditTrailModel audit = new AuditTrailModel { AuditTrailList = new List<AuditTrailViewModel>() };

            try
            {
                audit.AuditTrailList = GetAuditFromDb(meetingType, meetingDate);
                audit.Pager = new Pager(audit.AuditTrailList.Count(), page);
                audit.AuditTrailList.Skip((audit.Pager.CurrentPage - 1) * audit.Pager.PageSize).Take(audit.Pager.PageSize);

            }
            catch (Exception ex)
            {
                audit.ErrorCode = ex.HResult;
                audit.ErrorMessage = ex.Message;
            }

            return Json(audit, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
       /* public JsonResult GetAttendanceReport(int? page, Guid? userId, Guid? meetingTypeid)
        {
            UserReportViewModel userReportViewModel = new UserReportViewModel();
            try
            {

                userReportViewModel.ListUsers = new List<UserManagementViewModels>();
                var result = db.StoreProcedureUserAttendanceReport(userId, meetingTypeid).ToList();
                foreach (var item in result)
                {
                    UserManagementViewModels userManagementViewModels = new UserManagementViewModels();
                    userManagementViewModels.UserId = item.UserId;
                    userManagementViewModels.FirstName = FindUserDetails(item.UserId);
                    userManagementViewModels.MeetingTypeId = item.meetingId;
                    userManagementViewModels.MeetingType = item.meetingType;
                    userManagementViewModels.EndDate = item.meetingDate;
                    userManagementViewModels.UserResponse = item.userResponse;
                    userReportViewModel.ListUsers.Add(userManagementViewModels);

                }
                var user = db.AspNetUsers.Where(x => x.FirstName != "Super Admin").ToList();
                userReportViewModel.UserList = new List<UserViewModels>();
                user.ForEach(item =>
                {
                    userReportViewModel.UserList.Add(new UserViewModels
                    {
                        userId = new Guid(item.Id),
                        firstName = item.FirstName
                    });

                });
                var meetingType = db.tblMeetingTypes.ToList();
                userReportViewModel.MeetingTypes = new List<MeetingTypeViewModel>();
                meetingType.ForEach(item =>
                {
                    userReportViewModel.MeetingTypes.Add(new MeetingTypeViewModel
                    {
                        meetingTypeId = item.meetingTypeId,
                        meetingType = item.meetingType
                    });
                });
                userReportViewModel.Pager = new Pager(userReportViewModel.ListUsers.Count(), page);
                userReportViewModel.ListUsers.Skip((userReportViewModel.Pager.CurrentPage - 1) * userReportViewModel.Pager.PageSize).Take(userReportViewModel.Pager.PageSize);

            }
            catch (Exception ex)
            {
                userReportViewModel.ErrorCode = ex.HResult;
                userReportViewModel.ErrorMessage = ex.Message;
            }

            return Json(userReportViewModel, JsonRequestBehavior.AllowGet);
        }
        */
        private string FindUserDetails(string userId)
        {
            var dbResult = db.AspNetUsers.FirstOrDefault(x => x.Id == userId);
            if (dbResult != null)
            {
                return dbResult.FirstName;
            }
            return null;

        }

        [HttpGet]
        public JsonResult GetMeetingType()
        {
            List<MeetingTypeViewModel> meetingTypeViewModel = new List<MeetingTypeViewModel>();
            try
            {
                var meetingType = db.tblMeetingTypes.ToList();

                meetingType.ForEach(item =>
                {
                    meetingTypeViewModel.Add(new MeetingTypeViewModel
                    {
                        createdDate = item.createdDate,
                        meetingAbbreviation = item.meetingAbbreviation,
                        meetingType = item.meetingType,
                        meetingTypeId = item.meetingTypeId,
                        useAbbreviation = item.useAbbreviation
                    });
                });
            }
            catch (Exception ex)
            {
            }

            return Json(meetingTypeViewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetMeetingByMeetingType(Guid? meetingType)
        {
            List<MeetingViewModels> meetingViewModels = new List<MeetingViewModels>();
            try
            {
                var meeting = db.tblMeetings.Where(x => x.meetingType == meetingType).ToList();
                meeting.ForEach(item =>
                {
                    meetingViewModels.Add(new MeetingViewModels
                    {
                        meetingTypeId = item.meetingType,
                        meetingDate = item.meetingDate ?? DateTime.Now,
                        meetingId = item.meetingId
                    });
                });

            }
            catch (Exception ex)
            {
            }

            return Json(meetingViewModels, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Private Method

        private List<AuditTrailViewModel> GetAuditFromDb(Guid? meetingType, Guid? meetingDate)
        {
            List<AuditTrail> auditTrail = new List<AuditTrail>();
            List<AuditTrailViewModel> auditTrailViewModel = new List<AuditTrailViewModel>();

            if (meetingType != null && meetingDate != null)
            {
                auditTrail = db.AuditTrails.Where(x => x.MeetingTypeId == meetingType && x.MeetingId == meetingDate).ToList();
            }
            else if (meetingType != null)
            {
                auditTrail = db.AuditTrails.Where(x => x.MeetingTypeId == meetingType).ToList();
            }
            else
            {
                auditTrail = db.AuditTrails.ToList();
            }

            foreach (var item in auditTrail)
            {
                AuditTrailViewModel auditlist = new AuditTrailViewModel
                {
                    UrlAccess = item.UrlAccess,
                    SessionId = item.SessionId,
                    AfterData = item.AfterData,
                    BeforeData = item.BeforeData,
                    IpAddress = item.IpAddress,
                    UserId = item.UserId,
                    UserName = item.UserName,
                    TimeAccessed = item.TimeAccessed,
                    ActionPerform = item.ActionPerform,
                    Name = db.AspNetUsers.FirstOrDefault(x => x.Id == item.UserId.ToString())?.FirstName?.ToString()
                };
                auditTrailViewModel.Add(auditlist);
            }
            return auditTrailViewModel;
        }

        private UserReportViewModel GetDataFromDB(UserReportViewModel model)
        {
            UserReportViewModel userReportViewModel = new UserReportViewModel();
            userReportViewModel.ListUsers = new List<UserManagementViewModels>();
            var result = db.StoreProcedureUserInRoleAssign(model.selectedMeetingTyeId, model.StartDate, model.EndDate).ToList();
            foreach (var item in result)
            {
                UserManagementViewModels userManagementViewModels = new UserManagementViewModels();
                userManagementViewModels.UserId = item.UserID;
                userManagementViewModels.FirstName = item.FirstName;
                userManagementViewModels.MeetingTypeId = item.MeetingType == null ? new Guid(item.MeetingType) : item.MeetingTypeId;
                userManagementViewModels.MeetingType = item.MeetingType;
                userManagementViewModels.StartDate = item.StartDate;
                userManagementViewModels.EndDate = item.EndDate;
                userManagementViewModels.FormatStartDate = userManagementViewModels.StartDate.ToString("dd/MM/yyyy");
                userManagementViewModels.FormatEndDate = userManagementViewModels.StartDate.ToString("dd/MM/yyyy");
                userManagementViewModels.Roles = new List<RoleViewModels>();
                userManagementViewModels.Roles.Add(new RoleViewModels
                {
                    Id = item.RoleID,
                    Name = item.RoleName
                });
                userReportViewModel.ListUsers.Add(userManagementViewModels);
            }

            userReportViewModel.MeetingTypes = new List<MeetingTypeViewModel>();

            var meetingTypes = (from x in db.tblMeetingTypes select x);
            foreach (var item in meetingTypes)
            {

                userReportViewModel.MeetingTypes.Add(new MeetingTypeViewModel
                {
                    meetingTypeId = item.meetingTypeId,
                    meetingType = item.meetingType,
                    meetingAbbreviation = item.meetingAbbreviation,
                });
            }

            return userReportViewModel;
        }

        #endregion

        #region User Profile Report

        public JsonResult GetUserProfileReport(int? page)
        {
            UserProfileReportModel userProfile = new UserProfileReportModel();

            try
            {
                var userProfileDb = db.AspNetUsers.Where(x => x.FirstName != "Super Admin").ToList();
                userProfile.UserProfileList = new List<RegisterViewModel>();
                userProfileDb.ForEach(item =>
                {
                    userProfile.UserProfileList.Add(
                        new RegisterViewModel
                        {
                            Username = item.FirstName,
                            Id = item.Id,
                            Email = item.UserName,
                            PhoneNumber = item.PhoneNumber,
                            Designation = item.Designation,
                            TypeOfMemberSelected = item.TypeOfMember,
                            UniversityId = item.UniversityID,
                            ExecutiveAssistantName = item.ExecutiveAssistantName,
                            ExecutiveAssistantemail = item.ExecutiveAssistantemail,
                            Gender = item.Gender
                        });
                });
                userProfile.Pager = new Pager(userProfile.UserProfileList.Count(), page);
                userProfile.UserProfileList.Skip((userProfile.Pager.CurrentPage - 1) * userProfile.Pager.PageSize).Take(userProfile.Pager.PageSize);

            }
            catch (Exception ex)
            {
                userProfile.ErrorCode = ex.HResult;
                userProfile.ErrorMessage = ex.Message;
            }

            return Json(userProfile, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUserProfileById(string id)
        {
            RegisterViewModel userProfile = new RegisterViewModel();

            try
            {
                var userProfileDb = db.AspNetUsers.FirstOrDefault(x => x.Id == id);

                userProfile.Username = userProfileDb.FirstName;
                userProfile.Email = userProfileDb.UserName;
                userProfile.PhoneNumber = userProfileDb.PhoneNumber;
                userProfile.Designation = userProfileDb.Designation;
                userProfile.TypeOfMemberSelected = userProfileDb.TypeOfMember;
                userProfile.UniversityId = userProfileDb.UniversityID;
                userProfile.ExecutiveAssistantName = userProfileDb.ExecutiveAssistantName;
                userProfile.ExecutiveAssistantemail = userProfileDb.ExecutiveAssistantemail;
                userProfile.Gender = userProfileDb.Gender;
                userProfile.FirstName = userProfileDb.FirstName;
                userProfile.DietaryRequirements = userProfileDb.DietaryRequirements;
                userProfile.Avatar = userProfileDb.Avatar;
                userProfile.Title = userProfileDb.Title;
            }
            catch (Exception ex)
            {
                userProfile.ErrorCode = ex.HResult;
                userProfile.ErrorMessage = ex.Message;
            }

            return Json(userProfile, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}