﻿using BoardsXpress.Models;
using BoardsXpress.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace BoardsXpress.Controllers
{
    [Authorize]
    public class PublishController : Controller
    {
        private BoardExpressEntities db = new BoardExpressEntities();
        HelperRepo repo = new HelperRepo();

        // GET: Publish
        [HttpGet]
        public ActionResult Index(string meetingId)
        {
            try
            {
                PublishViewModel newPublishModel = new PublishViewModel(meetingId);
                //AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
                //{
                //    UserName = User.Identity.GetUserName(),
                //    UserId = new Guid(User.Identity.GetUserId()),
                //    AfterData = JsonConvert.SerializeObject(newPublishModel, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }),
                //    BeforeData = JsonConvert.SerializeObject(null),
                //    IpAddress = Request.UserHostAddress,
                //    SessionId = HttpContext.Session.SessionID,
                //    ActionPerform = "GET",
                //    UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
                //};
                //auditTrailViewModel.AuditTrail();
                return View(newPublishModel);
            }
            catch (Exception ex)
            {
                return Redirect("/MeetingTypes");
            }
        }

        [HttpPost]
        public ActionResult Index()
        {
            try
            {
                var meetingTypeId = Guid.Parse(Request.Form["meetingTypeId"]);
                var meetingId = Guid.Parse(Request.Form["meetingId"]);

                // Get the specific meeting
                var tblMeeting = db.tblMeetings.First(x => x.meetingType == meetingTypeId && x.meetingId == meetingId);

                // Apply the needed info
                var publishViewModel = new PublishViewModel
                {
                    meetingId = meetingId,
                    meetingTypeId = tblMeeting.meetingType,
                    meetingTypeName = tblMeeting.tblMeetingType.meetingType,
                    meetingDate = tblMeeting.meetingDate.Value.ToString("dd/MM/yyyy"),
                    status = db.tblPublishLibraries.Any(x => x.meetingId == meetingId) ? "Published" : "Draft"
                };
                //AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
                //{
                //    UserName = User.Identity.GetUserName(),
                //    UserId = new Guid(User.Identity.GetUserId()),
                //    AfterData = JsonConvert.SerializeObject(publishViewModel, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }),
                //    BeforeData = JsonConvert.SerializeObject(null),
                //    IpAddress = Request.UserHostAddress,
                //    SessionId = HttpContext.Session.SessionID,
                //    ActionPerform = "GET",
                //    UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
                //};
                //auditTrailViewModel.AuditTrail();
                return View(publishViewModel);
            }
            catch (Exception ex)
            {
                return Redirect("/MeetingTypes");
            }
        }

        private List<AgendaItemsViewModels> GetAgendaItemsWithDocuments(Guid meetingId, tblMeeting tblMeeting)
        {
            // Find all agenda items by meetingId
            var agendaItemList = db.tblAgendaItems
                .Where(w => w.meetingTypeId == meetingId)
                .Include(m => m.tblAgendaItemsRelateds)
                .OrderBy(m => m.itemOrder);
            
            var agendaItemsViewModelList = new List<AgendaItemsViewModels>();

            foreach (var item in agendaItemList)
            {
                var agendaItemsRelatedList = item
                    .tblAgendaItemsRelateds
                    .OrderBy(m => m.itemOrder)
                    .ToList();
                
                var agendaItemsViewModel = new AgendaItemsViewModels
                {
                    agendaId = item.Id,
                    agendaName = item.agendaItem,
                    meetingTypeId = tblMeeting.tblMeetingType.meetingTypeId,
                    meetingType = tblMeeting.tblMeetingType.meetingType,
                    meetingId = meetingId,
                    itemOrder = item.itemOrder,
                    AgendaItemsRelatedList = AutoMapper.Mapper.Map<List<tblAgendaItemsRelated>, List<AgendaItemsRelatedViewModel>>(agendaItemsRelatedList)
                };

                // Find if the parent has any document attached
                var document = item.tblDocumentLibraries.FirstOrDefault(x => x.agendaItem == item.Id && x.agendaItemRelatedId == null);

                if (document != null)
                {
                    if (document.docCreatedDate != null)
                        agendaItemsViewModel.PublishItemViewModel = new PublishItemViewModel
                        {
                            docId = document.docId,
                            meetingId = document.meetingId,
                            docAuthor = document.userId,
                            publisher = db.AspNetUsers.Where(s => s.Id == document.userId).Select(p => p.UserName)
                                .FirstOrDefault(),
                            agendaItemId = document.agendaItem,
                            agendaItem = document.tblAgendaItem.agendaItem,
                            agendaItemRelatedId = document.agendaItemRelatedId,
                            docName = document.docName,
                            docPath = document.docPath,
                            docCreatedDate = (DateTime) document.docCreatedDate,
                            approve = document.tblDocReviews.Any(x => { return x.approve != null && x.approve.Value; }),
                            review = document.tblDocReviews.Any(x => { return x.review != null && x.review.Value; }),
                            printRestriction = document.printRestriction,
                            draftMinutesItem = document.tblAgendaItem.draftMinutesItem
                        };
                }

                // Find if the children have documents attached.
                foreach (var agendaItemRelated in agendaItemsViewModel.AgendaItemsRelatedList)
                {
                    var relatedDocument = item.tblDocumentLibraries.FirstOrDefault(x => x.agendaItem == item.Id && x.agendaItemRelatedId == agendaItemRelated.Id);

                    if (relatedDocument != null)
                    {
                        agendaItemRelated.PublishItemViewModel = new PublishItemViewModel
                        {
                            docId = relatedDocument.docId,
                            meetingId = relatedDocument.meetingId,
                            docAuthor = relatedDocument.userId,
                            publisher = db.AspNetUsers.Where(s => s.Id == relatedDocument.userId).Select(p => p.UserName).FirstOrDefault(),
                            agendaItemId = relatedDocument.agendaItem,
                            agendaItem = relatedDocument.tblAgendaItemsRelated.name,
                            agendaItemRelatedId = relatedDocument.agendaItemRelatedId,
                            docName = relatedDocument.docName,
                            docPath = relatedDocument.docPath,
                            docCreatedDate = (DateTime)relatedDocument.docCreatedDate,
                            approve = relatedDocument.tblDocReviews.Any(x => x.approve.Value),
                            review = relatedDocument.tblDocReviews.Any(x => x.review.Value),
                            printRestriction = relatedDocument.printRestriction,
                            draftMinutesItem = relatedDocument.tblAgendaItem.draftMinutesItem
                        };
                    }
                }

                agendaItemsViewModelList.Add(agendaItemsViewModel);
            }

            return agendaItemsViewModelList;
        }

        private void GetPublishedDocumentsList(Guid meetingId, PublishViewModel publishViewModel)
        {
            var tblDocumentLibraries = db.tblDocumentLibraries
                                .Where(m =>
                                    m.meetingId == meetingId &&
                                    m.agendaItemRelatedId == null)
                                .Include(m => m.tblDocReviews);

            foreach (var agendaItemDocument in tblDocumentLibraries)
            {
                var publishItemViewModel = new PublishItemViewModel
                {
                    docId = agendaItemDocument.docId,
                    meetingId = agendaItemDocument.meetingId,
                    docAuthor = agendaItemDocument.userId,
                    publisher = db.AspNetUsers.Where(s => s.Id == agendaItemDocument.userId).Select(p => p.UserName).FirstOrDefault(),
                    agendaItemId = agendaItemDocument.agendaItem,
                    agendaItem = agendaItemDocument.tblAgendaItem.agendaItem,
                    agendaItemRelatedId = agendaItemDocument.agendaItemRelatedId,
                    docName = agendaItemDocument.docName,
                    docPath = agendaItemDocument.docPath,
                    docCreatedDate = (DateTime)agendaItemDocument.docCreatedDate,
                    approve = agendaItemDocument.tblDocReviews.Any(x => x.approve.Value),
                    review = agendaItemDocument.tblDocReviews.Any(x => x.review.Value)
                };

                publishViewModel.PublishItemViewModelList.Add(publishItemViewModel);
            }
        }

        private void GetPublishedRelatedDocumentsList(Guid meetingId, PublishViewModel publishViewModel)
        {
            var tblRelatedDocumentLibraries = db.tblDocumentLibraries
                                .Where(m =>
                                    m.meetingId == meetingId &&
                                    m.agendaItemRelatedId != null)
                                .Include(m => m.tblDocReviews);

            foreach (var agendaItemRelatedDocument in tblRelatedDocumentLibraries)
            {
                // Get the parent Agenda Item
                var agendaItemRelated = publishViewModel
                    .PublishItemViewModelList
                    .FirstOrDefault(m =>
                        m.agendaItemId == agendaItemRelatedDocument.agendaItem &&
                        m.agendaItemRelatedId == null);

                if (agendaItemRelated != null)
                {
                    agendaItemRelated.ListRelatedDocuments.Add(new PublishItemViewModel
                    {
                        docId = agendaItemRelatedDocument.docId,
                        meetingId = agendaItemRelatedDocument.meetingId,
                        docAuthor = agendaItemRelatedDocument.userId,
                        publisher = db.AspNetUsers.Where(s => s.Id == agendaItemRelatedDocument.userId).Select(p => p.UserName).FirstOrDefault(),
                        agendaItemId = agendaItemRelatedDocument.agendaItem,
                        agendaItem = agendaItemRelatedDocument.tblAgendaItemsRelated.name,
                        agendaItemRelatedId = agendaItemRelatedDocument.agendaItemRelatedId,
                        docName = agendaItemRelatedDocument.docName,
                        docPath = agendaItemRelatedDocument.docPath,
                        docCreatedDate = (DateTime)agendaItemRelatedDocument.docCreatedDate,
                        approve = agendaItemRelatedDocument.tblDocReviews.Any(x => x.approve.Value),
                        review = agendaItemRelatedDocument.tblDocReviews.Any(x => x.review.Value)
                    });
                }
            }
        }

        [HttpPost]
        public JsonResult publishContent(string[] data, Guid meetingID, string draftMinutesItem)
        {
            int i = 0;

            //check if previously published, if true then update
            var existingRecord = db.tblPublishLibraries.Where(x => x.meetingId == meetingID);
            var recordsInDbForMeeting = db.tblDocumentLibraries.Where(x => x.meetingId == meetingID);
            if (existingRecord.GetEnumerator().MoveNext() == false) //hasn't been published yet
            {
                using (db = new BoardExpressEntities())
                {
                    foreach (var item in data)
                    {
                        if (data.Count() != 0)
                        {
                            if (item != "")
                            {
                                try
                                {
                                    var docId = Guid.Parse(item);

                                    var document = db.tblDocumentLibraries.First(m => m.docId == docId);

                                    tblPublishLibrary oLib = new tblPublishLibrary()
                                    {
                                        docId = document.docId,
                                        meetingId = meetingID,
                                        agendaItemId = document.agendaItem,
                                        agendaItemRelatedId = document.agendaItemRelatedId,
                                        publishDate = DateTime.Now,
                                        publishOrder = i,
                                        publisher = User.Identity.Name.ToString()
                                    };
                                    db.tblPublishLibraries.Add(oLib);
                                    i++;
                                }
                                catch (Exception ex) { throw; }
                            }

                        }

                    }
                    try
                    {

                        db.SaveChanges();
                        //send notification to users
                        EmailProviderController emailController = new EmailProviderController();
                        var meetingDetail = db.tblMeetings.Where(x => x.meetingId == meetingID).ToList();
                        var meetingDate = meetingDetail.Select(x => x.meetingDate).FirstOrDefault().Value.ToString("dd MMMM, yyyy");
                        var meetingType = meetingDetail.Select(x => x.meetingType).FirstOrDefault();
                        var meetingName = db.tblMeetingTypes.Where(x => x.meetingTypeId == meetingType);
                        string mailBody = "We are pleased to advise that the papers for the " +meetingName.Select(x => x.meetingType).FirstOrDefault()+" on "+ meetingDetail.Select(x => x.meetingDate).FirstOrDefault().Value.ToString("dd/MM/yyyy") + "  have been published to the meeting website and are ready for your review.";
                        emailController.NotifyUsers("Papers have been published.", mailBody, meetingDetail.Select(x => x.meetingType).FirstOrDefault());

                        if (draftMinutesItem == "True")
                        {
                            EmailProviderController emailControllerDraftMinutes = new EmailProviderController();
                            var meetingDetailDraft = db.tblMeetings.Where(x => x.meetingId == meetingID).ToList();
                            var meetingDateDraft = meetingDetail.Select(x => x.meetingDate).FirstOrDefault().Value.ToString("dd MMMM, yyyy");
                            var meetingTypeDraft = meetingDetail.Select(x => x.meetingType).FirstOrDefault();
                            var meetingNameDraft = db.tblMeetingTypes.Where(x => x.meetingTypeId == meetingType);
                            string mailBodyDraft = "We are pleased to advise that the Draft Minutes for the " + meetingName.Select(x => x.meetingType).FirstOrDefault() + " on " + meetingDetail.Select(x => x.meetingDate).FirstOrDefault().Value.ToString("dd/MM/yyyy") + "  have been published to the meeting website and are ready for your review.";
                            emailControllerDraftMinutes.NotifyUsers("Draft Minutes have been published.", mailBodyDraft, meetingDetail.Select(x => x.meetingType).FirstOrDefault());
                        }

                    }
                    catch (Exception ex) { throw; }

                }
            }

            else
            {
                using (db = new BoardExpressEntities())
                {
                    //meetingID already exist update the existin record
                    foreach (var item in data)
                    {
                        if (data.Count() != 0)
                        {
                            if (item != "")
                            {
                                if (existingRecord.Count() != recordsInDbForMeeting.Count()) //check if new doc has been added after published first time
                                {
                                    var docId = Guid.Parse(item);

                                    var document = db.tblDocumentLibraries.First(m => m.docId == docId);

                                    var recordAsList = existingRecord.Select(x => x.docId).ToList();
                                    if (!recordAsList.Contains(Guid.Parse(item)))
                                    {
                                        tblPublishLibrary oLib = new tblPublishLibrary()
                                        {
                                            docId = docId,
                                            meetingId = meetingID,
                                            agendaItemId = document.agendaItem,
                                            publishDate = DateTime.Now,
                                            publishOrder = i,
                                            publisher = User.Identity.Name.ToString()
                                        };
                                        db.tblPublishLibraries.AddOrUpdate(oLib);


                                    }

                                }
                                try
                                {
                                    Guid docId = Guid.Parse(item);
                                    var docLevel = existingRecord.Where(x => x.docId == docId).FirstOrDefault();
                                    if (docLevel != null)
                                    {
                                        docLevel.publishDate = DateTime.Now;
                                        docLevel.publishOrder = i;
                                        docLevel.publisher = User.Identity.Name.ToString();
                                    }
                                    i++;
                                }
                                catch (Exception ex) { throw; }
                            }

                        }

                    }
                    try
                    {

                        db.SaveChanges();
                        //send users notification
                        EmailProviderController emailController = new EmailProviderController();
                        var meetingDetail = db.tblMeetings.Where(x => x.meetingId == meetingID).ToList();
                        var meetingType = meetingDetail.Select(x => x.meetingType).FirstOrDefault();
                        var meetingName = db.tblMeetingTypes.Where(x => x.meetingTypeId == meetingType);
                        string mailBody = "We are pleased to advise that the papers for the " + meetingName.Select(x => x.meetingType).FirstOrDefault() + " on " + meetingDetail.Select(x => x.meetingDate).FirstOrDefault().Value.ToString("dd/MM/yyyy") + "  have been published to the meeting website and are ready for your review.";

                        emailController.NotifyUsers("Papers have been published.", mailBody, meetingDetail.Select(x => x.meetingType).FirstOrDefault());


                        if (draftMinutesItem == "True")
                        {
                            EmailProviderController emailControllerDraftMinutes = new EmailProviderController();
                            var meetingDetailDraft = db.tblMeetings.Where(x => x.meetingId == meetingID).ToList();
                            var meetingDateDraft = meetingDetail.Select(x => x.meetingDate).FirstOrDefault().Value.ToString("dd MMMM, yyyy");
                            var meetingTypeDraft = meetingDetail.Select(x => x.meetingType).FirstOrDefault();
                            var meetingNameDraft = db.tblMeetingTypes.Where(x => x.meetingTypeId == meetingType);
                            string mailBodyDraft = "We are pleased to advise that the Draft Minutes for the " + meetingName.Select(x => x.meetingType).FirstOrDefault() + " on " + meetingDetail.Select(x => x.meetingDate).FirstOrDefault().Value.ToString("dd/MM/yyyy") + "  have been published to the meeting website and are ready for your review.";
                            emailControllerDraftMinutes.NotifyUsers("Draft Minutes have been published.", mailBodyDraft, meetingDetail.Select(x => x.meetingType).FirstOrDefault());
                        }

                    }
                    catch (Exception ex) { throw; }
                }


            }

            var results = new { Success = "True", Message = "Documents Published Successfully." };
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult updateReview(
            string review, 
            bool status, 
            Guid docId, 
            Guid meetingId,
            int agendaItemId,
            int? agendaItemRelatedId)
        {
            try
            {
                var model = db.tblDocReviews.Where(x => x.docId == docId).FirstOrDefault();
                if (model != null)
                {
                    if (review == "Review")
                        model.review = status;
                    else
                        model.approve = status;
                }
                else
                {
                    model = new tblDocReview
                    {
                        docId = docId,
                        meetingId = meetingId,
                        agendaItemId = agendaItemId,
                        agendaItemRelatedId = agendaItemRelatedId,
                        approvalDate = DateTime.Now
                    };
                    if (review == "Review")
                        model.review = status;
                    else
                        model.approve = status;


                    
                    db.tblDocReviews.Add(model);
                }


                db.SaveChanges();

                //AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
                //{
                //    UserName = User.Identity.GetUserName(),
                //    UserId = new Guid(User.Identity.GetUserId()),
                //    AfterData = JsonConvert.SerializeObject(model, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }),
                //    BeforeData = tempReviewData,
                //    IpAddress = Request.UserHostAddress,
                //    SessionId = HttpContext.Session.SessionID,
                //    ActionPerform = "EDIT",
                //    UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
                //};
                //auditTrailViewModel.AuditTrail();

            }
            catch (Exception ex) { }

            var result = new { Success = "False", Message = "Could not change status!!" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //delete document from library
        [HttpPost]
        public JsonResult DeleteDocument(Guid docId, Guid meetingId, int agendaItemId)
        {
            var currentTransaction = db.Database.CurrentTransaction ?? db.Database.BeginTransaction();

            try
            {
                using (var dbContextTransaction = currentTransaction)
                {
                    try
                    {
                        var tblDocumentLibrary = db.tblDocumentLibraries
                            .Include(m => m.tblMeeting)
                            .Include(m => m.tblPublishLibraries)
                            .Include(m => m.tblDocReviews)
                            .Include(m => m.tblHtmlLibraries)
                            .First(m => 
                                m.docId == docId &&
                                m.meetingId == meetingId &&
                                m.agendaItem == agendaItemId
                            );

                        // Delete the file and folder.
                        DirectoryInfo info = new DirectoryInfo(Server.MapPath("~/Documents/") + "/" + tblDocumentLibrary.tblMeeting.meetingType);

                        // Get the file name without extension and adds a wildcard
                        var fileNameWithWildcard = string.Format("{0}*", Path.GetFileNameWithoutExtension(tblDocumentLibrary.docName));

                        if (info.Exists)
                        {
                            foreach (var file in info.GetFiles(fileNameWithWildcard))
                                file.Delete();

                            if (info.GetFiles().Count() == 0)
                                info.Delete(true);
                        }
                        var tblData = db.tblMeetings.FirstOrDefault(x => x.meetingId == meetingId);
                        Guid? meetingTypeId = null;
                        if (tblData!=null)
                        {
                            meetingTypeId =tblData.meetingType;
                        }
    
                        db.tblDocumentLibraries.Remove(tblDocumentLibrary);
                        
                        if (db.SaveChanges() == 0)
                        {
                            dbContextTransaction.Rollback();
                            return Json(new { Success = false, Message = "Document could not be deleted." }, JsonRequestBehavior.AllowGet);
                        }
                        
                        dbContextTransaction.Commit();
                        AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
                        {
                            UserName = User.Identity.GetUserName(),
                            UserId = new Guid(User.Identity.GetUserId()),
                            AfterData = tblDocumentLibrary.docName,//JsonConvert.SerializeObject(tblDocumentLibrary.docName, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }),
                            BeforeData = null, //JsonConvert.SerializeObject(null),
                            IpAddress = Request.UserHostAddress,
                            SessionId = HttpContext.Session.SessionID,
                            ActionPerform = "Delete",
                            MeetingsId = meetingId,
                            MeetingTypeId = meetingTypeId,
                            TimeAccessed = DateTime.Now,
                            UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
                        };
                        auditTrailViewModel.AuditTrail();
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        return Json(new { Success = false, Message = "Document could not be deleted." }, JsonRequestBehavior.AllowGet);
                    }
                }               
                return Json(new { Success = true, Message = "Document was deleted successfully!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex) {
                return Json(new { Success = false, Message = "Document could not be deleted." }, JsonRequestBehavior.AllowGet);
            }   
        }

        //delete document from library
        public void DeleteDocument(Guid id, string documentsFolderPath)
        {
            try
            {
                var tblDocumentLibrary = db.tblDocumentLibraries
                            .Include(m => m.tblMeeting)
                            .Include(m => m.tblPublishLibraries)
                            .First(m => m.docId == id);

                // Delete the file and folder.
                DirectoryInfo info = new DirectoryInfo(documentsFolderPath + tblDocumentLibrary.tblMeeting.meetingType);

                if (info.Exists)
                {
                    // Get the file name without extension and adds a wildcard
                    var fileNameWithWildcard = string.Format("{0}*", Path.GetFileNameWithoutExtension(tblDocumentLibrary.docName));

                    foreach (var file in info.GetFiles(fileNameWithWildcard))
                    {
                        file.Delete();
                    }

                    if (info.GetFiles().Count() == 0)
                    {
                        info.Delete(true);
                    }
                }

                db.tblPublishLibraries.RemoveRange(tblDocumentLibrary.tblPublishLibraries);
                db.tblDocumentLibraries.Remove(tblDocumentLibrary);
            }
            catch (Exception ex)
            {
                throw new Exception("Something wrong happened while deleting an existing document", ex);
            }
        }


        #region Agenda Items List - Partial

        [HttpGet]
        public ActionResult GetPublishedDocumentsList(Guid? meetingTypeId, Guid? meetingId)
        {
            // Get the specific meeting
            var tblMeeting = db.tblMeetings.First(x => x.meetingId == meetingId);

            // Apply the needed info
            var publishViewModel = new PublishViewModel
            {
                meetingTypeId = tblMeeting.meetingType,
                meetingTypeName = tblMeeting.tblMeetingType.meetingType,
                meetingDate = tblMeeting.meetingDate.Value.ToString("dd/MM/yyyy"),
                status = db.tblPublishLibraries.Any(x => x.meetingId == meetingId) ? "Published" : "Draft"
            };
            List<AgendaItemsViewModels> agendaItemsViewModelList = GetAgendaItemsWithDocuments(meetingId.Value, tblMeeting);

            publishViewModel.AgendaItemsViewModelList = agendaItemsViewModelList;

            return PartialView("_PublishedDocumentsList", publishViewModel);
        }

        #endregion
    }
}