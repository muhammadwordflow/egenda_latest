﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BoardsXpress.Models;

namespace BoardsXpress.Controllers
{
    /*public class TrainingController : BaseController
    {
        // GET: Help
        public ActionResult Index()
        {
            return View("UserGuide");
        }

        public ActionResult InductionTraining(int pageNumber = 0)
        {
            return View(pageNumber == 0 ?
                "~/Views/Training/Training.cshtml" :
                string.Format("~/Views/Training/Training-{0}.cshtml", pageNumber.ToString()));
        }
        [HttpPost]
        public JsonResult FinishTraining(bool training)
        {
            var userId = User.Identity.GetUserId();

            var inductionTraining = db.tblTrainings
                .Where(m => m.UserId == userId);

            if (inductionTraining.Any())
            {
                return Json(new { Success = true, Message = "Your induction training is already completed" }, JsonRequestBehavior.AllowGet);
            }

            tblTraining tblInductiontraining = new tblTraining
            {
                TrainingId = Guid.NewGuid(),
                UserId = userId,
                Training = training,
                InductionDateTime = DateTime.Now
            };

            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.tblTrainings.Add(tblInductiontraining);

                    if (db.SaveChanges() == 0)
                    {
                        dbContextTransaction.Rollback();
                        return Json(new { Success = false, myresult = tblInductiontraining, Message = "Your training is not finished please try again." }, JsonRequestBehavior.AllowGet);
                    }

                    dbContextTransaction.Commit();
                }
                catch (Exception)
                {
                    dbContextTransaction.Rollback();
                    return Json(new { Success = false, myresult = tblInductiontraining, Message = "Your training is not finished please try again." }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { Success = true, myresult = tblInductiontraining, Message = "Your induction training is completed" }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetInductionTranningReport(int? page)
        {
            TrainingViewModel trainingViewModel = new TrainingViewModel();

            try
            {
                var userInductionDb = (from aspnetUsers in db.AspNetUsers
                                       join inductionTbl in db.tblTrainings on aspnetUsers.Id equals inductionTbl.UserId
                                       select new
                                       {
                                           UserName = aspnetUsers.FirstName,
                                           UserId = aspnetUsers.Id,
                                           TrainnigDate = inductionTbl.InductionDateTime,
                                           Status = inductionTbl.Training
                                       }).ToList();
                trainingViewModel.InductionTrainingList = new List<TrainingViewModel>();
                userInductionDb.ForEach(item =>
                {
                    trainingViewModel.InductionTrainingList.Add(
                        new TrainingViewModel
                        {
                            UserId = item.UserId,
                            UserName = item.UserName,
                            InductionTranningdateTime = item.TrainnigDate,
                            TrainingStatus = item.Status
                        });
                });
                trainingViewModel.Pager = new Pager(trainingViewModel.InductionTrainingList.Count(), page);
                trainingViewModel.InductionTrainingList.Skip((trainingViewModel.Pager.CurrentPage - 1) * trainingViewModel.Pager.PageSize).Take(trainingViewModel.Pager.PageSize);

            }
            catch (Exception ex)
            {
                trainingViewModel.ErrorCode = ex.HResult;
                trainingViewModel.ErrorMessage = ex.Message;
            }

            return Json(trainingViewModel, JsonRequestBehavior.AllowGet);
        }

    }*/
}