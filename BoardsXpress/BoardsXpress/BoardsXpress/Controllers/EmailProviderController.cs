﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using System.Net;
using System.Configuration;
using System.Web.Configuration;
using System.Net.Configuration;
using System.Net.Mime;
using System.IO;
using HtmlAgilityPack;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using BoardsXpress.Properties;

namespace BoardsXpress.Controllers
{
    public class EmailProviderController : Controller
    {
       public bool sendMail(List<string> attendees,string Subject,string Body,string emailType)
        {
             string subject = Subject;
             string body = Body;
             Configuration configFile = WebConfigurationManager.OpenWebConfiguration("~/web.config");
            MailSettingsSectionGroup mailSetting = configFile.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
            if (mailSetting != null)
            {
                var fromAddress = new MailAddress(mailSetting.Smtp.From, mailSetting.Smtp.Network.UserName);
                
                string fromPassword = mailSetting.Smtp.Network.Password;
                var smtp = new SmtpClient
                {
                    Host = mailSetting.Smtp.Network.Host,
                    Port = mailSetting.Smtp.Network.Port,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };

                foreach (var item in attendees)
                {
                    var toAddress = new MailAddress(item);
                    string fileContent = string.Empty;
                    string filePath = string.Empty;
                    if(emailType == "Welcome")
                    {
                        //new user welcome
                        filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "Email/WelcomeMessage.html"); 
                    }
                    else if(emailType == "Reset")
                    {
                        //reset password
                        filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "Email/Reset.html");
                    }
                    else if (emailType == "notification")
                    {
                        //notify admin that a document has been added.
                        //meetingnotice.html

                    }
                    else
                    {
                        //meeting Invite
                        filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "Email/Invite.html");
                    }
                    try
                    {
                        fileContent = System.IO.File.OpenText(filePath).ReadToEnd();
                        fileContent = AppendRedirectURl(fileContent,body);
                    }
                    catch(Exception ex) { throw; }
                    
                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        IsBodyHtml = true,
                        Subject = subject,
                        Body = fileContent,
                    })
                    {
                        try
                        {
                            smtp.Send(message);
                        }
                        catch (Exception ex) { }

                    }
                }
                
            }
           
           
            return false;
        }
        private string AppendRedirectURl(string mainContent,string callBack)
        {
            string newContent = string.Empty;
            var document = new HtmlDocument();
            document.LoadHtml(mainContent);
            var anchorNode = document.GetElementbyId("mcnButton");
            
            if (anchorNode != null)
            {
                anchorNode.Attributes["href"].Value = callBack;
                newContent = document.DocumentNode.OuterHtml.ToString();
            }

            var logo = document.GetElementbyId("mcnImage");

            if (logo != null) {
                logo.Attributes["src"].Value = string.Format("{0}Media/Assets/{1}", Resources.siteRoot, Resources.customerLogo);
                newContent = document.DocumentNode.OuterHtml.ToString();
            }

            return newContent;
        }
        /// <summary>
        /// notify administrator(s) that a user has uploaded an agenda item.
        /// </summary>
        /// <param name="mailSubject"></param>
        /// <param name="bodyText"></param>
        public void NotifyAdministrator(string mailSubject,Dictionary<string,string>replaceData)
        {
            var administrators = new List<string>();
            //var u = HttpContext.User.Identity.GetUserId();
            //var CurrentUser = User.Identity.GetUserId();
            BoardExpressEntities bxEnt = new BoardExpressEntities();
            try
            {
                var admins = from x in bxEnt.AspNetUserRoles
                             where x.RoleId == "f5dac7f8-b1d2-42b6-895d-03fd19f73390"
                             //where x.UserId != CurrentUser
                             select x;

                if (admins.Any())
                {
                    administrators.Clear();
                    foreach (var item in admins)
                    {
                        var adminEmail = item.AspNetUser.Email;
                        administrators.Add(adminEmail);
                    }
                }
            }
            catch (Exception ex) { }

            Configuration configFile = WebConfigurationManager.OpenWebConfiguration("~/web.config");
            MailSettingsSectionGroup mailSetting = configFile.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
            if (mailSetting != null)
            {
                var fromAddress = new MailAddress(mailSetting.Smtp.From, mailSetting.Smtp.Network.UserName);

                string fromPassword = mailSetting.Smtp.Network.Password;
                var smtp = new SmtpClient
                {
                    Host = mailSetting.Smtp.Network.Host,
                    Port = mailSetting.Smtp.Network.Port,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,

                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                string fileContent = string.Empty;
                string filePath = string.Empty;
                string emailType = "Notification";
                if (emailType == "Notification")
                {
                    //new user welcome
                    filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "Email/meetingnotice.html");
                }
                else
                {
                    filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "Email/publishnotice.html");
                }
                try
                {
                    fileContent = System.IO.File.OpenText(filePath).ReadToEnd();
                    fileContent = ReplaceStandards(fileContent, replaceData);
                }
                catch (Exception ex) { }
                foreach (var item in administrators)
                {
                    var toAddress = new MailAddress(item);
                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        IsBodyHtml = true,
                        Subject = mailSubject,
                        Body = fileContent,
                    })
                    {
                        try
                        {
                            smtp.Send(message);
                        }
                        catch (Exception ex) { }
                    }

                }
            }
        }
        /// <summary>
        /// replace details of the file being added from standar template with the actual data
        /// </summary>
        /// <param name="fileContent"></param>
        /// <param name="replaceValues"></param>
        /// <returns></returns>
        private string ReplaceStandards(string fileContent, Dictionary<string, string> replaceValues)
        {
            string newFileContent = string.Empty;
            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(fileContent);
            var userName = document.GetElementbyId("username");
            var docName = document.GetElementbyId("docname");
            var agendaItem = document.GetElementbyId("agendaitem");
            var meetingDate = document.GetElementbyId("meetingdate");
            var meetingType = document.GetElementbyId("meetingtype");
            var callBackUrl = document.GetElementbyId("mcnButton");
            if (userName != null)
            {
                userName.InnerHtml = replaceValues["User"];
                docName.InnerHtml = replaceValues["fileName"];
                agendaItem.InnerHtml = replaceValues["agendaItem"];
                meetingDate.InnerHtml = replaceValues["meetingDate"];
                meetingType.InnerHtml = replaceValues["meetingType"];
                callBackUrl.Attributes["href"].Value = replaceValues["callBack"];
                newFileContent = document.DocumentNode.OuterHtml.ToString();
            }
            return newFileContent;
        }
        /// <summary>
        /// notify all users that the papers have been published for the given meeting
        /// </summary>
        /// <param name="mailSubject"></param>
        /// <param name="bodyText"></param>
        public void NotifyUsers(string mailSubject, string bodyText,Guid? meetingID)
        {
            var NormalUsers = new List<string>();
            BoardExpressEntities bxEnt = new BoardExpressEntities();
            try
            {
                var users = from x in bxEnt.AspNetUserRoles
                             where x.RoleId != "f5dac7f8-b1d2-42b6-895d-03fd19f73390"
                             where x.MeetingTypeId == meetingID
                             select x;
                if (users.Any())
                {
                    NormalUsers.Clear();
                    foreach (var item in users)
                    {
                        var adminEmail = item.AspNetUser.Email;
                        NormalUsers.Add(adminEmail);
                    }
                }
            }
            catch (Exception ex) { }

            Configuration configFile = WebConfigurationManager.OpenWebConfiguration("~/web.config");
            MailSettingsSectionGroup mailSetting = configFile.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
            if (mailSetting != null)
            {
                var fromAddress = new MailAddress(mailSetting.Smtp.From, mailSetting.Smtp.Network.UserName);

                string fromPassword = mailSetting.Smtp.Network.Password;
                var smtp = new SmtpClient
                {
                    Host = mailSetting.Smtp.Network.Host,
                    Port = mailSetting.Smtp.Network.Port,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,

                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                foreach (var item in NormalUsers)
                {
                    var toAddress = new MailAddress(item);
                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        IsBodyHtml = true,
                        Subject = mailSubject,
                        Body = bodyText,
                    })
                    {
                        try
                        {
                            smtp.Send(message);
                        }
                        catch (Exception ex) { }
                    }

                }
            }


        }
    }
}