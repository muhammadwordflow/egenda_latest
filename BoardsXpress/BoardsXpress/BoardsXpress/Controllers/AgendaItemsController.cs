﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using BoardsXpress.Models;

namespace BoardsXpress.Controllers
{
    [Authorize]
    public class AgendaItemsController : BaseController
    {
        #region Index

        //// GET: AgendaItems
        //public ActionResult Index(string id)
        //{
        //    try
        //    {
        //        if (string.IsNullOrEmpty(id))
        //            throw new HttpException(404, "File Not Found");

        //        var MeetingId = Guid.Parse(id);
        //        var agendaItemsViewModelList = new List<AgendaItemsViewModels>();
        //        var agendaItemList = db.tblAgendaItems.Where(w => w.meetingTypeId == MeetingId).OrderBy(x=>x.Id);
        //        var meetingDetailsForTheMeetingId = db.tblMeetings.Find(MeetingId);//for meetingtype and date

        //        if (meetingDetailsForTheMeetingId != null)
        //        {
        //            DateTime? meetingDate = meetingDetailsForTheMeetingId.meetingDate;
        //            ViewBag.meetingDate = meetingDate;
        //            var meetingTypeId = meetingDetailsForTheMeetingId.meetingType;
        //            var meetingType = db.tblMeetingTypes.Find(meetingTypeId); //for meetingtype name

        //            if (agendaItemList.Count() != 0)
        //            {
        //                foreach (var item in agendaItemList)
        //                {
        //                    var agendaItemsViewModel = new AgendaItemsViewModels
        //                    {
        //                        agendaId = item.Id,
        //                        agendaName = item.agendaItem,
        //                        meetingTypeId = meetingType.meetingTypeId,
        //                        meetingType = meetingType.meetingType,
        //                        meetingId = MeetingId,
        //                        recurringItem = item.recurringItem.Value
        //                    };
        //                    agendaItemsViewModelList.Add(agendaItemsViewModel);
        //                }
        //            }
        //            else
        //            {
        //                var agendaItemsViewModel = new AgendaItemsViewModels
        //                {
        //                    meetingTypeId = meetingType.meetingTypeId,
        //                    meetingType = meetingType.meetingType,
        //                    meetingId = MeetingId,
        //                    recurringItem = true
        //                };
        //                agendaItemsViewModelList.Add(agendaItemsViewModel);
        //            }
        //        }

        //        AgendaViewModel agendaViewModel = new AgendaViewModel()
        //        {
        //            meetingTypeId = MeetingId,
        //            AgendaItemsViewModelList = agendaItemsViewModelList
        //        };
        //        //return View(agendaItemsViewModelList);
        //        return View(agendaViewModel);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Redirect("/MeetingTypes");
        //    }
        //}

        //[HttpPost]
        //public ActionResult Index()
        //{
        //    HelperRepo repo = new HelperRepo();
        //    //For an immedeiate release for AIST, the task tie agendaItem to meetingDate has been fixed as below:
        //    //The term (in this class and the view, meeting type refers to meetingId. Later on, the DB has to
        //    //be changed and rename meetingtype to meetingId and cascade it everywhere on this class and it's views
        //    //note: it also affect the way agenda item is loaded while uploading document in shared/layout
        //    var MeetingId = Guid.Parse(Request.Form["selectMeetingDate"].ToString()); //gives meetingId
        //                                                                              //meetingTypeIdLocal = MeetingId;

        //    CreateRecurringItems(true, MeetingId);

        //    var agendaItemsViewModelList = new List<AgendaItemsViewModels>();
        //    var agendaItemList = db.tblAgendaItems.Where(w => w.meetingTypeId == MeetingId).OrderBy(x=>x.Id);
        //    //ViewBag.meetingId = MeetingId;
        //    var meetingDetailsForTheMeetingId = db.tblMeetings.Find(MeetingId);//for meetingtype and date

        //    if (meetingDetailsForTheMeetingId != null)
        //    {
        //        DateTime? meetingDate = meetingDetailsForTheMeetingId.meetingDate;
        //        ViewBag.meetingDate = meetingDate;
        //        var meetingTypeId = meetingDetailsForTheMeetingId.meetingType;
        //        var meetingType = db.tblMeetingTypes.Find(meetingTypeId); //for meetingtype name
        //        //var meetingType = db.tblMeetingTypes.Find(MeetingId);

        //        if (agendaItemList.Count() != 0)
        //        {
        //            foreach (var item in agendaItemList)
        //            {
        //                var agendaItemsViewModel = new AgendaItemsViewModels
        //                {
        //                    agendaId = item.Id,
        //                    agendaName = item.agendaItem,
        //                    meetingTypeId = meetingType.meetingTypeId,
        //                    meetingType = meetingType.meetingType,
        //                    meetingId = MeetingId,
        //                    recurringItem = item.recurringItem.Value
        //                };
        //                agendaItemsViewModelList.Add(agendaItemsViewModel);
        //            }
        //        }
        //        else
        //        {
        //            var agendaItemsViewModel = new AgendaItemsViewModels
        //            {
        //                meetingTypeId = meetingType.meetingTypeId,
        //                meetingType = meetingType.meetingType,
        //                meetingId = MeetingId,
        //                recurringItem = true
        //            };
        //            agendaItemsViewModelList.Add(agendaItemsViewModel);

        //        }
        //    }

        //    AgendaViewModel agendaViewModel = new AgendaViewModel()
        //    {
        //        meetingTypeId = MeetingId,
        //        AgendaItemsViewModelList = agendaItemsViewModelList
        //    };

        //    //return View(agendaItemsViewModelList);
        //    return View(agendaViewModel);
        //}


        // GET: AgendaItems
        public ActionResult Index(string id)
        {
            try
            {
                var _meetingId = Guid.Parse(id); // from tblMeeting

                var agendaItemsViewModelList = new List<AgendaItemsViewModels>();

                // Find all agenda items by meetingId
                var agendaItemList = db.tblAgendaItems
                    .Where(w => w.meetingTypeId == _meetingId)
                    .Include(m => m.tblAgendaItemsRelateds)
                    .OrderBy(x => x.itemOrder);

                // Get the meeting details such as date, start time, finish time, etc.
                var meeting = db.tblMeetings.Find(_meetingId);

                if (meeting != null)
                {
                    ViewBag.meetingDate = meeting.meetingDate;

                    var meetingType = db.tblMeetingTypes.Find(meeting.meetingType);

                    if (agendaItemList.Count() > 0)
                    {
                        foreach (var item in agendaItemList)
                        {
                            var agendaItemsViewModel = new AgendaItemsViewModels
                            {
                                agendaId = item.Id,
                                agendaName = item.agendaItem,
                                meetingTypeId = meetingType.meetingTypeId,
                                meetingType = meetingType.meetingType,
                                meetingId = _meetingId,
                                recurringItem = item.recurringItem.Value,
                                itemOrder = item.itemOrder,
                                AgendaItemsRelatedList = AutoMapper.Mapper.Map<List<tblAgendaItemsRelated>, List<AgendaItemsRelatedViewModel>>(item.tblAgendaItemsRelateds.OrderBy(m => m.itemOrder).ToList())
                            };
                            agendaItemsViewModelList.Add(agendaItemsViewModel);
                        }
                    }
                    else
                    {
                        var agendaItemsViewModel = new AgendaItemsViewModels
                        {
                            meetingTypeId = meetingType.meetingTypeId,
                            meetingType = meetingType.meetingType,
                            meetingId = _meetingId,
                            recurringItem = true,
                            parentAgendaId = 0,
                            itemOrder = 1,
                            AgendaItemsRelatedList = new List<AgendaItemsRelatedViewModel>()
                        };
                        agendaItemsViewModelList.Add(agendaItemsViewModel);

                    }
                }

                AgendaViewModel agendaViewModel = new AgendaViewModel()
                {
                    meetingId = _meetingId,
                    meetingTypeId = meeting.meetingType,
                    AgendaItemsViewModelList = agendaItemsViewModelList
                };
                //AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
                //{
                //    UserName = User.Identity.GetUserName(),
                //    UserId = new Guid(User.Identity.GetUserId()),
                //    AfterData = JsonConvert.SerializeObject(agendaViewModel, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }),
                //    BeforeData = JsonConvert.SerializeObject(null),
                //    IpAddress = Request.UserHostAddress,
                //    SessionId = HttpContext.Session.SessionID,
                //    ActionPerform = "GET",
                //    UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
                //};
                //auditTrailViewModel.AuditTrail();

                return View(agendaViewModel);
            }
            catch (Exception ex)
            {
                return Redirect("/MeetingTypes");
            }
        }

        /// <summary>
        /// Gets the Index page
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index()
        {
            try
            {
                var meetingId = Guid.Parse(Request.Form["selectMeetingDate"].ToString()); // from tblMeeting
                var meetingTypeId = Guid.Parse(Request.Form["meetingTypeId"].ToString()); // from tblMeetingType

                var agendaItemsViewModelList = new List<AgendaItemsViewModels>();

                CreateRecurringItems(true, meetingId, meetingTypeId);

                // Find all agenda items by meetingId
                var agendaItemList = db.tblAgendaItems
                    .Where(w => w.meetingTypeId == meetingId)
                    .Include(m => m.tblAgendaItemsRelateds)
                    .OrderBy(x => x.itemOrder);

                // Get the meeting details such as date, start time, finish time, etc.
                var meeting = db.tblMeetings.Find(meetingId);

                if (meeting != null)
                {
                    ViewBag.meetingDate = meeting.meetingDate;

                    var meetingType = db.tblMeetingTypes.Find(meetingTypeId);

                    if (agendaItemList.Count() > 0)
                    {
                        foreach (var item in agendaItemList)
                        {
                            // Get the parentAgendaId for this agendaItem
                            var parentAgendaItem = db.tblAgendaAssociations.FirstOrDefault(m => m.child_agenda == item.Id);

                            var agendaItemsViewModel = new AgendaItemsViewModels
                            {
                                agendaId = item.Id,
                                agendaName = item.agendaItem,
                                parentAgendaId = parentAgendaItem != null ? parentAgendaItem.parent_agenda.Value : 0,
                                meetingTypeId = meetingType.meetingTypeId,
                                meetingType = meetingType.meetingType,
                                meetingId = meetingId,
                                recurringItem = item.recurringItem.Value,
                                itemOrder = item.itemOrder,
                                AgendaItemsRelatedList = AutoMapper.Mapper.Map<List<tblAgendaItemsRelated>, List<AgendaItemsRelatedViewModel>>(item.tblAgendaItemsRelateds.OrderBy(x => x.itemOrder).ToList())
                            };
                            agendaItemsViewModelList.Add(agendaItemsViewModel);
                        }
                    }
                    else
                    {
                        var agendaItemsViewModel = new AgendaItemsViewModels
                        {
                            meetingTypeId = meetingType.meetingTypeId,
                            meetingType = meetingType.meetingType,
                            meetingId = meetingId,
                            parentAgendaId = 0,
                            recurringItem = true,
                            itemOrder = 1,
                            AgendaItemsRelatedList = new List<AgendaItemsRelatedViewModel>()
                        };
                        agendaItemsViewModelList.Add(agendaItemsViewModel);
                    }
                }

                AgendaViewModel agendaViewModel = new AgendaViewModel()
                {
                    meetingId = meetingId,
                    meetingTypeId = meetingTypeId,
                    AgendaItemsViewModelList = agendaItemsViewModelList
                };
                //AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
                //{
                //    UserName = User.Identity.GetUserName(),
                //    UserId = new Guid(User.Identity.GetUserId()),                    
                //    AfterData = JsonConvert.SerializeObject(agendaViewModel, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }),
                //    BeforeData = JsonConvert.SerializeObject(null),
                //    IpAddress = Request.UserHostAddress,
                //    SessionId = HttpContext.Session.SessionID,
                //    ActionPerform = "GET",
                //    UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
                //};
                //auditTrailViewModel.AuditTrail();
                return View(agendaViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        // POST: AgendaItems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public JsonResult Create(string agendaName, string meetingId, bool recurringItem, bool draftMinutesItem)
        {
            var _meetingId = Guid.Parse(meetingId);

            int itemOrder = 1;

            var agendaItems = db.tblAgendaItems
                .Where(m => m.meetingTypeId == _meetingId);

            if (agendaItems.Any())
                itemOrder = agendaItems.Max(m => m.itemOrder) + 1;

            tblAgendaItem tblAgendaItem = new tblAgendaItem
            {
                agendaItem = agendaName,
                meetingTypeId = _meetingId,
                draftMinutesItem = draftMinutesItem,
                recurringItem = recurringItem,
                itemOrder = itemOrder
            };

            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.tblAgendaItems.Add(tblAgendaItem);

                    if (db.SaveChanges() == 0)
                    {
                        dbContextTransaction.Rollback();
                        return Json(new { Success = false, Message = "Agenda Item was not created." }, JsonRequestBehavior.AllowGet);
                    }

                    dbContextTransaction.Commit();

                    //AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
                    //{
                    //    UserName = User.Identity.GetUserName(),
                    //    UserId = new Guid(User.Identity.GetUserId()),
                    //    AfterData = JsonConvert.SerializeObject(tblAgendaItem, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }),
                    //    BeforeData = JsonConvert.SerializeObject(null),
                    //    IpAddress = Request.UserHostAddress,
                    //    SessionId = HttpContext.Session.SessionID,
                    //    ActionPerform = "CREATE",
                    //    UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
                    //};
                    //auditTrailViewModel.AuditTrail();
                }
                catch (Exception)
                {
                    dbContextTransaction.Rollback();
                    return Json(new { Success = false, Message = "Agenda Item was not created." }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { Success = true, Message = "Agenda Item has been created successfully!" }, JsonRequestBehavior.AllowGet);
        }

        // POST: AgendaItems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public JsonResult Edit(int agendaId, string agendaName, Guid meetingId, bool recurringItem, bool draftMinutesItem)
        {
            var tblAgendaItem = db.tblAgendaItems.First(m =>
                m.meetingTypeId == meetingId &&
                m.Id == agendaId);
            //var tempBeforeData = JsonConvert.SerializeObject(tblAgendaItem, Formatting.None,
            //    new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            tblAgendaItem.agendaItem = agendaName;
            tblAgendaItem.recurringItem = recurringItem;
            tblAgendaItem.draftMinutesItem = draftMinutesItem;

            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.Entry(tblAgendaItem).State = EntityState.Modified;

                    if (db.SaveChanges() == 0)
                    {
                        dbContextTransaction.Rollback();
                        return Json(new { Success = false, Message = "Agenda Item was not updated." }, JsonRequestBehavior.AllowGet);
                    }

                    dbContextTransaction.Commit();

                    //AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
                    //{
                    //    UserName = User.Identity.GetUserName(),
                    //    UserId = new Guid(User.Identity.GetUserId()),
                    //    AfterData = JsonConvert.SerializeObject(tblAgendaItem, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }),
                    //    BeforeData = tempBeforeData,
                    //    IpAddress = Request.UserHostAddress,
                    //    SessionId = HttpContext.Session.SessionID,
                    //    ActionPerform = "EDIT",
                    //    UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
                    //};
                    //auditTrailViewModel.AuditTrail();
                }
                catch (Exception)
                {
                    dbContextTransaction.Rollback();
                    return Json(new { Success = false, Message = "Agenda Item was not updated." }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { Success = true, Message = "Agenda Item has been updated successfully!" }, JsonRequestBehavior.AllowGet);
        }

        // POST: AgendaItems/Delete/5
        [HttpPost]
        public JsonResult DeleteConfirmed(int id, Guid meetingId)
        {
            var publishController = new PublishController();

            var agendaItem = db.tblAgendaItems
                .Include(m => m.tblAgendaItemsRelateds)
                .Include(m => m.tblDocumentLibraries)
                .First(m =>
                    m.meetingTypeId == meetingId &&
                    m.Id == id);

            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    // Delete the document from the Library, if one exists already
                    if (agendaItem.tblDocumentLibraries.Any())
                        publishController.DeleteDocument(agendaItem.tblDocumentLibraries.First().docId, Server.MapPath("~/Documents/"));

                    // Reorder items
                    var agendaItems = db.tblAgendaItems
                        .Where(m =>
                            m.meetingTypeId == meetingId &&
                            m.itemOrder > agendaItem.itemOrder);

                    foreach (var item in agendaItems)
                    {
                        item.itemOrder--;

                        db.Entry(item).State = EntityState.Modified;
                    }

                    // Delete the agendaItem from tblAgendaItem
                    db.tblAgendaItems.Remove(agendaItem);

                    if (db.SaveChanges() == 0)
                    {
                        dbContextTransaction.Rollback();
                        return Json(new { Success = false, Message = "Agenda Item was not deleted." }, JsonRequestBehavior.AllowGet);
                    }

                    dbContextTransaction.Commit();

                    publishController.Dispose();

                    //AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
                    //{
                    //    UserName = User.Identity.GetUserName(),
                    //    UserId = new Guid(User.Identity.GetUserId()),
                    //    AfterData = JsonConvert.SerializeObject(agendaItem, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }),
                    //    BeforeData = JsonConvert.SerializeObject(null),
                    //    IpAddress = Request.UserHostAddress,
                    //    SessionId = HttpContext.Session.SessionID,
                    //    ActionPerform = "DELETE",
                    //    UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
                    //};
                    //auditTrailViewModel.AuditTrail();
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    return Json(new { Success = false, Message = "Agenda Item was not deleted." }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { Success = true, Message = "Agenda Item was deleted successfully!" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult getDatesForMeeting(Guid meetingTypeID)
        {
            var meetingDDLViewModelList = new List<MeetingDDLViewModel>();

            meetingDDLViewModelList.Insert(0, new MeetingDDLViewModel
            {
                MeetingId = Guid.Empty,
                MeetingDate = "Select a meeting date"
            });

            var meetings = db.tblMeetings
                .Where(m => m.meetingType == meetingTypeID)
                .OrderBy(m => m.meetingDate.Value);

            foreach (var meeting in meetings)
            {
                meetingDDLViewModelList.Add(new MeetingDDLViewModel
                {
                    MeetingId = meeting.meetingId,
                    RotaryResolution = meeting.rotaryResolution,
                    MeetingDate = meeting.meetingDate.Value.ToString("dd MMMM, yyyy")
                });
            }
            return Json(meetingDDLViewModelList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult getAgendaForMeeting(Guid? meetingID)
        {
            var agendaItemDDLViewModelList = new List<AgendaItemDDLViewModel>();

            agendaItemDDLViewModelList.Insert(0, new AgendaItemDDLViewModel
            {
                AgendaItemId = 0,
                AgendaName = "Select an agenda item"
            });

            if (meetingID.HasValue)
            {
                var agendaItems = db.tblAgendaItems
                    .Where(m => m.meetingTypeId == meetingID.Value)
                    .OrderBy(m => m.itemOrder);

                foreach (var agendaItem in agendaItems)
                {
                    agendaItemDDLViewModelList.Add(new AgendaItemDDLViewModel
                    {
                        AgendaItemId = agendaItem.Id,
                        AgendaName = string.Format("{0} - {1}", agendaItem.itemOrder, agendaItem.agendaItem)
                    });
                }

                return Json(agendaItemDDLViewModelList, JsonRequestBehavior.AllowGet);
            }

            return Json(new { agendaItemDDLViewModelList }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetParentAgendaItems()
        {
            try
            {
                int agendaItemId;
                Guid meetingId;

                var isAgendaItemIdValid = int.TryParse(Request.QueryString["agendaItemId"], out agendaItemId);
                var isMeetingIdValid = Guid.TryParse(Request.QueryString["meetingId"], out meetingId);

                // Get all the agendaItems, except the one is getting the data
                if (isAgendaItemIdValid && isMeetingIdValid)
                {
                    var agendaItems = (from x in db.tblAgendaItems where x.Id != agendaItemId && x.meetingTypeId == meetingId select new { Id = x.Id, agendaItem = x.agendaItem }).ToList();

                    // Add the blank item
                    agendaItems.Insert(0, new { Id = 0, agendaItem = "Select a parent" });

                    return Json(new { agendaItems = agendaItems }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var agendaItems = (from x in db.tblAgendaItems where x.meetingTypeId == meetingId select new { Id = x.Id, agendaItem = x.agendaItem }).ToList();

                    // Add the blank item
                    agendaItems.Insert(0, new { Id = 0, agendaItem = "Select a parent" });

                    return Json(new { agendaItems = agendaItems }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("An error occured on the method getParentAgendaItems", ex);
            }
        }

        #region Private Methods

        private int CreateAgendaAssociation(int parentId, int childId)
        {
            var agendaAssociation = new tblAgendaAssociation();

            // Check if there's an association already set.
            var childAgendaAssociation = db.tblAgendaAssociations.FirstOrDefault(m => m.child_agenda == childId);

            if (childAgendaAssociation != null)
            {
                // If it was found, change the parent agendaItem to the current one
                childAgendaAssociation.parent_agenda = parentId;
                db.Entry(childAgendaAssociation).State = EntityState.Modified;
            }
            else
            {
                agendaAssociation.parent_agenda = parentId;
                agendaAssociation.child_agenda = childId;

                db.tblAgendaAssociations.Add(agendaAssociation);
            }

            try
            {
                //AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
                //{
                //    UserName = User.Identity.GetUserName(),
                //    UserId = new Guid(User.Identity.GetUserId()),
                //    AfterData = JsonConvert.SerializeObject(agendaAssociation, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }),
                //    BeforeData = JsonConvert.SerializeObject(null),
                //    IpAddress = Request.UserHostAddress,
                //    SessionId = HttpContext.Session.SessionID,
                //    ActionPerform = "CREATE",
                //    UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
                //};
                //auditTrailViewModel.AuditTrail();
                return db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("An exception has been thrown on the CreateAgendaAssociation method.", ex);
            }
        }

        /// <summary>
        /// Create recurring items based on the form response.
        /// </summary>
        /// <param name="createRecurring">Check if it's needed to create recurring agenda items</param>
        /// <param name="meetingId">meetingId that the recurring items will be based</param>
        private void CreateRecurringItems(bool createRecurring, Guid meetingId, Guid meetingTypeId)
        {
            try
            {
                if (createRecurring && db.tblAgendaItems.Count(w => w.meetingTypeId == meetingId) == 0)
                {
                    using (var dbContextTransaction = db.Database.BeginTransaction())
                    {
                        try
                        {
                            // Get all the recurring items from the table
                            foreach (var item in GetRecurringItems(meetingTypeId))
                            {
                                int itemOrder = 1;

                                var agendaItems = db.tblAgendaItems
                                    .Where(m => m.meetingTypeId == meetingId);

                                if (agendaItems.Any())
                                    itemOrder = agendaItems.Max(m => m.itemOrder) + 1;

                                // Save each item on the database under this meetingId
                                var newAgendaItem = new tblAgendaItem
                                {
                                    agendaItem = item.agendaItem,
                                    meetingTypeId = meetingId,
                                    recurringItem = false,
                                    itemOrder = itemOrder
                                };

                                db.tblAgendaItems.Add(newAgendaItem);

                                if (db.SaveChanges() > 0)
                                {
                                    // Create related recurring items
                                    foreach (var agendaItemRelated in item.tblAgendaItemsRelateds.Where(m => m.recurringItem))
                                    {
                                        int itemRelatedOrder = 1;

                                        var agendaRelatedItems = db.tblAgendaItemsRelateds
                                            .Where(m => m.agendaItemId == newAgendaItem.Id);

                                        if (agendaRelatedItems.Any())
                                            itemRelatedOrder = agendaRelatedItems.Max(m => m.itemOrder) + 1;

                                        // Save each item on the database under this meetingId
                                        var newAgendaItemRelated = new tblAgendaItemsRelated
                                        {
                                            agendaItemId = newAgendaItem.Id,
                                            name = agendaItemRelated.name,
                                            recurringItem = false,
                                            itemOrder = itemRelatedOrder
                                        };

                                        db.tblAgendaItemsRelateds.Add(newAgendaItemRelated);

                                        db.SaveChanges();
                                    }
                                }
                            }

                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            throw new Exception("An error occured on the method CreateRecurringItems", ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occured on the method CreateRecurringItems", ex);
            }
        }

        private List<tblAgendaItem> GetRecurringItems(Guid meetingTypeId)
        {
            try
            {
                var recurringItemsList = (from x in db.tblAgendaItems
                                          join y in db.tblMeetings
                                          on x.meetingTypeId equals y.meetingId
                                          join z in db.tblMeetingTypes
                                          on y.meetingType equals z.meetingTypeId
                                          where z.meetingTypeId == meetingTypeId &&
                                          x.recurringItem.Value
                                          orderby x.itemOrder
                                          select x).
                                          Include(m => m.tblAgendaItemsRelateds)
                                          .ToList();

                return recurringItemsList;
            }
            catch (Exception ex)
            {
                throw new Exception("An error occured on the method GetRecurringItems", ex);
            }
        }

        private void DeleteAgendaAssociations(int meetingId, bool deleteAsParent = true, bool deleteAsChild = true)
        {
            try
            {
                if (deleteAsParent)
                {
                    // Delete association as a parent agenda item from tblAgendaAssociations
                    db.tblAgendaAssociations.RemoveRange(db.tblAgendaAssociations.Where(m => m.parent_agenda == meetingId));
                }

                if (deleteAsChild)
                {
                    // Delete association as a child agenda item from tblAgendaAssociations
                    db.tblAgendaAssociations.RemoveRange(db.tblAgendaAssociations.Where(m => m.child_agenda == meetingId));
                }

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Something wrong happened on DeleteAgendaParentAssociation method.", ex);
            }
        }

        [HttpPost]
        public JsonResult UpdateSortingOrder(
            Guid meetingId,
            int agendaItemId,
            int? agendaItemRelatedId,
            int draggedOrder,
            int swappedAgendaItemdId,
            int? swappedAgendaItemRelatedId,
            int swappedOrder
            )
        {
            // Update from the related items order
            if (agendaItemRelatedId.HasValue)
            {
                var draggedRelatedItem = db.tblAgendaItemsRelateds
                    .First(m => m.tblAgendaItem.meetingTypeId == meetingId
                                && m.agendaItemId == agendaItemId
                                && m.id == agendaItemRelatedId.Value);

                var swappedRelatedItem = db.tblAgendaItemsRelateds
                    .First(m => m.tblAgendaItem.meetingTypeId == meetingId
                                && m.agendaItemId == swappedAgendaItemdId
                                && m.id == swappedAgendaItemRelatedId.Value);

                swappedRelatedItem.itemOrder = draggedOrder;
                draggedRelatedItem.itemOrder = swappedOrder;

                db.Entry(swappedRelatedItem).State = EntityState.Modified;
                db.Entry(draggedRelatedItem).State = EntityState.Modified;

                if (db.SaveChanges() > 0)
                {
                    return Json(new { Success = true, Message = "Item was successfully updated!" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var draggedAgendaItem = db.tblAgendaItems
                    .First(m => m.meetingTypeId == meetingId
                                && m.Id == agendaItemId
                                && m.itemOrder == draggedOrder);

                var swappedAgendaItem = db.tblAgendaItems
                    .First(m => m.meetingTypeId == meetingId
                                && m.Id == swappedAgendaItemdId &&
                                m.itemOrder == swappedOrder);

                swappedAgendaItem.itemOrder = draggedOrder;
                draggedAgendaItem.itemOrder = swappedOrder;

                db.Entry(swappedAgendaItem).State = EntityState.Modified;
                db.Entry(draggedAgendaItem).State = EntityState.Modified;

                if (db.SaveChanges() > 0)
                {
                    return Json(new { Success = true, Message = "Item was successfully updated!" }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { Success = false, Message = "Item was not updated!" }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #endregion

        #region AgendaItemsRelated

        [HttpPost]
        public JsonResult CreateAgendaItemRelated(int agendaItemId, string name, bool recurringItem)
        {
            int itemOrder = 1;

            var agendaItemsRelated = db.tblAgendaItemsRelateds
                .Where(m => m.agendaItemId == agendaItemId);

            if (agendaItemsRelated.Any())
                itemOrder = agendaItemsRelated.Max(m => m.itemOrder) + 1;

            tblAgendaItemsRelated tblAgendaItemsRelated = new tblAgendaItemsRelated
            {
                agendaItemId = agendaItemId,
                name = name,
                itemOrder = itemOrder,
                recurringItem = recurringItem
            };

            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.tblAgendaItemsRelateds.Add(tblAgendaItemsRelated);

                    if (db.SaveChanges() == 0)
                    {
                        dbContextTransaction.Rollback();
                        return Json(new { Success = false, Message = "Related Item was not created." }, JsonRequestBehavior.AllowGet);
                    }

                    dbContextTransaction.Commit();
                }
                catch (Exception)
                {
                    dbContextTransaction.Rollback();
                    return Json(new { Success = false, Message = "Related Item was not created." }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { Success = true, Message = "Related Item has been created successfully!" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EditAgendaItemRelated(int id, int agendaItemId, string name, bool recurringItem)
        {
            var tblAgendaItemsRelated = db.tblAgendaItemsRelateds.FirstOrDefault(m => m.id == id && m.agendaItemId == agendaItemId);

            if (tblAgendaItemsRelated != null)
            {
                db.Entry(tblAgendaItemsRelated).State = EntityState.Modified;

                tblAgendaItemsRelated.name = name;
                tblAgendaItemsRelated.recurringItem = recurringItem;

                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (db.SaveChanges() == 0)
                        {
                            dbContextTransaction.Rollback();
                            return Json(new { Success = false, Message = "Related Item was not updated." }, JsonRequestBehavior.AllowGet);
                        }

                        dbContextTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        dbContextTransaction.Rollback();
                        return Json(new { Success = false, Message = "Related Item was not updated." }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new { Success = true, Message = "Related Item has been updated successfully!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Success = false, Message = "Related Item was not found." }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        public JsonResult DeleteAgendaItemRelated(int id, int agendaItemId)
        {
            var publishController = new PublishController();

            var tblAgendaItemsRelated = db.tblAgendaItemsRelateds
                .Include(m => m.tblAgendaItem)
                .Include(m => m.tblDocumentLibraries)
                .FirstOrDefault(m => m.id == id && m.agendaItemId == agendaItemId);

            if (tblAgendaItemsRelated != null)
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        // Delete the document from the Library, if one exists already
                        if (tblAgendaItemsRelated.tblDocumentLibraries.Any())
                            publishController.DeleteDocument(tblAgendaItemsRelated.tblDocumentLibraries.First().docId, Server.MapPath("~/Documents/"));

                        // Delete the agendaItem from tblAgendaItem
                        db.tblAgendaItemsRelateds.Remove(tblAgendaItemsRelated);

                        if (db.SaveChanges() == 0)
                        {
                            dbContextTransaction.Rollback();
                            return Json(new { Success = false, Message = "Related Item was not deleted." }, JsonRequestBehavior.AllowGet);
                        }

                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        return Json(new { Success = false, Message = "Related Item was not deleted." }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new { Success = true, Message = "Related Item has been deleted successfully!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Success = false, Message = "Related Item was not deleted." }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetAgendaItemRelatedList(int agendaItemId)
        {
            var ddlItems = new List<DropDownListViewModel>();

            var agendaItemRelatedList = db.tblAgendaItemsRelateds
                .Where(m => m.agendaItemId == agendaItemId)
                .OrderBy(m => m.itemOrder);

            foreach (var agendaItemRelated in agendaItemRelatedList)
            {
                ddlItems.Add(new DropDownListViewModel
                {
                    Value = agendaItemRelated.id,
                    Text = string.Format("{0} - {1}", agendaItemRelated.itemOrder, agendaItemRelated.name)
                });
            }

            ddlItems.Insert(0, new DropDownListViewModel
            {
                Value = 0,
                Text = "Select the related item"
            });

            return Json(ddlItems, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Agenda Items List - Partial

        [HttpGet]
        public ActionResult GetAgendaItemsList(Guid? meetingTypeId, Guid? meetingId)
        {
            var agendaItemsViewModelsList = new List<AgendaItemsViewModels>();

            var agendaItemViewModel = new AgendaViewModel
            {
                meetingTypeId = meetingTypeId.Value,
                meetingId = meetingId.Value,
                AgendaItemsViewModelList = agendaItemsViewModelsList
            };

            if ((meetingId.HasValue && meetingId.Value != Guid.Empty))
            {
                // Find all agenda items by meetingId
                var agendaItemList = db.tblAgendaItems
                    .Where(w => w.meetingTypeId == meetingId)
                    .Include(m => m.tblAgendaItemsRelateds)
                    .OrderBy(x => x.itemOrder);

                // Get the meeting details such as date, start time, finish time, etc.
                var meeting = db.tblMeetings.Find(meetingId);

                if (meeting != null)
                {
                    ViewBag.meetingDate = meeting.meetingDate;

                    var meetingType = db.tblMeetingTypes.Find(meetingTypeId);

                    foreach (var item in agendaItemList)
                    {
                        // Get the parentAgendaId for this agendaItem
                        var parentAgendaItem = db.tblAgendaAssociations.FirstOrDefault(m => m.child_agenda == item.Id);

                        var agendaItemsViewModel = new AgendaItemsViewModels
                        {
                            agendaId = item.Id,
                            agendaName = item.agendaItem,
                            parentAgendaId = parentAgendaItem != null ? parentAgendaItem.parent_agenda.Value : 0,
                            meetingTypeId = meetingType.meetingTypeId,
                            meetingType = meetingType.meetingType,
                            meetingId = meetingId.Value,
                            recurringItem = item.recurringItem.Value,
                            itemOrder = item.itemOrder,
                            AgendaItemsRelatedList = AutoMapper.Mapper.Map<List<tblAgendaItemsRelated>, List<AgendaItemsRelatedViewModel>>(item.tblAgendaItemsRelateds.OrderBy(x => x.itemOrder).ToList())
                        };
                        agendaItemsViewModelsList.Add(agendaItemsViewModel);
                    }
                }
            }

            return PartialView("_AgendaItemsList", agendaItemViewModel);
        }

        #endregion
    }
}