﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BoardsXpress;

using Microsoft.AspNet.Identity.Owin;
using BoardsXpress.Models;
using Microsoft.AspNet.Identity;
using System.Web.Security;

namespace BoardsXpress.Controllers
{
    [Authorize]

    public class MeetingAttendeesController : Controller
    {
        private BoardExpressEntities db = new BoardExpressEntities();
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: MeetingAttendee
        public ActionResult Index(Guid? id)
        {
            var userInRole = User.IsInRole("Super Admin");
            List<AspNetUser> user = new List<AspNetUser>();
            user = (from x in db.AspNetUsers
                    join y in db.AspNetUserRoles
                    on x.Id equals y.UserId
                    where y.MeetingTypeId == id && x.FirstName != "Super Admin"
                    select x).Distinct().ToList();
            //if (userInRole) //if the user is super admin show her all the users
            //{
            //    //user = (from x in db.AspNetUsers
            //    //        where x.FirstName != "Super Admin"
            //    //        select x).Distinct().ToList();
            //}
            //else
            //{
            //    user = (from x in db.AspNetUsers
            //            join y in db.AspNetUserRoles
            //            on x.Id equals y.UserId
            //            where y.MeetingTypeId == id && x.FirstName != "Super Admin"
            //            select x).Distinct().ToList();
            //}
            //List<ApplicationUser> user = UserManager.Users.Where(w => w.FirstName != "Super Admin").ToList();
            var roleAssignmentList = new List<UserRoleAssignmentViewModels>();
            if (id == null)
            {
                var meetingDateList = GetMeetingsInformations(id.Value);
                var meetingInformations = db.tblMeetings.Where(w => w.meetingType == id.Value).ToList();
                var test = db.AspNetRoles.Where(w => w.Name != "Super Admin").Where(w => w.Name != "Admin").ToList();
                var userList = new List<UserInfoViewModels>();
                foreach (var item in user)
                {
                    var usr = new UserInfoViewModels();
                    usr.FirstName = item.FirstName;
                    usr.UserId = item.Id;
                    usr.userRole = new SelectList(test, "Id", "Name");
                    userList.Add(usr);
                }

                var roleAssignment = new UserRoleAssignmentViewModels();
                roleAssignment.userList = userList;

                roleAssignment.MeetingTypeId = id.Value;

                return View(roleAssignment);
            }
            else
            {

                var userRoles = db.AspNetUserRoles.Where(w => w.MeetingTypeId == id.Value).ToList();

                var list = user.Where(w => userRoles.Any(z => z.UserId == w.Id));
                var roleAssignment = new UserRoleAssignmentViewModels();
                var roleList = db.AspNetRoles.Where(w => w.Name != "Super Admin").Where(w => w.Name != "Admin").ToList();
                var userList = new List<UserInfoViewModels>();
                foreach (var item in user)
                {
                    var usr = new UserInfoViewModels();
                    usr.FirstName = item.FirstName;
                    usr.UserId = item.Id;
                    usr.userRoleSelected = userRoles.Where(w => w.UserId == item.Id).Select(s => s.RoleId).FirstOrDefault();
                    usr.userRole = new SelectList(roleList, "Id", "Name", userRoles.Where(w => w.UserId == item.Id).Select(s => s.RoleId).FirstOrDefault());

                    userList.Add(usr);
                }
                var meetingInformations = db.tblMeetings.Where(w => w.meetingType == id.Value).ToList();

                roleAssignment.userList = userList;

                roleAssignment.MeetingTypeId = id.Value;
                roleAssignment.MeetingType = db.tblMeetingTypes.Where(w => w.meetingTypeId == id.Value).Select(s => s.meetingType).FirstOrDefault();

                return View(roleAssignment);
            }


        }

        public ActionResult Selection(Guid id)
        {

            return RedirectToAction("Index", new
            {
                meetingId = id
            });


        }
        public ActionResult Save(UserRoleAssignmentViewModels UserRoleList)
        {
            ///check if user is already present 
            ///int userrole table before adding 
            ///

            var meetingId = Guid.Empty;
            try
            {
                foreach (var item in UserRoleList.userList.Where(w => w.userRoleSelected != null))
                {
                    var meetingTypeId = UserRoleList.MeetingTypeId;
                    var userId = item.UserId;
                    var selectedRole = item.userRoleSelected;
                    var checkAnotherRole = db.AspNetUserRoles.Where(w => w.UserId == userId).Where(w => w.MeetingTypeId == meetingTypeId);
                    if (checkAnotherRole.Count() == 1)
                    {
                        //delete the previous and add new one
                        var something = (AspNetUserRole)checkAnotherRole.FirstOrDefault();
                        db.AspNetUserRoles.Remove(something);
                        AddToRoleAsync(item.UserId, item.userRoleSelected, meetingId, UserRoleList.MeetingTypeId);
                        //updateCode
                        //update role

                    }


                    var checkRole = db.AspNetUserRoles.Where(w => w.UserId == userId).Where(w => w.MeetingTypeId == meetingTypeId).Where(w => w.RoleId == selectedRole);
                    if (checkRole.Count() == 0)
                    {

                        AddToRoleAsync(item.UserId, item.userRoleSelected, meetingId, UserRoleList.MeetingTypeId);
                    }
                }
                foreach (var item in UserRoleList.userList.Where(w => w.userRoleSelected == null))
                {
                    var userId = item.UserId;
                    var checkRole = db.AspNetUserRoles.Where(w => w.UserId == userId).Where(w => w.MeetingTypeId == UserRoleList.MeetingTypeId).ToList();
                    if (checkRole.Count() == 1)
                    {
                        var something = (AspNetUserRole)checkRole.FirstOrDefault();
                        db.AspNetUserRoles.Remove(something);
                        db.SaveChanges();
                        //delete the user

                    }

                }
                #region Save and Send 
                var isInvite = Request.Form["isInvite"];
                if (isInvite != null)
                {
                    var messageBody = Request.Form["txtMessageBody"];
                    var subject = "Meeting Invite";
                    EmailProviderController oController = new EmailProviderController();
                    List<string> attendees = new List<string>();

                    //get users from the list who are assigned roles;
                    foreach (var item in UserRoleList.userList)
                    {
                        if (!string.IsNullOrEmpty(item.userRoleSelected))
                        {
                            var user =  UserManager.FindByIdAsync(item.UserId);
                            attendees.Add(user.Result.Email);
                        }
                    }



                    oController.sendMail(attendees, subject, messageBody,"Invite");
                }
                #endregion
                return RedirectToAction("Index", new
                {
                    id = UserRoleList.MeetingTypeId
                });


            }
            catch (Exception ex)
            {

                throw;
            }



        }
        [HttpPost]
        public void SaveAndSend()
        {

        }



        public List<SelectListItem> UserRoleList()
        {

            List<SelectListItem> groupList = new List<SelectListItem>();
            var userGroups = db.AspNetRoles.Where(w => w.Name != "Super Admin").Where(w => w.Name != "Admin").ToList();
            foreach (var item in userGroups)
            {
                groupList.Add(new SelectListItem
                {
                    Text = item.Name,
                    Value = item.Id,
                    Selected = false
                });
            }
            return groupList;
        }

        //public SelectList UserRoleList()
        //{

        //    List<SelectListItem> groupList = new List<SelectListItem>();
        //    var userGroups = db.AspNetRoles.Where(w => w.Name != "Super Admin").Where(w => w.Name != "Admin").ToList();
        //    var gList = new SelectList(userGroups, "ID", "NAME");


        //    foreach (var item in userGroups)
        //    {
        //        groupList.Add(new SelectListItem
        //        {
        //            Text = item.Name,
        //            Value = item.Id,
        //            Selected = false
        //        });
        //    }
        //    return gList;
        //}


        public bool AddToRoleAsync(string userId, string role, Guid? meetingId, Guid? meetingTypeId)
        {

            
            var roleDetails = db.AspNetRoles.Find(role);
            var roleId = roleDetails.Id;
            
            AspNetUserRole userRole = new AspNetUserRole();
            userRole.MeetingId = meetingId;
            userRole.MeetingTypeId = meetingTypeId;
            userRole.UserId = userId;
            userRole.RoleId = roleId;
            userRole.StartDate = DateTime.Today;
            userRole.Id = Guid.NewGuid();
            try
            {
                db.AspNetUserRoles.Add(userRole);
                var results = db.SaveChanges();
                if (results == 1)
                {
                    return true;

                }
            }
            catch(Exception ex) { }
            
            
            

            return false;

        }
        public bool UpdateToRole(string userId, string role, Guid? meetingId, Guid? meetingTypeId)
        {


            var roleDetails = db.AspNetRoles.Find(role);
            var roleId = roleDetails.Id;

            AspNetUserRole userRole = new AspNetUserRole();
            userRole.MeetingId = meetingId;
            userRole.MeetingTypeId = meetingTypeId;
            userRole.UserId = userId;
            userRole.RoleId = roleId;

            db.AspNetUserRoles.Add(userRole);
            var results = db.SaveChanges();
            if (results == 1)
            {
                return true;

            }

            return false;

        }

        public bool DeleteUserRoleByMeetingTypeId(string userId, Guid? meetingTypeId)
        {
            try
            {
                var userRole = db.AspNetUserRoles.FirstOrDefault(m => m.UserId == userId && m.MeetingTypeId == meetingTypeId.Value);

                if (userRole != null)
                    db.AspNetUserRoles.Remove(userRole);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        ///get all meeting for particular meetingType
        ///
        public List<tblMeeting> GetMeetingsInformations(Guid meetingTypeId)
        {

            var meetingInformations = db.tblMeetings.Where(w => w.meetingType == meetingTypeId).ToList();
            return meetingInformations;

        }


        public List<SelectListItem> MeetingsList(Guid meetingTypeId)
        {
            var meetingList = GetMeetingsInformations(meetingTypeId);

            List<SelectListItem> meetingSelectList = new List<SelectListItem>();

            foreach (var item in meetingList)
            {

                meetingSelectList.Add(new SelectListItem
                {
                    Text = item.meetingDate.Value.ToShortDateString(),
                    Value = item.meetingId.ToString(),
                    Selected = false
                });
            }
            return meetingSelectList;
        }
    }
}
