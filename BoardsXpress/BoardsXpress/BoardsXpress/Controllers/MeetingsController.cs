﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BoardsXpress;
using BoardsXpress.Models;
using System.Globalization;
using System.Configuration;
using System.Web.Configuration;
using System.Net.Configuration;
using System.Net.Mail;
using System.IO;
using System.Net.Mime;
using BoardsXpress.Email;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace BoardsXpress.Controllers
{
    [Authorize]

    public class MeetingsController : Controller
    {
        private BoardExpressEntities db = new BoardExpressEntities();
        CultureInfo australianCulture = new CultureInfo("en-AU");
        // GET: Meetings
        public ActionResult Index(Guid id)
        {
            var meetingType = db.tblMeetingTypes.Find(id);
            TempData["MeetingTypeName"] = meetingType.meetingType;
            TempData.Keep("MeetingTypeName");
            List<MeetingViewModels> meetingViewModels = new List<MeetingViewModels>();
            var meetingList = db.tblMeetings.ToList().Where(w => w.meetingType == id).OrderBy(o => o.meetingDate);
            AutoMapper.Mapper.Map(meetingList, meetingViewModels);
            if (meetingViewModels.Count() == 0)
            {

                var tempmeetingViewModel = new MeetingViewModels();
                AutoMapper.Mapper.Map(meetingType, tempmeetingViewModel);
                meetingViewModels.Add(tempmeetingViewModel);

            }
            else
            {
                foreach (var item in meetingViewModels)
                {
                    AutoMapper.Mapper.Map(meetingType, item);

                }
            }

            var t = meetingViewModels.Where(w => w.meetingId == Guid.Parse("00000000-0000-0000-0000-000000000000")).Count();
            return View(meetingViewModels);
        }

        // GET: Meetings/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblMeeting tblMeeting = db.tblMeetings.Find(id);

            if (tblMeeting == null)
            {
                return HttpNotFound();
            }
            var meetingType = db.tblMeetingTypes.Find(tblMeeting.meetingType);
            MeetingViewModels meetingViewModel = new MeetingViewModels();

            //var meetingType = db.tblMeetingTypes.Find(tblMeeting.meetingType);
            //t.meetingDetails = tblMeeting;
            //t.meetingType = meetingType;

            AutoMapper.Mapper.Map<tblMeeting, MeetingViewModels>(tblMeeting, meetingViewModel);
            AutoMapper.Mapper.Map<tblMeetingType, MeetingViewModels>(meetingType, meetingViewModel);

            return View(meetingViewModel);
        }

        // GET: Meetings/Create
        public ActionResult Create(string id)
        {
            ///get the meetingtype list and populate the view
            ///


            var meetingId = Guid.Parse(id);
            var meetingTypes = db.tblMeetingTypes.Find(meetingId);
            MeetingViewModels meetingViewModel = new MeetingViewModels();
            meetingViewModel.meetingType = meetingTypes.meetingType;


            return View(meetingViewModel);
        }

        // POST: Meetings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<JsonResult> Create(Guid id, string eventdate, string StartTime, string finishTime, string eventVenue, bool rotaryResolution)
        {

            DateTime eventDate = DateTime.ParseExact(eventdate, "dd/MM/yyyy", null);
            tblMeeting tblMeeting = new tblMeeting();
            tblMeeting.meetingId = Guid.NewGuid();
            tblMeeting.meetingType = id;
            tblMeeting.startTime = StartTime;
            tblMeeting.finishTime = finishTime;
            tblMeeting.meetingDate = eventDate;
            tblMeeting.meetingVenue = eventVenue;
            tblMeeting.rotaryResolution = rotaryResolution;
            db.tblMeetings.Add(tblMeeting);
            var result = db.SaveChanges();
            if (result == 1)
            {
                List<AspNetUser> user = new List<AspNetUser>();
                user = (from x in db.AspNetUsers
                        join y in db.AspNetUserRoles
                            on x.Id equals y.UserId
                        where y.MeetingTypeId == tblMeeting.meetingType && x.FirstName != "Super Admin"
                        select x).Distinct().ToList();
                foreach (var item in user)
                {
                    tblMeetingAttendee meetingAttendee = new tblMeetingAttendee();
                    meetingAttendee.eventId = Guid.NewGuid();
                    meetingAttendee.UserId = item.Id;
                    meetingAttendee.meetingId = tblMeeting.meetingId;
                    meetingAttendee.userResponse = null;
                    var callbackUrl = Url.Action("GotMeetingConfirm", "Meetings",
                        new { userId = item.Id, meetingId = tblMeeting.meetingId },
                        protocol: Request.Url.Scheme);
                    try
                    {
                        var exchangeMeetingId = await SendInvitationAndEmail.SendInvitationByEWS(TempData.Peek("MeetingTypeName").ToString(),
                            StartTime, finishTime, DateTime.ParseExact(eventdate, "dd/MM/yyyy", null), eventVenue, item.Email, tblMeeting.meetingId, User.Identity.GetUserId(), callbackUrl);
                        meetingAttendee.ExchangeInvitationId = exchangeMeetingId;
                        db.tblMeetingAttendees.Add(meetingAttendee);
                        if (db.SaveChanges() == 0)
                        {
                            var dbresults = new { Success = "False", Message = "Event Creation Failed to Add on Database" };  //Need to fix this
                            return Json(dbresults, JsonRequestBehavior.AllowGet);
                        }
                    }
                    catch (Exception ex)
                    {
                        var dbresults = new { Success = "False", Message = "Failed to send invitation to the Attendees. Please refresh the page or contact system Admin." };  //Need to fix this
                        return Json(dbresults, JsonRequestBehavior.AllowGet);
                    }
                    
                    
                    //var callbackUrl = Url.Action("GotMeetingConfirm", "Meetings",
                    //    new { userId = item.Id, meetingId = tblMeeting.meetingId },
                    //    protocol: Request.Url.Scheme);
                    //try
                    //{
                    //    await SendInvitationAndEmail.SendInvitationByEWS(TempData.Peek("MeetingTypeName").ToString(),
                    //        StartTime, finishTime, DateTime.ParseExact(eventdate, "dd/MM/yyyy", null), eventVenue, item.Email, tblMeeting.meetingId, User.Identity.GetUserId(), callbackUrl);
                    //}
                    //catch (Exception ex)
                    //{
                    //    Console.WriteLine(ex.StackTrace);
                    //}

                    
                }
                var results = new { Success = "True", Message = "Meeting Creation has been completed" };  //Need to fix this
                return Json(results, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var results = new { Success = "False", Message = "Event Creation Failed!!" };  //Need to fix this
                return Json(results, JsonRequestBehavior.AllowGet);
            }



        }
        [AllowAnonymous]
        [HttpGet]
        public ActionResult GotMeetingConfirm(string userId, Guid meetingId, bool resposne)
        {

            var meetingAttendess = db.tblMeetingAttendees.Where(m => m.UserId == userId && m.meetingId == meetingId)
                .FirstOrDefault();
                 meetingAttendess.userResponse = resposne;

            db.Entry(meetingAttendess).State = EntityState.Modified;
            if (db.SaveChanges() == 0)
            {
                var results = new { Success = "False", Message = "Faile to Upload the Database !!" };  //Need to fix this
                return Json(results, JsonRequestBehavior.AllowGet);
            }
            
            return View();
        }

        //   

        // GET: Meetings/Edit/5
        [HttpPost]
        public JsonResult Edit(string meetingTypeId, string meetingId, string eventdate, string eventVenue, string eventstartTime, string eventFinishTime, bool rotaryResolution)
        {
            tblMeeting tblMeeting = new tblMeeting();
            tblMeeting.meetingId = Guid.Parse(meetingId);
            tblMeeting.meetingType = Guid.Parse(meetingTypeId);
            tblMeeting.startTime = eventstartTime;
            tblMeeting.finishTime = eventFinishTime;
            tblMeeting.meetingDate = DateTime.ParseExact(eventdate, "dd/MM/yyyy", null);
            tblMeeting.meetingVenue = eventVenue;
            tblMeeting.rotaryResolution = rotaryResolution;

            db.Entry(tblMeeting).State = EntityState.Modified;
            var result = db.SaveChanges();

            if (result == 1)
            {
                var results = new { Success = "True", Message = "Meeting updated successfully." };  //Need to fix this
                return Json(results, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var results = new { Success = "False", Message = "Meeting could not be updated." };  //Need to fix this
                return Json(results, JsonRequestBehavior.AllowGet);
            }
        }

        // POST: Meetings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MeetingViewModels meetingInfo)
        {
            if (ModelState.IsValid)
            {
                tblMeeting tblMeeting = new tblMeeting();
                AutoMapper.Mapper.Map<MeetingViewModels, tblMeeting>(meetingInfo, tblMeeting);
                //AutoMapper.Mapper.Map<tblMeetingType, MeetingViewModels>(meetingType, meetingViewModel);
                db.Entry(tblMeeting).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = meetingInfo.meetingTypeId });
            }
            return RedirectToAction("Index", new { id = meetingInfo.meetingTypeId });
        }

        // GET: Meetings/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblMeeting tblMeeting = db.tblMeetings.Find(id);
            if (tblMeeting == null)
            {
                return HttpNotFound();
            }
            return View(tblMeeting);
        }

        // POST: Meetings/Delete/5
        [HttpPost]
        public JsonResult DeleteConfirmed(string id)
        {

            var meetingId = Guid.Parse(id);
            tblMeeting tblMeeting = db.tblMeetings.Find(meetingId);
            var meetingTypeId = tblMeeting.meetingType;
            db.tblMeetings.Remove(tblMeeting);
            try
            {
                db.SaveChanges();
                var results = new { Success = "True", Message = "Meeting  has been Deleted" };  //Need to fix this
                return Json(results, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var results = new { Success = "False", Message = "Could not delete " + ex.Message };  //Need to fix this
                return Json(results, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
