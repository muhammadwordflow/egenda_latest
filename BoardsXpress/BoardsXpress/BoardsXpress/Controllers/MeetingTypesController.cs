﻿using BoardsXpress.Models;
using BoardsXpress.Util;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace BoardsXpress.Controllers
{
    public class MeetingTypesController : Controller
    {
        private BoardExpressEntities db = new BoardExpressEntities();

        // GET: MeetingTypes
        [Authorize(Roles = "Super Admin,Meeting Administrator")]
        public ActionResult Index()
        {
            var usr = User.Identity.GetUserId();
            List<MeetingViewModels> meetingViewModel = new List<MeetingViewModels>();
            if (User.IsInRole("Super Admin"))
            {
                var meetingTypeList = from x in db.tblMeetingTypes orderby x.createdDate
                                      select new
                                      {
                                          MeetingTypeDetails = x
                                      };
                meetingViewModel = AutoMapper.Mapper.Map<List<tblMeetingType>, List<MeetingViewModels>>(meetingTypeList.Select(x => x.MeetingTypeDetails).ToList());
            }
            else
            {
                var defaultRoleId = db.AspNetRoles
                    .Where(m => m.fullAccess.Value).Select(m => m.Id);
                var meetingTypesByUserRole = db.AspNetUserRoles
                    .Where(m => m.UserId == usr && defaultRoleId.Any(n => n == m.RoleId))
                    .Select(m => m.MeetingTypeId);
                var meetingTypes = db.tblMeetingTypes
                    .Where(m => meetingTypesByUserRole.Any(n => n.Value == m.meetingTypeId))
                    .OrderBy(m => m.createdDate);

                AutoMapper.Mapper.Map(meetingTypes, meetingViewModel);

            }
            return View(meetingViewModel);
        }

        [HttpPost]
        public JsonResult getMeetingTypes()
        {
            var meetingTypeList = new List<MeetingTypeViewModel>();

            if (User.IsInRole("Super Admin"))
            {
                meetingTypeList = db.tblMeetingTypes
                .Select(m =>
                    new MeetingTypeViewModel
                    {
                        meetingTypeId = m.meetingTypeId,
                        meetingType = m.meetingType,
                        meetingAbbreviation = m.meetingAbbreviation,
                        useAbbreviation = m.useAbbreviation
                    }
                ).ToList();
            }
            else
            {
                var currentUserId = User.Identity.GetUserId();

                //var currentUser = db.AspNetUsers.First(m => m.Id == User.Identity.GetUserId())
                
                var user = db.AspNetUsers
                    .Include(m => m.AspNetUserRoles)
                    .First(m => 
                        m.Id == currentUserId);

                var userRolesArray = user.AspNetUserRoles.Select(m => m.MeetingTypeId);

                meetingTypeList = db.tblMeetingTypes
                    .Where(m => userRolesArray.Any(b => b == m.meetingTypeId))
                .Select(m =>
                    new MeetingTypeViewModel
                    {
                        meetingTypeId = m.meetingTypeId,
                        meetingType = m.meetingType
                    }
                ).ToList();
            }

            meetingTypeList.Insert(0, new MeetingTypeViewModel
            {
                meetingTypeId = Guid.Empty,
                meetingType = "Select the meeting type"
            });

            return Json(meetingTypeList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SelectMeeting(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblMeetingType tblMeetingType = db.tblMeetingTypes.Find(id);
            if (tblMeetingType == null)
            {
                return HttpNotFound();
            }
            MeetingsController m = new MeetingsController();
            return RedirectToAction("Index", "Meetings", new { id = tblMeetingType.meetingTypeId });

        }

        // GET: MeetingTypes/Details/5
        [HttpGet]
        public JsonResult Details(Guid meetingTypeId)
        {
            var tblMeetingType = db.tblMeetingTypes.Find(meetingTypeId);

            return Json(new {
                Success = true,
                Message = "Meeting Type has been updated successfully!",
                MeetingType = new {
                    meetingTypeId = tblMeetingType.meetingTypeId,
                    name = tblMeetingType.meetingType,
                    abbreviation = tblMeetingType.meetingAbbreviation,
                    useabbreviation = tblMeetingType.useAbbreviation
                }
            }, JsonRequestBehavior.AllowGet);
        }

        // POST: MeetingTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public JsonResult Create(tblMeetingType tblMeetingType)
        {
            var usr = User.Identity.GetUserId();

            if (String.IsNullOrEmpty(tblMeetingType.meetingType))
                return Json(new { success = false, message = "Meeting Type is empty." });
            else if(db.tblMeetingTypes.Any(m => m.meetingType == tblMeetingType.meetingType.Trim()))
                return Json(new { success = false, message = "Meeting Type already exists." });
            else
            {
                tblMeetingType.meetingTypeId = Guid.NewGuid();
                tblMeetingType.createdDate = DateTime.Now;
                db.tblMeetingTypes.Add(tblMeetingType);
                db.SaveChanges();
                //AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
                //{
                //    UserName = User.Identity.GetUserName(),
                //    UserId = new Guid(User.Identity.GetUserId()),
                //    AfterData = JsonConvert.SerializeObject(tblMeetingType),
                //    BeforeData = JsonConvert.SerializeObject(tblMeetingType),
                //    IpAddress = Request.UserHostAddress,
                //    SessionId = HttpContext.Session.SessionID,
                //    ActionPerform = "Create",
                //    UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
                //};
                //auditTrailViewModel.AuditTrail();

                var defaultRoleId = db.AspNetRoles.First(m => m.defaultForMeetingType.Value);

                // Assign user as a Meeting Admin for this Meeting
                var tblAspNetUserRole = new AspNetUserRole
                {
                    Id = Guid.NewGuid(),
                    MeetingTypeId = tblMeetingType.meetingTypeId,
                    StartDate = DateTime.Today,
                    UserId = usr,
                    RoleId = defaultRoleId.Id
                };

                db.AspNetUserRoles.Add(tblAspNetUserRole);
                db.SaveChanges();


                return Json(new { success = true });
            }
        }
        
        [HttpPost]
        public JsonResult Edit(Guid meetingTypeId, string name, string abbreviation, bool useAbbreviation)
        {
            var tblMeetingType = db.tblMeetingTypes.Find(meetingTypeId);
            var beforeUpdateTempData = JsonConvert.SerializeObject(tblMeetingType);
            if (db.tblMeetingTypes.Any(m => m.meetingType == name.Trim() && m.meetingTypeId != tblMeetingType.meetingTypeId))
                return Json(new JsonResponse { Success = false, Message = "Meeting Type already exists.", Title = "Alert" }, JsonRequestBehavior.AllowGet);
            else
            {
                tblMeetingType.meetingType = name;
                tblMeetingType.meetingAbbreviation = abbreviation;
                tblMeetingType.useAbbreviation = useAbbreviation;

                db.Entry(tblMeetingType).State = EntityState.Modified;
                db.SaveChanges();
                //AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
                //{
                //    UserName = User.Identity.GetUserName(),
                //    UserId = new Guid(User.Identity.GetUserId()),
                //    AfterData = JsonConvert.SerializeObject(tblMeetingType),
                //    BeforeData = beforeUpdateTempData,
                //    IpAddress = Request.UserHostAddress,
                //    SessionId = HttpContext.Session.SessionID,
                //    ActionPerform = "Edit",
                //    UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
                //};
                //auditTrailViewModel.AuditTrail();
                var meetingTypeViewModel = new MeetingTypeViewModel();

                AutoMapper.Mapper.Map(tblMeetingType, meetingTypeViewModel);

                var result = new JsonResponse {
                    Success = true,
                    Message = "Meeting Type has been updated successfully!",
                    Title = "Alert",
                    Data = meetingTypeViewModel
                };

                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        
        // GET: MeetingTypes/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ///Check for if meeting type is connected to any meeting
            ///if yes do not delete
            tblMeetingType tblMeetingType = db.tblMeetingTypes.Find(id);
            if (tblMeetingType == null)
            {
                return HttpNotFound();
            }
            //AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
            //{
            //    UserName = User.Identity.GetUserName(),
            //    UserId = new Guid(User.Identity.GetUserId()),
            //    AfterData = JsonConvert.SerializeObject(tblMeetingType),
            //    BeforeData = JsonConvert.SerializeObject(null),
            //    IpAddress = Request.UserHostAddress,
            //    SessionId = HttpContext.Session.SessionID,
            //    ActionPerform = "Delete",
            //    UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
            //};
            //auditTrailViewModel.AuditTrail();
            List<MeetingViewModels> meetingViewModelList = new List<MeetingViewModels>();
            var meetingList = db.tblMeetings.ToList().Where(w => w.meetingType == id).ToList();

            AutoMapper.Mapper.Map<List<tblMeeting>, List<MeetingViewModels>>(meetingList, meetingViewModelList);

            if (meetingViewModelList != null)
            {
                foreach (var item in meetingViewModelList)
                {
                    AutoMapper.Mapper.Map<tblMeetingType, MeetingViewModels>(tblMeetingType, item);
                }
            }
            else
            {
                AutoMapper.Mapper.Map<tblMeetingType, MeetingViewModels>(tblMeetingType, meetingViewModelList.FirstOrDefault());
            }



            return View(meetingViewModelList);
        }

        // POST: MeetingTypes/Delete/5
        [HttpPost]
        public JsonResult DeleteConfirmed(string[] meetingTypeId)
        {
            if (meetingTypeId != null)
            {
                foreach (var item in meetingTypeId.Where(s => !string.IsNullOrEmpty(s)))
                {
                    using (var dbContextTransaction = db.Database.BeginTransaction())
                    {
                        try
                        {
                            var id = Guid.Parse(item);

                            // Get the Meeting Type
                            var meetingType = db.tblMeetingTypes.Find(id);
                                                   
                            // Get meetings attendees related to these Meetings
                            var meetingsAttendees = db.tblMeetingAttendees.Where(m => m.meetingId == id);

                            // Get userRoles by meetingTypeId
                            var usersRoles = db.AspNetUserRoles.Where(m => m.MeetingTypeId == id);

                            // Get meetings related to this Meeting Type
                            var meetings = db.tblMeetings.Where(m => m.meetingType == id);
                            var meetingsIds = meetings.Select(m => m.meetingId).ToArray();

                            // Get agenda items related to all meetings
                            var agendaItems = from a in db.tblAgendaItems
                                              where meetingsIds.Any(b => b == a.meetingTypeId)
                                              select a;
                            var agendaItemsIds = agendaItems.Select(m => m.Id);

                            // Get all agenda Items Related to all agenda Items
                            var agendaItemsRelated = from a in db.tblAgendaItemsRelateds
                                                     where agendaItemsIds.Any(b => b == a.agendaItemId)
                                                     select a;

                            // Get all agenda associtations related to all agenda items
                            var agendaAssociations = from a in db.tblAgendaAssociations
                                                     where agendaItemsIds.Any(b => b == a.parent_agenda || b == a.child_agenda)
                                                     select a;

                            // Get all the documents related to all meetings
                            var documents = from a in db.tblDocumentLibraries
                                            where meetingsIds.Any(b => b == a.meetingId)
                                            select a;
                            var docsIds = documents.Select(m => m.docId);

                            // Get all annotations
                            var annotations = from a in db.tblAnnotations
                                              where docsIds.Any(b => b == a.docId)
                                              select a;
                            var annotationsIds = annotations.Select(m => m.annotationId);

                            // Get all annotations ranges
                            var annotationRanges = from a in db.tblAnnotationsRanges
                                                   where annotationsIds.Any(b => b == a.annotationId)
                                                   select a;

                            // Get all the documents in HTML format related to all documents
                            var documentsHTML = from a in db.tblHtmlLibraries
                                                where docsIds.Any(b => b == a.docId)
                                                select a;

                            // Get published documents by meeting
                            var publishedDocuments = from a in db.tblPublishLibraries
                                                     where meetingsIds.Any(b => b == a.meetingId)
                                                     select a;

                            // Get all the document reviews by documents
                            var docReviews = from a in db.tblDocReviews
                                             where docsIds.Any(b => b == a.docId)
                                             select a;

                            // Get all the reference documents related to the meeting type
                            var referenceDocuments = db.tblRefDocs.Where(m => m.meetingTypeId == id);

                            // Get all the RSS feeds related to the meeting type
                            var rssFeeds = db.tblRssFeedManagers.Where(m => m.meetingId == id);

                            // START DELETING THE ITEMS FROM LOWER TO HIGHER LEVEL
                            db.tblAnnotationsRanges.RemoveRange(annotationRanges);
                            db.tblAnnotations.RemoveRange(annotations);
                            db.tblRssFeedManagers.RemoveRange(rssFeeds);
                            db.tblRefDocs.RemoveRange(referenceDocuments);
                            db.tblDocReviews.RemoveRange(docReviews);
                            db.tblPublishLibraries.RemoveRange(publishedDocuments);
                            db.tblHtmlLibraries.RemoveRange(documentsHTML);
                            db.tblDocumentLibraries.RemoveRange(documents);
                            db.tblAgendaItemsRelateds.RemoveRange(agendaItemsRelated);
                            db.tblAgendaAssociations.RemoveRange(agendaAssociations);
                            db.tblAgendaItems.RemoveRange(agendaItems);
                            db.tblMeetings.RemoveRange(meetings);
                            db.AspNetUserRoles.RemoveRange(usersRoles);

                            db.tblMeetingTypes.Remove(meetingType);

                            if (db.SaveChanges() == 0)
                            {
                                dbContextTransaction.Rollback();
                                return Json(new { Success = false, Message = "Meeting Type was not deleted!" }, JsonRequestBehavior.AllowGet);
                            }

                            dbContextTransaction.Commit();
                            //AuditTrailViewModel auditTrailViewModel = new AuditTrailViewModel
                            //{
                            //    UserName = User.Identity.GetUserName(),
                            //    UserId = new Guid(User.Identity.GetUserId()),
                            //    AfterData = JsonConvert.SerializeObject(meetingType),
                            //    BeforeData = JsonConvert.SerializeObject(null),
                            //    IpAddress = Request.UserHostAddress,
                            //    SessionId = HttpContext.Session.SessionID,
                            //    ActionPerform = "Delete",
                            //    UrlAccess = $"{this.ControllerContext.RouteData.Values["controller"].ToString()}/{this.ControllerContext.RouteData.Values["action"].ToString()}"
                            //};
                            //auditTrailViewModel.AuditTrail();
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            return Json(new { Success = false, Message = "Meeting Type was not deleted!" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                return Json(new { Success = true, Message = "Meeting Type was created successfully!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var results = new { Success = "False", Message = "Select Meeting Type you want to delete" };  //Need to fix this
                return Json(results, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
