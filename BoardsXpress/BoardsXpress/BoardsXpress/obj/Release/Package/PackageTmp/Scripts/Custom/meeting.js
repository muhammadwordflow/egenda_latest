﻿$(document).ready(function () {
    // Set validation rules to all the needed forms on the page.
    //setFormValidation('#frmAddEvent', '#btnSubmit');
    //setFormValidation('#frmEditEvent', '#done');
    //Set validation rules to all the needed forms on the page.

    $('#eventDate').datepicker({
        format: "dd/mm/yyyy"
    });

    $('#eventDate').on('changeDate', function () {
        $("#divEventDate").removeClass("has-error");   
        $("#eventDate-error").css({ display: "none" });
        $('#eventDate').datepicker('hide');
        $('#eventStartTime').focus();
    });

    $('#editEventDate').datepicker({
        format: "dd/mm/yyyy"
    });

    $('#editEventDate').on('changeDate', function () {
        $("#divEditEventDate").removeClass("has-error");
        $("#editEventDate-error").css({ display: "none" });
        $('#editEventDate').datepicker('hide');
        $('#editEventStartTime').focus();
    });
   
    // For adding event
    $('#eventStartTime').timepicker({
        'minTime': '8:00am',
        'maxTime': '10.00pm',
        'step': '15'
    });

    $('#eventFinishTime').timepicker({
        'minTime': '8:00am',
        'maxTime': '10.00pm',
        'step': '15',
        'showDuration': true
    });

    $('#eventStartTime').on('changeTime', function () {
        $('#eventFinishTime').timepicker('option', 'minTime', $(this).val());
        $('#eventFinishTime').focus();
    });

    //For edit event
    $('#editEventStartTime').timepicker({
        'minTime': '8:00am',
        'maxTime': '10.00pm',
        'step': '15'
    });

    $('#editEventFinishTime').timepicker({
        'minTime': '8:00am',
        'maxTime': '10.00pm',
        'step': '15',
        'showDuration': true
    });

    $('#editEventStartTime').on('changeTime', function () {
        $('#editEventFinishTime').timepicker('option', 'minTime', $(this).val());
        $('#editEventFinishTime').focus();
    });


    // Add Event Validation

    $("#frmAddEvent").validate({
        rules: {
            eventDate: {
                required: true
            },
            meetingStartTime: {
                required: true
            }
        },

        messages: {
            eventDate: {
                required: "Meeting Date Required."
            },
            meetingStartTime: {
                required: "Meeting Start Time Required."
            }
        },

        highlight: function (element) {
            $(element).closest('div').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass('has-error').addClass('has-success');
        }
    });

    // Edit Event Validation

    $("#frmEditEvent").validate({
        rules: {
            editEventDate: {
                required: true
            },
            editEventStartTime: {
                required: true
            }
        },

        messages: {
            eventDate: {
                required: "Meeting Date Required."
            },
            meetingStartTime: {
                required: "Meeting Start Time Required."
            }
        },

        highlight: function (element) {
            $(element).closest('div').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass('has-error').addClass('has-success');
        }
    });

    //add new calendar event
    $("#btnSubmit").click(function () {
        if ($("#frmAddEvent").valid()) {

            var id = $("#MeetingTypesID").html();
            var date = $("#eventDate").val();
            var startTime = $("#eventStartTime").val();
            var finishTime = $("#eventFinishTime").val();
            var venue = $("#eventVenue").val();
            var rotaryResolution = $("#rotaryResolution").is(":checked");
            $.ajax({
                url: 'Meetings/Create',
                async: false,
                type: "POST",
                data: JSON.stringify({
                    "id": id,
                    "eventdate": date,
                    "StartTime": startTime,
                    "finishTime": finishTime,
                    "eventVenue": venue,
                    "rotaryResolution": rotaryResolution
                }),
                dataType: "json",
                contentType: "application/json",
                error: function (data) {
                    console.log(data);
                },
                success: function (data) {
                    location.reload();
                }
            });
        }
    });

    // Edit user data
    $(".editEvent").unbind().click(function (event) {
        // get parent TR
        var parentRow = $(this).closest("#row");

        // Set values in the modal:
        $("#editMeetingId").val(parentRow.find("td[data-column='meetingId']").text());
        $("#editEventDate").val(parentRow.find("td[data-column='meetingDate']").text());
        $("#editEventVenue").val(parentRow.find("td[data-column='meetingVenue']").text());
        $("#editEventStartTime").val(parentRow.find("td[data-column='meetingStartTime']").text());
        $("#editEventFinishTime").val(parentRow.find("td[data-column='meetingFinishTime']").text());
        if (parentRow.find("td[data-column='rotaryResolution']").text() == 'True') {
            $('#editRotaryResolution').prop('checked', true);
        } else {
            $('#editRotaryResolution').prop('checked', false);
        }
        //edit ajax call
        $('#done').unbind().click(function() {
            if ($("#editEventDate").val() === '') {
                $("#editEventDate").attr('placeholder', 'please select the meeting date');
            } else {
                if ($("#frmEditEvent").valid()) {
                    $.ajax({
                        url: 'Meetings/Edit',
                        async: false,
                        type: "POST",
                        data: JSON.stringify({
                            "meetingTypeId": $("#MeetingTypesID").html(),
                            "meetingId": $("#editMeetingId").val(),
                            "eventdate": $("#editEventDate").val().replace(' (Rotary Resolution)', ''),
                            "eventVenue": $("#editEventVenue").val(),
                            "eventstartTime": $("#editEventStartTime").val(),
                            "eventFinishTime": $("#editEventFinishTime").val(),
                            "rotaryResolution": $("#editRotaryResolution").is(":checked")
                        }),
                        dataType: "json",
                        contentType: "application/json",
                        error: function(data) {
                            console.log(data.Message)
                        },
                        success: function(data) {
                            location.reload();
                        }
                    });
                }
            }
        });

    });

    //function to check multiple calendarEvent
    var multipleCalendarCheck = function (event) {
        //variable to check the number of current meeting
        var count = 0;
        // gets all the current meeting items out of the table row
        var allCurrent = $('#sort tr td:nth-child(7)');
        //get number of current meeting items
        for (var i = 0; i < allCurrent.length; i++) {
            if (allCurrent[i].innerHTML === 'Yes') {
                count++;
            }
        }
        //get the changed option value of currrent meeting
        var check = $(event).val();
        //if current meeting equals 1 increase counter
        if (check === '1') {
            count += 1;
        }

        if (count >= 2) {
            return false;
        } else {
            return true;
        }

    }

    $('#okay').click(function () {
        location.reload();
    })

    // Delete users
    $(".delete").unbind().click(function (event) {
        var meetingId = $(this).closest('tr').children()[0].textContent.trim();
        //console.log(meetingId);
        $('#yes').click(function () {
            $.ajax({
                url: 'Meetings/DeleteConfirmed',
                async: true,
                type: "POST",
                data: JSON.stringify({
                    "id": meetingId
                }),
                dataType: "json",
                contentType: "application/json",
                error: function (data) {
                    console.log(data.Message);
                },
                success: function (data) {
                    location.reload();
                }
            });
        });
    });

    //remove data when modal is hidden
    $(".modal").on("hidden.bs.modal", function () {
        $(this).removeData('bs.modal');
    });

    // Add meeting calendar validation
    $('.calendarValidation').validate({
        feedback: {
            success: 'glyphicon-ok',
            error: 'glyphicon-remove'
        },
        rules: {
            eventDate: "required"
        },
        messages: {
            'eventDate': {
                required: "The meeting date cannot be left empty."
            }
        }
    });

    //disable submit button until calendar meeting date is filled
    //$('#eventDate').on('change', function (e) {
    //    if ($(this).val() === '') {
    //        $('.submitEvent').attr('disabled', 'disabled');
    //    } else {
    //        $('.submitEvent').removeAttr('disabled');
    //    }
    //});

    //Calendar Edit validation
    $('.calendarEdit').validate({
        feedback: {
            success: 'glyphicon-ok',
            error: 'glyphicon-remove'
        },
        rules: {
            editEventDate: "required",
        },
        messages: {
            'editEventDate': {
                required: "The meeting date cannot be left empty."
            }
        }
    });

//disable submit button if meeting date is empty
//$('#editEventDate').on('change', function (e) {
//    console.log($('#editEventDate').val());
//    if ($(this).val() === '') {
//        $('#done').attr('disabled', 'disabled');
//    } else {
//        $('#done').removeAttr('disabled');
//    }
//});


// set forms validation
//function setFormValidation(frmId, saveBtnId) {

//    $(frmId + ' input').on('keyup blur', function () { // fires on every keyup & blur
//        $(saveBtnId).prop('disabled', !$(frmId).valid());        // enable/disable button
//    });
//}


});

