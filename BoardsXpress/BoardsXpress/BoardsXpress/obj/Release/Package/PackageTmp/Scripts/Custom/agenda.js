﻿var Agenda = {
    getAgendaItemsList: function getAgendaItemsList(meetingTypeId, meetingId) {
        $.ajax({
            url: '/AgendaItems/GetAgendaItemsList',
            async: false,
            type: "GET",
            data: {
                meetingTypeId: meetingTypeId,
                meetingId: meetingId
            },
            contentType: "application/json",
            dataType: "html",
            success: function (data) {
                $('#agendaItemsList').html(data);
            },
            statusCode: {
                404: function () {
                    alert("page not found");
                },
                500: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            }
        });
    },
    setDefaultFormValues: function setDefaultFormValues() {
        $("#agendaItemName").val("");
        $("#chkRecurringItem").prop("checked", true);
    },
    addAgendaItem: function addAgendaItem() {
        $.ajax({
            url: '/AgendaItems/Create',
            type: "POST",
            data: JSON.stringify({
                "agendaName": $("#agendaItemName").val(),
                "meetingId": $("#_meetingId").text(),
                "draftMinutesItem": $("#chkDraftMinutesItem").prop("checked"),
                "recurringItem": $("#chkRecurringItem").prop("checked")
            }),
            contentType: "application/json",
            success: function (result) {
                Agenda.closeModalOnSuccess();
            },
            error: function (error) {
                console.log(error);
            }
        });
    },
    editAgendaItem: function editAgendaItem() {

        $.ajax({
            url: '/AgendaItems/Edit',
            async: false,
            type: "POST",
            data: JSON.stringify({
                "agendaId": $("#agendaId").val(),
                "agendaName": $("#agendaItemName").val(),
                "meetingId": $("#meetingId").val(),
                "draftMinutesItem": $("#chkDraftMinutesItem").prop("checked"),
                "recurringItem": $("#chkRecurringItem").prop("checked")
            }),
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                Agenda.closeModalOnSuccess()
            },
            error: function (data) {
                console.log(data);
            }
        });
    },
    addAgendaItemRelated: function addAgendaItemRelated() {
        $.ajax({
            url: '/AgendaItems/CreateAgendaItemRelated',
            type: "POST",
            data: JSON.stringify({
                "agendaItemId": $("#agendaItemRelated_agendaItemId").val(),
                "name": $("#agendaItemRelated_Name").val(),
                "recurringItem": $("#chkRecurringRelatedItem").prop("checked")
            }),
            contentType: "application/json",
            success: function (result) {
                Agenda.closeModalOnSuccess();
            },
            error: function (error) {
                console.log(error);
            }
        });
    },
    editAgendaItemRelated: function editAgendaItemRelated() {
        $.ajax({
            url: '/AgendaItems/EditAgendaItemRelated',
            async: false,
            type: "POST",
            data: JSON.stringify({
                "id": $("#agendaItemRelated_Id").val(),
                "agendaItemId": $("#agendaItemRelated_agendaItemId").val(),
                "name": $("#agendaItemRelated_Name").val(),
                "recurringItem": $("#chkRecurringRelatedItem").prop("checked")
            }),
            dataType: "json",
            contentType: "application/json",
            error: function (data) {
                console.log(data);
            },
            success: function (data) {
                Agenda.closeModalOnSuccess();
            }
        });
    },
    closeModalOnSuccess: function closeModalOnSuccess() {
        Agenda.setDefaultFormValues();
        $('#addPigeon').modal('hide');
        $('#agendaItemRelatedModal').modal('hide');
        $('#deleteModal').modal('hide');

        Agenda.getAgendaItemsList($('#MeetingTypesID').text(), $('#_meetingId').text());
    },
    loadParentAgendaItems: function loadParentAgendaItems(agendaItemId, parentAgendaId) {
        $.getJSON(
            '/AgendaItems/GetParentAgendaItems',
            { agendaItemId: agendaItemId, meetingId: $("#_meetingId").text() },
            function (result) {
                var options = $("#parentAgendaId");

                options.empty();

                $.each(result.agendaItems, function () {
                    options.append($("<option />").val(this.Id).text(this.agendaItem));
                });

                options.val(parentAgendaId);
            });
    }
}

$(".dropdown-menu li a").click(function () {
    $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
    $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
});

$(document).ready(function () {
    Agenda.getAgendaItemsList($('#MeetingTypesID').text(), $('#_meetingId').text());

    $(document).on('click', '.btnAgendaItem', function () {
        // Set btnSubmit action 
        $("#btnSubmit").data('action', $(this).data("action"));

        // Check if the the action is from a new button or an edit button
        if ($(this).data("action") === "add") {
            // Clear any data on the form
            Agenda.setDefaultFormValues();

            // Set the button value to Save
            $("#btnSubmit").text("Add");

            // Set the modal-title to the correct string
            $("#agendaItemTitle").text("Add new");
        }
        else {
            // Set the button value to Done
            $("#btnSubmit").text("Done");

            // Set the modal-title to the correct string
            $("#agendaItemTitle").text("Edit");

            $('#addPigeon').data('currentRow', $(this).closest("#row"));

            var parentRow = $(this).closest("#row");

            // Set values in the modal:
            $("#agendaId").val(parentRow.find("td[data-column='agendaId']").text());
            $("#meetingId").val(parentRow.find("td[data-column='meetingId']").text());
            $("#meetingTypeId").val(parentRow.find("td[data-column='meetingTypeId']").text());
            $("#agendaItemName").val(parentRow.find("td[data-column='agendaName']").text());
            $("#chkRecurringItem").prop("checked", parentRow.find("td[data-column='agendaRecurringItem'] input").prop("checked"));
            $("#printRestriction").prop("checked", parentRow.find("td[data-column='printRestriction'] input").prop("checked"));
        }
    });

    $('#addPigeon').on('shown.bs.modal', function () {
        $('#frmAgendaItem').focus_first();
    });

    $('#frmAgendaItem').submit(function (e) {
        e.preventDefault();

        if ($("#btnSubmit").data('action') === 'add')
            Agenda.addAgendaItem();
        else
            Agenda.editAgendaItem();
    });

    $(document).on('click', '.btnAgendaItemRelated', function (ev) {
        // Set btnSubmit action 
        $("#btnSubmitAgendaItemRelated").data('action', $(this).data("action"));

        // get parent TR
        var parentRow = $(this).closest("#row");

        // Check if the the action is from a new button or an edit button
        if ($(this).data("action") === "add") {
            // Clear any data on the form
            $('#frmAgendaItemRelated').clearForm();

            // Set the button value to Save
            $("#btnSubmitAgendaItemRelated").text("Add");

            // Set the modal-title to the correct string
            $("#agendaItemRelatedTitle").text("Add new");

            // Set values in the modal
            $("#agendaItemRelated_agendaItemId").val(parentRow.find("td[data-column='agendaId']").text());
        }
        else {
            // Set the button value to Done
            $("#btnSubmitAgendaItemRelated").text("Done");

            // Set the modal-title to the correct string
            $("#agendaItemRelatedTitle").text("Edit");
            
            // Set values in the modal:
            $("#agendaItemRelated_Id").val(parentRow.find("td[data-column='id']").text());
            $("#agendaItemRelated_agendaItemId").val(parentRow.find("td[data-column='agendaItemId']").text());
            $("#agendaItemRelated_Name").val(parentRow.find("td[data-column='name']").text());
            $("#chkRecurringRelatedItem").prop("checked", parentRow.find("td[data-column='recurringItem'] input").prop("checked"));
        }
    });

    $('#agendaItemRelatedModal').on('shown.bs.modal', function () {
        $('#frmAgendaItemRelated').focus_first();
    });

    $('#frmAgendaItemRelated').submit(function (e) {
        e.preventDefault();

        if ($("#btnSubmitAgendaItemRelated").data('action') === 'add')
            Agenda.addAgendaItemRelated();
        else
            Agenda.editAgendaItemRelated();
    });

    // Delete Agenda item
    $(document).on('click', '.btnDeleteAgendaItem', function () {
        $('#yes').data('source', 'agendaItem');
        $('#yes').data('parentRow', $(this).closest("#row"));
    });

    // Delete Agenda Item Related
    $(document).on('click', '.btnDeleteAgendaItemRelated', function () {
        $('#yes').data('source', 'agendaItemRelated');
        $('#yes').data('parentRow', $(this).closest("#row"));
    });

    $(document).on('click', '#yes', function () {

        var parentRow = $('#yes').data('parentRow');
        
        // Check the source of request
        if ($('#yes').data('source') === 'agendaItem') {
            $.ajax({
                url: '/AgendaItems/DeleteConfirmed',
                async: false,
                type: "POST",
                data: JSON.stringify({
                    "id": parentRow.find("td[data-column='agendaId']").text(),
                    "meetingId": parentRow.find("td[data-column='meetingId']").text()
                }),
                dataType: "json",
                contentType: "application/json",
                error: function (data) {
                    console.log(data);
                },
                success: function (data) {
                    Agenda.closeModalOnSuccess();
                }
            });
        }
        else {
            $.ajax({
                url: '/AgendaItems/DeleteAgendaItemRelated',
                async: false,
                type: "POST",
                data: JSON.stringify({
                    "id": parentRow.find("td[data-column='id']").text(),
                    "agendaItemId": parentRow.find("td[data-column='agendaItemId']").text()
                }),
                dataType: "json",
                contentType: "application/json",
                error: function (data) {
                    console.log(data);
                },
                success: function (data) {
                    Agenda.closeModalOnSuccess();
                }
            });
        }
        
    });
    
    $(document).on('click', '.btnUpDown', function (ev) {
        var btn = $(ev.currentTarget);
        var currentRow = btn.closest('tr#row');
        var destinationRow;

        if (btn.data('action') === 'up') {
            destinationRow = currentRow.prev('tr#row').length === 0 ? currentRow.prevAll('tr#row')[0] : currentRow.prev('tr#row');
        }
        else if (btn.data('action') === 'down') {
            destinationRow = currentRow.next('tr#row').length === 0 ? currentRow.nextAll('tr#row')[0] : currentRow.next('tr#row');
        }

        if ($(currentRow).length > 0 && $(destinationRow).length > 0)
        {
            if ($(currentRow).find('td[data-column="agendaId"]').length > 0) {
                $.ajax({
                    url: '/AgendaItems/UpdateSortingOrder',
                    async: false,
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "meetingId": $(currentRow).find("td[data-column='meetingId']").text(),
                        "agendaItemId": $(currentRow).find("td[data-column='agendaId']").text(),
                        "agendaItemRelatedId": null,
                        "draggedOrder": $(currentRow).data('item-order'),
                        "swappedAgendaItemdId": $(destinationRow).find("td[data-column='agendaId']").text(),
                        "swappedAgendaItemRelatedId": null,
                        "swappedOrder": $(destinationRow).data('item-order')
                    }),
                    success: function (result) {
                        Agenda.getAgendaItemsList($('#MeetingTypesID').text(), $('#_meetingId').text());
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
            else {
                $.ajax({
                    url: '/AgendaItems/UpdateSortingOrder',
                    async: false,
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify({
                        "meetingId": $(currentRow).find("td[data-column='meetingId']").text(),
                        "agendaItemId": $(currentRow).find("td[data-column='agendaItemId']").text(),
                        "agendaItemRelatedId": $(currentRow).find("td[data-column='id']").text(),
                        "draggedOrder": $(currentRow).data('item-order'),
                        "swappedAgendaItemdId": $(destinationRow).find("td[data-column='agendaItemId']").text(),
                        "swappedAgendaItemRelatedId": $(destinationRow).find("td[data-column='id']").text(),
                        "swappedOrder": $(destinationRow).data('item-order')
                    }),
                    success: function (result) {
                        Agenda.getAgendaItemsList($('#MeetingTypesID').text(), $('#_meetingId').text());
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
        }
    });
});