﻿var loc = window.location.pathname;
$('.nav').find('a').each(function () {
    $(this).toggleClass('active', $(this).attr('href') == loc);
});

$('.refDocs').bind("click", function () {
    $('.refDocMenu').html("");
    $.ajax({
        url: '/RefDocs/getRefNavItem',
        type: "GET",
        success: function (result) {
            for (var i = 0; i < result.Message.length; i++) {
                $('.refDocMenu').append('<li><a href="#" class="getHTMLContent">' + result.Message[i] + '</a></li>');
            }

            $('.getHTMLContent').click(function () {
                var clicked = $(this).text();
                $.ajax({
                    url: '/Umbraco/Surface/handlePublishing/htmlForEachPegionHoleItem',// the method we are calling
                    type: "POST",
                    data: JSON.stringify({ "pegItemName": clicked, "database": "tblHtmlContent", "isRef": 1 }),
                    contentType: "application/json",
                    success: function (result) {
                        if (!result) {
                            $('#ajaxModal').modal('show');
                            return;
                        }
                        var pattern = new RegExp('^(?:[a-z]+:)?//', 'i')
                        if (pattern.test(result)) {
                            var win = window.open(result, '_blank');
                            win.focus();
                        }
                        $('.bodyContainer').html(result);
                        $('.active').removeClass('active');
                        $('.refDocs').addClass('active');
                    },
                    error: function (error) {
                        $('#ajaxModal').find('.modal-title')[0].innerHTML = 'Message';
                        $('#ajaxModal').find('#ajaxModalBody')[0].innerHTML = 'The content has not been published yet.';
                        $('#ajaxModal').modal('show');
                    }
                });
            })
        },
        error: function (error) {
            //console.log(error);
        }
    });
});
//get future meeting dates
$('.refDocs').bind("click", function () {
    $('.calendarMenu').html("");
    $.ajax({
        url: '/Umbraco/Surface/handlePublishing/getFutureMeetingDates',
        type: "GET",
        success: function (result) {
            for (var i = 0; i < result.Message.length; i++) {
                $('.calendarMenu').append('<li><a href="#" class = "getFutureMeetingDetails">' + result.Message[i] + '</a></li>');
            }
            $('.getFutureMeetingDetails').click(function () {
                var clicked = $(this).text();
                $('#ajaxModal').find('.modal-title')[0].innerHTML = 'Future Meeting Date';
                $('#ajaxModal').find('#ajaxModalBody')[0].innerHTML = 'This meeting is on: ' + clicked;
                $('#ajaxModal').modal('show');
            })
        },
        error: function (error) {
            //console.log(error);
        }
    });
});
