﻿$(document).ready(function () {
    getPublishedDocumentsList($('#MeetingTypesID').text(), $('#currentMeetingId').text());

    $(document).on('click', '#publish', function () {
        var bar = $('.progress-bar');
        var percent = $('.progress-bar');
        var status = $('#status');
        var meetingId;
        var allCheckBox = $("[class^='data']");
        var count_checked = allCheckBox.filter(":checked").length;

        if (count_checked == 0) {
            $('#checkModal').modal('show');
        } else if (count_checked != allCheckBox.length) {
            $('#checkModal').modal('show');
        } else {
            $('#publishModal').modal('show');
            $("#yesPublish").click(function () {
                var docs = {};
                var finalDocs = [];
                $("#row.contentRow").each(function () {
                    var docId = $(this).find("td[data-column='docId']").text();
                    meetingId = $(this).find("td[data-column='meetingId']").text();
                    finalDocs.push(docId);
                });
                var percent = $('.progress-bar');
                $.ajax({
                    url: '/Publish/publishContent',
                    type: "POST",
                    async: true,
                    beforeSend: function () {
                        $('#publishModal').modal('hide');
                        $('#progressModal').modal('show');
                    },
                    datatype: JSON,
                    data: JSON.stringify({ "data": finalDocs, "meetingID": meetingId }),
                    contentType: "application/json",
                    success: function (result) {
                        $('#progressModal').modal('hide');
                        $('#sucessModal').modal('show');
                        $('.successful').click(function () {
                            getPublishedDocumentsList($('#MeetingTypesID').text(), $('#currentMeetingId').text());
                        });
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            })
        }
    });

    

    //$(document).unbind().on('change', "input[type='checkbox']", function () {
        
    //});

    $(document).on('click', '.delete', function () {
        var parentRow = $(this).closest('tr#row');

        $('#deleteRepo').click(function () {
            $.ajax({
                url: '/Publish/DeleteDocument',
                async: true,
                type: "POST",
                data: JSON.stringify({
                    "docId": parentRow.find('td[data-column="docId"]').text(),
                    "meetingId": parentRow.find('td[data-column="meetingId"]').text(),
                    "agendaItemId": parentRow.find('td[data-column="agendaItemId"]').text()
                }),
                dataType: "json",
                contentType: "application/json",
                error: function (data) {
                    console.log(data);
                },
                success: function (data) {
                    getPublishedDocumentsList($('#MeetingTypesID').text(), $('#currentMeetingId').text());

                    $('#deleteModal').modal('hide');
                }
            });
        });
    });


});

//function changeCheckboxState(ev) {
//    var parentRow = $(ev).closest('tr#row');

//    var changedItem = parentRow.find('td[data-column="docId"]').text();
//    var itemState = $(ev).is(':checked');
//    var identifyHeader = $(ev).hasClass('data1');
//    var changedHeader;

//    if (identifyHeader) {
//        changedHeader = 'Review';
//    } else {
//        changedHeader = 'Approve';
//    }

//    $.ajax({
//        url: '/Publish/updateReview',
//        type: "POST",
//        async: false,
//        datatype: JSON,
//        data: JSON.stringify({
//            "review": changedHeader,
//            "status": itemState,
//            "docId": parentRow.find('td[data-column="docId"]').text(),
//            "meetingId": parentRow.find('td[data-column="meetingId"]').text(),
//            "agendaItemId": parentRow.find('td[data-column="agendaItemId"]').text(),
//            "agendaItemRelatedId": parentRow.find('td[data-column="relatedItemId"]').text()
//        }),
//        contentType: "application/json",
//        success: function (result) {
//            getPublishedDocumentsList($('#MeetingTypesID').text(), $('#currentMeetingId').text());
//        },
//        error: function (error) { }
//    });
//}

function getPublishedDocumentsList(meetingTypeId, meetingId) {
    $.ajax({
        url: '/Publish/GetPublishedDocumentsList',
        async: false,
        type: "GET",
        data: {
            meetingTypeId: meetingTypeId,
            meetingId: meetingId
        },
        dataType: "html",
        success: function (data) {
            $('#publishedDocumentsList').html(data)
        }
    });
}