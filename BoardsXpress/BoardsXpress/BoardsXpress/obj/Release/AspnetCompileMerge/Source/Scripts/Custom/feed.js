﻿var Feed = {
    setDefaultFormValues: function setDefaultFormValues() {
        $("#feedId").val("");
        $("#txtFeedProvider").val("");
        $("#txtFeedURL").val("");
    },
    addFeed: function addFeed() {
        $.ajax({
            url: '/FeedManager/Create',
            type: "POST",
            async: true,
            data: JSON.stringify({
                "meetingTypeId": $("#meetingTypeId").val(),
                "feedProvider": $("#txtFeedProvider").val(),
                "feedURL": $("#txtFeedURL").val()
            }),
            contentType: "application/json",
            success: function (data) {
                Feed.closeModalOnSuccess();

                $('#sucessUploadModal').find('.modal-title')[0].innerHTML = data.Title;
                $('#sucessUploadModal').find('.modal-body')[0].innerHTML = data.Message;
                $('#sucessUploadModal').modal('show');
            },
            error: function (error) {
                console.log(error);
            }
        });
    },
    editFeed: function editFeed() {

        $.ajax({
            url: '/FeedManager/Edit',
            async: true,
            type: "POST",
            data: JSON.stringify({
                "id": $("#feedId").val(),
                "meetingTypeId": $("#meetingTypeId").val(),
                "feedProvider": $("#txtFeedProvider").val(),
                "feedURL": $("#txtFeedURL").val()
            }),
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                Feed.closeModalOnSuccess();

                $('#sucessUploadModal').find('.modal-title')[0].innerHTML = data.Title;
                $('#sucessUploadModal').find('.modal-body')[0].innerHTML = data.Message;
                $('#sucessUploadModal').modal('show');
            },
            error: function (data) {
                console.log(data);
            }
        });
    },
    getManageFeedList: function getManageFeedList(meetingTypeId) {
        $.ajax({
            url: '/FeedManager/GetManageFeedList',
            async: false,
            type: "GET",
            data: {
                meetingTypeId: meetingTypeId
            },
            dataType: "html",
            success: function (data) {
                $('#manageFeedList').html(data)
            }
        });
    },
    closeModalOnSuccess: function closeModalOnSuccess() {
        Feed.setDefaultFormValues();

        $('#addFeed').modal('hide');
        $('#deleteModal').modal('hide');

        Feed.getManageFeedList($('#manageFeedList').data('meetingtypeid'));
    }
}

$(document).ready(function () {
    Feed.getManageFeedList($('#manageFeedList').data('meetingtypeid'));

    $(document).on('click', '.btnFeed', function () {
        // Set btnSubmit action 
        $("#btnSubmit").data('action', $(this).data("action"));

        // Check if the the action is from a new button or an edit button
        if ($(this).data("action") === "add") {
            // Clear any data on the form
            Feed.setDefaultFormValues();

            // Set the button value to Save
            $("#btnSubmit").text("Add");

            // Set the modal-title to the correct string
            $("#feedTitle").text("Add new");
        }
        else {
            // Set the button value to Done
            $("#btnSubmit").text("Done");

            // Set the modal-title to the correct string
            $("#feedTitle").text("Edit");

            $('#addFeed').data('currentRow', $(this).closest("#row"));

            var parentRow = $(this).closest("#row");

            // Set values in the modal:
            $("#feedId").val(parentRow.find("td[data-column='id']").text());
            $("#meetingTypeId").val(parentRow.find("td[data-column='meetingTypeId']").text());
            $("#txtFeedProvider").val(parentRow.find("td[data-column='feedProvider']").text());
            $("#txtFeedURL").val(parentRow.find("td[data-column='feedURL']").text());
        }
    });

    $('#addFeed').on('shown.bs.modal', function () {
        $('#frmFeed').focus_first();
    });

    $(document).on('submit', '#frmFeed', function (e) {
        e.preventDefault();

        if ($("#btnSubmit").data('action') === 'add')
            Feed.addFeed();
        else
            Feed.editFeed();
    });
    
    $('#deleteModal').on('shown.bs.modal', function () {
        var parentRow = $(this).data('parentRow');

        $('#deleteFeedName').text(parentRow.find("td[data-column='feedProvider']").text());
    });

    $('#deleteModal').on('hidden.bs.modal', function () {
        $('#deleteFeedName').text('');
    });

    $(document).on('click', '.btnDeleteFeed', function () {
        $('#deleteModal').data('parentRow', $(this).closest("#row"));
    });

    $(document).on('click', '#yes', function () {

        var parentRow = $('#deleteModal').data('parentRow');

        $.ajax({
            url: '/FeedManager/DeleteFeed',
            async: true,
            type: "POST",
            data: JSON.stringify({
                "id": parentRow.find("td[data-column='id']").text(),
                "meetingTypeId": parentRow.find("td[data-column='meetingTypeId']").text()
            }),
            dataType: "json",
            contentType: "application/json",
            error: function (data) {
            },
            success: function (data) {
                Feed.closeModalOnSuccess();
            }
        });
    });
});