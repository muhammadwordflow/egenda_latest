﻿var _docId = "";

// prepare the form when the DOM is ready
$(document).ready(function () {
    setbtnPrintDocumentState($("#btnPrintDocument"));


    getHomeTreeView();
    getTreeView();

    $('#btnPrintDocument').click(function () {
        var progress = $('.progress');
        var bar = $('.progress-bar');
        var percent = $('.progress-bar');
        var modalPrintDocument = $('#modalPrintDocument');
        var modalPrintDocumentContent = $('#modalPrintDocumentContent');

        $('#frmPrintDocument').ajaxForm({
            beforeSerialize: function ($form, options) {
                // return false to cancel submit   
                $('#_docId').val(_docId);
            },
            beforeSend: function () {
                progress.show();
                modalPrintDocument.modal('show');
                modalPrintDocumentContent.html("The document is being sent to the printer...");
            },
            success: function (data) {
                modalPrintDocumentContent.html(data.Message);

                progress.hide();
            },
            complete: function () {
                setTimeout(function () {
                    modalPrintDocument.modal('hide');
                }, 1000);
            }
        });
    });
});

//function load_js() {
//    var head = document.getElementsByTagName('head')[0];
//    var script = document.createElement('script');
//    script.type = 'text/javascript';
//    script.src = '/scripts/Custom/annotate.js';
//    head.appendChild(script);
//}

function setbtnPrintDocumentState(btn) {
    btn.prop('disabled', _docId === null);
}

function getTreeView() {
    // Get the treeview
    $.ajax({
        url: '/Home/GetTreeViewNavigation',
        async: false,
        data: {
            meetingId: $('#treeView').data('key')
        },
        type: "GET",
        dataType: 'json',
        success: function (result) {
            $('#treeView').treeview({
                color: "#ffffff",
                backColor: "transparent",
                onhoverColor: "#ef4123",
                selectedColor: "#ffffff",
                selectedBackColor: "#ef4123",
                showBorder: false,
                enableLinks: true,
                expandIcon: 'glyphicon glyphicon glyphicon-triangle-right',
                collapseIcon: 'glyphicon glyphicon glyphicon-triangle-bottom',
                emptyIcon: 'glyphicon glyphicon glyphicon-triangle-bottom',
                onNodeSelected: function (event, node) {
                    loadHtmlContent(node["data-docId"]);

                    $('#treeView').treeview('toggleNodeExpanded', [node.nodeId, { silent: true }]);
                },
                data: result
            });

            $('#treeView').treeview('collapseAll', { silent: true });
        }
    });
}

function getHomeTreeView() {
    // Get the treeview
    $.ajax({
        url: '/Home/GetTreeStucuteForMeeting',
        async: false,
        type: "GET",
        dataType: 'json',
        success: function (result) {
            $('#hometree').treeview({
                color: "#ffffff",
                backColor: "transparent",
                onhoverColor: "#ef4123",
                selectedColor: "#ffffff",
                selectedBackColor: "#ef4123",
                showBorder: false,
                enableLinks: true,
                expandIcon: 'glyphicon glyphicon glyphicon-triangle-right',
                collapseIcon: 'glyphicon glyphicon glyphicon-triangle-bottom',
                emptyIcon: 'glyphicon glyphicon glyphicon-triangle-bottom',
                //onNodeSelected: function (event, node) {
                //    //$('#hometree').treeview('toggleNodeExpanded', [node.nodeId, { silent: true }]);
                //},
                data: result

            });

            $('#hometree').treeview('collapseAll', { silent: true });
        }
    });
}

function loadHtmlContent(docId) {
    // Set the selected doc to the variable
    _docId = docId;
    
    setbtnPrintDocumentState($("#btnPrintDocument"));

    if (docId !== null && typeof docId !== 'undefined') {
        $.ajax({
            url: '/DisplayHtml/getHtmlData',
            async: false,
            type: "GET",
            data: { docId: docId },
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                
                AnnotatorJS.init(docId);

                $(".doc-content").data('doc-id', docId);

                $("#mainContent").html(data.htmlContent);

                getRightContentPanel(docId, data.meetingId);

                AnnotatorJS.load();
            }
        });
    }
    else {
        getRightContentPanel(docId, null);
        $("#mainContent").html("<div class='alert alert-info text-center' role='alert'>There are no documents for this Agenda Item</div>");
    }
}

function getRightContentPanel(docId, meetingId) {
    $.ajax({
        url: '/Home/GetRightPanelContent',
        async: true,
        type: "GET",
        data: {
            docId: docId,
            meetingId: meetingId
        },
        dataType: "html",
        success: function (data) {
            $('#rightPanelContent').html(data)
        }
    });
}