//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BoardsXpress
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblAgendaItem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblAgendaItem()
        {
            this.tblAgendaItemsRelateds = new HashSet<tblAgendaItemsRelated>();
            this.tblDocReviews = new HashSet<tblDocReview>();
            this.tblDocumentLibraries = new HashSet<tblDocumentLibrary>();
            this.tblHtmlLibraries = new HashSet<tblHtmlLibrary>();
            this.tblPublishLibraries = new HashSet<tblPublishLibrary>();
        }
    
        public int Id { get; set; }
        public string agendaItem { get; set; }
        public System.Guid meetingTypeId { get; set; }
        public Nullable<bool> recurringItem { get; set; }
        public string attachedItem { get; set; }
        public string attachedItem1 { get; set; }
        public string attachedItem2 { get; set; }
        public string attachedItem3 { get; set; }
        public int itemOrder { get; set; }
        public bool draftMinutesItem { get; set; }
    
        public virtual tblAgendaItem tblAgendaItems1 { get; set; }
        public virtual tblAgendaItem tblAgendaItem1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblAgendaItemsRelated> tblAgendaItemsRelateds { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblDocReview> tblDocReviews { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblDocumentLibrary> tblDocumentLibraries { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblHtmlLibrary> tblHtmlLibraries { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblPublishLibrary> tblPublishLibraries { get; set; }
    }
}
