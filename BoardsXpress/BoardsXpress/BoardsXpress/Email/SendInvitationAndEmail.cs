﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using HtmlAgilityPack;
using Microsoft.Exchange.WebServices.Data;
using Task = System.Threading.Tasks.Task;

namespace BoardsXpress.Email
{
    public static class SendInvitationAndEmail
    {
        //public static readonly string UserName = "wordflowliquidity@hotmail.com";
        public static readonly string Password = "admin@Wordflow100";
        public static readonly string UserName = "anandwordflow@outlook.com";




        public static async Task SendInvitation(string meetingTypeName, string startTime, string finishTime,
            DateTime? meetingDate, string eventVenue, string receiver, Guid meetingId, string userId, string callBackUrl)
        {
            string fileContent = string.Empty;
            string filePath = string.Empty;
            string TimeFormat = "yyyyMMdd\\THHmmss\\Z";
            Configuration configFile = WebConfigurationManager.OpenWebConfiguration("~/web.config");
            MailSettingsSectionGroup mailSetting = configFile.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
            var fromAddress = new MailAddress(mailSetting.Smtp.From, mailSetting.Smtp.Network.UserName);
            string fromPassword = mailSetting.Smtp.Network.Password;
            var smtp = new SmtpClient
            {
                Host = mailSetting.Smtp.Network.Host,
                Port = mailSetting.Smtp.Network.Port,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,

                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };

            filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "Email/calendarInvitation.ics");
            fileContent = System.IO.File.OpenText(filePath).ReadToEnd();
            fileContent = fileContent.Replace("#TO#", receiver);
            fileContent = fileContent.Replace("#FROM#", fromAddress.Address);
            fileContent = fileContent.Replace("#LOCATION#", eventVenue);
            fileContent = fileContent.Replace("#UID#", Guid.NewGuid().ToString().Replace("-", ""));
            fileContent = fileContent.Replace("#CREATED-AT#", Convert.ToDateTime(meetingDate).ToString(TimeFormat));
            fileContent = fileContent.Replace("#DTSTART#", Convert.ToDateTime(startTime).ToString(TimeFormat));
            fileContent = fileContent.Replace("#DTEND#", Convert.ToDateTime(finishTime).ToString(TimeFormat));
            filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "Email/InvitationEmail.html");
            string fileInviationContent = System.IO.File.OpenText(filePath).ReadToEnd();
            String body = AppendRedirectURl(fileInviationContent, callBackUrl, meetingTypeName);
            fileContent = fileContent.Replace("#XALTDESC#", body);
            fileContent = fileContent.Replace("#DESCRIPTION#", body);
            MailMessage message = new MailMessage
            {
                IsBodyHtml = true,
                From = new MailAddress(fromAddress.Address)
            };
            message.To.Add(new MailAddress(receiver));
            message.Body = body;
            message.Subject =
                $"Invitation for   {meetingTypeName} on {Convert.ToDateTime(meetingDate).ToString("dddd")} {Convert.ToDateTime(startTime).ToString("MMMM")} {Convert.ToDateTime(meetingDate).ToString("dd")} , {Convert.ToDateTime(startTime).ToString("yyyy")} {startTime} - {finishTime}";
            var iCalendarContentType = new ContentType("text/calendar; method=REQUEST");
            var calendarView = AlternateView.CreateAlternateViewFromString(fileContent, iCalendarContentType);
            calendarView.TransferEncoding = TransferEncoding.SevenBit;
            message.AlternateViews.Add(calendarView);
            await smtp.SendMailAsync(message);
        }

        private static string AppendRedirectURl(string mainContent, string callBack, string meetingTypeName)
        {
            string newContent = string.Empty;
            var document = new HtmlDocument();
            document.LoadHtml(mainContent);
            var anchorYesNode = document.GetElementbyId("YesConfirmMeeting");
            var anchorMeetingType = document.GetElementbyId("meetingType");
            var anchorNoNode = document.GetElementbyId("NoConfirmMeeting");
            if (anchorYesNode != null && anchorNoNode != null)
            {
                anchorYesNode.Attributes["href"].Value = string.Format("{0}{1}{2}", callBack, "&resposne=", bool.TrueString);
                anchorNoNode.Attributes["href"].Value = string.Format("{0}{1}{2}", callBack, "&resposne=", bool.FalseString);
                anchorYesNode.Attributes.Add("class", "btn btn-primary");
                anchorNoNode.Attributes.Add("class", "btn btn-danger");
                anchorMeetingType.InnerHtml = meetingTypeName;
                newContent = document.DocumentNode.OuterHtml.ToString();
            }
            return newContent;
        }

        public static async Task<string> SendInvitationByEWS(string meetingTypeName, string startTime, string finishTime,
            DateTime? meetingDate, string eventVenue, string receiver, Guid meetingId, string userId,
            string callBackUrl)
        {


            ServicePointManager.ServerCertificateValidationCallback = CertificateValidationCallBack;
            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2013);
            service.Credentials = new WebCredentials(UserName, Password);

            service.TraceEnabled = true;
            service.TraceFlags = TraceFlags.All;

            service.AutodiscoverUrl(UserName, RedirectionUrlValidationCallback);

            Appointment appointment = new Appointment(service);

            // Set properties on the appointment. Add two required attendees and one optional attendee.
            appointment.Subject = $"Invitation for   {meetingTypeName} on {Convert.ToDateTime(meetingDate).ToString("dddd")} {Convert.ToDateTime(startTime).ToString("MMMM")} {Convert.ToDateTime(meetingDate).ToString("dd")} , {Convert.ToDateTime(startTime).ToString("yyyy")} {startTime} - {finishTime}";
            appointment.Start = Convert.ToDateTime(startTime);
            appointment.End = Convert.ToDateTime(finishTime);
            appointment.Location = eventVenue;
            appointment.RequiredAttendees.Add(receiver);
            appointment.Body = null;
            //appointment.Body = $"You are invited to the  ({meetingTypeName}) meeting - details of which are above.<br/> Please send your RSVP.";

            // Send the meeting request to all attendees and save a copy in the Sent Items folder.
            appointment.Save(SendInvitationsMode.SendToAllAndSaveCopy);
            return appointment.Id.ToString();


        }

        public static bool RedirectionUrlValidationCallback(string redirectionUrl)
        {
            // The default for the validation callback is to reject the URL.
            bool result = false;

            Uri redirectionUri = new Uri(redirectionUrl);

            // Validate the contents of the redirection URL. In this simple validation
            // callback, the redirection URL is considered valid if it is using HTTPS
            // to encrypt the authentication credentials. 
            if (redirectionUri.Scheme == "https")
            {
                result = true;
            }
            return result;
        }

        public static bool CertificateValidationCallBack(
            object sender,
            System.Security.Cryptography.X509Certificates.X509Certificate certificate,
            System.Security.Cryptography.X509Certificates.X509Chain chain,
            System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            // If the certificate is a valid, signed certificate, return true.
            if (sslPolicyErrors == System.Net.Security.SslPolicyErrors.None)
            {
                return true;
            }

            // If there are errors in the certificate chain, look at each error to determine the cause.
            if ((sslPolicyErrors & System.Net.Security.SslPolicyErrors.RemoteCertificateChainErrors) != 0)
            {
                if (chain != null && chain.ChainStatus != null)
                {
                    foreach (System.Security.Cryptography.X509Certificates.X509ChainStatus status in chain.ChainStatus)
                    {
                        if ((certificate.Subject == certificate.Issuer) &&
                            (status.Status == System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.UntrustedRoot))
                        {
                            // Self-signed certificates with an untrusted root are valid. 
                            continue;
                        }
                        else
                        {
                            if (status.Status != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
                            {
                                // If there are any other errors in the certificate chain, the certificate is invalid,
                                // so the method returns false.
                                return false;
                            }
                        }
                    }
                }

                // When processing reaches this line, the only errors in the certificate chain are 
                // untrusted root errors for self-signed certificates. These certificates are valid
                // for default Exchange server installations, so return true.
                return true;
            }
            else
            {
                // In all other cases, return false.
                return false;
            }
        }

    }
}