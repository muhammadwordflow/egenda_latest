﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace BoardsXpress.Util
{
    public class JsonResponse
    {
        private bool success = false;

        //public HttpResponseBase HttpResponseBase { get; set; }
        public bool Success
        {
            get
            {
                return success;
            }
            set
            {
                if (value)
                {
                    success = value;

                    // Set the request based on success
                    //HttpResponseBase.StatusCode = (int)HttpStatusCode.OK;
                }
                //else
                //    HttpResponseBase.StatusCode = (int)HttpStatusCode.BadRequest;
            }
        }

        public string Title { get; set; }
        public string Message { get; set; }

        public object Data { get; set; }

        //public bool ShouldSerializeHttpResponseBase()
        //{
        //    return false;
        //}
    }
}