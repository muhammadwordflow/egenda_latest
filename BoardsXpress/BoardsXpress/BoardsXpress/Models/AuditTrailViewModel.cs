﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BoardsXpress.Controllers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BoardsXpress.Models
{
    public class AuditTrailModel : GenericErrors
    {
        public List<AuditTrailViewModel> AuditTrailList { get; set; }
    }

    public class AuditTrailViewModel 
    {
        protected BoardExpressEntities db;

        public AuditTrailViewModel()
        {
            db = new BoardExpressEntities();
        }
        public System.Guid AuditId => Guid.NewGuid();
        public Guid? MeetingTypeId { get; set; }
        public Guid MeetingsId { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public System.Guid UserId { get; set; }
        public string UrlAccess { get; set; }
        public string IpAddress { get; set; }
        public string SessionId { get; set; }
        public DateTime? TimeAccessed { get; set; }

        public string ChangedData => AfterData;
        //public string ChangedData
        //{
        //    get
        //    {
        //        //String newStringValue = String.Empty;
        //        //var diff = BeforeData.Except(AfterData);
        //        //var newStringValue = string.Join("", diff);

        //        //JObject sourceJObject = JsonConvert.DeserializeObject<JObject>(BeforeData);
        //        //JObject targetJObject = JsonConvert.DeserializeObject<JObject>(AfterData);

        //        //if (!JToken.DeepEquals(sourceJObject, targetJObject) && sourceJObject!= null)
        //        //{
        //        //    foreach (KeyValuePair<string, JToken> sourceProperty in sourceJObject)
        //        //    {
        //        //        JProperty targetProp = targetJObject.Property(sourceProperty.Key);

        //        //        if (!JToken.DeepEquals(sourceProperty.Value, targetProp.Value))
        //        //        {
        //        //            newStringValue += String.Join(" , ",$"{sourceProperty.Key} : {targetProp.Value}");
        //        //           // Console.WriteLine(string.Format("{0} property value is changed", sourceProperty.Key));
        //        //        }
        //        //        else
        //        //        {
        //        //           // Console.WriteLine(string.Format("{0} property value didn't change", sourceProperty.Key));
        //        //        }
        //        //    }
        //        //}
        //        //else
        //        //{
        //        //    newStringValue = String.Empty;
        //        //}

        //        return AfterData;
        //    }
        //}
        public string FTimeAccessed
        {
            get
            {
                if (TimeAccessed != null)
                {
                    return TimeAccessed.Value.ToString("dd/MM/yyyy hh:mm:ss");
                }
                return null;
            }
        }

        public string BeforeData { get; set; }
        public string AfterData { get; set; }
        public string ActionPerform { get; set; }

        

        public void AuditTrail()
        {
            db.AuditTrails.Add(new AuditTrail
            {
                AuditId = Guid.NewGuid(),
                AfterData = this.AfterData,
                BeforeData = this.BeforeData,
                IpAddress = this.IpAddress,
                SessionId = this.SessionId,
                UrlAccess = this.UrlAccess,
                UserId = this.UserId,
                UserName = this.UserName,
                TimeAccessed = this.TimeAccessed,
                ChangedData = this.ChangedData,
                ActionPerform = this.ActionPerform,
                MeetingTypeId = this.MeetingTypeId,
                MeetingId = this.MeetingsId
            });
            db.SaveChanges();
        }
    }
}