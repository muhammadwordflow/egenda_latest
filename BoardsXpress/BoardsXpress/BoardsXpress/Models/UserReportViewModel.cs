﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardsXpress.Models
{
    public class UserReportViewModel : GenericErrors
    {
        public string FstartDate { get; set; }
        public string FendDate { get; set; }

        public Nullable<DateTime> StartDate
        {
            get
            {
                return FstartDate!=null ? DateTime.ParseExact(FstartDate, "dd/MM/yyyy", null) : (DateTime?)null;
            }
        }
        public Nullable<DateTime> EndDate
        {
            get
            {
                return FendDate != null? DateTime.ParseExact(FendDate, "dd/MM/yyyy", null): (DateTime?)null;
            }
        }

        public Guid? selectedMeetingTyeId { get; set; }
        public List<UserManagementViewModels> ListUsers { get; set; }
        public List<MeetingTypeViewModel> MeetingTypes { get; set; }
        public List<UserViewModels> UserList { get; set; }
    }
}