﻿using System;
using System.Collections.Generic;

namespace BoardsXpress.Models
{
    public class TrainingViewModel : GenericErrors
    {
        public Guid TrainingId { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public bool TrainingStatus { get; set; }
        public DateTime? InductionTranningdateTime { get; set; }

        public string FInductionTranningdateTime => InductionTranningdateTime?.ToString("dd MMMM, yyyy");

        public List<CommentsModel> ListInductionTraining { get; set; }

        public List<TrainingViewModel> InductionTrainingList { get; set; }
    }
}