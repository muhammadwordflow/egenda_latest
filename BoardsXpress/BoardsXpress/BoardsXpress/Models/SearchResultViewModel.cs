﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardsXpress.Models
{
    public class SearchResultViewModel
    {
        public string HtmlContent { get; set; }
        public Guid meetingId { get; set; }
        public Guid? meetingTypeId { get; set; }
        public string agendaItem { get; set; }
        public Guid htmlId { get; set; }
        public DateTime? meetingDate { get; set; }
        public string meetingType { get; set; }
    }
}