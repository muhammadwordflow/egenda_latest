﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BoardsXpress.Models
{
    public class PublishViewModel
    {
        private List<PublishItemViewModel> publishItemViewModelList;

        public List<PublishItemViewModel> PublishItemViewModelList
        {
            get
            {
                return publishItemViewModelList ?? (publishItemViewModelList = new List<PublishItemViewModel>());
            }
            set
            {
                publishItemViewModelList = value;
            }
        }

        private List<AgendaItemsViewModels> agendaItemsViewModelList;

        public List<AgendaItemsViewModels> AgendaItemsViewModelList
        {
            get
            {
                return agendaItemsViewModelList ?? (agendaItemsViewModelList = new List<AgendaItemsViewModels>());
            }
            set
            {
                agendaItemsViewModelList = value;
            }
        }

        private BoardExpressEntities db;
        public Guid meetingTypeId { get; set; }
        public Guid meetingId { get; set; }
        public string meetingType { get; set; }
        public string meetingTypeName { get; set; }
        public string status { get; set; }
        public string meetingDate { get; set; }
        public List<PublishItemViewModel> publishItemList { get; set; }

        public PublishViewModel(string meetingIdGlobal)
        {
            this.db = new BoardExpressEntities();
            Guid meetingId = Guid.Parse(meetingIdGlobal);

            this.meetingTypeId = (Guid)db.tblMeetings.Where(x => x.meetingId == meetingId).Select(x => x.meetingType).FirstOrDefault();
            this.meetingDate = db.tblMeetings.Where(x => x.meetingId == meetingId).Select(x => x.meetingDate).FirstOrDefault().ToString();
            this.meetingType = db.tblMeetingTypes.Where(s => s.meetingTypeId == meetingTypeId).Select(y => y.meetingType).FirstOrDefault();
            
            var details = Enumerable.ToList((from x in db.tblDocumentLibraries
                                             where x.meetingId == meetingId
                                             join y in db.tblAgendaItems on x.agendaItem equals y.Id
                                             orderby x.orderIn
                                             select new PublishItemViewModel
                                             {
                                                 docId = x.docId,
                                                 meetingId = x.meetingId,
                                                 docAuthor = x.userId,
                                                 publisher = db.AspNetUsers.Where(s => s.Id == x.userId).Select(p => p.UserName).FirstOrDefault(),
                                                 agendaItem = y.agendaItem,
                                                 docName = x.docName,
                                                 docPath = x.docPath,
                                                 docCreatedDate = (DateTime)x.docCreatedDate,
                                             }));

            if (details != null)
            {
                var tblDocReviewDetails = db.tblDocReviews.OrderByDescending(x => x.approvalDate).ToList();
                foreach (var item in details)
                {
                    item.review = tblDocReviewDetails.Where(s => s.docId == item.docId).Select(y => y.review).FirstOrDefault().GetValueOrDefault(false);
                    item.approve = tblDocReviewDetails.Where(s => s.docId == item.docId).Select(y => y.approve).FirstOrDefault().GetValueOrDefault(false);

                }

                this.publishItemList = details;
            }


            var publishStatus = db.tblPublishLibraries.Where(x => x.meetingId == meetingId).Count();
            this.status = (publishStatus == details.Count && details.Count() != 0) ? "Published" : "Draft";
        }

        public PublishViewModel() { }
    }

    public class PublishItemViewModel
    {
        public Guid docId { get; set; }
        public string docAuthor { get; set; }
        public string docName { get; set; }
        public DateTime docCreatedDate { get; set; }
        public string docPath { get; set; }
        public Guid? meetingTypeId { get; set; }
        public Guid? meetingId { get; set; }
        public int? agendaItemId { get; set; }
        public int? agendaItemRelatedId { get; set; }
        public string agendaItem { get; set; }
        public bool draftMinutesItem { get; set; }
        public string publisher { get; set; }
        public bool review { get; set; }
        public bool approve { get; set; }
        public bool printRestriction { get; set; }

        private List<PublishItemViewModel> listRelatedDocuments;

        public List<PublishItemViewModel> ListRelatedDocuments
        {
            get
            {
                return listRelatedDocuments ?? (listRelatedDocuments = new List<PublishItemViewModel>());
            }
            set
            {
                listRelatedDocuments = value;
            }
        }
        
    }
}