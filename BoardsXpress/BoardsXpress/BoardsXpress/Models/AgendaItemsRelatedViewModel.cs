﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardsXpress.Models
{
    public class AgendaItemsRelatedViewModel
    {
        public int Id { get; set; }
        public int AgendaItemId { get; set; }
        public string Name { get; set; }
        public int itemOrder { get; set; }
        public bool recurringItem { get; set; }

        private PublishItemViewModel publishItemViewModel;

        public PublishItemViewModel PublishItemViewModel
        {
            get
            {
                return publishItemViewModel;
            }
            set
            {
                publishItemViewModel = value;
            }
        }
    }
}