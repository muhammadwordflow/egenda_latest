﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardsXpress.Models
{
    public class RoleViewModels
    {
        public string Id { get; set; }
        
        public string Name { get; set; }
        public string Description { get; set; }
        public bool? readAccess { get; set; }
        public bool? writeAccess { get; set; }
        public bool? fullAccess { get; set; }
    }
}