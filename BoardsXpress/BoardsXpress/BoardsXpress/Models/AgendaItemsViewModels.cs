﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardsXpress.Models
{
    public class AgendaViewModel
    {
        public Guid? meetingId;
        public Guid? meetingTypeId;

        private List<AgendaItemsViewModels> agendaItemsViewModelList;
        public List<AgendaItemsViewModels> AgendaItemsViewModelList {
            get
            {
                return agendaItemsViewModelList == null ? new List<AgendaItemsViewModels>() : agendaItemsViewModelList;
            }
            set
            {
                agendaItemsViewModelList = value;
            }
        }
    }
    public class AgendaItemsViewModels
    {
        private List<AgendaItemsRelatedViewModel> agendaItemsRelatedList;
        private PublishItemViewModel publishItemViewModel;

        public int agendaId { get; set; }
        public Guid meetingTypeId { get; set; }
        public Guid meetingId { get; set; }
        public string meetingType { get; set; }
        public string agendaName { get; set; }
        public int parentAgendaId { get; set; }
        public bool recurringItem { get; set; }
        public bool draftMinutesItem { get; set; }
        public string attachedItem { get; set; }
        public string attachedItem1 { get; set; }
        public string attachedItem2 { get; set; }
        public string attachedItem3 { get; set; }
        public int itemOrder { get; set; }

        public List<AgendaItemsRelatedViewModel> AgendaItemsRelatedList {
            get {
                return agendaItemsRelatedList == null ? new List<AgendaItemsRelatedViewModel>() : agendaItemsRelatedList;
            }
            set
            {
                agendaItemsRelatedList = value;
            }
        }

        public PublishItemViewModel PublishItemViewModel
        {
            get
            {
                return publishItemViewModel;
            }
            set
            {
                publishItemViewModel = value;
            }
        }
    }

    public class AgendaItemDDLViewModel
    {
        public int? AgendaItemId { get; set; }
        public string AgendaName { get; set; }
    }
}