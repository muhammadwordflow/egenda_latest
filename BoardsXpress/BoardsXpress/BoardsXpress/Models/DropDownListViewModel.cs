﻿using System;

namespace BoardsXpress.Models
{
    public class DropDownListViewModel
    {
        public object Value { get; set; }
        public string Text { get; set; }

        public DropDownListViewModel(int value, string text)
        {
            Value = value;
            Text = text;
        }

        public DropDownListViewModel(Guid value, string text)
        {
            Value = value;
            Text = text;
        }

        public DropDownListViewModel() { }
    }
}