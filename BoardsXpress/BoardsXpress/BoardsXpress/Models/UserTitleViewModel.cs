﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardsXpress.Models
{
    public class UserTitleViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}