﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardsXpress.Models
{
    public class navigationViewModel
    {
        public string meeting { get; set; }
        public Guid meetingId { get; set; }
        public Guid meetingDateId { get; set; }
        public DateTime? meetingDate { get; set; }
        public Dictionary<string, List<tblMeeting>> navDataFields { get; set; }
    }

    public class BootstrapTreeViewViewModel
    {
        public string text { get; set; }
        public List<Node> nodes { get; set; }
    }

    public class Node
    {
        public string text { get; set; }
        public string href { get; set; }
        public Guid meetingId { get; set; }
        public Guid meetingType { get; set; }
    }


}