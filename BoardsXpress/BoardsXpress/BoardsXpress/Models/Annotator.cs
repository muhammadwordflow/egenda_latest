﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardsXpress.Models
{
    public class Annotator
    {
        public Guid id { get; set; }
        //public string annotator_schema_version { get; set; }
        public DateTime? created { get; set; }
        //public DateTime? updated { get; set; }
        public string text { get; set; }
        public string quote { get; set; }
        public string uri { get; set; }
        public List<AnnotationRange> ranges { get; set; }
        public string user { get; set; }
        public Guid docId { get; set; }
    }

    public class AnnotationRange
    {
        //public Guid annotationRangeId { get; set; }
        //public Guid annotationId { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public int startOffset { get; set; }
        public int endOffset { get; set; }
    }

    public class AnnotationSearch
    {
        public List<Annotator> rows { get; set; }
        public int total { get; set; }
    }
}