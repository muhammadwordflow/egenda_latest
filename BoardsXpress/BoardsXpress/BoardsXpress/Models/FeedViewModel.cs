﻿using System;
using System.Collections.Generic;

namespace BoardsXpress.Models
{
    public class FeedManagerViewModel
    {
        public int Id { get; set; }
        public Guid MeetingTypeId { get; set; }
        public string FeedProvider { get; set; }
        public string FeedURL { get; set; }
    }

    public class ManageFeedViewModel
    {
        public ManageFeedViewModel()
        {
            ListFeedManagerViewModel = new List<FeedManagerViewModel>();
        }

        public Guid MeetingTypeId { get; set; }
        public string MeetingName { get; set; }

        public List<FeedManagerViewModel> ListFeedManagerViewModel { get; set; }
    }

    public class FeedsViewModel
    {
        public FeedsViewModel()
        {
            Feeds = new Dictionary<string, string>();
        }

        public Dictionary<string, string> Feeds { get; set; }
    }
}