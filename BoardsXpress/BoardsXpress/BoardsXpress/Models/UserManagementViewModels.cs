﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BoardsXpress.Models
{
    public class UserManagementViewModels : GenericErrors
    {

        #region aspUser

        public string UserId { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
        [DisplayName("FirstName")]
        public string FirstName { get; set; }
        public string Organisation { get; set; }
        public string Designation { get; set; }
        public string Avatar { get; set; }
        public string Password { get; set; }
        public bool SelectedUser { get; set; }
        public string SelectTitle { get; set; }
        #endregion

        public Guid MeetingTypeId { get; set; }
        public string MeetingType { get; set; }
        public Guid MeetingId { get; set; }
        public HttpPostedFileBase ImageUpload { get; set; }

        #region Roles
        // public List<tblUserGroup> UserGroupList { get; set; }
        public List<SelectListItem> UserGroupList { get; set; }
        public List<RoleViewModels> Roles { get; set; }
        public List<MeetingViewModels> Meetings { get; set; }
        public RoleViewModels userInRole { get; set; }
        public string SelectedRoleId { get; set; }
        public Guid AspNetUserRoleId { get; set; }

        public DateTime StartDate { get; set; } = DateTime.Now;
        public DateTime? EndDate { get; set; }

        public string FormatStartDate { get; set; } = DateTime.Now.ToString("dd/MM/yyyy");
        public string FormatEndDate { get; set; }

        public String ExtFormatStartDate
        {
            get
            {
                return EndDate?.ToString("dd/MM/yyyy");
            }
        }
        public String ExtFormatEndDate
        {
            get
            {
                return EndDate != null ? EndDate.Value.ToString("dd/MM/yyyy") : null;
            }
        }

        public bool? UserResponse { get; set; }

        // public string groupName { get; set; }
        #endregion



    }
}