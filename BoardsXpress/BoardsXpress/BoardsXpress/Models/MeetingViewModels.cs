﻿using System;
using System.Collections.Generic;

namespace BoardsXpress.Models
{
    public class MeetingViewModels : GenericErrors
    {
        // public IEnumerable<tblMeetingType> meetingTypeList { get; set; }
        public List<MeetingViewModels> ListMeetings { get; set; } // This need to be Remove if we not user report management service in Report Controller
        public Guid meetingId { get; set; }
        public Guid meetingTypeId { get; set; }
        public DateTime createdDate { get; set; }
        public string Venue { get; set; }
        public string FCreateDate => meetingDate.ToString("dd/MM/yyyy");
        public bool? UserResponse { get; set; }
        public DateTime meetingDate { get; set; }
        public string startTime { get; set; }
        public string finishTime { get; set; }
        public string meetingVenue { get; set; }
        public string meetingNotes { get; set; }
        public string meetingType { get; set; }
        public string meetingAbbreviation { get; set; }
        public bool useAbbreviation { get; set; }
        public bool isCurrent { get; set; }
        public bool rotaryResolution { get; set; }
        public ApplicationUser userProfile { get; set; }
    }

    public class MeetingDDLViewModel
    {
        public Guid? MeetingId { get; set; }
        public string MeetingDate { get; set; }
        public bool RotaryResolution { get; set; }
    }
}