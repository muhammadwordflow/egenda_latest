﻿using System;
using System.Collections.Generic;

namespace BoardsXpress.Models
{
    public class CommentsModel
    {
        public Guid id { get; set; }
        public Guid userId { get; set; }
        public Guid meetingId { get; set; }
        public string comment { get; set; }
        public DateTime? dateTime { get; set; }
        public List<CommentsModel> ListComments { get; set; }
    }
}