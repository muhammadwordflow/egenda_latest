﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardsXpress.Models
{
    public class MyDocumentsViewModel
    {
        public Guid docId { get; set; }
        public string docName { get; set; }
        public DateTime? meetingDate { get; set; }
        public DateTime? dateSubmitted { get; set; }
        public string meetingType { get; set; }
        public string docPath { get; set; }
        public string agendaItem { get; set; }
    }
}