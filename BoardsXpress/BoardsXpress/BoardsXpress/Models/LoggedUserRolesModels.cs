﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardsXpress.Models
{
    public class LoggedUserRolesModels
    {

        public string userId { get; set; }
        public string RoleName { get; set; }
        public string RoleId { get; set; }
        public Guid   MeetingTypeId { get; set; }
    }
}