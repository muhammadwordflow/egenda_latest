﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardsXpress.Models
{
    public class TreeViewViewModel
    {
        public int? agendaItemId { get; set; }
        public int? agendaItemRelatedId { get; set; }
        public string text { get; set; }
        public string icon { get; set; }
        public string selectedIcon { get; set; }
        public string color { get { return "#414042"; } }
        public string backColor { get; set; }
        public string href { get; set; }
        public bool selectable { get; set; }
        public Guid? MyProperty { get; set; }
        public string emptyIcon { get { return "glyphicon glyphicon-minus"; } }
        public string collapseIcon { get { return "glyphicon glyphicon-minus"; } }
        public string expandIcon { get { return "glyphicon glyphicon-plus"; } }

        private List<TreeViewViewModel> nodes;
        public List<TreeViewViewModel> Nodes
        {
            get
            {
                return nodes ?? (nodes = new List<TreeViewViewModel>());
            }
            set
            {
                Nodes = value;
            }
        }
    }
}