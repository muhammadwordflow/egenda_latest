﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardsXpress.Models
{
    public class MeetingAttendeesView : GenericErrors
    {

        public List<MeetingAttendeesViewModel> ListMeetings { get; set; }

    }

    public class MeetingAttendeesViewModel
    {
        public string FirstName { get; set; }
        public string UserId { get; set; }
        public bool? UserResponse { get; set; }
    }
}