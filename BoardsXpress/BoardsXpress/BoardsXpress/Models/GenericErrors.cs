﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardsXpress.Models
{
    public class GenericErrors : PaginationViewModel
    {
        public int ErrorCode { get; set; }
        public string Error { get; set; }
        public string ErrorMessage { get; set; }
    }
}