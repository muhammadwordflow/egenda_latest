﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardsXpress.Models
{
    public class CalendarView : GenericErrors
    {
        public List<MeetingTypeViewModel> MeetingTypeList { get; set; }
    }

    public class CalendarViewModel 
    {
        public Guid meetingId { get; set; }
        public Guid meetingTypeId { get; set; }
        public string meetingType { get; set; }
        public DateTime? meetingDate { get; set; }
        public string FmeetingDate => meetingDate.Value.ToString("dd/MM/yyyy");
        public string meetingVenue { get; set; }
        public string startTime { get; set; }
    }
}