﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardsXpress.Models
{
    public class RefDocViewModel
    {
       public Guid refDocId { get; set;  }
       public string meetingType { get; set; }
       public string refDocName { get; set; }

    }
}