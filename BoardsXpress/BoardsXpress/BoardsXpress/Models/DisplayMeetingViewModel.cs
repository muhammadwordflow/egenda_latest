﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardsXpress.Models
{
    public class DisplayMeetingViewModel
    {
        private MeetingViewModels meetingViewModel;
        public MeetingViewModels MeetingViewModel
        {
            get
            {
                return meetingViewModel ?? (meetingViewModel = new MeetingViewModels());
            }
            set
            {
                meetingViewModel = value;
            }
        }

        private CommentsModel commentsModel;
        public CommentsModel CommentsModel
        {
            get
            {
                return commentsModel ?? (commentsModel = new CommentsModel());
            }
            set
            {
                commentsModel = value;
            }
        }

        private MeetingTypeViewModel meetingTypeViewModel;
        public MeetingTypeViewModel MeetingTypeViewModel
        {
            get
            {
                return meetingTypeViewModel ?? (meetingTypeViewModel = new MeetingTypeViewModel());
            }
            set
            {
                meetingTypeViewModel = value;
            }
        }

        public string Environment { get; set; }

        private List<PublishItemViewModel> publishItemViewModelList;
        public List<PublishItemViewModel> PublishItemViewModelList
        {
            get
            {
                return publishItemViewModelList ?? (publishItemViewModelList = new List<PublishItemViewModel>());
            }
            set
            {
                publishItemViewModelList = value;
            }
        }

        public string HtmlContent { get; set; }
        public bool Print { get; set; }

        public Guid docId { get; set; }
        public string   agendaItem { get; set; }
        public Dictionary<string, List<string>> AgendaParentChilds { get; set; }
        
    }
}