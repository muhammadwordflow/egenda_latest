﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardsXpress.Models
{
    public class UploadFileModel
    {
        public string mtypes { get; set; }
        public string mdates { get; set; }
        public int magendaitem { get; set; }
        public HttpPostedFile file { get; set; }
    }
}