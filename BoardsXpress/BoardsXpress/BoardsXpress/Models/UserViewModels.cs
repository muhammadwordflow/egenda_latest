﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardsXpress.Models
{
    public class UserViewModels
    {

        public Guid userId { get; set; }
        public string  firstName { get; set; }
        public string  lastName { get; set; }
        public string  middleName { get; set; }
        public string fullName { get; set; }
        public string  phoneNumber { get; set; }
        public string  userName { get; set; }
        public string email { get; set; }

        public string avatar { get; set; }

        public string  gender { get; set; }
        public string password { get; set; }
        public  tblUserGroup userRole { get; set; }
    }
}