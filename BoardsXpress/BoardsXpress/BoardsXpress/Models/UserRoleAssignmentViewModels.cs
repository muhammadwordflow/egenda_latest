﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BoardsXpress.Models
{
    public class UserRoleAssignmentViewModels : GenericErrors
    {


        public List<UserInfoViewModels> userList { get; set; }
        public List<UserManagementViewModels> userListViewModel { get; set; }
        public Guid MeetingId { get; set; }
        public Guid MeetingTypeId { get; set; }
        public string MeetingType { get; set; }
        public List<UserTitleViewModel> TypeOfMember { get; set; }
        public List<UserTitleViewModel> UserTitle { get; set; }


    }


    public class UserInfoViewModels
    {

        public string UserId { get; set; }
        public string FirstName { get; set; }
        public SelectList userRole { get; set; }
        public string userRoleSelected { get; set; }

    }
}