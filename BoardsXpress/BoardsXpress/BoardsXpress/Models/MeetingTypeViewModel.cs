﻿using System;
using System.Collections.Generic;

namespace BoardsXpress.Models
{
    public class MeetingTypeViewModel
    {
        public Guid meetingTypeId { get; set; }
        public string meetingType { get; set; }
        public string meetingAbbreviation { get; set; }
        public bool rotaryResolution { get; set; }
        public bool useAbbreviation { get; set; }
        public DateTime? createdDate { get; set; }
        public List<MeetingViewModels> ListMeetings { get; set; }
        private List<MeetingDDLViewModel> meetingsList;
        public List<MeetingDDLViewModel> MeetingsList
        {
            get { return meetingsList = meetingsList == null ? new List<MeetingDDLViewModel>() : meetingsList; }
            set { meetingsList = value; }
        }
    }
}