﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BoardsXpress.Models
{
    public class UserProfileReportModel : GenericErrors
    {
        public List<RegisterViewModel> UserProfileList { get; set; }
    }
}