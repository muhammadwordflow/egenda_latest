﻿using System;

namespace BoardsXpress.Models
{
    public class RightPanelContentModel
    {
        public Guid DocId { get; set; }
        public Guid MeetingId { get; set; }
        public bool ShowContent { get; set; }
        public bool ShowLogo { get; set; }

        public bool HasDLM { get; set; }
        public string ProtectiveMarking { get; set; }
        public string ReasonForSensitivity { get; set; }
    }
}