﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoardsXpress.Core.Repositories.DocumentProcessRepo
{
    /// <summary>
    /// Caters converted htmldocument
    /// </summary>
    /// <note>Improves converted Html</note>
    class ConversionManagementRepo
    {
        /// <summary>
        /// Removes unnecesary Empty H Tag
        /// in converted html file
        /// h1, h2 , h3 is removed
        /// can be extended to remove other headings as well
        /// </summary>
        /// <param name="path">html file path</param>
        /// <author>Sunil karki</author>
        public void RemoveEmptyHTag(string path)
        {
            string htmlContent = string.Empty;
            using (var reader = new StreamReader(path))
            {
                htmlContent = reader.ReadToEnd();
            }

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var h1List = doc.DocumentNode.Descendants("h1");
            var h2List = doc.DocumentNode.Descendants("h2");
            var h3List = doc.DocumentNode.Descendants("h3");
            foreach (var h1 in h1List)
            {
                var imageContent = h1.ChildNodes.ToList().Where(w => w.Name == "img").ToList();
                if (imageContent.Count == 0)
                {
                    var innerText = h1.InnerText.Trim().ToString();
                    if (string.IsNullOrWhiteSpace(innerText) || innerText == "&#xa0;")
                    {
                        h1.Remove();
                    }
                }

            }
            foreach (var h2 in h2List)
            {
                var imageContent = h2.ChildNodes.ToList().Where(w => w.Name == "img").ToList();
                if (imageContent.Count == 0)
                {
                    var innerText = h2.InnerText.Trim().ToString();
                    if (string.IsNullOrWhiteSpace(innerText) || innerText == "&#xa0;")
                    {
                        h2.Remove();
                    }
                }

            }
            foreach (var h3 in h3List)
            {
                var imageContent = h3.ChildNodes.ToList().Where(w => w.Name == "img").ToList();
                if (imageContent.Count == 0)
                {
                    var innerText = h3.InnerText.Trim().ToString();
                    if (string.IsNullOrWhiteSpace(innerText) || innerText == "&#xa0;")
                    {
                        h3.Remove();
                    }
                }

            }
            htmlContent = doc.DocumentNode.OuterHtml;
            using (var wr = new StreamWriter(path))
            {
                wr.Write(htmlContent);
            }


        }

        /// <summary>
        /// Removes empty span of converted html
        /// also removes span with "&#xa0;"
        /// </summary>
        /// <param name="path">html file path</param>
        /// <author>sunil karki</author>
        /// <note>call this method before calling removing empty PTag</note>
        public void RemoveEmptySpanTag(string path)
        {
            string htmlContent;
            using (StreamReader sr = new StreamReader(path))
            {
                htmlContent = sr.ReadToEnd();

            }

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var spanList = doc.DocumentNode.Descendants("span").ToList();
            foreach (var item in spanList)
            {

                var imgNode = item.ChildNodes.Where(w => w.Name == "img").ToList();
                if (imgNode.Count() == 0)
                {
                    var innertext = item.InnerText.Trim().ToString();
                    if (string.IsNullOrWhiteSpace(innertext) || innertext == "&#xa0;")
                    {
                        item.Remove();
                    }

                }
            }
            htmlContent = doc.DocumentNode.OuterHtml;
            using (var wr = new StreamWriter(path))
            {
                wr.Write(htmlContent);
            }

        }

        /// <summary>
        /// Removes empty p tag of converted html
        /// also removes span with "&#xa0;"
        /// </summary>
        /// <param name="path">html file path</param>
        /// <author>sunil karki</author>
        /// <note>call this method after calling removing empty spanTag</note>
        public void RemoveEmptyPTag(string path)
        {
            string htmlContent;
            using (StreamReader sr = new StreamReader(path))
            {
                htmlContent = sr.ReadToEnd();

            }

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            var spanList = doc.DocumentNode.Descendants("p").ToList();
            foreach (var item in spanList)
            {

                var imgNode = item.ChildNodes.Where(w => w.Name == "img").ToList();
                if (imgNode.Count() == 0)
                {
                    var innertext = item.InnerText.Trim().ToString();
                    if (string.IsNullOrWhiteSpace(innertext) || innertext == "&#xa0;")
                    {

                        item.Remove();
                    }

                }
            }
            htmlContent = doc.DocumentNode.OuterHtml;
            using (var wr = new StreamWriter(path))
            {
                wr.Write(htmlContent);
            }
        }

        /// <summary>
        /// removes all the style within html file
        /// </summary>
        /// <param name="path">html file path</param>
        /// <author>sunil karki</author>
        public void RemoveHtmlSyle(string path)
        {

            string htmlContent = string.Empty;

            using (var sr = new StreamReader(path))
            {
                htmlContent = sr.ReadToEnd();
            }
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);
            var styleInfo = doc.DocumentNode.SelectNodes("//@style");
            foreach (var data in styleInfo)
            {
                data.Attributes["style"].Remove();
            }

            htmlContent = doc.DocumentNode.OuterHtml;
            using (var wr = new StreamWriter(path))
            {
                wr.Write(htmlContent);
            }
        }

        /// <summary>
        ///  removes paricular attribute of html 
        ///  such as class.
        /// </summary>
        /// <param name="path">html file path</param>
        /// <author>sunil karki</author>
        /// <note>this method removes class attribute in html</note>
        public void RemoveStyleAttribute(string path)
        {
            string htmlContent;
            using (StreamReader sr = new StreamReader(path))
            {
                htmlContent = sr.ReadToEnd();

            }
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(htmlContent);


            var classList = doc.DocumentNode.Descendants().Where(w => w.Attributes.Contains("class"));
            foreach (var item in classList)
            {
                item.Attributes.RemoveAll();
            }
            htmlContent = doc.DocumentNode.OuterHtml;
            using (var wr = new StreamWriter(path))
            {
                wr.Write(htmlContent);
            }


        }


        /// <summary>
        /// 
        /// </summary>
        /// 
        /// <author>bikram khadka</author>
        /// <note>moved from aist project..bikram knows about it</note>
        public void ReplaceImageSourceInHtml(string path)
        {
            #region added by sunil
            ///this code also changes image src attribute
            var htmlContent = string.Empty;
            using (var sr = new StreamReader(path))
            {
                htmlContent = sr.ReadToEnd();
            }
            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(htmlContent);
            var imgCollection = htmlDoc.DocumentNode.Descendants("img");
            foreach (var item in imgCollection)
            {
                var src = item.GetAttributeValue("src", "");
                //var imgId = imageInfo.Where(w => w.ImageName == src).Select(s => s.UploadedImageIdSquiz).FirstOrDefault();
                var imgId = string.Empty;
                item.SetAttributeValue("src", "./?a=" + imgId);

            }

            var updatedContent = htmlDoc.DocumentNode.OuterHtml;
            using (var sw = new StreamWriter(path))
            {
                sw.Write(updatedContent);
            }
            #endregion

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        /// <author>Bikram Khadka</author>
        /// <note>changed sgmlbuilder functioanlity with htmlagility
        /// for consistency by sunil</note>
        public string TailoredContentBody(string path)
        {

            string htmlContent = string.Empty;

            using (var sr = new StreamReader(path))
            {
                htmlContent = sr.ReadToEnd();
            }
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);

            htmlContent=doc.DocumentNode.SelectSingleNode("//body").InnerHtml.Trim();
            return htmlContent;

        }
    }
}
