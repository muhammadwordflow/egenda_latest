﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aspose.Words;
using Aspose.Words.Tables;
using Aspose.Words.Saving;

namespace BoardsXpress.Core.Repositories.DocumentProcessRepo
{
    class ConversionRepo
    {
        /// <summary>
        /// Converts word to html and 
        /// make adjustment to image and tags
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        /// <author>Bikram khadka</author>
        /// <note> adjustment needs to be made</note>
        public string ConvertWordToHtml(string path)
        {

            string htmlFileContent = string.Empty;
            string filename = Path.GetFileName(path);
            string directory = Path.GetDirectoryName(path);
            string imagePath = GetImageDestination();

            if (!Directory.Exists(imagePath))
            {
                try
                {
                    Directory.CreateDirectory(imagePath);
                }
                catch (Exception ex) { }
            }


            Document doc = new Document(path);
            Table table = (Table)doc.GetChild(NodeType.Table, 0, true);
            table.AutoFit(AutoFitBehavior.AutoFitToWindow);


            HtmlSaveOptions saveOptions = new HtmlSaveOptions();
            saveOptions.CssStyleSheetType = CssStyleSheetType.External;
            saveOptions.PrettyFormat = true;
            saveOptions.ExportDocumentProperties = true;
            saveOptions.ImagesFolder = imagePath;
            saveOptions.ExportHeadersFootersMode = ExportHeadersFootersMode.None;
            //s.ExportTextInputFormFieldAsText = false;
            saveOptions.ExportListLabels = ExportListLabels.Auto;
            saveOptions.ImageResolution = 360;

            string htmlFileName = Path.GetFileNameWithoutExtension(filename) + ".html";
            //htmlFileName = dir + "/" + htmlFileName;
            directory += "/" + htmlFileName;
            try
            {
                doc.Save(directory, saveOptions);
               
            }
            catch (Exception ex) { }

            ConversionManagementRepo oConversionMangement = new ConversionManagementRepo();
            oConversionMangement.ReplaceImageSourceInHtml(directory);
            oConversionMangement.RemoveEmptyHTag(directory);
            oConversionMangement.RemoveEmptySpanTag(directory);
            oConversionMangement.RemoveEmptyPTag(directory);


            return directory;
        }

        private string GetImageDestination()
        {
            throw new NotImplementedException();
        }
    }
}
