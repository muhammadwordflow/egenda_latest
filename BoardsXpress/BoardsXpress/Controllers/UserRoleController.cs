﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BoardsXpress.Entities;

namespace BoardsXpress.Controllers
{
    public class UserRoleController : Controller
    {
        private BoardXpressEntities db = new BoardXpressEntities();

        // GET: UserRole
        public ActionResult Index()
        {
            var userGroupList = db.tblUserGroups.ToList();

            userGroupList = userGroupList.Where(s => s.groupName != "SuperAdmin").ToList();
            return View(userGroupList);
        }

        // GET: UserRole/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblUserGroup tblUserGroup = db.tblUserGroups.Find(id);
            if (tblUserGroup == null)
            {
                return HttpNotFound();
            }
            return View(tblUserGroup);
        }

        // GET: UserRole/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserRole/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "groupId,groupName,readAccess,writeAccess,fullAccess")] tblUserGroup tblUserGroup)
        {
            if (ModelState.IsValid)
            {
                tblUserGroup.groupId = Guid.NewGuid();
                db.tblUserGroups.Add(tblUserGroup);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tblUserGroup);
        }

        // GET: UserRole/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblUserGroup tblUserGroup = db.tblUserGroups.Find(id);
            if (tblUserGroup == null)
            {
                return HttpNotFound();
            }
            return View(tblUserGroup);
        }

        // POST: UserRole/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "groupId,groupName,readAccess,writeAccess,fullAccess")] tblUserGroup tblUserGroup)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblUserGroup).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tblUserGroup);
        }

        // GET: UserRole/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblUserGroup tblUserGroup = db.tblUserGroups.Find(id);
            if (tblUserGroup == null)
            {
                return HttpNotFound();
            }
            return View(tblUserGroup);
        }

        // POST: UserRole/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            tblUserGroup tblUserGroup = db.tblUserGroups.Find(id);
            db.tblUserGroups.Remove(tblUserGroup);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
