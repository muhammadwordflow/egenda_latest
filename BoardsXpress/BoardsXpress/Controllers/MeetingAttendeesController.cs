﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BoardsXpress;
using BoardsXpress.Entities;

namespace BoardsXpress.Controllers
{
    public class MeetingAttendeesController : Controller
    {
        private BoardXpressEntities db = new BoardXpressEntities();

        // GET: MeetingAttendees
        public ActionResult Index()
        {
            return View(db.tblMeetingAttendees.ToList());
        }

        // GET: MeetingAttendees/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblMeetingAttendee tblMeetingAttendee = db.tblMeetingAttendees.Find(id);
            if (tblMeetingAttendee == null)
            {
                return HttpNotFound();
            }
            return View(tblMeetingAttendee);
        }

        // GET: MeetingAttendees/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MeetingAttendees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "eventId,meetingId,groupId")] tblMeetingAttendee tblMeetingAttendee)
        {
            if (ModelState.IsValid)
            {
                tblMeetingAttendee.eventId = Guid.NewGuid();
                db.tblMeetingAttendees.Add(tblMeetingAttendee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tblMeetingAttendee);
        }

        // GET: MeetingAttendees/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblMeetingAttendee tblMeetingAttendee = db.tblMeetingAttendees.Find(id);
            if (tblMeetingAttendee == null)
            {
                return HttpNotFound();
            }
            return View(tblMeetingAttendee);
        }

        // POST: MeetingAttendees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "eventId,meetingId,groupId")] tblMeetingAttendee tblMeetingAttendee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblMeetingAttendee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tblMeetingAttendee);
        }

        // GET: MeetingAttendees/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblMeetingAttendee tblMeetingAttendee = db.tblMeetingAttendees.Find(id);
            if (tblMeetingAttendee == null)
            {
                return HttpNotFound();
            }
            return View(tblMeetingAttendee);
        }

        // POST: MeetingAttendees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            tblMeetingAttendee tblMeetingAttendee = db.tblMeetingAttendees.Find(id);
            db.tblMeetingAttendees.Remove(tblMeetingAttendee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
