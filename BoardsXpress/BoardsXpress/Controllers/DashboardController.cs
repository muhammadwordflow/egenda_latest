﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BoardsXpress.Models;
using BoardsXpress.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace BoardsXpress.Controllers
{
    public class DashboardController : Controller
    {
       
        private BoardXpressEntities db = new BoardXpressEntities();
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        /// <summary>
        /// Check user role 
        /// based on user role provide 
        /// capabilities of dashboard
        /// </summary>
        /// <returns></returns>
        /// <note></note>
        public ActionResult Index()
        {
            ///get user information 
            ///get user role aswell

            var currentUser = User.Identity.GetUserId();

            var userProfileInformation = UserManager.Users.ToList().Find(s => s.Id == currentUser);
            var meetingViewModel = new List<MeetingViewModels>();

            ///for test purpose only using id
            ///
            //var id = Guid.Parse("87c56b8a-e3f7-405b-8415-ad38a166bd52").ToString();
            //var userProfileInformation = db.AspNetUsers.Where(w => w.Id == id).FirstOrDefault();

            var meetingTypeList = db.tblMeetingTypes.ToList();
            if (meetingTypeList.Count() == 0)
            {
                var meetingViewModelTemp = new MeetingViewModels();
                meetingViewModelTemp.userProfile = userProfileInformation;
                meetingViewModel.Add(meetingViewModelTemp);
            }
            else
            {
                AutoMapper.Mapper.Map(meetingTypeList, meetingViewModel);
                foreach (var item in meetingViewModel)
                {

                    item.userProfile = userProfileInformation;
                }
            }
            

            return View(meetingViewModel);
        }


        public ActionResult UserProfile(string id)
        {

            var currentUser = User.Identity.GetUserId();

            var userProfileInformation = UserManager.Users.ToList().Find(s => s.Id == currentUser);
            MeetingViewModels meetingViewModel = new MeetingViewModels();
            meetingViewModel.userProfile = userProfileInformation;
            return PartialView("_UserProfile", meetingViewModel);
        }

        public ActionResult MyDocuments(Guid id)
        {
            return PartialView("_MyDocuments");
        }

        public ActionResult MeetingAdministration(string id)
        {
            List<MeetingViewModels> meetingViewModel = new List<MeetingViewModels>();
            ///for test purpose only using id
            ///
            //var id = Guid.Parse("87c56b8a-e3f7-405b-8415-ad38a166bd52");
            var currentUser = User.Identity.GetUserId();

            var userProfileInformation = UserManager.Users.ToList().Find(s => s.Id == currentUser);
            var meetingTypeList = db.tblMeetingTypes.ToList();
            if (meetingTypeList.Count() == 0)
            {
                var meetingViewModelTemp = new MeetingViewModels();
                meetingViewModelTemp.userProfile = userProfileInformation;
                meetingViewModel.Add(meetingViewModelTemp);
            }
            else
            {
                AutoMapper.Mapper.Map(meetingTypeList, meetingViewModel);
                foreach (var item in meetingViewModel)
                {

                    item.userProfile = userProfileInformation;
                }
            }
            return PartialView("_MeetingAdministration", meetingViewModel);
        }
    }
}