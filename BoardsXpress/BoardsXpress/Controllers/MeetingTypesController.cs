﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BoardsXpress;
using BoardsXpress.Entities;
using BoardsXpress.Models;

namespace BoardsXpress.Controllers
{
    public class MeetingTypesController : Controller
    {
        private BoardXpressEntities db = new BoardXpressEntities();

        // GET: MeetingTypes
        public ActionResult Index()
        {
            var meetingTypeList = db.tblMeetingTypes.ToList();
            var meetingViewModel = AutoMapper.Mapper.Map<List<tblMeetingType>, List<MeetingViewModels>>(meetingTypeList);


            return View(meetingViewModel);
        }

        public ActionResult SelectMeeting(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblMeetingType tblMeetingType = db.tblMeetingTypes.Find(id);
            if (tblMeetingType == null)
            {
                return HttpNotFound();
            }
            MeetingsController m = new MeetingsController();
            return RedirectToAction("Index", "Meetings", new { id = tblMeetingType.meetingTypeId });

        }

        // GET: MeetingTypes/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblMeetingType tblMeetingType = db.tblMeetingTypes.Find(id);
            if (tblMeetingType == null)
            {
                return HttpNotFound();
            }
            return View(tblMeetingType);
        }

        // GET: MeetingTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MeetingTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public JsonResult Create(tblMeetingType tblMeetingType)
        
        {

            if (tblMeetingType.meetingType != null)
            {
                tblMeetingType.meetingTypeId = Guid.NewGuid();
                db.tblMeetingTypes.Add(tblMeetingType);
                db.SaveChanges();
                return Json(new { success = true });
            }

            return Json(new { success = false, message = "Meeting Type is Empty" });
        }

        // GET: MeetingTypes/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblMeetingType tblMeetingType = db.tblMeetingTypes.Find(id);
            if (tblMeetingType == null)
            {
                return HttpNotFound();
            }
            return View(tblMeetingType);
        }

        // POST: MeetingTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "meetingTypeId,meetingType")] tblMeetingType tblMeetingType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblMeetingType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tblMeetingType);
        }

        // GET: MeetingTypes/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ///Check for if meeting type is connected to any meeting
            ///if yes do not delete
            tblMeetingType tblMeetingType = db.tblMeetingTypes.Find(id);
            if (tblMeetingType == null)
            {
                return HttpNotFound();
            }

            List<MeetingViewModels> meetingViewModelList = new List<MeetingViewModels>();
            var meetingList = db.tblMeetings.ToList().Where(w => w.meetingType == id).ToList();

            AutoMapper.Mapper.Map<List<tblMeeting>, List<MeetingViewModels>>(meetingList, meetingViewModelList);

            if (meetingViewModelList != null)
            {
                foreach (var item in meetingViewModelList)
                {
                    AutoMapper.Mapper.Map<tblMeetingType, MeetingViewModels>(tblMeetingType, item);
                }
            }
            else
            {
                AutoMapper.Mapper.Map<tblMeetingType, MeetingViewModels>(tblMeetingType, meetingViewModelList.FirstOrDefault());
            }



            return View(meetingViewModelList);
        }

        // POST: MeetingTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            tblMeetingType tblMeetingType = db.tblMeetingTypes.Find(id);
            db.tblMeetingTypes.Remove(tblMeetingType);
            var meetingList = db.tblMeetings.ToList().Where(w => w.meetingType == id);
            if (meetingList.Count() > 0)
            {
                foreach (var item in meetingList)
                {

                    db.tblMeetings.Remove(item);
                }
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
