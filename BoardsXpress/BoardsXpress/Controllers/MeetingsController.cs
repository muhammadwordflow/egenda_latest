﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BoardsXpress;
using BoardsXpress.Entities;
using BoardsXpress.Models;

namespace BoardsXpress.Controllers
{
    public class MeetingsController : Controller
    {
        private BoardXpressEntities db = new BoardXpressEntities();

        // GET: Meetings
        public ActionResult Index(Guid id)
        {
            var meetingType = db.tblMeetingTypes.Find(id);


            List<MeetingViewModels> meetingViewModels = new List<MeetingViewModels>();
            var meetingList = db.tblMeetings.ToList().Where(w => w.meetingType == id);
            AutoMapper.Mapper.Map(meetingList, meetingViewModels);
            foreach (var item in meetingViewModels)
            {
                AutoMapper.Mapper.Map(meetingType, item);

            }
            return View(meetingViewModels);
        }

        // GET: Meetings/Details/5
        public ActionResult Details(Guid? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblMeeting tblMeeting = db.tblMeetings.Find(id);

            if (tblMeeting == null)
            {
                return HttpNotFound();
            }
            var meetingType = db.tblMeetingTypes.Find(tblMeeting.meetingType);
            MeetingViewModels meetingViewModel = new MeetingViewModels();

            //var meetingType = db.tblMeetingTypes.Find(tblMeeting.meetingType);
            //t.meetingDetails = tblMeeting;
            //t.meetingType = meetingType;

            AutoMapper.Mapper.Map<tblMeeting, MeetingViewModels>(tblMeeting, meetingViewModel);
            AutoMapper.Mapper.Map<tblMeetingType, MeetingViewModels>(meetingType, meetingViewModel);

            return View(meetingViewModel);
        }

        // GET: Meetings/Create
        public ActionResult Create(string id)
        {
            ///get the meetingtype list and populate the view
            ///


            var meetingId = Guid.Parse(id);
            var meetingTypes = db.tblMeetingTypes.Find(meetingId);
            MeetingViewModels meetingViewModel = new MeetingViewModels();
            meetingViewModel.meetingType = meetingTypes.meetingType;


            return View(meetingViewModel);
        }

        // POST: Meetings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MeetingViewModels meetingInfo)
        
        {
            //var vt = meetingInfo.selectedContent;
            //meetingInfo.meetingDetails.startTime = DateTime.Now.TimeOfDay;
            //meetingInfo.meetingDetails.finishTime = DateTime.Now.TimeOfDay;

            //if (ModelState.IsValid)
            //{
            //    tblMeeting tblMeeting = new tblMeeting();
            //    tblMeeting.meetingId = Guid.NewGuid();
            //    tblMeeting.meetingType = meetingInfo.meetingType.meetingTypeId;
            //    tblMeeting.startTime = meetingInfo.meetingDetails.startTime;
            //    tblMeeting.finishTime = meetingInfo.meetingDetails.finishTime;

            //    tblMeeting.meetingDate = DateTime.Now.Date;
            //    tblMeeting.meetingVenue = meetingInfo.meetingDetails.meetingVenue;
            //    tblMeeting.meetingNotes=meetingInfo.meetingDetails.meetingNotes;
            //    db.tblMeetings.Add(tblMeeting);
            //    db.SaveChanges();
            //    return RedirectToAction("Index", new { id = meetingInfo.meetingType.meetingTypeId });
            //}
            //else
            //{
            //    return RedirectToAction("Create",new { id=meetingInfo.meetingType.meetingTypeId});

            //}
            //meetingInfo.selectedContent = string.Empty;
            //var meetingTypes = db.tblMeetingTypes;
            ////MeetingViewModels m = new MeetingViewModels();
            //meetingInfo.meetingTypeList = new SelectList(meetingTypes.ToList(), "meetingTypeId", "meetingType");
            return View(meetingInfo);
        }

        // GET: Meetings/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblMeeting tblMeeting = db.tblMeetings.Find(id);
            MeetingViewModels meetingViewModel = new MeetingViewModels();
            //  meetingViewModel.meetingDetails = tblMeeting;
            var meetingType = db.tblMeetingTypes.Find(tblMeeting.meetingType);
            AutoMapper.Mapper.Map<tblMeeting, MeetingViewModels>(tblMeeting, meetingViewModel);
            AutoMapper.Mapper.Map<tblMeetingType, MeetingViewModels>(meetingType, meetingViewModel);
            //m.selectedContent = meetingType;
            if (tblMeeting == null)
            {
                return HttpNotFound();
            }
            return View(meetingViewModel);
        }

        // POST: Meetings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MeetingViewModels meetingInfo)
        {
            if (ModelState.IsValid)
            {
                tblMeeting tblMeeting = new tblMeeting();
                AutoMapper.Mapper.Map<MeetingViewModels, tblMeeting>(meetingInfo, tblMeeting);
                //AutoMapper.Mapper.Map<tblMeetingType, MeetingViewModels>(meetingType, meetingViewModel);
                db.Entry(tblMeeting).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = meetingInfo.meetingTypeId });
            }
            return RedirectToAction("Index", new { id = meetingInfo.meetingTypeId });
        }

        // GET: Meetings/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblMeeting tblMeeting = db.tblMeetings.Find(id);
            if (tblMeeting == null)
            {
                return HttpNotFound();
            }
            return View(tblMeeting);
        }

        // POST: Meetings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            tblMeeting tblMeeting = db.tblMeetings.Find(id);
            var meetingTypeId = tblMeeting.meetingType;
            db.tblMeetings.Remove(tblMeeting);
            db.SaveChanges();
            return RedirectToAction("Index", new { id = meetingTypeId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
