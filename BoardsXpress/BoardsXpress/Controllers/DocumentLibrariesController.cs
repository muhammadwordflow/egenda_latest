﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BoardsXpress;
using BoardsXpress.Entities;

namespace BoardsXpress.Controllers
{
    public class DocumentLibrariesController : Controller
    {
        private BoardXpressEntities db = new BoardXpressEntities();

        // GET: DocumentLibraries
        public ActionResult Index()
        {
            var tblDocumentLibraries = db.tblDocumentLibraries.Include(t => t.tblAgendaItem).Include(t => t.tblMeeting).Include(t => t.tblUserProfile);
            return View(tblDocumentLibraries.ToList());
        }

        // GET: DocumentLibraries/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblDocumentLibrary tblDocumentLibrary = db.tblDocumentLibraries.Find(id);
            if (tblDocumentLibrary == null)
            {
                return HttpNotFound();
            }
            return View(tblDocumentLibrary);
        }

        // GET: DocumentLibraries/Create
        public ActionResult Create()
        {
            ViewBag.agendaItem = new SelectList(db.tblAgendaItems, "Id", "agendaItem");
            ViewBag.meetingId = new SelectList(db.tblMeetings, "meetingId", "meetingVenue");
            ViewBag.docAuthor = new SelectList(db.tblUserProfiles, "userId", "UserFullName");
            return View();
        }

        // POST: DocumentLibraries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "docId,docAuthor,docName,docCreatedDate,docPath,meetingId,agendaItem")] tblDocumentLibrary tblDocumentLibrary)
        {
            if (ModelState.IsValid)
            {
                tblDocumentLibrary.docId = Guid.NewGuid();
                db.tblDocumentLibraries.Add(tblDocumentLibrary);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.agendaItem = new SelectList(db.tblAgendaItems, "Id", "agendaItem", tblDocumentLibrary.agendaItem);
            ViewBag.meetingId = new SelectList(db.tblMeetings, "meetingId", "meetingVenue", tblDocumentLibrary.meetingId);
            ViewBag.docAuthor = new SelectList(db.tblUserProfiles, "userId", "UserFullName", tblDocumentLibrary.docAuthor);
            return View(tblDocumentLibrary);
        }

        // GET: DocumentLibraries/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblDocumentLibrary tblDocumentLibrary = db.tblDocumentLibraries.Find(id);
            if (tblDocumentLibrary == null)
            {
                return HttpNotFound();
            }
            ViewBag.agendaItem = new SelectList(db.tblAgendaItems, "Id", "agendaItem", tblDocumentLibrary.agendaItem);
            ViewBag.meetingId = new SelectList(db.tblMeetings, "meetingId", "meetingVenue", tblDocumentLibrary.meetingId);
            ViewBag.docAuthor = new SelectList(db.tblUserProfiles, "userId", "UserFullName", tblDocumentLibrary.docAuthor);
            return View(tblDocumentLibrary);
        }

        // POST: DocumentLibraries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "docId,docAuthor,docName,docCreatedDate,docPath,meetingId,agendaItem")] tblDocumentLibrary tblDocumentLibrary)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblDocumentLibrary).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.agendaItem = new SelectList(db.tblAgendaItems, "Id", "agendaItem", tblDocumentLibrary.agendaItem);
            ViewBag.meetingId = new SelectList(db.tblMeetings, "meetingId", "meetingVenue", tblDocumentLibrary.meetingId);
            ViewBag.docAuthor = new SelectList(db.tblUserProfiles, "userId", "UserFullName", tblDocumentLibrary.docAuthor);
            return View(tblDocumentLibrary);
        }

        // GET: DocumentLibraries/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblDocumentLibrary tblDocumentLibrary = db.tblDocumentLibraries.Find(id);
            if (tblDocumentLibrary == null)
            {
                return HttpNotFound();
            }
            return View(tblDocumentLibrary);
        }

        // POST: DocumentLibraries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            tblDocumentLibrary tblDocumentLibrary = db.tblDocumentLibraries.Find(id);
            db.tblDocumentLibraries.Remove(tblDocumentLibrary);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
