﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BoardsXpress;

namespace BoardsXpress.Controllers
{
    public class AgendaItemsController : Controller
    {
        private BXEntities db = new BXEntities();

        // GET: AgendaItems
        public ActionResult Index()
        {
            return View(db.tblAgendaItems.ToList());
        }

        // GET: AgendaItems/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblAgendaItem tblAgendaItem = db.tblAgendaItems.Find(id);
            if (tblAgendaItem == null)
            {
                return HttpNotFound();
            }
            return View(tblAgendaItem);
        }

        // GET: AgendaItems/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AgendaItems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,agendaItem")] tblAgendaItem tblAgendaItem)
        {
            if (ModelState.IsValid)
            {
                db.tblAgendaItems.Add(tblAgendaItem);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tblAgendaItem);
        }

        // GET: AgendaItems/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblAgendaItem tblAgendaItem = db.tblAgendaItems.Find(id);
            if (tblAgendaItem == null)
            {
                return HttpNotFound();
            }
            return View(tblAgendaItem);
        }

        // POST: AgendaItems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,agendaItem")] tblAgendaItem tblAgendaItem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblAgendaItem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tblAgendaItem);
        }

        // GET: AgendaItems/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblAgendaItem tblAgendaItem = db.tblAgendaItems.Find(id);
            if (tblAgendaItem == null)
            {
                return HttpNotFound();
            }
            return View(tblAgendaItem);
        }

        // POST: AgendaItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblAgendaItem tblAgendaItem = db.tblAgendaItems.Find(id);
            db.tblAgendaItems.Remove(tblAgendaItem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
