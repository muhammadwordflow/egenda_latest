﻿using AutoMapper;
using BoardsXpress.Entities;
using BoardsXpress.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BoardsXpress.App_Start
{
    public class MappingConfig:Profile
    {
        public static void RegisterMaps()
        {

            Mapper.Initialize(config =>
            {
                config.CreateMap<tblMeetingType, MeetingViewModels>()
                .ForMember(dest => dest.meetingTypeId, opt => opt.MapFrom(src => src.meetingTypeId))
                .ForMember(dest => dest.meetingType, opt => opt.MapFrom(src => src.meetingType));


                config.CreateMap<tblMeeting,MeetingViewModels>()
                .ForMember(dest => dest.meetingId, opt => opt.MapFrom(src => src.meetingId))
                .ForMember(dest => dest.meetingTypeId, opt => opt.Ignore())
                .ForMember(dest => dest.meetingDate, opt => opt.MapFrom(src => src.meetingDate))
                .ForMember(dest => dest.startTime, opt => opt.MapFrom(src => src.startTime))
                .ForMember(dest => dest.finishTime, opt => opt.MapFrom(src => src.finishTime))
                .ForMember(dest => dest.meetingVenue, opt => opt.MapFrom(src => src.meetingVenue))
                .ForMember(dest => dest.meetingNotes, opt => opt.MapFrom(src => src.meetingNotes));

                config.CreateMap<MeetingViewModels,tblMeeting>()
                .ForMember(dest => dest.meetingId, opt => opt.MapFrom(src => src.meetingId))
                .ForMember(dest => dest.meetingType, opt => opt.MapFrom(src=>src.meetingTypeId))
                .ForMember(dest => dest.meetingDate, opt => opt.MapFrom(src => src.meetingDate))
                .ForMember(dest => dest.startTime, opt => opt.MapFrom(src => src.startTime))
                .ForMember(dest => dest.finishTime, opt => opt.MapFrom(src => src.finishTime))
                .ForMember(dest => dest.meetingVenue, opt => opt.MapFrom(src => src.meetingVenue))
                .ForMember(dest => dest.meetingNotes, opt => opt.MapFrom(src => src.meetingNotes));


                config.CreateMap<AspNetUser, UserRoleAssignmentViewModels>();
                
                config.CreateMap<tblUserGroup, UserRoleAssignmentViewModels>();
                // config.CreateMap<AspNetUser, UserViewModels>()
                // .ForMember(dest => dest.userId, opt => opt.MapFrom(src => src.Id))
                // .ForMember(dest => dest.email, opt => opt.MapFrom(src => src.Email))
                // .ForMember(dest => dest.password, opt => opt.MapFrom(src => src.PasswordHash))
                // .ForMember(dest => dest.phoneNumber, opt => opt.MapFrom(src => src.PhoneNumber))
                //   .ForMember(dest => dest.userName, opt => opt.MapFrom(src => src.UserName));

                // config.CreateMap<tblUserProfile, UserViewModels>()
                //.ForMember(dest => dest.fullName, opt => opt.MapFrom(src => src.UserFullName))
                //    .ForMember(dest => dest.avatar, opt => opt.MapFrom(src => src.userAvatar))
                //    .ForMember(dest => dest.gender, opt => opt.MapFrom(src => src.userGender));


                //config.CreateMap<tblUserProfile, MeetingViewModels>()
                // .ForMember(dest => dest.userProfile.userId, opt => opt.MapFrom(s => s.userId));



            });


        }
    

        
        

    }
}